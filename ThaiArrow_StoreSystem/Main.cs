﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace ThaiArrow_StoreSystem
{
    public partial class Main : Form
    {
        private SqlConnection con = new SqlConnection(ConfigurationSettings.AppSettings["SQLConnection"]);
        private SqlCommand cmd;
        private SqlDataReader reader;
        Color menuButtonSelectColor = Color.FromArgb(3, 57, 108);
        Color menuButtonUnSelectColor = Color.FromArgb(51, 54, 59);
        DataTable dt = new DataTable();
        DataTable data_log = new DataTable();
        DataTable dt_add = new DataTable();
        DataTable dt_pickup = new DataTable();//pickup stock
        DataTable dt_pickupGrid = new DataTable();
        DataTable dt_pickupCut = new DataTable();
        DataTable dt_partData = new DataTable();
        DataTable dt_partbox = new DataTable();//keep data part and pox for pickupstock page
        DataTable dt_editstock = new DataTable();
        DataTable dt_editstock_local = new DataTable();
        
        DataTable dt_rank = new DataTable();
        DataTable dt_local = new DataTable();
        DataTable dt_line = new DataTable();
        DataTable dt_addstock = new DataTable();
        DataTable dt_countstock = new DataTable();

        DataTable dt_log = new DataTable();
        DataTable dt_log_transaction = new DataTable();
        DataTable dt_transaction_pickup = new DataTable();
        //globalvariable
        bool partboxid_check = false;
        int count_log = 0;
        int count_page_log = 0;
        bool check_first_pickup = true;
        DateTime eventPickup = new DateTime();
        int count_addstock = 0;
        int count_logout = 0;
        bool check_firstbackward = false;//log page
        bool check_firstforward = false;//log page

        //initial position
        int x0 = 0;
        int y0 = 0;

        public Main()
        {
            InitializeComponent();
            labelLineUserNameTopPanel.Text = Login.userName;
            labelLevel.Text = Login.position;
            MousePosition_(1, 1);
            tabControl1.Appearance = TabAppearance.FlatButtons;
            tabControl1.ItemSize = new Size(0, 1);
            tabControl1.SizeMode = TabSizeMode.Fixed;
            dataGridView6.Rows.Clear();
            dt_addstock.Rows.Clear();
            //timer_logout.Start();
            disable_page();
            check_permiss();
        }
        private void Bt_userSetup_Click(object sender, EventArgs e)
        {
          //  bt_Pickup.BackColor = menuButtonUnSelectColor;
          //  bt_addStock.BackColor = menuButtonUnSelectColor;
           // bt_manage.BackColor = menuButtonUnSelectColor;
          //  bt_userSetup.BackColor = menuButtonSelectColor;
            this.Hide();
            userSetup fm = new userSetup();
            fm.Show();
        }
        //before strart programe
        private void disable_page()
        {
            bt_LAddStock.Enabled = false;
            bt_addStock.Enabled = false;
            bt_leditstock.Enabled = false;
            bt_LLogs.Enabled = false;          
            bt_lmcline.Enabled = false;
            bt_partdata.Enabled = false;
            bt_Lusersetup.Enabled = false; 
            bt_LPickupPart.Enabled = false;
            bt_Pickup.Enabled = false;         
            bt_Lusersetup.Enabled = false;
            bt_userSetup.Enabled = false;
        }
        private void check_permiss()
        {
            List<string> check_permiss = new List<string>();
            check_permiss = Login.permiss;
            for (int i = 0; i < check_permiss.Count; i++)
            {
                if (check_permiss[i] == "AddStock")
                {
                    bt_LAddStock.Enabled = true;
                    bt_addStock.Enabled = true;
                }
                if (check_permiss[i] == "EditStock")
                {
                    bt_leditstock.Enabled = true;
                }
                if (check_permiss[i] == "Logs")
                {
                    bt_LLogs.Enabled = true;
                }
                if (check_permiss[i] == "MCLine")
                {
                    bt_lmcline.Enabled = true;
                }
                if (check_permiss[i] == "PartsData")
                {
                    bt_partdata.Enabled = true;
                }
                if (check_permiss[i] == "PermissionSetup")
                {
                    bt_Lusersetup.Enabled = true;
                }
                if (check_permiss[i] == "PickupPart")
                {
                    bt_LPickupPart.Enabled = true;
                    bt_Pickup.Enabled = true;
                }
                if (check_permiss[i] == "UserSetup")
                {
                    bt_Lusersetup.Enabled = true;
                    bt_userSetup.Enabled = true;
                }
            }
        }
        public void MousePosition_ (int x1 , int y1)
        {
           // count_logout++;
            if(x0 != x1 && y0!=y1)
            {
                x0 = x1; y0 = y1;
                
            }
            else
            {
                this.Close();
                Login am = new Login();
                am.Show();
            }

        }
        private void Timer_logout_Tick(object sender, EventArgs e)
        {
            MousePosition_(Cursor.Position.X, Cursor.Position.Y);
        }
        private void Main_KeyDown(object sender, KeyEventArgs e)
        {
            my_keyboard();
        }
        private void my_keyboard ()
        {
            timer_logout.Interval = 10000;
        }

        private void Main_Load(object sender, EventArgs e)
        {

        }

        private void Panel5_Paint(object sender, PaintEventArgs e)
        {

        }

        private void ButtonClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        private void ButtonMinimize_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
        //button page of programe
        #region
        // load part data page
        private void Button9_Click(object sender, EventArgs e)
        {
            //  tabControl1.SelectedTab(3);
            tabControl1.SelectTab(4);
            bt_Pickup.BackColor = menuButtonUnSelectColor;
            bt_addStock.BackColor = menuButtonUnSelectColor;
            bt_manage.BackColor = menuButtonSelectColor;
            bt_userSetup.BackColor = menuButtonUnSelectColor;

            dataGridView16.Columns.Clear();
            dataGridView17.Columns.Clear();

            //clear row table
            dataGridView1.Rows.Clear();
            dataGridView16.Rows.Clear();


            //hearder location
            dataGridView16.Columns.Add("shelf", "Shelf");
            dataGridView16.Columns.Add("zone", "Zone");
            dataGridView16.Columns.Add("floor", "Floor");
            dataGridView16.Columns.Add("side", "Side");
            dataGridView16.Columns.Add("slot", "Slot");
            dataGridView16.Columns[0].Width = 88;
            dataGridView16.Columns[1].Width = 88;
            dataGridView16.Columns[2].Width = 88;
            dataGridView16.Columns[3].Width = 88;
            dataGridView16.Columns[4].Width = 88;
            //herader lineDB
            dataGridView17.Columns.Add("lineno", "Line No(DB)");
            dataGridView17.Columns[0].Width = 247;

            //combobox part in partdata page
  
            tpd_partno.Text = "";
            tpd_partname.Text = "";

            load_pdTable();
            load_pdLocalTable();
            load_pdLineTable();
        }
        private void Button10_Click(object sender, EventArgs e)
        {
            tabControl1.SelectTab(5);
            bt_Pickup.BackColor = menuButtonUnSelectColor;
            bt_addStock.BackColor = menuButtonUnSelectColor;
            bt_manage.BackColor = menuButtonSelectColor;
            bt_userSetup.BackColor = menuButtonUnSelectColor;
            create_dtEditstock();
            fill_table_editstock();
        }
        private void Bt_LLogs_Click(object sender, EventArgs e)
        {
            tabControl1.SelectTab(6);
            bt_Pickup.BackColor = menuButtonUnSelectColor;
            bt_addStock.BackColor = menuButtonUnSelectColor;
            bt_manage.BackColor = menuButtonSelectColor;
            bt_userSetup.BackColor = menuButtonUnSelectColor;
            check_firstbackward = false;
            check_firstforward = false;
            create_dtlog();
            load_tableLog();
        }
        private void Bt_LUserSetup_Click(object sender, EventArgs e)
        {

            bt_Pickup.BackColor = menuButtonUnSelectColor;
            bt_addStock.BackColor = menuButtonUnSelectColor;
            bt_manage.BackColor = menuButtonUnSelectColor;
            bt_userSetup.BackColor = menuButtonSelectColor;
            this.Hide();
            userSetup fm = new userSetup();
            fm.Show();
        }
        private void Button15_Click(object sender, EventArgs e)
        {
            tabControl1.SelectTab(7);
            bt_Pickup.BackColor = menuButtonUnSelectColor;
            bt_addStock.BackColor = menuButtonUnSelectColor;
            bt_manage.BackColor = menuButtonSelectColor;
            bt_userSetup.BackColor = menuButtonUnSelectColor;
            fill_mclineTable();
            dropdown_mcline();
        }

        private void Button8_Click(object sender, EventArgs e)
        {
            tabControl1.SelectTab(3);
            bt_Pickup.BackColor = menuButtonUnSelectColor;
            bt_addStock.BackColor = menuButtonUnSelectColor;
            bt_manage.BackColor = menuButtonSelectColor;
            bt_userSetup.BackColor = menuButtonUnSelectColor;
        }

        private void Label1_Click(object sender, EventArgs e)
        {
            tabControl1.SelectTab(0);
            bt_Pickup.BackColor = menuButtonUnSelectColor;
            bt_addStock.BackColor = menuButtonUnSelectColor;
            bt_manage.BackColor = menuButtonUnSelectColor;
            bt_userSetup.BackColor = menuButtonUnSelectColor;
        }

        private void Bt_Pickup_Click(object sender, EventArgs e)
        {
            tabControl1.SelectTab(1);
            bt_Pickup.BackColor = menuButtonSelectColor;
            bt_addStock.BackColor = menuButtonUnSelectColor;
            bt_manage.BackColor = menuButtonUnSelectColor;
            bt_userSetup.BackColor = menuButtonUnSelectColor;
            fill_dtpickupgrid();
            fill_tablepickup();
            writeto_monitor_before_pickup();



        }

        private void Bt_LPickupPart_Click(object sender, EventArgs e)
        {
            tabControl1.SelectTab(1);
            bt_Pickup.BackColor = menuButtonSelectColor;
            bt_addStock.BackColor = menuButtonUnSelectColor;
            bt_manage.BackColor = menuButtonUnSelectColor;
            bt_userSetup.BackColor = menuButtonUnSelectColor;
            fill_dtpickupgrid();
            fill_tablepickup();
            writeto_monitor_before_pickup();

        }

        private void Bt_addStock_Click(object sender, EventArgs e)
        {
            tabControl1.SelectTab(2);
            bt_Pickup.BackColor = menuButtonUnSelectColor;
            bt_addStock.BackColor = menuButtonSelectColor;
            bt_manage.BackColor = menuButtonUnSelectColor;
            bt_userSetup.BackColor = menuButtonUnSelectColor;
            fill_dtaddstock();
        }

        private void Bt_LAddStock_Click(object sender, EventArgs e)
        {
            tabControl1.SelectTab(2);
            bt_Pickup.BackColor = menuButtonUnSelectColor;
            bt_addStock.BackColor = menuButtonSelectColor;
            bt_manage.BackColor = menuButtonUnSelectColor;
            bt_userSetup.BackColor = menuButtonUnSelectColor;
            fill_dtaddstock();
        }

        private void Bt_manage_Click(object sender, EventArgs e)
        {
            tabControl1.SelectTab(3);
            bt_Pickup.BackColor = menuButtonUnSelectColor;
            bt_addStock.BackColor = menuButtonUnSelectColor;
            bt_manage.BackColor = menuButtonSelectColor;
            bt_userSetup.BackColor = menuButtonUnSelectColor;
        }
        // log out 
        private void Btmain_logout_Click(object sender, EventArgs e)
        {
            this.Hide();
            Login am = new Login();
            am.Show();
        }
        //back main button
        private void Btmain_back_Click(object sender, EventArgs e)
        {
            if (tabControl1.SelectedTab.Text == tabControl1.TabPages["tabPickup"].Text || tabControl1.SelectedTab.Text == tabControl1.TabPages["tabAddStock"].Text || tabControl1.SelectedTab.Text == tabControl1.TabPages["tabManage"].Text )
            {
                tabControl1.SelectTab(0);
                bt_Pickup.BackColor = menuButtonUnSelectColor;
                bt_addStock.BackColor = menuButtonUnSelectColor;
                bt_manage.BackColor = menuButtonUnSelectColor;
                bt_userSetup.BackColor = menuButtonUnSelectColor;
            }
            else if (tabControl1.SelectedTab.Text == tabControl1.TabPages["tabAddPart"].Text || tabControl1.SelectedTab.Text == tabControl1.TabPages["tabEditPart"].Text )
            {
                tabControl1.SelectTab(4);
            }
            else
            {
                tabControl1.SelectTab(3);
                bt_Pickup.BackColor = menuButtonUnSelectColor;
                bt_addStock.BackColor = menuButtonUnSelectColor;
                bt_manage.BackColor = menuButtonSelectColor;
                bt_userSetup.BackColor = menuButtonUnSelectColor;
            }
        }
        #endregion
        //Add Part page part data
        #region
        private void Button22_Click(object sender, EventArgs e)
        {
            tabControl1.SelectTab(8);
            comAdd_shelf.Text = "";
            comAdd_shelf.Items.Clear();
            comAdd_Zone.Text = "";
            comAdd_Zone.Items.Clear();
            comAdd_Slot.Text = "";
            comAdd_Slot.Items.Clear();
            comAdd_side.Text = "";
            comAdd_side.Items.Clear();
            checkAdd_Rank.Checked = false;

            dt_local.Rows.Clear();
            dt_line.Rows.Clear();
            //clear datagridview befor use
            dataGridView2.Rows.Clear();
            dataGridView3.Rows.Clear();
            dataGridView2.Columns.Clear();
            dataGridView3.Columns.Clear();
            //datatable location
            dt_local.Columns.Clear();
            dt_local.Columns.Add("Shelf");
            dt_local.Columns.Add("Zone");
            dt_local.Columns.Add("Floor");
            dt_local.Columns.Add("Side");
            dt_local.Columns.Add("Slot");
            //datatable rank
            dt_rank.Columns.Clear();
            dt_rank.Columns.Add("Rank");
            dt_rank.Columns.Add("Action");
            //datatable line
            dt_line.Columns.Clear();
            dt_line.Columns.Add("Line No");
            // load_rank(); 
            load_line();
            load_location();
            load_propertyPart();
            load_typePart();
            // TableRank();
            TableLocation();
            TableLine();

            bt_Pickup.BackColor = menuButtonUnSelectColor;
            bt_addStock.BackColor = menuButtonUnSelectColor;
            bt_manage.BackColor = menuButtonSelectColor;
            bt_userSetup.BackColor = menuButtonUnSelectColor;

        }
        private void load_line()
        {
            cmd = new SqlCommand();
            cmd.Connection = con;
            comAdd_Line.Text = "";
            comAdd_Line.Items.Clear();
            try
            {
                con.Open();
                cmd.CommandText = "SELECT [line_no] FROM [THAIARROW_PART_STORE].[dbo].[Master_line] ORDER BY line_no ASC";
                reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        string line = reader.GetValue(0).ToString();
                        comAdd_Line.Items.Add(line);
                    }
                }
            }
            catch (Exception ab)
            {
                MessageBox.Show(ab.Message);
            }
            finally
            {
                con.Close();
            }
        }

        private void load_location()
        {
            cmd = new SqlCommand();
            cmd.Connection = con;
            // string sql_cmd = "";
            string shelf = comAdd_shelf.Text;
            string zone = comAdd_Zone.Text;
            string floor = comAdd_floor.Text;
            string side = comAdd_side.Text;
            string slot = comAdd_Slot.Text;
            comAdd_shelf.Text = "";
            comAdd_shelf.Items.Clear();
            comAdd_Zone.Text = "";
            comAdd_Zone.Items.Clear();
            comAdd_floor.Text = "";
            comAdd_floor.Items.Clear();
            comAdd_side.Text = "";
            comAdd_side.Items.Clear();
            comAdd_Slot.Text = "";
            comAdd_Slot.Items.Clear();

            if (dt_local.Rows.Count == 0)
            {

                try
                {
                    con.Open();
                    cmd.CommandText = " SELECT DISTINCT [shelf] FROM [THAIARROW_PART_STORE].[dbo].[Master_location] where part_no_registered Is NULL AND part_registered_user IS NULL AND part_registered_time IS NULL ";
                    reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            string shelf_ = reader.GetValue(0).ToString();
                            comAdd_shelf.Items.Add(shelf_);
                        }
                    }
                }
                catch (Exception ab)
                {
                    MessageBox.Show(ab.Message);
                }
                finally
                {
                    con.Close();
                    reader.Close();
                }
            }
            if (dt_local.Rows.Count > 0)
            {
                string shelf_in = dt_local.Rows[0].ItemArray[0].ToString();

                try
                {
                    con.Open();
                    cmd.CommandText = " SELECT DISTINCT [shelf] FROM [THAIARROW_PART_STORE].[dbo].[Master_location] where part_no_registered Is NULL AND part_registered_user IS NULL AND part_registered_time IS NULL AND shelf ='" + shelf_in + "'";
                    reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            string shelf_ = reader.GetValue(0).ToString();
                            comAdd_shelf.Items.Add(shelf_);
                        }
                    }
                }
                catch (Exception ab)
                {
                    MessageBox.Show(ab.Message);
                }
                finally
                {
                    con.Close();
                    reader.Close();
                }
            }
        }
        private void selectZone()
        {
            string shelf = comAdd_shelf.Text;
            string zone = comAdd_Zone.Text;
            string floor = comAdd_floor.Text;
            string side = comAdd_side.Text;
            string slot = comAdd_Slot.Text;

            if (zone == "" && shelf != "" && dt_local.Rows.Count == 0)
            {
                comAdd_floor.Text = "";
                comAdd_side.Text = "";
                comAdd_Slot.Text = "";
                try
                {
                    con.Open();
                    cmd.CommandText = " SELECT DISTINCT [zone] FROM [THAIARROW_PART_STORE].[dbo].[Master_location] where shelf ='" + shelf + "' AND part_no_registered IS NULL AND part_registered_user IS NULL AND part_registered_time IS NULL";

                    reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            string zone_ = reader.GetValue(0).ToString();
                            comAdd_Zone.Items.Add(zone_);

                        }
                    }
                }
                catch (Exception ab)
                {
                    MessageBox.Show(ab.Message);
                }
                finally
                {
                    reader.Close();
                    con.Close();
                }
            }
            if (zone == "" && shelf != "" && dt_local.Rows.Count > 0)
            {
                comAdd_floor.Text = "";
                comAdd_side.Text = "";
                comAdd_Slot.Text = "";

                string zone_in = dt_local.Rows[0].ItemArray[1].ToString();
                try
                {
                    con.Open();
                    cmd.CommandText = " SELECT DISTINCT [zone] FROM [THAIARROW_PART_STORE].[dbo].[Master_location] where shelf ='" + shelf + "' AND part_no_registered IS NULL AND part_registered_user IS NULL AND part_registered_time IS NULL AND zone ='" + zone_in + "'";

                    reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            string zone_ = reader.GetValue(0).ToString();
                            comAdd_Zone.Items.Add(zone_);

                        }
                    }
                }
                catch (Exception ab)
                {
                    MessageBox.Show(ab.Message);
                }
                finally
                {
                    reader.Close();
                    con.Close();
                }
            }

        }
        private void selectFloor()
        {
            string shelf = comAdd_shelf.Text;
            string zone = comAdd_Zone.Text;
            string floor = comAdd_floor.Text;
            string side = comAdd_side.Text;

            if (floor == "" && shelf != "" && zone != "" && dt_local.Rows.Count == 0)
            {
                comAdd_side.Text = "";
                try
                {
                    con.Open();
                    cmd.CommandText = " SELECT DISTINCT [floor] FROM [THAIARROW_PART_STORE].[dbo].[Master_location] where shelf ='" + shelf + "' AND zone='" + zone + "'AND part_no_registered IS NULL AND part_registered_user IS NULL AND part_registered_time IS NULL ";

                    reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            string shelf_ = reader.GetValue(0).ToString();
                            comAdd_floor.Items.Add(shelf_);
                        }
                    }
                }
                catch (Exception ab)
                {
                    MessageBox.Show(ab.Message);
                }
                finally
                {
                    con.Close();
                    reader.Close();
                }
            }
            if (floor == "" && shelf != "" && zone != "" && dt_local.Rows.Count > 0)
            {
                comAdd_side.Text = "";
                string floor_in = dt_local.Rows[0].ItemArray[2].ToString();
                try
                {
                    con.Open();
                    cmd.CommandText = " SELECT DISTINCT [floor] FROM [THAIARROW_PART_STORE].[dbo].[Master_location] where shelf ='" + shelf + "' AND zone='" + zone + "'AND part_no_registered IS NULL AND part_registered_user IS NULL AND part_registered_time IS NULL AND floor ='" + floor_in + "'";

                    reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            string shelf_ = reader.GetValue(0).ToString();
                            comAdd_floor.Items.Add(shelf_);
                        }
                    }
                }
                catch (Exception ab)
                {
                    MessageBox.Show(ab.Message);
                }
                finally
                {
                    con.Close();
                    reader.Close();
                }
            }
        }
        private void selectSide()
        {
            string shelf = comAdd_shelf.Text;
            string zone = comAdd_Zone.Text;
            string side = comAdd_side.Text;
            string floor = comAdd_floor.Text;

            if (side == "" && shelf != "" && zone != "" && dt_local.Rows.Count == 0)
            {
                comAdd_Slot.Text = "";
                try
                {
                    con.Open();
                    cmd.CommandText = " SELECT DISTINCT [side] FROM [THAIARROW_PART_STORE].[dbo].[Master_location] where shelf ='" + shelf + "' AND zone='" + zone + "'AND floor ='"+floor+"' AND part_no_registered IS NULL AND part_registered_user IS NULL AND part_registered_time IS NULL ";

                    reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            string shelf_ = reader.GetValue(0).ToString();
                            comAdd_side.Items.Add(shelf_);
                        }
                    }
                }
                catch (Exception ab)
                {
                    MessageBox.Show(ab.Message);
                }
                finally
                {
                    con.Close();
                    reader.Close();
                }
            }
            if (side == "" && shelf != "" && zone != "" && dt_local.Rows.Count > 0)
            {
                comAdd_Slot.Text = "";
                string side_in = dt_local.Rows[0].ItemArray[3].ToString();
                try
                {
                    con.Open();
                    cmd.CommandText = " SELECT DISTINCT [side] FROM [THAIARROW_PART_STORE].[dbo].[Master_location] where shelf ='" + shelf + "' AND zone='" + zone + "'AND floor ='"+floor+"' AND part_no_registered IS NULL AND part_registered_user IS NULL AND part_registered_time IS NULL AND side ='" + side_in + "'";

                    reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            string shelf_ = reader.GetValue(0).ToString();
                            comAdd_side.Items.Add(shelf_);
                        }
                    }
                }
                catch (Exception ab)
                {
                    MessageBox.Show(ab.Message);
                }
                finally
                {
                    con.Close();
                    reader.Close();
                }
            }
        }
        private void selectSlot()
        {
            string shelf = comAdd_shelf.Text;
            string zone = comAdd_Zone.Text;
            string floor = comAdd_floor.Text;
            string side = comAdd_side.Text;
            string slot = comAdd_Slot.Text;
            string temp_slot_leading = "";
            string temp_slot_lagging = "";
            List<string> slot_ina = new List<string>();
            List<string> temp_slot_leading_ar = new List<string>();
            List<string> temp_slot_lagging_ar = new List<string>();
            slot_ina.Clear();
            temp_slot_lagging_ar.Clear();
            temp_slot_leading_ar.Clear();
            int k = -1;
            bool hasValue = false;
            if (slot == "" && shelf != "" && floor != "" && zone != "" && side != "" && dt_local.Rows.Count == 0)
            {

                try
                {
                    con.Open();
                    cmd.CommandText = " SELECT DISTINCT [slot] FROM [THAIARROW_PART_STORE].[dbo].[Master_location] where shelf ='" + shelf + "' AND zone='" + zone + "' AND floor ='" + floor + "' AND side ='"+side+"' AND part_no_registered IS NULL AND part_registered_user IS NULL AND part_registered_time IS NULL";

                    reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            string shelf_ = reader.GetValue(0).ToString();
                            comAdd_Slot.Items.Add(shelf_);

                        }
                    }
                }
                catch (Exception ab)
                {
                    MessageBox.Show(ab.Message);
                }
                finally
                {
                    reader.Close();
                    con.Close();
                }
            }
            if (slot == "" && shelf != "" && floor != "" && zone != "" && side != "" && dt_local.Rows.Count > 0)
            {
                string slot_in = dt_local.Rows[0].ItemArray[4].ToString();
                decimal spand = dt_local.Rows.Count / 2;
                try
                {
                    con.Open();
                    cmd.CommandText = " SELECT DISTINCT [slot] FROM [THAIARROW_PART_STORE].[dbo].[Master_location] where shelf ='" + shelf + "' AND zone='" + zone + "' AND floor ='" + floor + "' AND side ='"+side+"' AND part_no_registered IS NULL AND part_registered_user IS NULL AND part_registered_time IS NULL ";

                    reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {

                        while (reader.Read())
                        {

                            slot_ina.Add(reader.GetValue(0).ToString());

                        }
                    }


                    for (int j = 0; j < slot_ina.Count - 1; j++)
                    {
                        if (slot_ina[j] == slot_in) { k = j; hasValue = true; }
                        if (hasValue)
                        {

                            for (int l = 1; l < dataGridView2.Rows.Count; l++)
                            {
                                if (k - l < 0 || k + l > slot_ina.Count - 1 && k != -1) { }
                                if (k - l >= 0 && k - l < slot_ina.Count - 1 && k != -1) { comAdd_Slot.Items.Add(slot_ina[k - l]); }

                                if (k + l <= slot_ina.Count - 1 && k != -1) { comAdd_Slot.Items.Add(slot_ina[k + l]); }
                                //  if(k+l <=slot_ina.Count - 1 && k == 0) { textAdd_Slot.Items.Add(slot_ina[k + 1]); }
                                if (k - l < 0 && k != -1) { }
                            }
                            hasValue = false;
                        }
                    }

                }
                catch (Exception ab)
                {
                    MessageBox.Show(ab.Message);
                }
                finally
                {
                    reader.Close();
                    con.Close();
                }
            }
        }
        private void load_typePart()
        {
            comAdd_parttype.Items.Clear();
            comAdd_parttype.Items.Add("1.Common");
            comAdd_parttype.Items.Add("2.L1");
            comAdd_parttype.Items.Add("3.L2");
            comAdd_parttype.Items.Add("4.L3");
            comAdd_parttype.Items.Add("5.L4");
            comAdd_parttype.Items.Add("6.L5");
            comAdd_parttype.Items.Add("7.Oth.");
        }
        private void load_propertyPart()
        {
            comAdd_partpackage.Items.Clear();
            comAdd_partpackage.Items.Add("1.Roll เล็ก");
            comAdd_partpackage.Items.Add("2.Roll ใหญ่");
            comAdd_partpackage.Items.Add("3.Connector");
            comAdd_partpackage.Items.Add("4.CPU");
        }
        private void TableLocation()
        {
            dataGridView2.Columns.Add("shelf", "Shelf");
            dataGridView2.Columns.Add("zone", "Zone");
            dataGridView2.Columns.Add("floor", "Floor");
            dataGridView2.Columns.Add("side", "Side");
            dataGridView2.Columns.Add("slot", "Slot");
            dataGridView2.Columns[0].Width = 80;
            dataGridView2.Columns[1].Width = 80;
            dataGridView2.Columns[2].Width = 80;
            dataGridView2.Columns[3].Width = 80;
            dataGridView2.Columns[4].Width = 80;

        }
        private void TableLine()
        {
            dataGridView3.Columns.Add("line", "Line");
            dataGridView3.Columns[0].Width = 310;
        }


        private void NumericUpDown2_ValueChanged(object sender, EventArgs e)
        {

        }

        private void Button1_Click(object sender, EventArgs e)
        {
            string partNo = textAdd_partno.Text;
            string shelf = comAdd_shelf.Text;
            string zone = comAdd_Zone.Text;
            string floor = comAdd_floor.Text;
            string side = comAdd_side.Text;
            string slot = comAdd_Slot.Text;

            cmd = new SqlCommand();
            cmd.Connection = con;
            if (!sameLocation(partNo, shelf, zone, floor, side, slot))
            {

                try
                {
                    con.Open();

                    dataGridView2.Rows.Add(shelf, zone, floor, side, slot);
                    dt_local.Rows.Add(shelf, zone, floor, side, slot);


                    dataGridView2.DefaultCellStyle.Font = new Font("Tahoma", 12);

                }
                catch (Exception ab)
                {
                    MessageBox.Show(ab.Message);
                }
                finally
                {
                    con.Close();
                    load_location();
                }

            }
            else
            {
                MessageBox.Show("มี Part No :" + partNo + "ได้ถูกเพิ่มในที่เก็บนี้แล้ว กรุณาเลือกสถานที่จัดเก็บใหม่");
            }
        }
        private bool sameLocation(string partNo, string shelf, string zone, string floor, string side , string slot)
        {
            bool result = false;

            if (dataGridView2.Rows.Count > 1)
            {
                try
                {

                    for (int i = 0; i < dataGridView2.Rows.Count - 1; i++)
                    {
                        if (dataGridView2.Rows[i].Cells[0].Value.ToString() == shelf && dataGridView2.Rows[i].Cells[1].Value.ToString() == zone && dataGridView2.Rows[i].Cells[2].Value.ToString() == floor && dataGridView2.Rows[i].Cells[3].Value.ToString() == side && dataGridView2.Rows[i].Cells[4].Value.ToString() == slot)
                        {
                            result = true;
                        }
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }

            return result;
        }

        private void BtAdd_del_Click(object sender, EventArgs e)
        {
            int rowIndex = dataGridView2.CurrentCell.RowIndex;
            try
            {
                if (rowIndex >= 0)
                {
                    dataGridView2.Rows.RemoveAt(rowIndex);
                    dt_local.Rows.RemoveAt(rowIndex);
                    load_location();
                }
            }
            catch (Exception ab)
            {
                MessageBox.Show(ab.Message);
            }
            finally
            {
                // load_location();
            }
        }

        private void ComAdd_Area_SelectedIndexChanged(object sender, EventArgs e)
        {
            comAdd_Zone.Text = "";
            comAdd_Zone.Items.Clear();
            comAdd_side.Items.Clear();
            comAdd_Slot.Items.Clear();
            selectZone();
        }

        private void ComAdd_Zone_SelectedIndexChanged(object sender, EventArgs e)
        {
            comAdd_side.Text = "";
            comAdd_floor.Items.Clear();
            comAdd_side.Items.Clear();
            comAdd_Slot.Items.Clear();
            selectFloor();
           
        }
        private void ComAdd_floor_SelectedIndexChanged(object sender, EventArgs e)
        {
            comAdd_side.Text = "";
            comAdd_side.Items.Clear();
            comAdd_Slot.Items.Clear();
            selectSide();
        }

        private void ComAdd_Shelf_SelectedIndexChanged(object sender, EventArgs e)
        {
            comAdd_Slot.Text = "";
            comAdd_Slot.Items.Clear();
            selectSlot();
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            string partNo = textAdd_partno.Text;
            string line = comAdd_Line.Text;
            if (!sameLine(partNo, line))
            {

                try
                {

                    dataGridView3.Rows.Add(line);
                    dt_line.Rows.Add(line);
                    dataGridView3.DefaultCellStyle.Font = new Font("Tahoma", 12);

                }
                catch (Exception ab)
                {
                    MessageBox.Show(ab.Message);
                }
                finally
                {

                }

            }
            else
            {
                MessageBox.Show("มี Part No :" + partNo + "ได้ถูกเพิ่มใน Line นี้แล้ว");
            }
        }
        private bool sameLine(string partNo, string line)
        {
            bool result = false;

            if (dataGridView3.Rows.Count > 1)
            {
                try
                {

                    for (int i = 0; i < dataGridView3.Rows.Count - 1; i++)
                    {
                        if (dataGridView3.Rows[i].Cells[0].Value.ToString() == line)
                        {
                            result = true;
                        }
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }

            return result;
        }

        private void BtAdd_delLocal_Click(object sender, EventArgs e)
        {
            int rowIndex = dataGridView3.CurrentCell.RowIndex;
            try
            {
                if (rowIndex >= 0)
                {
                    dataGridView3.Rows.RemoveAt(rowIndex);
                    dt_line.Rows.RemoveAt(rowIndex);
                    load_line();
                }
            }
            catch (Exception ab)
            {
                MessageBox.Show(ab.Message);
            }
        }

        private void BtAdd_save_Click(object sender, EventArgs e)
        {
            try
            {
                if (textAdd_partno.Text != "")
                {
                    string partNo = textAdd_partno.Text;
                    string partName = textAdd_partname.Text;
                    string supplier = textAdd_supplier.Text;
                    uint qty = Convert.ToUInt32(numAdd_qty.Value);
                    string model = textAdd_model.Text;
                    uint maxStock = Convert.ToUInt32(numAdd_maxstock.Value);
                    uint minStock = Convert.ToUInt32(numAdd_minstock.Value);
                    int Local = dt_local.Rows.Count;
                    int Line = dt_line.Rows.Count;
                    bool fifo = checkAdd_FIFO.Checked;
                    bool rankEnabled = checkAdd_Rank.Checked;
                    string createUser = Login.name;
                    string userID = Login.userName;
                    bool checkPart = false;
                    bool checkLocal = false;
                    bool checkLine = false;
                    string detail = "";
                    string rank_detail = "";
                    string local_detail = "";
                    string line_detail = "";
                    string eventAction = "";
                    string propertyPart = "";
                    string typePart = "";
                    if (comAdd_partpackage.Text != "") {  propertyPart = comAdd_partpackage.Text.Substring(2, comAdd_partpackage.Text.Length - 2); }
                    else {  propertyPart = ""; }
                    if (comAdd_parttype.Text !="") { typePart = comAdd_parttype.Text.Substring(2, comAdd_parttype.Text.Length - 2); }
                    else {  typePart = ""; }

                    DateTime eventTime = DateTime.Now;

                    if (!SamePartNo(partNo))
                    {
                        if (partNo != "" && partName != "" && supplier != "" && qty != 0 && model != "" && maxStock != 0 && minStock != 0 )
                        {
                            checkPart = addPart();
                       
                            if (!checkPart)
                            {
                                checkLocal = add_MLocal();
                                checkLine = add_MLine();
                                if (!checkLocal)
                                {
                                    for (int j = 0; j < dt_local.Rows.Count; j++)
                                    {
                                        local_detail = local_detail + dt_local.Rows[j]["Shelf"].ToString() + "|" + dt_local.Rows[j]["Zone"].ToString() + "|" + dt_local.Rows[j]["Floor"].ToString() + "|" + dt_local.Rows[j]["Side"].ToString() + "|" + dt_local.Rows[j]["Slot"].ToString() + ",";
                                    }
                                }
                                if (!checkLine)
                                {
                                    for (int k = 0; k < dt_line.Rows.Count; k++)
                                    {
                                        line_detail = line_detail + dt_line.Rows[k]["Line No"].ToString() + "|";
                                    }
                                }
                                eventAction = "AddPart";
                                detail = "part_no =" + partNo + ":part_name =" + partName + ":supplier =" + supplier + ":Qty =" + qty.ToString() + ":model =" + model + ":maxStock =" + maxStock + ":minStock =" + minStock + ":FIFO =" + fifo.ToString() + ":RankEnabled =" + rankEnabled.ToString() + ":location =" + local_detail + ":lineNo =" + line_detail;
                                add_MEvent(eventAction, partNo, userID, eventTime, detail);
                                dt_local.Rows.Clear();
                                dt_rank.Rows.Clear();
                                dt_line.Rows.Clear();
                                textAdd_partno.Text = "";
                                textAdd_partname.Text = "";
                                textAdd_model.Text = "";
                                numAdd_maxstock.Value = 0;
                                numAdd_minstock.Value = 0;
                                numAdd_qty.Value = 0;
                                textAdd_supplier.Text = "";
                                checkAdd_FIFO.Checked = false;
                                checkAdd_Rank.Checked = false;
                                comAdd_partpackage.Text = "";
                                comAdd_partpackage.Items.Clear();
                                comAdd_parttype.Text = "";
                                comAdd_parttype.Items.Clear();
                                // load_rank();
                                MessageBox.Show("ทำการเพิ่มข้อมูล Part :" + partNo + "เสร็จสมบูรณ์");
                                load_propertyPart();
                                load_typePart();
                                load_location();
                                load_line();

                            }
                        }
                        else
                        {
                            MessageBox.Show("กรุณากรอกข้อมูลให้ครบถ้วน", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    else
                    {
                        MessageBox.Show("มีข้อมูล Part นี้ในระบบแล้ว", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    MessageBox.Show("กรุณากรอกข้อมูลให้ครบถ้วน", "Error",MessageBoxButtons.OK,MessageBoxIcon.Error);
                }
            }
            catch(Exception ab)
            {
                MessageBox.Show(ab.Message);
            }
        }
        private bool addPart()
        {
            string partNo = textAdd_partno.Text;
            string partName = textAdd_partname.Text;
            string supplier = textAdd_supplier.Text;
            uint qty = Convert.ToUInt32(numAdd_qty.Value);
            string model = textAdd_model.Text;
            uint maxStock = Convert.ToUInt32(numAdd_maxstock.Value);
            uint minStock = Convert.ToUInt32(numAdd_minstock.Value);
            bool fifo = checkAdd_FIFO.Checked;
            bool rankEnabled = checkAdd_Rank.Checked;
            string createUser = Login.userName;
            DateTime createDate = new DateTime();
            createDate = DateTime.Now;
            bool result = false;
            string propertyPart = "";
            string typePart = "";
            if (comAdd_partpackage.Text != "") { propertyPart = comAdd_partpackage.Text.Substring(2, comAdd_partpackage.Text.Length - 2); }
            if (comAdd_parttype.Text != "")
            { typePart = comAdd_parttype.Text.Substring(2, comAdd_parttype.Text.Length - 2); }
            cmd = new SqlCommand();
            cmd.Connection = con;
            if (partNo != "" || partName != "" || qty != 0 || maxStock != 0 || minStock != 0 || propertyPart!="" || typePart!="")
            {
                try
                {
                    con.Open();
                    cmd.CommandText = "INSERT INTO [THAIARROW_PART_STORE].[dbo].[Master_part_data] (part_no,part_name,model,FIFO_enable,rank_enable,stock_available,stock_hold,max_stock,min_stock,qty_pcs,create_user,create_date,update_user,update_date,supplier,part_type,part_package) VALUES ('" + partNo + "','" + partName + "','" + model + "','" + fifo + "','" + rankEnabled + "','" + 0 + "','" + 0 + "','" + maxStock + "','" + minStock + "','" + qty + "','" + createUser + "','" + DateTime.Now + "','" + createUser + "','" + DateTime.Now + "','" + supplier + "','" + typePart + "'," + "N'" + propertyPart + "')";
                    int b = cmd.ExecuteNonQuery();
                    if (b == 0)
                    {
                        MessageBox.Show("เพิ่มข้อมูล Part No ไม่สำเร็จ");
                        result = true;
                    }
                    else
                    {
                        // MessageBox.Show("เพิ่มข้อมูลสำเร็จ");
                        result = false;
                    }
                }
                catch (Exception a)
                {
                    MessageBox.Show(a.Message);
                }
                finally
                {
                    con.Close();
                }
            }
            else
            {
                MessageBox.Show("WARNING", "กรุณากรอกข้อมูลให้ครบถ้วน", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                result = true;
            }
            return result;
        }
        //add data to db data event
        private void add_MEvent(string eventAction, string part_no, string user, DateTime eventTime, string detail)
        {
            cmd = new SqlCommand();
            cmd.Connection = con;
            try
            {
                con.Open();
                cmd.CommandText = "INSERT INTO [THAIARROW_PART_STORE].[dbo].[data_event] (event_action,part_no,event_user,event_time,detail) VALUES ('" + eventAction + "','" + part_no + "','" + user + "','" + eventTime + "','" + detail + "')";
                int b = cmd.ExecuteNonQuery();
                if (b == 0) { MessageBox.Show("เกิดข้อผิดพลาดในการส่งข้อมูลเข้าสู่ฐานข้อมูลเหตุการณ์"); }
            }
            catch (Exception ab)
            {
                MessageBox.Show(ab.Message);
            }
            finally
            {
                con.Close();
            }
        }
        //inert to db Master Line Part
        private bool add_MLine()
        {
            string lineNo = "";
            string partNo = textAdd_partno.Text;
            bool result = false;
            cmd = new SqlCommand();
            cmd.Connection = con;
            try
            {
                con.Open();
                for (int i = 0; i < dt_line.Rows.Count; i++)
                {
                    lineNo = dt_line.Rows[i]["Line No"].ToString();
                    cmd.CommandText = "INSERT INTO [THAIARROW_PART_STORE].[dbo].[Master_line_part] (line_no,part_no) VALUES ('" + lineNo + "','" + partNo + "')";
                    int b = cmd.ExecuteNonQuery();
                    if (b == 0) { MessageBox.Show("เกิดข้อผิดพลาดในการเพิ่ม Line กับ Part No ลงฐานข้อมูล"); result = true; }
                }
            }
            catch (Exception ab)
            {
                MessageBox.Show(ab.Message);
            }
            finally
            {
                con.Close();
                dataGridView3.Rows.Clear();
            }
            return result;
        }
        //insert to db Master Local
        private bool add_MLocal()
        {
            cmd = new SqlCommand();
            cmd.Connection = con;
            string shelf = "";
            string zone = "";
            string floor = "";
            string side = "";
            string slot = "";
            string partNo = textAdd_partno.Text;
            string user = Login.userName;
            bool result = false;
            DateTime regisTime = DateTime.Now;

            try
            {
                con.Open();
                for (int i = 0; i < dt_local.Rows.Count; i++)
                {
                    shelf = dt_local.Rows[i]["Shelf"].ToString();
                    zone = dt_local.Rows[i]["Zone"].ToString();
                    floor = dt_local.Rows[i]["Floor"].ToString();
                    side = dt_local.Rows[i]["Side"].ToString();
                    slot = dt_local.Rows[i]["Slot"].ToString();
                    cmd.CommandText = "UPDATE [THAIARROW_PART_STORE].[dbo].[Master_location] SET part_no_registered ='" + partNo + "', part_registered_user ='" + user + "',part_registered_time ='" + regisTime + "'where shelf ='" + shelf + "'AND zone ='" + zone + "'AND floor ='" + floor + "' AND side ='"+side+"' AND slot ='" + slot + "'";
                    int a = cmd.ExecuteNonQuery();
                    if (a == 0)
                    {
                        MessageBox.Show("เกิดข้อผิดพลาดในการเพิ่มสถานที่จัดเก็บลงฐานข้อมูล");
                        result = true;
                        dt_local.Rows.Clear();
                        dataGridView2.Rows.Clear();
                    }

                }

            }
            catch (Exception ab)
            {
                MessageBox.Show(ab.Message);
            }
            finally
            {
                con.Close();
                dataGridView2.Rows.Clear();
            }
            return result;
        }
        private bool SamePartNo(string partNo)
        {
            bool result = false;
            cmd = new SqlCommand();
            cmd.Connection = con;
            try
            {
                con.Open();
                cmd.CommandText = "SELECT DISTINCT part_no FROM [THAIARROW_PART_STORE].[dbo].[Master_part_data] WHERE part_no = '" + partNo + "' ";
                reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    result = true;
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            con.Close();
            return result;
        }

        private void Button7_Click(object sender, EventArgs e)
        {
            comAdd_shelf.Text = "";
            comAdd_shelf.Items.Clear();
            comAdd_Zone.Text = "";
            comAdd_Zone.Items.Clear();
            comAdd_Slot.Text = "";
            comAdd_Slot.Items.Clear();
            comAdd_side.Text = "";
            comAdd_side.Items.Clear();
            comAdd_partpackage.Text = "";
            comAdd_partpackage.Items.Clear();
            checkAdd_Rank.Checked = false;
          //  comAdd_partpackage.Enabled = false;
            dt_local.Rows.Clear();
            dt_line.Rows.Clear();
            //clear datagridview befor use
            dataGridView2.Rows.Clear();
            dataGridView3.Rows.Clear();
            dataGridView2.Columns.Clear();
            dataGridView3.Columns.Clear();
            //datatable location
            dt_local.Columns.Clear();
            dt_local.Columns.Add("Shelf");
            dt_local.Columns.Add("Zone");
            dt_local.Columns.Add("Floor");
            dt_local.Columns.Add("Side");
            dt_local.Columns.Add("Slot");
            //datatable rank
            dt_rank.Columns.Clear();
            dt_rank.Columns.Add("Rank");
            dt_rank.Columns.Add("Action");
            //datatable line
            dt_line.Columns.Clear();
            dt_line.Columns.Add("Line No");
            load_line();
            load_location();
            load_propertyPart();
            load_typePart();
           
            TableLocation();
            TableLine();
            bt_Pickup.BackColor = menuButtonUnSelectColor;
            bt_addStock.BackColor = menuButtonUnSelectColor;
            bt_manage.BackColor = menuButtonSelectColor;
            bt_userSetup.BackColor = menuButtonUnSelectColor;
           
        }
        #endregion
        //edit part
        #region
        //click edit in page part data
        private void Button13_Click(object sender, EventArgs e)
        {
        
            tabControl1.SelectTab(9);
            bt_Pickup.BackColor = menuButtonUnSelectColor;
            bt_addStock.BackColor = menuButtonUnSelectColor;
            bt_manage.BackColor = menuButtonSelectColor;
            bt_userSetup.BackColor = menuButtonUnSelectColor;
  

            //location table
            dataGridView5.Columns.Clear();
            dataGridView5.Columns.Add("shelf", "Shelf");
            dataGridView5.Columns.Add("zone", "Zone");
            dataGridView5.Columns.Add("floor", "Floor");
            dataGridView5.Columns.Add("side", "Side");
            dataGridView5.Columns.Add("slot", "Slot");
            dataGridView5.DefaultCellStyle.Font = new Font("tahoma",12);
            dataGridView5.Columns[0].Width = 78;
            dataGridView5.Columns[1].Width = 78;
            dataGridView5.Columns[2].Width = 78;
            dataGridView5.Columns[3].Width = 78;
            dataGridView5.Columns[4].Width = 78;
            //line table
            dataGridView4.Columns.Clear();
            dataGridView4.Columns.Add("line", "Line No(DB)");
            dataGridView4.DefaultCellStyle.Font = new Font("tahoma", 12);
            dataGridView4.Columns[0].Width = 310;
            //clear text box   
            comEdit_shelf.Text = "";
            comEdit_shelf.Items.Clear();
            comEdit_zone.Text = "";
            comEdit_zone.Items.Clear();
            comEdit_side.Text = "";
            comEdit_side.Items.Clear();
            comEdit_slot.Text = "";
            comEdit_slot.Items.Clear();
            comEdit_line.Text = "";
            comEdit_line.Items.Clear();
            //set button location
            btEdit_add.Visible = true;
            btEdit_delLocate.Visible = true;
            btEdit_addLocal.Visible = false;
            btEdit_cancelLocal.Visible = false;
            //set button line
            btEdit_addLine.Visible = true;
            btEdit_delLine.Visible = true;
            btEdit_addLineNo.Visible = false;
            btEdit_cancelLine.Visible = false;
            //set data table location
            dt_local.Columns.Clear();
            dt_local.Rows.Clear();
            dt_local.Columns.Add("Shelf");
            dt_local.Columns.Add("Zone");
            dt_local.Columns.Add("Floor");
            dt_local.Columns.Add("Side");
            dt_local.Columns.Add("Slot");
            //set data table line
            comEdit_line.Text = "";
            dt_line.Columns.Clear();
            dt_line.Rows.Clear();
            dt_line.Columns.Add("Line No");
            tEdit_partno.Enabled = false;
            Edit_drop();
          //  load_EditRank();
            edit_lacalTable();
            edit_lineTable();
            load_EditlocalDataTable(); //make data location table for edit part
            load_EditlineDataTable();
        }
        private void Edit_drop()
        {
            comEdit_partpackage.Items.Clear();
            comEdit_parttype.Items.Clear();
            comEdit_partpackage.Items.Add("1.Roll เล็ก");
            comEdit_partpackage.Items.Add("2.Roll ใหญ่");
            comEdit_partpackage.Items.Add("3.Connector");
            comEdit_partpackage.Items.Add("4.CPU");
            comEdit_parttype.Items.Add("1.Common");
            comEdit_parttype.Items.Add("2.L1");
            comEdit_parttype.Items.Add("3.L2");
            comEdit_parttype.Items.Add("4.L3");
            comEdit_parttype.Items.Add("5.L4");
            comEdit_parttype.Items.Add("6.L5");
            comEdit_parttype.Items.Add("7.Oth.");

        }
        private void edit_lacalTable()
        {
            cmd = new SqlCommand();
            cmd.Connection = con;
            try
            {
                dataGridView5.Rows.Clear();
                int index = 0;
                if (dataGridView16.Rows.Count <= 1) { index = 0; }
                else { index = dataGridView16.CurrentCell.RowIndex; }
                string part_no = dataGridView1.SelectedRows[0].Cells[0].Value + string.Empty;
                if (index > 0)
                {

                    try
                    {
                        string shelf_ = dataGridView16.Rows[0].Cells[0].Value.ToString();
                        con.Open();
                        cmd.CommandText = "SELECT [shelf],[zone],[floor],[side],[slot] FROM [THAIARROW_PART_STORE].[dbo].[Master_location] where part_no_registered ='" + part_no + "' AND area ='" + shelf_ + "'";
                        reader = cmd.ExecuteReader();
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                string shelf = reader.GetValue(0).ToString();
                                string zone = reader.GetValue(1).ToString();
                                string floor = reader.GetValue(2).ToString();
                                string side = reader.GetValue(3).ToString();
                                string slot = reader.GetValue(4).ToString();
                                dataGridView5.Rows.Add(shelf, zone, floor, side, slot);
                            }
                        }

                    }
                    catch (Exception ab)
                    {
                        MessageBox.Show(ab.Message);
                    }
                    finally
                    {
                        con.Close();
                    }
                }
                if (index <= 0)
                {
                    try
                    {
                        con.Open();
                        cmd.CommandText = "SELECT [shelf],[zone],[floor],[side],[slot] FROM [THAIARROW_PART_STORE].[dbo].[Master_location] where part_no_registered ='" + part_no + "'";
                        reader = cmd.ExecuteReader();
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                string shelf = reader.GetValue(0).ToString();
                                string zone = reader.GetValue(1).ToString();
                                string floor = reader.GetValue(2).ToString();
                                string side = reader.GetValue(3).ToString();
                                string slot = reader.GetValue(4).ToString();
                                dataGridView5.Rows.Add(shelf, zone, floor, side, slot);
                            }
                        }

                    }
                    catch (Exception ab)
                    {
                        MessageBox.Show(ab.Message);
                    }
                    finally
                    {
                        reader.Close();
                        con.Close();
                    }
                }
            }
            catch(Exception ab)
            {
                MessageBox.Show(ab.Message);
            }
        }
        private void edit_lineTable()
        {
            cmd = new SqlCommand();
            cmd.Connection = con;
            dataGridView4.Rows.Clear();
            
            try
            {
                string part_no = dataGridView1.SelectedRows[0].Cells[0].Value + string.Empty;
                con.Open();
                cmd.CommandText = "SELECT [line_no] FROM [THAIARROW_PART_STORE].[dbo].[Master_line_part] where part_no ='" + part_no + "'";
                reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        string lineNo = reader.GetValue(0).ToString();
                        dataGridView4.Rows.Add(lineNo);
                    }
                }

            }
            catch (Exception ab)
            {
                MessageBox.Show(ab.Message);
            }
            finally
            {
                con.Close();
            }
        }
        //load data into data table
        private void load_EditlocalDataTable()
        {
            try
            {
                for (int i = 0; i < dataGridView5.Rows.Count - 1; i++)
                {
                    string shelf = dataGridView5[0, i].Value.ToString();
                    string zone_ = dataGridView5[1, i].Value.ToString();
                    string floor = dataGridView5[2, i].Value.ToString();
                    string side = dataGridView5[3, i].Value.ToString();
                    string slot_ = dataGridView5[4, i].Value.ToString();
                    dt_local.Rows.Add(shelf, zone_, floor, side, slot_);
                }
            }
            catch (Exception ab)
            {
                MessageBox.Show(ab.Message);
            }
        }
        //load data into data table
        private void load_EditlineDataTable()
        {
            try
            {
                for (int i = 0; i < dataGridView4.Rows.Count - 1; i++)
                {
                    string line_ = dataGridView4[0, i].Value.ToString();
                    dt_line.Rows.Add(line_);
                }
            }
            catch (Exception ab)
            {
                MessageBox.Show(ab.Message);
            }
        }
        private void BtEdit_delpart_Click(object sender, EventArgs e)
        {

        }
        #endregion
        //table part data
        //part data region
        #region
        //load data for part data table
        private void load_pdTable()
        {
            cmd = new SqlCommand();
            cmd.Connection = con;
            dt_partData.Rows.Clear();
            dt_partData.Columns.Clear();

            try
            {
                con.Open();
                cmd.CommandText = "SELECT [part_no],[part_name],[model],[FIFO_enable],[rank_enable],[max_stock],[min_stock],[qty_pcs],[create_user],[create_date],[update_user],[update_date],[supplier],[part_package],[part_type],[stock_available] FROM [THAIARROW_PART_STORE].[dbo].[Master_part_data] ORDER BY part_no ASC";
                reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        string partNo = reader.GetValue(0).ToString();
                        string partName = reader.GetValue(1).ToString();
                        string model = reader.GetValue(2).ToString();
                        string fifo = reader.GetValue(3).ToString();
                        string rank = reader.GetValue(4).ToString();
                        string max = reader.GetValue(5).ToString();
                        string min = reader.GetValue(6).ToString();
                        string qty = reader.GetValue(7).ToString();
                        string createUser = reader.GetValue(8).ToString();
                        string createDate = reader.GetValue(9).ToString();
                        string updateUser = reader.GetValue(10).ToString();
                        string updateDate = reader.GetValue(11).ToString();
                        string supplier = reader.GetValue(12).ToString();
                        string partPackage = reader.GetValue(13).ToString();
                        string partType = reader.GetValue(14).ToString();
                        string stock_available = reader.GetValue(15).ToString();
                        // dt_partData.Rows.Add(partNo, partName, model, fifo, rank, max, min, qty, createUser, createDate, updateUser, supplier);
                        dataGridView1.Rows.Add(partNo, partName, model, fifo, rank, max, min, stock_available, createUser, createDate, updateUser, updateDate, supplier, partPackage, partType);


                    }

                }
            }
            catch (Exception ab)
            {
                MessageBox.Show(ab.Message);
            }
            finally
            {
                reader.Close();
                con.Close();
            }
        }
        private void load_pdLocalTable()
        {
            cmd = new SqlCommand();
            cmd.Connection = con;
           
            dataGridView16.Rows.Clear();
            try
            {
                int index = dataGridView1.CurrentCell.RowIndex;
                string partNo = dataGridView1.Rows[index].Cells[0].Value.ToString();
                con.Open();
                cmd.CommandText = "SELECT * FROM [THAIARROW_PART_STORE].[dbo].[Master_location] where part_no_registered ='" + partNo + "'";
                reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        string shelf = reader.GetValue(1).ToString();
                        string zone = reader.GetValue(2).ToString();
                        string floor = reader.GetValue(3).ToString();
                        string side = reader.GetValue(4).ToString();
                        string slot = reader.GetValue(5).ToString();
                        dataGridView16.Rows.Add(shelf, zone, floor, side,slot);
                        dataGridView16.DefaultCellStyle.Font = new Font("Tahoma", 9);
                    }
                }
            }
            catch (Exception ab)
            {
                MessageBox.Show(ab.Message);
            }
            finally
            {
                con.Close();
            }
        }
        private void load_pdLineTable()
        {
            cmd = new SqlCommand();
            cmd.Connection = con;
           
            dataGridView17.Rows.Clear();
            try
            {
                int index = dataGridView1.CurrentCell.RowIndex;
                string partNo = dataGridView1.Rows[index].Cells[0].Value.ToString();
                con.Open();
                cmd.CommandText = "SELECT [line_no],[part_no] FROM [THAIARROW_PART_STORE].[dbo].[Master_line_part] where part_no ='" + partNo + "'";
                reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        string line_no = reader.GetValue(0).ToString();
                        dataGridView17.Rows.Add(line_no);
                    }
                }
            }
            catch (Exception ab)
            {
                MessageBox.Show(ab.Message);
            }
            finally
            {
                con.Close();
            }
        }
      
        
        //table part data in data grid view
        private void DataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                string partNo = dataGridView1.SelectedRows[0].Cells[0].Value + string.Empty;
                string partName = dataGridView1.SelectedRows[0].Cells[1].Value + string.Empty;
                string model = dataGridView1.SelectedRows[0].Cells[2].Value + string.Empty;
                string fifo = dataGridView1.SelectedRows[0].Cells[3].Value + string.Empty;
                string rank = dataGridView1.SelectedRows[0].Cells[4].Value + string.Empty;
                string max = dataGridView1.SelectedRows[0].Cells[5].Value + string.Empty;
                string min = dataGridView1.SelectedRows[0].Cells[6].Value + string.Empty;
                string qty = dataGridView1.SelectedRows[0].Cells[7].Value + string.Empty;
                string supllier = dataGridView1.SelectedRows[0].Cells[12].Value + string.Empty;
                string partpacket = dataGridView1.SelectedRows[0].Cells[13].Value + string.Empty;
                string partType = dataGridView1.SelectedRows[0].Cells[14].Value + string.Empty;
                tEdit_partno.Text = partNo;
                tEdit_partname.Text = partName;
                tEdit_model.Text = model;
                tEdit_supplier.Text = supllier;
                
                if(partpacket !="")
                {
                    switch(partpacket)
                    {
                        case "Roll เล็ก":
                            partpacket = "1.Roll เล็ก";
                            break;
                        case "Roll ใหญ่":
                            partpacket = "2.Roll ใหญ่";
                            break;
                        case "Connector":
                            partpacket = "3.Connector";
                            break;
                        case "CPU":
                            partpacket = "4.CPU";
                            break;
                    }
                }
                comEdit_partpackage.Text = partpacket;
                if (partType !="")
                {
                    switch(partType)
                    {
                        case "Common":
                            partType = "1.Common";
                                break;
                        case "L1":
                            partType = "2.L1";
                            break;
                        case "L2":
                            partType = "3.L2";
                            break;
                        case "L3":
                            partType = "4.L3";
                            break;
                        case "L4":
                            partType = "5.L4";
                            break;
                        case "L5":
                            partType = "6.L5";
                            break;
                        case "Oth.":
                            partType = "7.Oth.";
                            break;

                    }
                }
                comEdit_parttype.Text = partType;
                if (fifo == "True") { checkEdit_FIFO.Checked = true; }
                if (fifo == "False") { checkEdit_FIFO.Checked = false; }
                if (rank == "False") { checkEdit_rank.Checked = false; }
                if (rank == "True") { checkEdit_rank.Checked = true; }
                numEdit_max.Value = UInt32.Parse(max);
                numEdit_min.Value = UInt32.Parse(min);
                numEdit_qty.Value = UInt32.Parse(qty);
                tpd_partno.Text = partNo;
                tpd_partname.Text = partName;
                load_pdLocalTable();
                load_pdLineTable();
            }
            catch (Exception ab)
            {
                 MessageBox.Show(ab.Message);
              //  MessageBox.Show("กรุณาเลือกข้อมูลให้ถูกต้อง");
            }
        }

        private void Button29_Click(object sender, EventArgs e)
        {
            cmd = new SqlCommand();
            cmd.Connection = con;
            string search = tpd_search.Text;
            if (search != "")
            {
                try
                {
                    con.Open();
                    cmd.CommandText = "SELECT [part_no],[part_name],[model],[FIFO_enable],[rank_enable],[max_stock],[min_stock],[stock_available],[create_user],[create_date],[update_user],[update_date],[supplier],[part_package],[part_type] FROM [THAIARROW_PART_STORE].[dbo].[Master_part_data] where part_no Like '%" + search + "%'OR part_name Like '%" + search + "%' OR model LIKE '%" + search + "%' OR FIFO_enable LIKE '%" + search + "%' OR rank_enable LIKE '%" + search + "%' OR max_stock LIKE '%" + search + "%' OR min_stock  LIKE '%" + search + "%' OR stock_available  LIKE '%" + search + "%' OR create_user  LIKE '%" + search + "%' OR create_date  LIKE '%" + search + "%' OR update_user  LIKE '%" + search + "%' OR supplier  LIKE '%" + search + "%' OR part_package  LIKE '%" + search + "%' OR part_type  LIKE '%" + search + "%'";
                    reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        dataGridView1.Rows.Clear();
                        while (reader.Read())
                        {
                            string partNo = reader.GetValue(0).ToString();
                            string partName = reader.GetValue(1).ToString();
                            string model = reader.GetValue(2).ToString();
                            string fifo = reader.GetValue(3).ToString();
                            string rank = reader.GetValue(4).ToString();
                            string max = reader.GetValue(5).ToString();
                            string min = reader.GetValue(6).ToString();
                            string stock_available = reader.GetValue(7).ToString();
                            string createUser = reader.GetValue(8).ToString();
                            string createDate = reader.GetValue(9).ToString();
                            string updateUser = reader.GetValue(10).ToString();
                            string updateDate = reader.GetValue(11).ToString();
                            string supplier = reader.GetValue(12).ToString();
                            string partPackage = reader.GetValue(13).ToString();
                            string partType = reader.GetValue(14).ToString();
                            // dt_partData.Rows.Add(partNo, partName, model, fifo, rank, max, min, qty, createUser, createDate, updateUser, supplier);
                            dataGridView1.Rows.Add(partNo, partName, model, fifo, rank, max, min, stock_available, createUser, createDate, updateUser, updateDate, supplier, partPackage, partType);
                        }
                    }
                }
                catch (Exception ab)
                {
                    MessageBox.Show(ab.Message);
                }
                finally
                {
                    reader.Close();
                    con.Close();
                }
            }
            else
            {
                load_pdTable();
            }
           
        }
        // delete button
        private void Bt_pddelpart_Click(object sender, EventArgs e)
        {
            //process in del part
            //1.del part_data
            //2. insert db data_event
            //3. del master line part
            //4.update location currrent part is null

            //del partdata
            List<string> detail = new List<string>();
            List<string> local_part = new List<string>();
            string details = "";
            string detail_local = "Location =";
            string d_line = "LineNo =";
            cmd = new SqlCommand();
            cmd.Connection = con;
            try
            {
                string part = dataGridView1.Rows[dataGridView1.CurrentCell.RowIndex].Cells[0].Value.ToString();
                con.Open();
                cmd.CommandText = "SELECT *  FROM [THAIARROW_PART_STORE].[dbo].[Master_part_data] where part_no ='"+part+"'";
                reader = cmd.ExecuteReader();
                if(reader.HasRows)
                {
                    while(reader.Read())
                    {
                        details = "part_no =" + reader.GetValue(1).ToString();
                        details = details + ":part_name =" + reader.GetValue(2).ToString();
                        details = details + ":model =" + reader.GetValue(3).ToString();
                        details = details + ":qc_enabled =" + reader.GetValue(4).ToString();
                        details = details + ":qc_lot =" + reader.GetValue(5).ToString();
                        details = details + ":fifo_enable =" + reader.GetValue(6).ToString();
                        details = details + ":rank_enable =" + reader.GetValue(7).ToString();
                        details = details + ":stock_available =" + reader.GetValue(8).ToString();
                        details = details + ":stock_hold =" + reader.GetValue(9).ToString();
                        details = details + ":max_stock =" + reader.GetValue(10).ToString();
                        details = details + ":min_stock =" + reader.GetValue(11).ToString();
                        details = details + ":qty_pcs =" + reader.GetValue(12).ToString();
                        details = details + ":create_user =" + reader.GetValue(13).ToString();
                        details = details + ":create_date =" + reader.GetValue(14).ToString();
                        details = details + ":update_user =" + reader.GetValue(15).ToString();
                        details = details + ":update_date =" + reader.GetValue(16).ToString();
                        details = details + ":supplier =" + reader.GetValue(17).ToString();
                        details = details + ":part_type =" + reader.GetValue(18).ToString();
                        details = details + ":part_package =" + reader.GetValue(19).ToString() + "," ;
                      //  details.Substring(1,deat)
                      //  detail.Add(details);

                    }
                }
                reader.Close();
                cmd.CommandText = "SELECT [shelf],[zone],[floor],[side],[slot] FROM [THAIARROW_PART_STORE].[dbo].[Master_location] where part_no_registered ='"+part+"'";
                reader = cmd.ExecuteReader();
              //  details = "";
                if(reader.HasRows)
                {
                    while(reader.Read())
                    {
                        detail_local = detail_local + "|" + reader.GetValue(0).ToString();
                        detail_local = detail_local + "|" + reader.GetValue(1).ToString();
                        detail_local = detail_local + "|" + reader.GetValue(2).ToString();
                        detail_local = detail_local + "|" + reader.GetValue(3).ToString();
                        detail_local = detail_local + "|" + reader.GetValue(4).ToString();
                        //local_part.Add(details);
                        detail_local = detail_local + ",";
                    }
                }
                reader.Close();
                cmd.CommandText = "SELECT line_no FROM [THAIARROW_PART_STORE].[dbo].[Master_line_part] where part_no ='"+part+"'";
                reader = cmd.ExecuteReader();
                if(reader.HasRows)
                {
                    while(reader.Read())
                    {
                        d_line = d_line+":"+reader.GetValue(0).ToString();
                    }
                }
                reader.Close();
                con.Close();
                del_dataEvent(part,details, detail_local,d_line);
                del_linepart_partdata(part);
                update_datalocation(part);
                //  if()
                con.Open();
                cmd.CommandText = "DELETE FROM [THAIARROW_PART_STORE].[dbo].[Master_part_data] where part_no ='" + part + "'";
                int b = cmd.ExecuteNonQuery();
                if (b == 0)
                {
                    MessageBox.Show("เกิดข้อผิดพลาดขณะลบข้อมูล Part ที่ master_line_part");
                    
                }


            }
            catch(Exception ab)
            {
                MessageBox.Show(ab.Message);
            }
            finally
            {
                con.Close();
                dataGridView16.Columns.Clear();
                dataGridView17.Columns.Clear();

                //clear row table
                dataGridView1.Rows.Clear();
                dataGridView16.Rows.Clear();


                //hearder location
                dataGridView16.Columns.Add("shelf", "Shelf");
                dataGridView16.Columns.Add("zone", "Zone");
                dataGridView16.Columns.Add("floor", "Floor");
                dataGridView16.Columns.Add("side", "Side");
                dataGridView16.Columns.Add("slot", "Slot");
             //   dataGridView16.Columns[0].Width = 69;
              //  dataGridView16.Columns[1].Width = 69;
              //  dataGridView16.Columns[2].Width = 69;
              //  dataGridView16.Columns[3].Width = 69;
              //  dataGridView16.Columns[4].Width = 69;
                //herader lineDB
                dataGridView17.Columns.Add("lineno", "Line No(DB)");
               // dataGridView17.Columns[0].Width = 150;
                 load_pdTable();
                 load_pdLocalTable();
                 load_pdLineTable();
            }

        }
        private bool update_datalocation(string part)
        {
            bool result = true;
            cmd = new SqlCommand();
            cmd.Connection = con;
            try
            {
                con.Open();
                cmd.CommandText = "UPDATE [THAIARROW_PART_STORE].[dbo].[Master_location] SET part_no_registered = NULL , part_registered_user = NULL ,part_registered_time = NULL where part_no_registered ='" +part+"'";
                int b = cmd.ExecuteNonQuery();
                if(b==0)
                {
                    MessageBox.Show("เกิดข้อผิดพลาดขณะทำการลบ Part ที่ master_location");
                }
            }
            catch(Exception ab)
            {
                MessageBox.Show(ab.Message);
            }
            finally
            {
                con.Close();
            }
            return result;
        }
        private bool del_dataEvent(string part,string detail, string detail_local,string d_line)
        {
            bool result = true;
            cmd = new SqlCommand();
            cmd.Connection = con;
            try
            {
                con.Open();
              //  detail = detail.Substring(1, detail.Length);
              //  detail_local = detail_local.Substring(1, detail_local.Length);
                string detail_all = detail + detail_local + d_line;
                string event_action = "DeletePart";
                cmd.CommandText = "INSERT INTO [THAIARROW_PART_STORE].[dbo].[data_event] (event_action,part_no,event_user,event_time,detail) VALUES ('"+event_action+"','"+part+"','"+Login.userName+"','"+DateTime.Now+"','"+detail_all+"')";
                int b = cmd.ExecuteNonQuery();
                if (b == 0)
                {
                    MessageBox.Show("เกิดข้อผิดพลากขณะลบข้อมูล Part ที่ data_event");
                    result = false;
                }

            }
            catch (Exception ab)
            {
                MessageBox.Show(ab.Message);
            }
            finally
            {
                con.Close();
            }
            return result;
        }
        private bool del_linepart_partdata(string part)
        {
            bool result = true;
            cmd = new SqlCommand();
            cmd.Connection = con;
            try
            {
                con.Open();
               
                cmd.CommandText = "DELETE FROM [THAIARROW_PART_STORE].[dbo].[Master_line_part] where part_no ='"+part+"'";
                int b = cmd.ExecuteNonQuery();
                if (b == 0)
                {
                    MessageBox.Show("เกิดข้อผิดพลากขณะลบข้อมูล Part ที่ master_line_part");
                    result = false;
                }

            }
            catch (Exception ab)
            {
                MessageBox.Show(ab.Message);
            }
            finally
            {
                con.Close();
            }
            return result;
        }
        #endregion
        //region add stock
        #region
        //addStock page
        private void fill_dtaddstock()
        {
            dt_addstock.Columns.Clear();
            dt_addstock.Rows.Clear();
            dt_addstock.Columns.Add("partno");
            dt_addstock.Columns.Add("model");
           
            dt_addstock.Columns.Add("partname");
            dt_addstock.Columns.Add("partcode");

            dt_addstock.Columns.Add("boxID");
            dt_addstock.Columns.Add("maker");
            dt_addstock.Columns.Add("shelf");
            dt_addstock.Columns.Add("slot");
            dt_addstock.Columns.Add("userIn");
            dt_addstock.Columns.Add("qty");
            dt_addstock.Columns.Add("mpn");
            dt_addstock.Columns.Add("floor");
            dt_addstock.Columns.Add("statusM");
            dt_addstock.Columns.Add("timeIn");
            dt_addstock.Columns.Add("rank");
            dt_addstock.Columns.Add("dcode");
            
            dt_addstock.Columns.Add("zone");
            dt_addstock.Columns.Add("statusDB");
            dt_addstock.Columns.Add("stock");
            dt_addstock.Columns.Add("ccode");
            dt_addstock.Columns.Add("min");
            dt_addstock.Columns.Add("rank_en");
            dt_addstock.Columns.Add("max");
            dt_addstock.Columns.Add("side");

        }
        private void TextBox44_KeyDown(object sender, KeyEventArgs e)
        {
            e.SuppressKeyPress = false;
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true; // for close sound whrn enter text box 
                string separator = tas_scan.Text.ToString().Substring(0, 2);
                if (separator == "<B")
                {
                    try
                    {

                        string[] barcode = tas_scan.Text.Replace("<B1>", "").Split(' ');
                        //string[] sub_barcode = new string[barcode.Length];
                        List<string> sub_barcode = new List<string>();
                       // int j = 0;

                        for (int i = 0; i < barcode.Length; i++)
                        {

                            if (barcode[i] != "")
                            {
                                sub_barcode.Add(barcode[i]);
                               // j++;
                            }

                        }
                        string partNo = sub_barcode[1];
                        tas_partname.Clear();
                        tas_rank.Clear();


                        if (sub_barcode.Count>5) { fill_addstock(1, partNo, barcode, sub_barcode); }
                        if (sub_barcode.Count == 5) { fill_addstock(2, partNo, barcode, sub_barcode); }
                    }
                    catch (Exception ab)
                    {
                        MessageBox.Show(ab.Message);
                    }
                }
                if (separator == "IP")
                    {
                    try
                    {

                        string[] barcode = tas_scan.Text.Split(' ');
                        //string[] sub_barcode = new string[barcode.Length];
                        List<string> sub_barcode = new List<string>();
                       // int j = 0;

                        for (int i = 0; i < barcode.Length; i++)
                        {

                            if (barcode[i] != "")
                            {
                                sub_barcode.Add(barcode[i]);
                              //  j++;
                            }

                        }
                        string partNo = sub_barcode[1].Substring(6, sub_barcode[1].Length - 6);
                        tas_partname.Clear();
                        tas_rank.Clear();
                    //    e.SuppressKeyPress = true;  // for close sound whrn enter text box 
                                                  
                        fill_addstock(3,partNo, barcode, sub_barcode);
                    }
                    catch (Exception ab)
                    {
                        MessageBox.Show(ab.Message);
                    }
                }
            }
        }

    private void fill_addstock(int select,string partNo, string[] barcode, List<string> sub_barcode )
        {
            if (select == 1)
            {
                cmd = new SqlCommand();
                cmd.Connection = con;
                int no = 0;
                int j = 0;
                string rank = sub_barcode[5];
                string rank_en = "";
                bool boxIdCheck = true;
                bool part_check = false;
              //  bool rank = false;
                string model = "";
                string part_name = "";
                string shelf = "";
                string zone = "";
                string floor = "";
                string side = "";
                string slot = "";
                string statusM = "";
                string statusDB = "INUSE";
                int stock_available = 0;
                int stock = 0;
                string boxID = sub_barcode[0];
                string MPN = sub_barcode[2];
                string maker = sub_barcode[3];
                string qty = sub_barcode[4];
                string ccode = sub_barcode[5];
                string dcode = sub_barcode[6];
                tas_scan.Text = partNo;
                string userIn = Login.userName;
                string TimeIn = DateTime.Now.ToString();
                string partcode = sub_barcode[0] + " " + sub_barcode[1] + " " + sub_barcode[2] + " " + sub_barcode[3] + " " + sub_barcode[4] + " " + sub_barcode[5] + " " + sub_barcode[6];
                List<string> box_idCheck = new List<string>();
                List<string> boxID_dbCheck = new List<string>();
                int max = 0;
                int min = 0;

                try
                {
                    
                    con.Open();
                    cmd.CommandText = "SELECT [box_id]FROM [THAIARROW_PART_STORE].[dbo].[Stock_part] where part_no='" + partNo + "'";
                    reader = cmd.ExecuteReader();
                    if(reader.HasRows)
                    {
                        while(reader.Read())
                        {
                            boxID_dbCheck.Add(reader.GetValue(0).ToString());
                        }
                    }
                    reader.Close();
                    for (j=0;j<dt_addstock.Rows.Count;j++)
                    {
                        if (boxID == dt_addstock.Rows[j].ItemArray[4].ToString() && partNo == dt_addstock.Rows[j].ItemArray[0].ToString() )
                    {
                            boxIdCheck = false;
                         //   pas_duplicate.Visible = true;
                        //    tas_alertboxid.Text = partNo;
                        //    timer_addstock.Enabled = true;
                    }
                    }
                    for(j=0;j<boxID_dbCheck.Count;j++)
                    {
                        if(boxID == boxID_dbCheck[j].ToString() && boxIdCheck)
                        {
                            boxIdCheck = false;
                          //  pas_duplicate.Visible = true;
                          //  tas_alertboxid.Text = partNo;
                          //  timer_addstock.Enabled = true;
                           
                        }
                    }
                    cmd.CommandText = "SELECT part_no FROM [THAIARROW_PART_STORE].[dbo].[Master_part_data] where part_no ='"+partNo+"'";
                    reader = cmd.ExecuteReader();
                    if(reader.HasRows)
                    {
                        part_check = true;
                    }
                    reader.Close();
                    if (part_check)
                    {
                        cmd.CommandText = "SELECT a.model,a.part_no,a.part_name,b.shelf,b.zone,b.floor,b.side,b.slot,a.qty_pcs,a.rank_enable,a.max_stock,a.min_stock,a.stock_available FROM [THAIARROW_PART_STORE].[dbo].[Master_part_data] AS a INNER JOIN [THAIARROW_PART_STORE].[dbo].[Master_location] AS b ON a.part_no = b.part_no_registered where a.part_no='" + partNo + "'";
                        reader = cmd.ExecuteReader();

                        if (reader.HasRows && boxIdCheck)
                        {

                            while (reader.Read())
                            {

                                model = reader.GetValue(0).ToString();
                                part_name = reader.GetValue(2).ToString();
                                shelf = reader.GetValue(3).ToString();
                                zone = reader.GetValue(4).ToString();
                                floor = reader.GetValue(5).ToString();
                                side = reader.GetValue(6).ToString();
                                slot = slot + "," + reader.GetValue(7).ToString();
                                stock = int.Parse(reader.GetValue(8).ToString());
                                rank_en = reader.GetValue(9).ToString();
                                max = int.Parse(reader.GetValue(10).ToString());
                                min = int.Parse(reader.GetValue(11).ToString());
                                if (stock > max) { statusDB = "OverStock"; }
                                else if (stock <= max && stock > min) { statusDB = "Normal"; }
                                else { statusDB = "Wanrning"; }
                                stock_available = reader.GetInt32(12);

                            }
                            if (dataGridView6.Rows.Count == 0) { count_addstock = 0; }
                            count_addstock++;
                             no += count_addstock;
                            slot = slot.Substring(1, slot.Length - 1);

                            dataGridView6.Rows.Add(no, model, rank, partNo, part_name, shelf, zone, floor, side, slot, statusDB, userIn, TimeIn, stock_available, partcode);
                            dt_addstock.Rows.Add(partNo, model, part_name, partcode, boxID, maker, shelf, slot, userIn, qty, MPN, floor, statusM, TimeIn, rank_en, dcode, zone, statusDB, stock, ccode, min, rank, max, side);

                            tas_partname.Text = part_name;
                            tas_boxID.Text = boxID;
                            tas_mpn.Text = MPN;
                            tas_qty.Text = qty;
                            tas_ccode.Text = ccode;
                            tas_dcode.Text = dcode;
                            reader.Close();
                            con.Close();
                            fill_addstockTextBox();

                            if (count_stock_over())
                            {
                                timer_addstock.Enabled = true;
                                // tas_rank.Text = partNo;
                                //tas_rank.TextAlign = HorizontalAlignment.Center;
                                tas_alertadd.Text = partNo;
                                panel52.Visible = true;
                            }
                            else
                            {
                                panel22.Visible = true;
                                tas_alertoverstock.Text = partNo;
                                timer_addstock.Enabled = true;
                            }
                        }
                        else if(!reader.HasRows)
                        {
                            reader.Close();
                            cmd.CommandText = "SELECT model,part_name,qty_pcs,rank_enable,max_stock,min_stock,stock_available FROM [THAIARROW_PART_STORE].[dbo].[Master_part_data] where part_no ='"+partNo+"'";
                            reader = cmd.ExecuteReader();
                            if(reader.HasRows && boxIdCheck)
                            {
                                while(reader.Read())
                                {
                                    model = reader.GetValue(0).ToString();
                                    part_name = reader.GetValue(1).ToString();
                                    shelf = "";
                                    zone = "";
                                    floor = "";
                                    side = "";
                                    slot = "";
                                    stock = int.Parse(reader.GetValue(2).ToString());
                                    rank_en = reader.GetValue(3).ToString();
                                    max = int.Parse(reader.GetValue(4).ToString());
                                    min = int.Parse(reader.GetValue(5).ToString());
                                    if (stock > max) { statusDB = "OverStock"; }
                                    else if (stock <= max && stock > min) { statusDB = "Normal"; }
                                    else { statusDB = "Wanrning"; }
                                    stock_available = reader.GetInt32(6);
                                }
                                if (dataGridView6.Rows.Count == 0) { count_addstock = 0; }
                                count_addstock++;
                                 no += count_addstock;
                                dataGridView6.Rows.Add(no, model, rank, partNo, part_name, shelf, zone, floor, side, slot, statusDB, userIn, TimeIn, stock_available, partcode);
                                dt_addstock.Rows.Add(partNo, model, part_name, partcode, boxID, maker, shelf, slot, userIn, qty, MPN, floor, statusM, TimeIn, rank_en, dcode, zone, statusDB, stock, ccode, min, rank, max, side);
                                tas_partname.Text = part_name;
                                tas_boxID.Text = boxID;
                                tas_mpn.Text = MPN;
                                tas_qty.Text = qty;
                                tas_ccode.Text = ccode;
                                tas_dcode.Text = dcode;
                                reader.Close();
                                con.Close();
                                fill_addstockTextBox();

                                if (count_stock_over())
                                {
                                    timer_addstock.Enabled = true;
                                    // tas_rank.Text = partNo;
                                    //tas_rank.TextAlign = HorizontalAlignment.Center;
                                    tas_alertadd.Text = partNo;
                                    panel52.Visible = true;
                                }
                                else
                                {
                                    panel22.Visible = true;
                                    tas_alertoverstock.Text = partNo;
                                    timer_addstock.Enabled = true;
                                }
                            }
                            else
                            {
                                if (!boxIdCheck)
                                {
                                    pas_duplicate.Visible = true;
                                    tas_alertboxid.Text = partNo;
                                    timer_addstock.Enabled = true;

                                }
                                else
                                {
                                    panel37.Visible = true;
                                    tas_partname.Text = partNo;
                                    tas_alertnotfound.Text = partNo;
                                    timer_addstock.Enabled = true;
                                }
                            }
                        }
                        else
                        {
                            if (!boxIdCheck)
                            {
                                pas_duplicate.Visible = true;
                                tas_alertboxid.Text = partNo;
                                timer_addstock.Enabled = true;

                            }
                            else
                            {
                                panel37.Visible = true;
                                tas_partname.Text = partNo;
                                tas_alertnotfound.Text = partNo;
                                timer_addstock.Enabled = true;
                            }

                        }
                    }
                    else
                    {
                        panel37.Visible = true;
                        tas_partname.Text = partNo;
                        tas_alertnotfound.Text = partNo;
                        timer_addstock.Enabled = true;
                    }

                }
                catch (Exception ab)
                {
                    MessageBox.Show(ab.Message);
                }
                finally
                {
                    reader.Close();
                    con.Close();
                   // if (boxIdCheck) { count_stock(); }
                }
            }
            if(select ==2)
            {
                cmd = new SqlCommand();
                cmd.Connection = con;
                int no = 0;
                int j = 0;
                string rank = "";
                string rank_en = "";
                bool boxIdCheck = true;
                bool part_check = false;
                //  bool rank = false;
                string model = "";
                string part_name = "";
                string shelf = "";
                string zone = "";
                string floor = "";
                string side = "";
                string slot = "";
                string statusM = "";
                string statusDB = "INUSE";
                int stock_available = 0;
                int stock = 0;
                string boxID = sub_barcode[0];
                string MPN = sub_barcode[2];
                string maker = sub_barcode[3];
                string qty = sub_barcode[4].Substring(8,sub_barcode[4].Length-8);
                string ccode = "";
                string dcode = "";
                tas_scan.Text = partNo;
                string userIn = Login.userName;
                string TimeIn = DateTime.Now.ToString();
                string partcode = sub_barcode[0] + " " + sub_barcode[1] + " " + sub_barcode[2] + " " + sub_barcode[3] + " " + sub_barcode[4] ;
                List<string> box_idCheck = new List<string>();
                List<string> boxID_dbCheck = new List<string>();
                int max = 0;
                int min = 0;


                try
                {
                    con.Open();
                    cmd.CommandText = "SELECT [box_id]FROM [THAIARROW_PART_STORE].[dbo].[Stock_part] where part_no='" + partNo + "'";
                    reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            boxID_dbCheck.Add(reader.GetValue(0).ToString());
                        }
                    }
                    reader.Close();
                    for (j = 0; j < dt_addstock.Rows.Count; j++)
                    {
                        if (boxID == dt_addstock.Rows[j].ItemArray[4].ToString() && partNo == dt_addstock.Rows[j].ItemArray[0].ToString())
                        {
                            boxIdCheck = false;
      
                        }
                    }
                    for (j = 0; j < boxID_dbCheck.Count; j++)
                    {
                        if (boxID == boxID_dbCheck[j].ToString() && boxIdCheck)
                        {
                            boxIdCheck = false;
                     
                        }
                    }
                    cmd.CommandText = "SELECT part_no FROM [THAIARROW_PART_STORE].[dbo].[Master_part_data] where part_no ='" + partNo + "'";
                    reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        part_check = true;
                    }
                    reader.Close();
                    if (part_check)
                    {
                        cmd.CommandText = "SELECT a.model,a.part_no,a.part_name,b.shelf,b.zone,b.floor,b.side,b.slot,a.qty_pcs,a.rank_enable,a.max_stock,a.min_stock,a.stock_available  FROM [THAIARROW_PART_STORE].[dbo].[Master_part_data] AS a INNER JOIN [THAIARROW_PART_STORE].[dbo].[Master_location] AS b ON a.part_no = b.part_no_registered where a.part_no='" + partNo + "'";
                        reader = cmd.ExecuteReader();

                        if (reader.HasRows && boxIdCheck)
                        {
                          //  timer_addstock.Enabled = true;
                          //  tas_rank.Text = partNo;
                            //tas_rank.TextAlign = HorizontalAlignment.Center;
                         //   tas_alertadd.Text = partNo;
                           // panel52.Visible = true;
                            while (reader.Read())
                            {

                                model = reader.GetValue(0).ToString();
                                part_name = reader.GetValue(2).ToString();

                                shelf = reader.GetValue(3).ToString();
                                zone = reader.GetValue(4).ToString();
                                floor = reader.GetValue(5).ToString();
                                side = reader.GetValue(6).ToString();
                                slot = slot + "," + reader.GetValue(7).ToString();
                                stock = int.Parse(reader.GetValue(8).ToString());
                                rank = reader.GetValue(9).ToString();
                                max = int.Parse(reader.GetValue(10).ToString());
                                min = int.Parse(reader.GetValue(11).ToString());
                                if (stock > max) { statusDB = "OverStock"; }
                                else if (stock <= max && stock > min) { statusDB = "Normal"; }
                                else { statusDB = "Wanrning"; }
                                stock_available = reader.GetInt32(12);

                            }
                            if (dataGridView6.Rows.Count == 0) { count_addstock = 0; }
                            count_addstock++;
                              no += count_addstock;
                    
                            slot = slot.Substring(1, slot.Length - 1);

                            dataGridView6.Rows.Add(no, model, rank, partNo, part_name, shelf, zone, floor, side, slot, statusDB, userIn, TimeIn, stock_available, partcode);
                            dt_addstock.Rows.Add(partNo, model, part_name, partcode, boxID, maker, shelf, slot, userIn, qty, MPN, floor, statusM, TimeIn, rank_en, dcode, zone, statusDB, stock, ccode, min, rank, max, side);

                            tas_partname.Text = part_name;
                            tas_boxID.Text = boxID;
                            tas_mpn.Text = MPN;
                            tas_qty.Text = qty;
                            tas_ccode.Text = ccode;
                            tas_dcode.Text = dcode;
                            reader.Close();
                            con.Close();
                            fill_addstockTextBox();

                            if (count_stock_over())
                            {
                                timer_addstock.Enabled = true;
                                tas_alertadd.Text = partNo;
                                panel52.Visible = true;
                            }
                            else
                            {
                                panel22.Visible = true;
                                tas_alertoverstock.Text = partNo;
                                timer_addstock.Enabled = true;
                            }
                        }
                        else if(!reader.HasRows)
                        {
                            reader.Close();
                            cmd.CommandText = "SELECT model,part_name,qty_pcs,rank_enable,max_stock,min_stock,stock_available FROM [THAIARROW_PART_STORE].[dbo].[Master_part_data] where part_no ='" + partNo + "'";
                            reader = cmd.ExecuteReader();
                            if (reader.HasRows && boxIdCheck)
                            {
                                while (reader.Read())
                                {
                                    model = reader.GetValue(0).ToString();
                                    part_name = reader.GetValue(1).ToString();
                                    shelf = "";
                                    zone = "";
                                    floor = "";
                                    side = "";
                                    slot = "";
                                    stock = int.Parse(reader.GetValue(2).ToString());
                                    rank_en = reader.GetValue(3).ToString();
                                    max = int.Parse(reader.GetValue(4).ToString());
                                    min = int.Parse(reader.GetValue(5).ToString());
                                    if (stock > max) { statusDB = "OverStock"; }
                                    else if (stock <= max && stock > min) { statusDB = "Normal"; }
                                    else { statusDB = "Wanrning"; }
                                    stock_available = reader.GetInt32(6);
                                }
                                 count_addstock++;
                                 no += count_addstock;
                          
                                dataGridView6.Rows.Add(no, model, rank, partNo, part_name, shelf, zone, floor, side, slot, statusDB, userIn, TimeIn, stock_available, partcode);
                                dt_addstock.Rows.Add(partNo, model, part_name, partcode, boxID, maker, shelf, slot, userIn, qty, MPN, floor, statusM, TimeIn, rank_en, dcode, zone, statusDB, stock, ccode, min, rank, max, side);
                                tas_partname.Text = part_name;
                                tas_boxID.Text = boxID;
                                tas_mpn.Text = MPN;
                                tas_qty.Text = qty;
                                tas_ccode.Text = ccode;
                                tas_dcode.Text = dcode;
                                reader.Close();
                                con.Close();
                                fill_addstockTextBox();

                                if (count_stock_over())
                                {
                                    timer_addstock.Enabled = true;
                                    // tas_rank.Text = partNo;
                                    //tas_rank.TextAlign = HorizontalAlignment.Center;
                                    tas_alertadd.Text = partNo;
                                    panel52.Visible = true;
                                }
                                else
                                {
                                    panel22.Visible = true;
                                    tas_alertoverstock.Text = partNo;
                                    timer_addstock.Enabled = true;
                                }
                            }
                            else
                            {
                                if (!boxIdCheck)
                                {
                                    pas_duplicate.Visible = true;
                                    tas_alertboxid.Text = partNo;
                                    timer_addstock.Enabled = true;

                                }
                                else
                                {
                                    panel37.Visible = true;
                                    tas_partname.Text = partNo;
                                    tas_alertnotfound.Text = partNo;
                                    timer_addstock.Enabled = true;
                                }
                            }

                        }
                        else
                        {
                            if (!boxIdCheck)
                            {
                                pas_duplicate.Visible = true;
                                tas_alertboxid.Text = partNo;
                                timer_addstock.Enabled = true;

                            }
                            else
                            {
                                panel37.Visible = true;
                                tas_partname.Text = partNo;
                                tas_alertnotfound.Text = partNo;
                                timer_addstock.Enabled = true;
                            }
                        }
                    }
                    else
                    {
                        panel37.Visible = true;
                        tas_partname.Text = partNo;
                        tas_alertnotfound.Text = partNo;
                        timer_addstock.Enabled = true;
                    }

                }
                catch (Exception ab)
                {
                    MessageBox.Show(ab.Message);
                }
                finally
                {
                    reader.Close();
                    con.Close();
                }
            }
            if(select == 3)
            {
                cmd = new SqlCommand();
                cmd.Connection = con;
                int no = 0;
                int j = 0;
                string rank_en = "";
                string rank = "";
                bool boxIdCheck = true;
                bool part_check = false;
                string model = "";
                string part_name = "";
                string shelf = "";
                string zone = "";
                string floor = "";
                string side = "";
                string slot = "";
                string statusM = "";
                string statusDB = "INUSE";
                int stock_available = 0;
                int stock = 0;
                string boxID = sub_barcode[0].Substring(2,sub_barcode[0].Length-2);
                string MPN = sub_barcode[2];
                string maker = "";
                string qty = sub_barcode[3];
                string ccode = "";
                // string dcode = sub_barcode[4];
                string dcode = "";
                tas_scan.Text = partNo;
                string userIn = Login.userName;
                string TimeIn = DateTime.Now.ToString();
                string partcode = sub_barcode[0] + " " + sub_barcode[1] + " " + sub_barcode[2] + " " + sub_barcode[3];
                List<string> box_idCheck = new List<string>();
                List<string> boxID_dbCheck = new List<string>();
                int max = 0;
                int min = 0;

                try
                {
                    con.Open();
                    cmd.CommandText = "SELECT [box_id]FROM [THAIARROW_PART_STORE].[dbo].[Stock_part] where part_no='" + partNo + "'";
                    reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            boxID_dbCheck.Add(reader.GetValue(0).ToString());
                        }
                    }
                    reader.Close();
                    for (j = 0; j < dt_addstock.Rows.Count; j++)
                    {
                        if (boxID == dt_addstock.Rows[j].ItemArray[4].ToString() && partNo == dt_addstock.Rows[j].ItemArray[0].ToString())
                        {
                            boxIdCheck = false;
                        }
                    }
                    for (j = 0; j < boxID_dbCheck.Count; j++)
                    {
                        if (boxID == boxID_dbCheck[j].ToString() && boxIdCheck)
                        {
                            boxIdCheck = false;

                        }
                    }
                    cmd.CommandText = "SELECT part_no FROM [THAIARROW_PART_STORE].[dbo].[Master_part_data] where part_no ='" + partNo + "'";
                    reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        part_check = true;
                    }
                    reader.Close();
                    if (part_check)
                    {
                        cmd.CommandText = "SELECT a.model,a.part_no,a.part_name,b.shelf,b.zone,b.floor,b.side,b.slot,a.qty_pcs,a.rank_enable,a.max_stock,a.min_stock,a.stock_available  FROM [THAIARROW_PART_STORE].[dbo].[Master_part_data] AS a INNER JOIN [THAIARROW_PART_STORE].[dbo].[Master_location] AS b ON a.part_no = b.part_no_registered where a.part_no='" + partNo + "'";
                        reader = cmd.ExecuteReader();

                        if (reader.HasRows && boxIdCheck)
                        {
                         //   timer_addstock.Enabled = true;
                         //   tas_alertadd.Text = partNo;
                         //   panel52.Visible = true;
                            while (reader.Read())
                            {

                                model = reader.GetValue(0).ToString();
                                part_name = reader.GetValue(2).ToString();
                                //   rank = reader.GetValue(3).ToString();
                                shelf = reader.GetValue(3).ToString();
                                zone = reader.GetValue(4).ToString();
                                floor = reader.GetValue(5).ToString();
                                side = reader.GetValue(6).ToString();
                                slot = slot + "," + reader.GetValue(7).ToString();
                                stock = int.Parse(reader.GetValue(8).ToString());
                                rank_en = reader.GetValue(9).ToString();
                                max = int.Parse(reader.GetValue(10).ToString());
                                min = int.Parse(reader.GetValue(11).ToString());
                                if (stock > max) { statusDB = "OverStock"; }
                                else if (stock <= max && stock > min) { statusDB = "Normal"; }
                                else { statusDB = "Wanrning"; }
                                stock_available = reader.GetInt32(12);

                            }
                            if (dataGridView6.Rows.Count == 0) { count_addstock = 0; }
                            count_addstock++;
                             no += count_addstock;
          
                            slot = slot.Substring(1, slot.Length - 1);
                            dataGridView6.Rows.Add(no, model, rank, partNo, part_name, shelf, zone, floor, side, slot, statusDB, userIn, TimeIn, stock_available, partcode);
                            dt_addstock.Rows.Add(partNo, model, part_name, partcode, boxID, maker, shelf, slot, userIn, qty, MPN, floor, statusM, TimeIn, rank_en, dcode, zone, statusDB, stock, ccode, min, rank, max, side);

                            tas_partname.Text = part_name;
                            tas_boxID.Text = boxID;
                            tas_mpn.Text = MPN;
                            tas_qty.Text = qty;
                            tas_ccode.Text = ccode;
                            tas_dcode.Text = dcode;
                            reader.Close();
                            con.Close();
                            fill_addstockTextBox();

                            if (count_stock_over())
                            {
                                timer_addstock.Enabled = true;
                                tas_alertadd.Text = partNo;
                                panel52.Visible = true;
                            }
                            else
                            {
                                panel22.Visible = true;
                                tas_alertoverstock.Text = partNo;
                                timer_addstock.Enabled = true;
                            }
                        }
                        else if(!reader.HasRows)
                        {
                            reader.Close();
                            cmd.CommandText = "SELECT model,part_name,qty_pcs,rank_enable,max_stock,min_stock,stock_available FROM [THAIARROW_PART_STORE].[dbo].[Master_part_data] where part_no ='" + partNo + "'";
                            reader = cmd.ExecuteReader();
                            if (reader.HasRows && boxIdCheck)
                            {
                                while (reader.Read())
                                {
                                    model = reader.GetValue(0).ToString();
                                    part_name = reader.GetValue(1).ToString();
                                    shelf = "";
                                    zone = "";
                                    floor = "";
                                    side = "";
                                    slot = "";
                                    stock = int.Parse(reader.GetValue(2).ToString());
                                    rank_en = reader.GetValue(3).ToString();
                                    max = int.Parse(reader.GetValue(4).ToString());
                                    min = int.Parse(reader.GetValue(5).ToString());
                                    if (stock > max) { statusDB = "OverStock"; }
                                    else if(stock <= max && stock > min) { statusDB = "Normal"; }
                                    else { statusDB = "Wanrning"; }
                                    stock_available = reader.GetInt32(6);
                              
                                }
                                if(dataGridView6.Rows.Count == 0) { count_addstock = 0; }
                                 count_addstock++;
                                 no += count_addstock;
                                //if (dataGridView6.Rows.Count > 1) { no = int.Parse(dataGridView6.Rows[dataGridView6.Rows.Count-1].Cells[0].Value.ToString())+1 ; }
                                dataGridView6.Rows.Add(no, model, rank, partNo, part_name, shelf, zone, floor, side, slot, statusDB, userIn, TimeIn, stock_available, partcode);
                                dt_addstock.Rows.Add(partNo, model, part_name, partcode, boxID, maker, shelf, slot, userIn, qty, MPN, floor, statusM, TimeIn, rank_en, dcode, zone, statusDB, stock, ccode, min, rank, max, side);
                                tas_partname.Text = part_name;
                                tas_boxID.Text = boxID;
                                tas_mpn.Text = MPN;
                                tas_qty.Text = qty;
                                tas_ccode.Text = ccode;
                                tas_dcode.Text = dcode;
                                reader.Close();
                                con.Close();
                                fill_addstockTextBox();

                                if (count_stock_over())
                                {
                                    timer_addstock.Enabled = true;
                                    // tas_rank.Text = partNo;
                                    //tas_rank.TextAlign = HorizontalAlignment.Center;
                                    tas_alertadd.Text = partNo;
                                    panel52.Visible = true;
                                }
                                else
                                {
                                    panel22.Visible = true;
                                    tas_alertoverstock.Text = partNo;
                                    timer_addstock.Enabled = true;
                                }
                            }
                            else
                            {
                                if (!boxIdCheck)
                                {
                                    pas_duplicate.Visible = true;
                                    tas_alertboxid.Text = partNo;
                                    timer_addstock.Enabled = true;

                                }
                                else
                                {
                                    panel37.Visible = true;
                                    tas_partname.Text = partNo;
                                    tas_alertnotfound.Text = partNo;
                                    timer_addstock.Enabled = true;
                                }
                            }

                        }
                        else
                        {
                            if (!boxIdCheck)
                            {
                                pas_duplicate.Visible = true;
                                tas_alertboxid.Text = partNo;
                                timer_addstock.Enabled = true;

                            }
                            else
                            {
                                panel37.Visible = true;
                                tas_partname.Text = partNo;
                                tas_alertnotfound.Text = partNo;
                                timer_addstock.Enabled = true;
                            }
                        }
                    }
                    else
                    {
                        panel37.Visible = true;
                        tas_partname.Text = partNo;
                        tas_alertnotfound.Text = partNo;
                        timer_addstock.Enabled = true;
                    }

                }
                catch (Exception ab)
                {
                    MessageBox.Show(ab.Message);
                }
                finally
                {
                    reader.Close();
                    con.Close();
                }
            }
        }
        private void Timer_addstock_Tick(object sender, EventArgs e)
        {
            panel52.Visible = false;
            panel37.Visible = false;
            pas_duplicate.Visible = false;
            panel21.Visible = false;
            panel22.Visible = false;
            timer_addstock.Enabled = false;
        }
        private void DataGridView6_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            fill_addstockTextBox();
        }
        private void fill_addstockTextBox()
        {
            try
            {
                int index = dataGridView6.CurrentCell.RowIndex;

                tas_partname.Text = dt_addstock.Rows[index].ItemArray[2].ToString();
                tas_boxID.Text = dt_addstock.Rows[index].ItemArray[4].ToString();
                tas_maker.Text = dt_addstock.Rows[index].ItemArray[5].ToString();
                tas_area.Text = dt_addstock.Rows[index].ItemArray[6].ToString();
                tas_slot.Text = dt_addstock.Rows[index].ItemArray[7].ToString();
                tas_userIn.Text = dt_addstock.Rows[index].ItemArray[8].ToString();
                tas_qty.Text = dt_addstock.Rows[index].ItemArray[9].ToString();
                tas_mpn.Text = dt_addstock.Rows[index].ItemArray[10].ToString();
                tas_shelf.Text = dt_addstock.Rows[index].ItemArray[11].ToString();
                tas_statusM.Text = dt_addstock.Rows[index].ItemArray[12].ToString();
                tas_timeIn.Text = dt_addstock.Rows[index].ItemArray[13].ToString();
               // tas_rank.Text = dt_addstock.Rows[index].ItemArray[14].ToString();
                tas_dcode.Text = dt_addstock.Rows[index].ItemArray[15].ToString();
                tas_zone.Text = dt_addstock.Rows[index].ItemArray[16].ToString();
                tas_statusDB.Text = dt_addstock.Rows[index].ItemArray[17].ToString();
                tas_stock.Text = dt_addstock.Rows[index].ItemArray[18].ToString();
                tas_ccode.Text = dt_addstock.Rows[index].ItemArray[19].ToString();
                tas_spare.Text = dt_addstock.Rows[index].ItemArray[20].ToString();//minstock
                tas_maxstock.Text = dt_addstock.Rows[index].ItemArray[22].ToString();
                tas_side.Text = dt_addstock.Rows[index].ItemArray[23].ToString();

            }
            catch(Exception ab)
            {
                MessageBox.Show(ab.Message);
            }
        }
        private void Btas_cancel_Click(object sender, EventArgs e)
        {
            
            try
            {
                int rowIndex = dataGridView6.CurrentCell.RowIndex;
                if (rowIndex >= 0)
                {
                    dataGridView6.Rows.RemoveAt(rowIndex);
                    dt_addstock.Rows.RemoveAt(rowIndex);
                    
                }
                if(dataGridView6.Rows.Count ==0)
                {
                    dt_addstock.Rows.Clear();
                    dt_countstock.Rows.Clear();
                    dataGridView6.Rows.Clear();
                    tas_partname.Text = "";
                    tas_boxID.Text = "";
                    tas_maker.Text = "";
                    tas_area.Text = "";
                    tas_slot.Text = "";
                    tas_userIn.Text = "";
                    tas_qty.Text = "";
                    tas_mpn.Text = "";
                    tas_shelf.Text = "";
                    tas_statusM.Text = "";
                    tas_timeIn.Text = "";
                    tas_rank.Text = "";
                    tas_dcode.Text = "";
                    tas_zone.Text = "";
                    tas_statusDB.Text = "";
                    tas_stock.Text = "";
                    tas_ccode.Text = "";
                    tas_spare.Text = "";
                    tas_maxstock.Text = "";
                    tas_side.Text = "";
                }
            }
            catch (Exception ab)
            {
                MessageBox.Show(ab.Message);
            }
            finally
            {
              
            }
        }
        private void Btas_ok_Click(object sender, EventArgs e)
        {
            if (dt_addstock.Rows.Count > 0) { db_transaction(1); }
            else
            {
                MessageBox.Show("กรุณากรอกข้อมูลให้ถูกต้อง");
            }
            

        }
        private bool db_transaction(int a)
        {
            bool result = false;
            if (a == 1)
            {
                cmd = new SqlCommand();
                cmd.Connection = con;
                string transaction_type = "AddStock";
                string user = Login.userName;
                DateTime myEvent = DateTime.Now;
                try
                {
                    con.Open();
                    cmd.CommandText = "INSERT INTO [THAIARROW_PART_STORE].[dbo].[Transaction] (transaction_type,transaction_user,event_time) VALUES ('" + transaction_type + "','" +user +"','"+myEvent+"')";
                    cmd.ExecuteNonQuery();
                }
                catch (Exception ab)
                {
                    MessageBox.Show(ab.Message);
                }
                finally
                {
                    con.Close();
                    result = true;
                    if (db_stockorder(transaction_type, myEvent) && db_transaction_detail(1, myEvent))
                    {
                        if (count_stock() && update_dbpartData())
                        {
                            // MessageBox.Show("ทำการเพิ่มรายการ Stock เสร็จสมบูรณ์");

                            panel21.Visible = true;
                            tas_alert_addStockcom.Text = "COMPLETE!!!";
                            timer_addstock.Enabled = true;
                            dt_addstock.Rows.Clear();
                            dt_countstock.Rows.Clear();
                            dataGridView6.Rows.Clear();
                            tas_partname.Text = "";
                            tas_boxID.Text = "";
                            tas_maker.Text = "";
                            tas_area.Text = "";
                            tas_slot.Text = "";
                            tas_userIn.Text = "";
                            tas_qty.Text = "";
                            tas_mpn.Text = "";
                            tas_shelf.Text = "";
                            tas_statusM.Text = "";
                            tas_timeIn.Text = "";
                            tas_rank.Text = "";
                            tas_dcode.Text = "";
                            tas_zone.Text = "";
                            tas_statusDB.Text = "";
                            tas_stock.Text = "";
                            tas_ccode.Text = "";
                            tas_spare.Text = "";
                            tas_maxstock.Text = "";
                            tas_side.Text = "";

                        }
                        else
                        {
                            panel21.Visible = true;
                            panel21.BackColor = Color.Red;
                            tas_alert_addStockcom.Text = "NOT COMPLETE!!!";
                            timer_addstock.Enabled = true;
                        }
                    }
                    else
                    {
                        panel21.Visible = true;
                        panel21.BackColor = Color.Red;
                        tas_alert_addStockcom.Text = "NOT COMPLETE!!!";
                        timer_addstock.Enabled = true;

                        dt_addstock.Rows.Clear();
                        dt_countstock.Rows.Clear();
                        dataGridView6.Rows.Clear();
                        tas_partname.Text = "";
                        tas_boxID.Text = "";
                        tas_maker.Text = "";
                        tas_area.Text = "";
                        tas_slot.Text = "";
                        tas_userIn.Text = "";
                        tas_qty.Text = "";
                        tas_mpn.Text = "";
                        tas_shelf.Text = "";
                        tas_statusM.Text = "";
                        tas_timeIn.Text = "";
                        tas_rank.Text = "";
                        tas_dcode.Text = "";
                        tas_zone.Text = "";
                        tas_statusDB.Text = "";
                        tas_stock.Text = "";
                        tas_ccode.Text = "";
                        tas_spare.Text = "";
                        tas_maxstock.Text = "";
                        tas_side.Text = "";
                    }
                }
            }
            return result;
        }
        private bool db_transaction_detail(int a,DateTime myEvent)
        {
            bool result = false;
            if(a==1)
            {
                cmd = new SqlCommand();
                cmd.Connection = con;
                string transaction_type = "AddStock";
                string user = Login.userName;
                string transaction_id = "";
                string partNo = "";
                string boxID = "";
               
                try
                {
                    con.Open();
                    cmd.CommandText = "SELECT [transaction_id] FROM [THAIARROW_PART_STORE].[dbo].[Transaction] where transaction_type='" + transaction_type + "' AND event_time ='" + myEvent + "'";
                    reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            transaction_id = reader.GetValue(0).ToString();
                        }
                    }
                    reader.Close();
                    for (int i = 0; i < dt_addstock.Rows.Count; i++)
                    {
                        partNo = dt_addstock.Rows[i]["partno"].ToString();
                        boxID = dt_addstock.Rows[i]["boxID"].ToString();
                        cmd.CommandText = "INSERT INTO [THAIARROW_PART_STORE].[dbo].[Transaction_detail] (transaction_id,part_no,box_id) VALUES ('" + transaction_id + "','" + partNo + "','" + boxID + "')";
                        cmd.ExecuteNonQuery();
                    }
                    con.Close();
                    monitor_addstock(int.Parse(transaction_id));
                }
                catch (Exception ab)
                {
                    MessageBox.Show(ab.Message);
                }
                finally
                {
                    con.Close();
                    result = true;
                 
                }
            }
            return result;
        }
        private void monitor_addstock(int tranasction_id)
        {
            cmd = new SqlCommand();
            cmd.Connection = con;
            string eventtype = "AddStock";
            string user = Login.userName;
            try
            {
                con.Open();
                cmd.CommandText = "DELETE FROM [THAIARROW_PART_STORE].[dbo].[Monitor]";
                cmd.ExecuteNonQuery();
                cmd.CommandText = "INSERT INTO [THAIARROW_PART_STORE].[dbo].[Monitor] (transaction_id,event_type,username) VALUES('"+ tranasction_id+"','"+eventtype+"','"+user+"')";
                int b = cmd.ExecuteNonQuery();
                if(b==0)
                {
                    MessageBox.Show("เกิดข้อผิดพลาด AddStock ขณะส่งข้อมูลไป db monitor");
                }
            }
            catch(Exception ab)
            {
                MessageBox.Show(ab.Message);
            }
            finally
            {
                con.Close();
            }
        }
        private bool db_stockorder(string transaction_type,DateTime myEvent)
        {
            string boxID = "";
            string partNo = "";
            string rank = "";
            string fullcode = "";
            string first_scan_user = Login.userName;
            DateTime first_scan_date = myEvent;
            int qty = 0;
            string stock_lot = myEvent.ToString("yyyyMMddHHmmss");
            DateTime stockTime = myEvent;
            string user = Login.userName;
            string stock_status = "InStock";
            string transaction_id = "";
            bool result = false;
            cmd = new SqlCommand();
            cmd.Connection = con;
            try
            {
                if(dt_addstock.Rows.Count>0)
                {
                    con.Open();
                    cmd.CommandText = "SELECT [transaction_id] FROM [THAIARROW_PART_STORE].[dbo].[Transaction] where transaction_type='"+ transaction_type+ "' AND event_time ='"+ myEvent +"'";
                    reader = cmd.ExecuteReader();
                    if(reader.HasRows)
                    {
                        while(reader.Read())
                        {
                            transaction_id = reader.GetValue(0).ToString();
                        }
                    }
                    reader.Close();
                    for (int i = 0; i < dt_addstock.Rows.Count; i++)
                    {
                        boxID = dt_addstock.Rows[i]["boxID"].ToString();
                        partNo = dt_addstock.Rows[i]["partno"].ToString();
                        rank = dt_addstock.Rows[i].ItemArray[21].ToString();
                        fullcode = dt_addstock.Rows[i]["partcode"].ToString();
                        qty = int.Parse(dt_addstock.Rows[i]["qty"].ToString());
                        cmd.CommandText = "INSERT INTO [THAIARROW_PART_STORE].[dbo].[Stock_part] (box_id,part_no,rank,full_code,first_scan_user,first_scan_date,qty_pcs,stock_lot_id,stock_time,stock_transaction,stock_user,stock_status) VALUES ('" + boxID + "','" + partNo + "','" + rank + "','" + fullcode + "','" + first_scan_user + "','" + first_scan_date + "','" + qty + "','" + stock_lot + "','" + stockTime + "','" + transaction_id + "','" + user + "','" + stock_status + "')";
                        cmd.ExecuteNonQuery();
                    }
                    result = true;
                }
                else
                {
                    MessageBox.Show("กรุณาทำรายการให้ถูกต้อง");
                }
            }
            catch(Exception ab)
            {
                MessageBox.Show(ab.Message);
            }
            finally
            {
                con.Close();
            }
            return result;
        }
        private bool count_stock_over()
        {
            bool result = false;
            cmd = new SqlCommand();
            cmd.Connection = con;
            string part = "";
            int maxstock = 0;
            List<string> check_part = new List<string>();
            string part_key = dt_addstock.Rows[dt_addstock.Rows.Count - 1].ItemArray[0].ToString();
            dt_countstock.Rows.Clear();
            dt_countstock.Columns.Clear();
            dt_countstock.Columns.Add("part");
            dt_countstock.Columns.Add("sum");
            for (int q = 0; q < dt_addstock.Rows.Count; q++)
            {
                check_part.Add(dt_addstock.Rows[q]["partno"].ToString());
            }
            List<string> sub_part = check_part.Distinct().ToList();
            int[] sum = new int[sub_part.Count];
            try
            {
                con.Open();

                for (int i = 0; i < sub_part.Count; i++)
                {
                    for (int j = 0; j < dt_addstock.Rows.Count; j++)
                    {//sub_part[i] == dt_addstock
                        if (part_key == dt_addstock.Rows[j].ItemArray[0].ToString())
                        {
                            part = dt_addstock.Rows[j].ItemArray[0].ToString();
                            sum[i] += int.Parse(dt_addstock.Rows[j]["qty"].ToString());

                        }
                    }
                    cmd.CommandText = "SELECT [qty_pcs],[max_stock] FROM [THAIARROW_PART_STORE].[dbo].[Master_part_data] where part_no ='" + part + "'";
                    reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            int qty = int.Parse(reader.GetValue(0).ToString());
                            maxstock = int.Parse(reader.GetValue(1).ToString());
                            sum[i] += qty;
                        }
                        result = true;
                    }
                    if (sum[i] > maxstock)
                    {
                        result = false;
                    }
                    dt_countstock.Rows.Add(part, sum[i]);
                    reader.Close();
                }

            }
            catch (Exception ab)
            {
                MessageBox.Show(ab.Message);
            }
            finally
            {
                reader.Close();
                con.Close();
            }

            return result;
        }
        private bool count_stock()
        {
            bool result = false;
            cmd = new SqlCommand();
            cmd.Connection = con;
            string part = "";
            int maxstock = 0;
            List<string> check_part = new List<string>();
            dt_countstock.Rows.Clear();
            dt_countstock.Columns.Clear();
            dt_countstock.Columns.Add("part");
            dt_countstock.Columns.Add("sum");
            for (int q = 0; q < dt_addstock.Rows.Count; q++)
            {
                check_part.Add(dt_addstock.Rows[q]["partno"].ToString());
            }
            List<string> sub_part = check_part.Distinct().ToList();
            int[] sum = new int[sub_part.Count];
            try
            {
                con.Open();
                reader.Close();
                for(int i=0;i<sub_part.Count;i++)
                {
                    for(int j=0;j<dt_addstock.Rows.Count;j++)
                    {
                        if (sub_part[i] == dt_addstock.Rows[j].ItemArray[0].ToString())
                        {
                             part = dt_addstock.Rows[j].ItemArray[0].ToString();
                            sum[i] += int.Parse(dt_addstock.Rows[j]["qty"].ToString());
                            
                        }
                    }
                    cmd.CommandText = "SELECT [qty_pcs],[max_stock] FROM [THAIARROW_PART_STORE].[dbo].[Master_part_data] where part_no ='" + part + "'";
                    reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            int qty = int.Parse(reader.GetValue(0).ToString());
                           maxstock = int.Parse(reader.GetValue(1).ToString());
                            sum[i] += qty;
                        }
                      
                    }
                   
                    dt_countstock.Rows.Add(part, sum[i]);
                    result = true;
                }

            }
            catch(Exception ab)
            {
                MessageBox.Show(ab.Message);
            }
            finally
            {
                reader.Close();
                con.Close();
            }

            return result;
        }
        private bool update_dbpartData()
        {
            bool result = false;
            string user = Login.userName;
            string box_id = "";
            int count = 1;
            DateTime updateDate = DateTime.Now;
            cmd = new SqlCommand();
            cmd.Connection = con;
            try
            {                
                con.Open();
               
               
                for (int i = 0; i < dt_countstock.Rows.Count; i++)
                {
                    count = 0;
                    string part = dt_countstock.Rows[i].ItemArray[0].ToString();
                    cmd.CommandText = "SELECT box_id FROM [THAIARROW_PART_STORE].[dbo].[Stock_part] where part_no ='" + part + "'AND pickup_time IS NULL AND pickup_transaction IS NULL";
                    reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            box_id = reader.GetValue(0).ToString();
                            count++;
                        }
                    }
                    else { count = 1; }
                    reader.Close();
                    int qty = int.Parse(dt_countstock.Rows[i].ItemArray[1].ToString());
                    cmd.CommandText = "UPDATE [THAIARROW_PART_STORE].[dbo].[Master_part_data] SET qty_pcs ='" + qty + "', update_user ='"+user+"',update_date ='"+updateDate+"' , stock_available = '"+count+"' where part_no ='" + part + "'";
                    int b = cmd.ExecuteNonQuery();
                    if (b > 0)
                    {
                        result = true;
                    }
                }
                
            }
            catch(Exception ab)
            {
                MessageBox.Show(ab.Message);
            }
            finally
            {
                con.Close();
            }

            return result;
        }

        #endregion

        //region pickup stock
        #region
        private void writeto_monitor_before_pickup()
        {
            cmd = new SqlCommand();
            cmd.Connection = con;
            string username = Login.userName;
            string type = "PickupStock";

            try
            {

                con.Open();

                // int transaction_id = int.Parse(dt_transaction_pickup.Rows[0].ItemArray[0].ToString());


                cmd.CommandText = "DELETE FROM [THAIARROW_PART_STORE].[dbo].[Monitor]";
                int a = cmd.ExecuteNonQuery();
                //  if(a==0)
                //  {
                //      MessageBox.Show("เกิดข้อผิดพลาดในการเตรียมการส่งข้อมูลไป db monitor");
                //   }
                cmd.CommandText = "INSERT INTO [THAIARROW_PART_STORE].[dbo].[Monitor] (event_type,username) VALUES ('" + type + "','" + username + "')";
                int b = cmd.ExecuteNonQuery();
                if (b == 0)
                {
                    MessageBox.Show("เกิดข้อผิดพลาดขณะส่งข้อมูลไป db monitor");
                }

            }
            catch (Exception ab)
            {
                MessageBox.Show(ab.Message);
            }
            finally
            {
                con.Close();
            }
        }
        private void fill_tablepickup()
        {
            dataGridView7.Rows.Clear();
            string user = Login.userName;
            cmd = new SqlCommand();
            cmd.Connection = con;
            string mcline = "";
            int no = 0;
            // DateTime order_time = new DateTime();
            string order_time = "";
            string partNo = "";
            string model = "";
            string fifo_ena = "";
            string rank_ena = "";
            string rank = "";
            int min_stock = 0;
            int quantity = 0;
            int qty_pcs = 0;
            string box_id = "";
            string stock_lot_id = "";
          

            string shelf = "";
            string zone = "";
            string floor = "";
            string side = "";
            string slot = "";
            bool check = true;
            string timee_in = "";

            int maxstock = 0;
            string supplier = "";
            List<string> mc_line = new List<string>();
            List<string> list_rank = new List<string>();
            List<string> part_no = new List<string>();
            List<string> part_check = new List<string>();
            List<string> orderTime = new List<string>();
            List<string> quant = new List<string>();
            List<string> order_status = new List<string>();
            List<string> pickup_box = new List<string>();
            //int[] quant = new int[5];
            // List<string> boxiid = new List<string>();
            int j = 0;
            int k = 0;
            List<Order_event> orderdata = new List<Order_event>();
            dt_partbox.Rows.Clear();
            dt_partbox.Columns.Clear();
            dt_partbox.Columns.Add("part");
            dt_partbox.Columns.Add("box");
            dt_partbox.Columns.Add("order_status");
            try
            {
                con.Open();
                cmd.CommandText = "SELECT a.part_no,a.mcline,a.rank,b.FIFO_enable,a.order_time,a.quantity,a.order_status,a.pickup_box_id FROM [THAIARROW_PART_STORE].[dbo].[Order_event] AS a INNER JOIN [THAIARROW_PART_STORE].[dbo].[Master_part_data] AS b ON a.part_no=b.part_no where a.order_user ='" + user + "' AND (a.transaction_id IS NULL OR a.order_status ='Pickup') ORDER BY a.order_time ASC";
                reader = cmd.ExecuteReader();
                if(reader.HasRows)
                {
                    while(reader.Read())
                    {
                      //  Order_event order = new Order_event();
                      //  order.part_no = reader.GetString(0);
                        //order.mcline = reader.GetString(1);
                       // orderdata.Add(order);

                        // if (reader.GetValue(1).ToString() == "True")
                        //  {
                        //     part_fifo.Add(reader.GetValue(0).ToString());
                        //      partNo = reader.GetValue(0).ToString();
                        //      dt_pickup.Rows.Add(partNo);
                        // }
                        // else
                        // {
                        //   partNo = reader.GetValue(0).ToString();
                        //    part_nfifo.Add(partNo);
                        //    dt_pickup.Rows.Add(partNo);
                        // }
                        part_no.Add(reader.GetValue(0).ToString());
                        mc_line.Add(reader.GetValue(1).ToString());
                        list_rank.Add(reader.GetValue(2).ToString());
                        orderTime.Add(reader.GetDateTime(4).ToString("yyyy-MM-dd HH:mm:ss.fff"));
                        quant.Add(reader.GetValue(5).ToString());
                        order_status.Add(reader.GetValue(6).ToString());
                        pickup_box.Add(reader.GetValue(7).ToString());
                        j++;

                    }
                }
                reader.Close();
                con.Close();
              
            
                for (int i = 0; i < part_no.Count; i++)
                {
                    partboxid_check = true;
                    part_box(part_no[i],i, order_status[i]);
                    con.Open();
                    cmd.CommandText = "SELECT b.model,b.FIFO_enable,b.rank_enable,b.min_stock,b.stock_available,c.box_id,c.stock_lot_id,b.supplier,b.max_stock FROM [THAIARROW_PART_STORE].[dbo].[Master_part_data] AS b INNER JOIN [THAIARROW_PART_STORE].[dbo].[Stock_part] AS c ON b.part_no = c.part_no  where b.part_no ='" + dt_partbox.Rows[i].ItemArray[0].ToString() + "'AND c.box_id ='"+dt_partbox.Rows[i].ItemArray[1].ToString() +"' AND pickup_transaction IS NULL ORDER BY stock_lot_id ASC";
                    reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                          while (reader.Read() && check)
                         {
                       // reader.Read();
                                // mcline = reader.GetValue(1).ToString();
                                mcline = mc_line[i].ToString();
                                model = reader.GetValue(0).ToString();
                                fifo_ena = reader.GetValue(1).ToString();
                                rank_ena = reader.GetValue(2).ToString();
                                // rank = reader.GetValue(5).ToString();
                                rank = list_rank[i].ToString();
                                min_stock = int.Parse(reader.GetValue(3).ToString());
                                quantity = int.Parse(quant[i]);
                                qty_pcs = int.Parse(reader.GetValue(4).ToString());
                                box_id = reader.GetValue(5).ToString();
                                stock_lot_id = reader.GetValue(6).ToString();
                                order_time = orderTime[i];
                              //  int millisec = order_time.Millisecond;
                                supplier = reader.GetValue(7).ToString();
                                maxstock = int.Parse(reader.GetValue(8).ToString());
                                check = false;
                                no++;
                            
                        }

                    }
                    else
                    {
                        no++;
                        check = false;
                         box_id = "";
                       stock_lot_id = "";
                    }
                    reader.Close();
                    cmd.CommandText = "SELECT a.shelf,a.zone,a.floor,a.side,a.slot FROM [THAIARROW_PART_STORE].[dbo].[Master_location] AS a where part_no_registered ='"+part_no[i].ToString()+"'";
                    reader = cmd.ExecuteReader();
                    if(reader.HasRows)
                    {
                        while(reader.Read() && !check)
                        {
                            shelf = reader.GetValue(0).ToString();
                            zone = reader.GetValue(1).ToString();
                            floor = reader.GetValue(2).ToString();
                            side = reader.GetValue(3).ToString();
                            slot = slot +","+reader.GetValue(4).ToString();
                           
                        }
                        slot = slot.Substring(1, slot.Length - 1);
                        //  DateTime time_order = Convert.ToDateTime(order_time);
             
                    }
                    DateTime timeee = Convert.ToDateTime(order_time);
                    timee_in = timeee.ToString("dd-MM-yyyy HH:mm:ss.fff");
                    // string timeee_ = order_time.ToString("yyyy-MM-dd HH:mm:ss.fff");
                    //  string time_event_order = time_order.ToString("dd-MM-yyyy HH:MM:ss.fff");
                    reader.Close();
                    con.Close();
                    if (order_status[i] == "") { order_status[i] = "InStock"; stock_lot_id = update_lot_id(box_id); }
                    if (order_status[i] == "Pickup")
                    {
                       // dt_pickupCut.Rows.Add(i,"", mcline,part_no[i].ToString());
                        box_id = pickup_box[i];
                        stock_lot_id = update_lot_id(box_id);
                        dt_pickupCut.Rows.Add(i, box_id, mcline, part_no[i].ToString(), order_time, qty_pcs, timeee);
                        order_status[i] = "PickupStock";
                    }
                    dataGridView7.Rows.Add(no, part_no[i].ToString(), mcline, model, fifo_ena, rank_ena, rank, shelf, zone, floor, side, slot, min_stock, quantity, qty_pcs, box_id, stock_lot_id, timee_in, order_status[i]);
                    dt_pickupGrid.Rows.Add(part_no[i].ToString(), mcline, model, fifo_ena, rank_ena, rank, shelf, zone, floor, slot, min_stock, quantity, qty_pcs, box_id, stock_lot_id, order_time, supplier, maxstock, side);
                    shelf = "";
                    zone = "";
                    floor = "";
                    side = "";
                    slot = "";
                    check = true;
                    //boxiid.Clear();
                }

            }
            catch(Exception ab)
            {
                MessageBox.Show(ab.Message);
            }
            finally
            {
                reader.Close();
                con.Close();
            }
        }
        private string update_lot_id(string box_id)
        {
            cmd = new SqlCommand();
            cmd.Connection = con;
            string lot_id = "";
            string user = Login.userName;
           
            try
            {
                con.Open();
                cmd.CommandText = "SELECT [stock_lot_id] FROM [THAIARROW_PART_STORE].[dbo].[Stock_part] where box_id ='"+box_id+"'";
                reader = cmd.ExecuteReader();
                if(reader.HasRows)
                {
                    while (reader.Read())
                    {
                        lot_id = reader.GetValue(0).ToString();
                    }
                }
                reader.Close();
                
            }
            catch(Exception ab)
            {
                MessageBox.Show(ab.Message);
            }
            finally
            {
                con.Close();
            }
            return lot_id;
        }
        private string part_box (string part,int i,string order_status)
        {
           // string partNo = "";
            string box = "";
            cmd = new SqlCommand();
            cmd.Connection = con;
            bool case_a = true;
            List<string> boxiid = new List<string>();
            List<string> part_no = new List<string>();
            part_no.Add(part);
            int num = 0;
            int count_pick = 0;
            bool check = true;
            
            for(int k = 0; k < dt_partbox.Rows.Count; k++)
            {
                boxiid.Add(dt_partbox.Rows[k].ItemArray[1].ToString());
            }
            for(int k = 0; k < dt_partbox.Rows.Count; k++)
            {
                if(part == dt_partbox.Rows[k].ItemArray[0].ToString())
                {
                    case_a = false;
                    part_no.Add(dt_partbox.Rows[k].ItemArray[0].ToString());
                }
                if(part != dt_partbox.Rows[k].ItemArray[0].ToString())
                {
                    case_a = true;
                    part_no.Add(dt_partbox.Rows[k].ItemArray[0].ToString());
                }
            }
            for(int k = 0; k<dt_partbox.Rows.Count;k++)
            {            
                if(part == dt_partbox.Rows[k].ItemArray[0].ToString() )
                {
                    num++;
                }
                if(part == dt_partbox.Rows[k].ItemArray[0].ToString() && ("Pickup" == dt_partbox.Rows[k].ItemArray[2].ToString() || "PickupStock" == dt_partbox.Rows[k].ItemArray[2].ToString()))
                {
                    count_pick++;
                }
            }
            int e = 0;
            num -= count_pick;
            try
            {
                con.Close();
                con.Open();
                cmd.CommandText = "SELECT box_id,part_no FROM [THAIARROW_PART_STORE].[dbo].[Stock_part] where part_no ='" + part + "' AND pickup_transaction IS NULL ORDER BY stock_lot_id ASC";
                     reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {

                        // if (reader.GetString(1) == part && partboxid_check && case_a)
                        //    {
                        //       boxiid.Add(reader.GetValue(0).ToString());
                        //       box = reader.GetString(0);
                        //      partboxid_check = false;

                        //    }
                        //   if (reader.GetString(1) == part && partboxid_check && !case_a)
                        //    {
                        //       for (int j = 0; j < boxiid.Count; j++)
                        //      {
                        //        if (reader.GetString(0) != boxiid[j] && partboxid_check)
                        //       {
                        //            box = reader.GetString(0);
                        //           partboxid_check = false;
                        //      }
                        //   }
                        //  }
                        
                        if (e==num)
                        {
                            box = reader.GetValue(0).ToString();
                        }
                        e++;
                    }
                }
              if(num>=e)
                {
                    box = "not avalilable";
                }
                reader.Close();
                con.Close();
            }
            catch (Exception ab)
            {
                MessageBox.Show(ab.Message);
            }
            finally
            {
                reader.Close();
                con.Close();
                dt_partbox.Rows.Add(part, box , order_status);
            }

            return box;
        }
        private void detail_pickupStock ()
        {
            int index = dataGridView7.CurrentCell.RowIndex;
            //tp_partname.Text
        }
        private void tp_scan_KeyDown(object sender, KeyEventArgs e)
        {
            e.SuppressKeyPress = false;
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true; // for close sound whrn enter text box 
                string separator = tp_scan.Text.ToString().Substring(0, 2);
                if (separator == "<B")
                {
                    try
                    {

                        string[] barcode = tp_scan.Text.Replace("<B1>", "").Split(' ');
                        //string[] sub_barcode = new string[barcode.Length];
                        List<string> sub_barcode = new List<string>();
                        // int j = 0;

                        for (int i = 0; i < barcode.Length; i++)
                        {

                            if (barcode[i] != "")
                            {
                                sub_barcode.Add(barcode[i]);
                                // j++;
                            }

                        }
                        string partNo = sub_barcode[1];
                        tas_partname.Clear();
                        tas_rank.Clear();


                        if (sub_barcode.Count > 5) { cut_pickupstock(1, partNo, barcode, sub_barcode); }
                        if (sub_barcode.Count == 5) { cut_pickupstock(2, partNo, barcode, sub_barcode); }
                    }
                    catch (Exception ab)
                    {
                        MessageBox.Show(ab.Message);
                    }
                }
                if (separator == "IP")
                {
                    try
                    {

                        string[] barcode = tp_scan.Text.Split(' ');
                        //string[] sub_barcode = new string[barcode.Length];
                        List<string> sub_barcode = new List<string>();
                        // int j = 0;

                        for (int i = 0; i < barcode.Length; i++)
                        {

                            if (barcode[i] != "")
                            {
                                sub_barcode.Add(barcode[i]);
                                //  j++;
                            }

                        }
                        string partNo = sub_barcode[1].Substring(6, sub_barcode[1].Length - 6);
                        tas_partname.Clear();
                        tas_rank.Clear();
                        //    e.SuppressKeyPress = true;  // for close sound whrn enter text box 

                        cut_pickupstock(3, partNo, barcode, sub_barcode);
                    }
                    catch (Exception ab)
                    {
                        MessageBox.Show(ab.Message);
                    }
                }
            }
        }
        private void cut_pickupstock(int select, string partNo, string[] barcode, List<string> sub_barcode)
        {
            if (select == 1)
            {
                cmd = new SqlCommand();
                cmd.Connection = con;
                int no = 0;
                int j = 0;
                string rank = sub_barcode[5];
                string rank_en = "";
                bool boxIdCheck = false;
                bool checkRank = false;
                bool pick_box = true;
                bool check_box_grid = false;
                bool boxnotmatch_butlotok = false;
                bool box_nogridview = false;
                string lot = "";
                string box_lot = "";
                //  bool rank = false;
                string model = "";
                string part_name = "";
                string shelf = "";
                string zone = "";
                string floor = "";
                string side = "";
                string slot = "";
                string statusM = "";
                string statusDB = "INUSE";
                int stock = 0;
                string boxID = sub_barcode[0];
                string MPN = sub_barcode[2];
                string maker = sub_barcode[3];
                string qty = sub_barcode[4];
                string ccode = sub_barcode[5];
                string dcode = sub_barcode[6];
                tas_scan.Text = partNo;
                string userIn = Login.userName;
                string TimeIn = DateTime.Now.ToString();
                string partcode = sub_barcode[0] + " " + sub_barcode[1] + " " + sub_barcode[2] + " " + sub_barcode[3] + " " + sub_barcode[4] + " " + sub_barcode[5] + " " + sub_barcode[6];
                List<string> box_idCheck = new List<string>();
                List<string> boxID_dbCheck = new List<string>();
                int max = 0;
                int min = 0;
                bool check_part = false;
                bool check_lot = false;
                bool selected = true;
                dt_pickup.Rows.Clear();
                dt_pickup.Rows.Clear();
                try
                {
                    if (dataGridView7.Rows.Count > 0)
                    {
                        con.Open();
                        for (int k = 0; k < dt_pickupGrid.Rows.Count; k++)
                        {
                            if (partNo == dt_pickupGrid.Rows[k][0].ToString() && !check_part)
                            {
                                check_part = true;
                            }
                        }
                        if (check_part)
                        {
                            cmd.CommandText = "SELECT a.box_id,a.rank,a.stock_lot_id,a.pickup_line,b.FIFO_enable,b.rank_enable,a.part_no,a.stock_lot_id FROM [THAIARROW_PART_STORE].[dbo].[Stock_part] AS a INNER JOIN [THAIARROW_PART_STORE].[dbo].[Master_part_data] AS b ON a.part_no = b.part_no where pickup_transaction IS NULL AND a.part_no ='" + partNo + "'AND pickup_transaction IS NULL";
                            reader = cmd.ExecuteReader();
                            if (reader.HasRows)
                            {
                                while (reader.Read())
                                {
                                    boxID_dbCheck.Add(reader.GetValue(0).ToString());
                                    //box_idCheck.Add(reader.GetValue(4).ToString(), reader.GetValue(5).ToString());
                                    dt_pickup.Rows.Add(reader.GetValue(6).ToString(), reader.GetValue(0).ToString(), reader.GetValue(4).ToString(), reader.GetValue(5).ToString(),reader.GetValue(7).ToString());
                                }
                            }
                            reader.Close();
                            //check boxid 
                            for (j = 0; j < dt_pickupGrid.Rows.Count; j++)
                            {
                                if (boxID == dt_pickupGrid.Rows[j].ItemArray[13].ToString() && partNo == dt_pickupGrid.Rows[j].ItemArray[0].ToString())
                                {
                                    boxIdCheck = true;
                                    check_box_grid = true;
                                    box_nogridview = true;

                                }
                            }
                            for (j = 0; j < boxID_dbCheck.Count; j++)
                            {
                                if (boxID == boxID_dbCheck[j].ToString() && !boxIdCheck)
                                {
                                    boxIdCheck = true;

                                }
                            }

                            if (dt_pickup.Rows[0].ItemArray[2].ToString() == "True" && dt_pickup.Rows[0].ItemArray[3].ToString() == "True")
                            {
                                for (j = 0; j < dt_pickupGrid.Rows.Count; j++)
                                {
                                    if (boxID == dt_pickupGrid.Rows[j].ItemArray[13].ToString()  && rank == dt_pickupGrid.Rows[j].ItemArray[5].ToString() && selected)
                                    {
                                        //boxid and rank match
                                        check_lot = true;
                                        selected = false;

                                    }
                                    if (boxID == dt_pickupGrid.Rows[j].ItemArray[13].ToString() && rank != dt_pickupGrid.Rows[j].ItemArray[5].ToString() && selected)
                                    {
                                        //alert rank wrong
                                        panel26.Visible = true;
                                        label77.Text = "Rank";
                                        tp_alertwrong.Text = rank;
                                        label75.Text = "Not Match";
                                        label76.Text = "หากนำไปใช้มีโอกาสผิดพลาด";
                                        timer_pickupstock.Enabled = true;
                                        checkRank = true;
                                        check_lot = true;
                                        selected = false;

                                    }
                                //    if (boxID != dt_pickupGrid.Rows[j].ItemArray[13].ToString() && rank == dt_pickupGrid.Rows[j].ItemArray[5].ToString() && selected)
                                //    {
                                        //alert fifo enable you must choost sort by lot id please select corect boxid
                                    //    check_lot = false;
                                   //     selected = false;
                                //    }
                                    // box not match but lot okay and rank okay
                                    if (boxID != dt_pickupGrid.Rows[j].ItemArray[13].ToString()  && partNo == dt_pickupGrid.Rows[j].ItemArray[0].ToString() && selected && !box_nogridview)
                                    {
                                        
                                            string lot_in = dt_pickupGrid.Rows[j].ItemArray[14].ToString();
                                            for (int c = 0; c < dt_pickup.Rows.Count; c++)
                                            {
                                                if (lot_in == dt_pickup.Rows[c].ItemArray[4].ToString() && boxID == boxID_dbCheck[c])
                                                {
                                                    check_lot = true;
                                                    selected = false;
                                                    boxnotmatch_butlotok = true;
                                                    box_lot = boxID_dbCheck[c];
                                                    lot = lot_in;
                                                }
                                            }
                                        if (rank != dt_pickupGrid.Rows[j].ItemArray[5].ToString() && !selected)
                                        {
                                            //alert rank wrong
                                            panel26.Visible = true;
                                            label77.Text = "Rank";
                                            tp_alertwrong.Text = rank;
                                            label75.Text = "Not Match";
                                            label76.Text = "หากนำไปใช้มีโอกาสผิดพลาด";
                                            timer_pickupstock.Enabled = true;
                                            checkRank = true;
                                       //     selected = false;
                                        }                                    
                                    }
                                    
                                }
                            }
                            if (dt_pickup.Rows[0].ItemArray[2].ToString() == "True" && dt_pickup.Rows[0].ItemArray[3].ToString() == "False")
                            {
                                for (j = 0; j < dt_pickupGrid.Rows.Count; j++)
                                {
                                    if (boxID == dt_pickupGrid.Rows[j][13].ToString())
                                    {
                                        check_lot = true;
                                    }
                                    if (boxID != dt_pickupGrid.Rows[j].ItemArray[13].ToString() && partNo == dt_pickupGrid.Rows[j].ItemArray[0].ToString() && selected && !box_nogridview)
                                    {

                                        string lot_in = dt_pickupGrid.Rows[j].ItemArray[14].ToString();
                                        for (int c = 0; c < dt_pickup.Rows.Count; c++)
                                        {
                                            if (lot_in == dt_pickup.Rows[c].ItemArray[4].ToString() && boxID == boxID_dbCheck[c])
                                            {
                                                check_lot = true;
                                                selected = false;
                                                boxnotmatch_butlotok = true;
                                                box_lot = boxID_dbCheck[c];
                                                lot = lot_in;
                                            }
                                        }
                                       
                                    }

                                }
                            }
                            if (dt_pickup.Rows[0].ItemArray[2].ToString() == "False" && dt_pickup.Rows[0].ItemArray[3].ToString() == "True")
                            {
                                for (j = 0; j < dt_pickupGrid.Rows.Count; j++)
                                {
                                    if (rank == dt_pickupGrid.Rows[j].ItemArray[4].ToString() && partNo == dt_pickupGrid.Rows[j].ItemArray[0].ToString() && !checkRank)
                                    {
                                        check_lot = true;
                                        checkRank = true;
                                    }
                                    if (rank != dt_pickupGrid.Rows[j].ItemArray[4].ToString() && partNo == dt_pickupGrid.Rows[j].ItemArray[0].ToString() && !checkRank)
                                    {
                                        //alert rank enable rank wrong
                                        panel26.Visible = true;
                                        label77.Text = "Rank";
                                        tp_alertwrong.Text = rank;
                                        label75.Text = "Not Match";
                                        label76.Text = "หากนำไปใช้มีโอกาสผิดพลาด";
                                        timer_pickupstock.Enabled = true;
                                        checkRank = true;
                                        check_lot = true;
                                    }
                                }
                            }
                            if (dt_pickup.Rows[0].ItemArray[2].ToString() == "False" && dt_pickup.Rows[0].ItemArray[3].ToString() == "False")
                            {
                                check_lot = true;
                                //freedom to select
                            }

                            if (check_lot && boxIdCheck)
                            {
                                for (j = 0; j < dataGridView7.Rows.Count; j++)
                                {
                                    if (boxID == dt_pickupGrid.Rows[j].ItemArray[13].ToString() && partNo == dt_pickupGrid.Rows[j].ItemArray[0].ToString() && dt_pickup.Rows[0].ItemArray[2].ToString() == "True")
                                    {
                                        int rowIndex = j;
                                        try
                                        {
                                            if (rowIndex >= 0)
                                            {
                                                string mcline = dataGridView7.Rows[rowIndex].Cells[2].Value.ToString();
                                                string order_time = dt_pickupGrid.Rows[rowIndex].ItemArray[15].ToString();
                                                DateTime pickupTime = DateTime.Now;
                                                dt_pickupCut.Rows.Add(rowIndex, boxID, mcline, partNo, order_time, qty, pickupTime);
                                                con.Close();
                                                check_pickup_scan(partNo, boxID, mcline, rank, order_time, qty, pickupTime);
                                                //  dataGridView7.Rows.RemoveAt(rowIndex);
                                                dataGridView7.Rows[rowIndex].Cells[18].Value = "PickupStock";
                                             //   dt_pickupGrid.Rows.RemoveAt(rowIndex);

                                            }
                                        }
                                        catch (Exception ab)
                                        {
                                            MessageBox.Show(ab.Message);
                                        }
                                        finally
                                        {
                                            con.Close();
                                           
                                        }

                                    }
                                    //box not match but lot okay
                                    else if(boxID != dt_pickupGrid.Rows[j].ItemArray[13].ToString() && dt_pickup.Rows[0].ItemArray[2].ToString() == "True" && boxnotmatch_butlotok && lot == dt_pickupGrid.Rows[j].ItemArray[14].ToString() && partNo == dt_pickupGrid.Rows[j].ItemArray[0].ToString())
                                    {
                                        int rowIndex = j;
                                        try
                                        {
                                            if (rowIndex >= 0)
                                            {
                                                string mcline = dataGridView7.Rows[rowIndex].Cells[2].Value.ToString();
                                                string order_time = dt_pickupGrid.Rows[rowIndex].ItemArray[15].ToString();
                                                DateTime pickupTime = DateTime.Now;
                                                dt_pickupCut.Rows.Add(rowIndex, boxID, mcline, partNo, order_time, qty, pickupTime);
                                                con.Close();
                                                check_pickup_scan(partNo, box_lot, mcline, rank, order_time, qty, pickupTime);
                                                //  dataGridView7.Rows.RemoveAt(rowIndex);
                                                dataGridView7.Rows[rowIndex].Cells[18].Value = "PickupStock";
                                                //   dt_pickupGrid.Rows.RemoveAt(rowIndex);
                                                dataGridView7.Rows[rowIndex].Cells[15].Value = box_lot;

                                            }
                                        }
                                        catch (Exception ab)
                                        {
                                            MessageBox.Show(ab.Message);
                                        }
                                        finally
                                        {
                                            con.Close();
                                            boxnotmatch_butlotok = false;
                                        }
                                    }
                                    else if (dt_pickup.Rows[0].ItemArray[2].ToString() == "False")
                                    { //part fifo not enable

                                        if (boxID == dt_pickupGrid.Rows[j].ItemArray[13].ToString() && partNo == dt_pickupGrid.Rows[j].ItemArray[0].ToString() && check_box_grid)
                                        {
                                            int rowIndex = j;

                                            try
                                            {
                                                if (rowIndex >= 0)
                                                {

                                                    string mcline = dataGridView7.Rows[rowIndex].Cells[2].Value.ToString();
                                                    string order_time = dt_pickupGrid.Rows[rowIndex].ItemArray[15].ToString();
                                                    DateTime pickupTime = DateTime.Now;
                                                    dt_pickupCut.Rows.Add(rowIndex, boxID, mcline, partNo, order_time, qty, pickupTime);
                                                    con.Close();
                                                    check_pickup_scan(partNo, boxID, mcline, rank, order_time, qty, pickupTime);
                                                    //  dataGridView7.Rows.RemoveAt(rowIndex);
                                                    dataGridView7.Rows[rowIndex].Cells[18].Value = "PickupStock";
                                                    //  dt_pickupGrid.Rows.RemoveAt(rowIndex);

                                                }
                                            }
                                            catch (Exception ab)
                                            {
                                                MessageBox.Show(ab.Message);
                                            }
                                            finally
                                            {
                                                con.Close();
                                              //  pick_box = false;

                                            }

                                        }
                                        for (int p = 0; p < boxID_dbCheck.Count; p++)
                                        {
                                            if (boxID == boxID_dbCheck[p] && partNo == dt_pickupGrid.Rows[j].ItemArray[0].ToString() && pick_box && dataGridView7.Rows[j].Cells[18].Value.ToString() != "PickupStock" && !check_box_grid)
                                            {
                                                int rowIndex = j;

                                                try
                                                {
                                                    if (rowIndex >= 0)
                                                    {

                                                        string mcline = dataGridView7.Rows[rowIndex].Cells[2].Value.ToString();
                                                        string order_time = dt_pickupGrid.Rows[rowIndex].ItemArray[15].ToString();
                                                        DateTime pickupTime = DateTime.Now;
                                                        dt_pickupCut.Rows.Add(rowIndex, boxID, mcline, partNo, order_time, qty, pickupTime);
                                                        con.Close();
                                                        check_pickup_scan(partNo, boxID, mcline, rank, order_time, qty, pickupTime);
                                                        //  dataGridView7.Rows.RemoveAt(rowIndex);
                                                        dataGridView7.Rows[rowIndex].Cells[18].Value = "PickupStock";
                                                        dataGridView7.Rows[rowIndex].Cells[15].Value = boxID;
                                                        dataGridView7.Rows[rowIndex].Cells[16].Value = dt_pickup.Rows[p].ItemArray[4].ToString();
                                                        //  dt_pickupGrid.Rows.RemoveAt(rowIndex);

                                                    }
                                                }
                                                catch (Exception ab)
                                                {
                                                    MessageBox.Show(ab.Message);
                                                }
                                                finally
                                                {
                                                    con.Close();
                                                }
                                                pick_box = false;
                                            }
                                        }
                                    }
                                }

                            }
                            else
                            {
                                if (!boxIdCheck && check_lot)
                                {
                                    //has been take out
                                    panel26.Visible = true;
                                    label77.Text = "BOXID";
                                    tp_alertwrong.Text = boxID;
                                    label75.Text = "Not Found";
                                    label76.Text = "";

                                    timer_pickupstock.Enabled = true;


                                }
                                if (!check_lot && boxIdCheck && !checkRank)
                                {
                                    //alert fifo enable and box id not found please recheck boxid in store management
                                    pap_lotidwrong.Visible = true;
                                    timer_pickupstock.Enabled = true;
                                }
                                if (!check_lot && !boxIdCheck)
                                {
                                    //alert not found please recheck boxid in store management
                                    panel26.Visible = true;
                                    label77.Text = "BOXID";
                                    tp_alertwrong.Text = boxID;
                                    label75.Text = "Not Found";
                                    label76.Text = "";

                                    timer_pickupstock.Enabled = true;

                                }

                            }
                        }
                        else
                        {
                            //incorect part
                            panel26.Visible = true;
                            label77.Text = "Part ";
                            tp_alertwrong.Text = partNo;
                            label75.Text = "Wrong parts";
                            label76.Text = "Please choose the corect parts!!";
                            timer_pickupstock.Enabled = true;
                        }
                    }
                    else
                    {
                        panel25.Visible = true;
                        tp_alertwrong.Text = partNo;
                        timer_pickupstock.Enabled = true;
                    }


                }
                catch (Exception ab)
                {
                    MessageBox.Show(ab.Message);
                }
                finally
                {
                    reader.Close();
                    con.Close();

                }

            }
            if (select == 2)
            {
                cmd = new SqlCommand();
                cmd.Connection = con;
                int no = 0;
                string rank = "";
                string rank_en = "";
                bool boxIdCheck = false;
                bool check_part = false;
                bool check_lot = false;
                bool checkRank = false;
                bool pick_box = true;
                bool check_box_grid = false;
                bool boxnotmatch_butlotok = false;
                bool box_nogridview = false;
                
                string lot = "";
                string box_lot = "";
                //  bool rank = false;
                string model = "";
                string part_name = "";
                string shelf = "";
                string zone = "";
                string floor = "";
                string side = "";
                string slot = "";
                string statusM = "";
                string statusDB = "INUSE";
                int stock = 0;
                string boxID = sub_barcode[0];
                string MPN = sub_barcode[2];
                string maker = sub_barcode[3];
                string qty = sub_barcode[4].Substring(8, sub_barcode[4].Length - 8);
                string ccode = "";
                string dcode = "";
                tas_scan.Text = partNo;
                string userIn = Login.userName;
                string TimeIn = DateTime.Now.ToString();
                string partcode = sub_barcode[0] + " " + sub_barcode[1] + " " + sub_barcode[2] + " " + sub_barcode[3] + " " + sub_barcode[4];
                List<string> box_idCheck = new List<string>();
                List<string> boxID_dbCheck = new List<string>();
                int max = 0;
                int min = 0;
                bool selected = true;
                dt_pickup.Rows.Clear();
                try
                {
                    if (dataGridView7.Rows.Count > 0)
                    {
                        con.Open();
                        for (int k = 0; k < dt_pickupGrid.Rows.Count; k++)
                        {
                            if (partNo == dt_pickupGrid.Rows[k][0].ToString() && !check_part)
                            {
                                check_part = true;
                            }
                        }
                        if (check_part)
                        {
                            cmd.CommandText = "SELECT a.box_id,a.rank,a.stock_lot_id,a.pickup_line,b.FIFO_enable,b.rank_enable,a.part_no,a.stock_lot_id  FROM [THAIARROW_PART_STORE].[dbo].[Stock_part] AS a INNER JOIN [THAIARROW_PART_STORE].[dbo].[Master_part_data] AS b ON a.part_no = b.part_no where pickup_transaction IS NULL AND a.part_no ='" + partNo + "'AND pickup_transaction IS NULL";
                            reader = cmd.ExecuteReader();
                            if (reader.HasRows)
                            {
                                while (reader.Read())
                                {
                                    boxID_dbCheck.Add(reader.GetValue(0).ToString());
                                    //box_idCheck.Add(reader.GetValue(4).ToString(), reader.GetValue(5).ToString());
                                    dt_pickup.Rows.Add(reader.GetValue(6).ToString(), reader.GetValue(0).ToString(), reader.GetValue(4).ToString(), reader.GetValue(5).ToString(), reader.GetValue(7).ToString());
                                }
                            }
                            reader.Close();
                            //check boxid 
                            for (int j = 0; j < dt_pickupGrid.Rows.Count; j++)
                            {
                                if (boxID == dt_pickupGrid.Rows[j].ItemArray[13].ToString() && partNo == dt_pickupGrid.Rows[j].ItemArray[0].ToString())
                                {
                                    boxIdCheck = true;
                                    check_box_grid = true;
                                    box_nogridview = true;

                                }
                            }
                            for (int j = 0; j < boxID_dbCheck.Count; j++)
                            {
                                if (boxID == boxID_dbCheck[j].ToString() && !boxIdCheck)
                                {
                                    boxIdCheck = true;

                                }
                            }

                            if (dt_pickup.Rows[0].ItemArray[2].ToString() == "True" && dt_pickup.Rows[0].ItemArray[3].ToString() == "True")
                            {
                                for (int j = 0; j < dt_pickupGrid.Rows.Count; j++)
                                {
                                    if (boxID == dt_pickupGrid.Rows[j].ItemArray[13].ToString() && rank == dt_pickupGrid.Rows[j].ItemArray[5].ToString())
                                    {
                                        //boxid and rank match
                                        check_lot = true;

                                    }
                                    if (boxID == dt_pickupGrid.Rows[j].ItemArray[13].ToString() && rank != dt_pickupGrid.Rows[j].ItemArray[5].ToString())
                                    {
                                        //alert rank wrong
                                        //  pap_ranknotmach.Visible = true;
                                        //  tp_alertranknotmach.Text = rank;
                                        // timer_pickupstock.Enabled = true;
                                        // checkRank = true;
                                        panel26.Visible = true;
                                        label77.Text = "Rank";
                                        tp_alertwrong.Text = rank;
                                        label75.Text = "Not Match";
                                        label76.Text = "หากนำไปใช้มีโอกาสผิดพลาด";
                                        timer_pickupstock.Enabled = true;
                                        checkRank = true;
                                        check_lot = true;

                                    }
                                    if (boxID != dt_pickupGrid.Rows[j].ItemArray[13].ToString() && rank == dt_pickupGrid.Rows[j].ItemArray[5].ToString())
                                  //  {
                                        //alert fifo enable you must choost sort by lot id please select corect boxid
                                 //       check_lot = false;
                                //    }
                                    if (boxID != dt_pickupGrid.Rows[j].ItemArray[13].ToString() && partNo == dt_pickupGrid.Rows[j].ItemArray[0].ToString() && selected && !box_nogridview)
                                    {

                                        string lot_in = dt_pickupGrid.Rows[j].ItemArray[14].ToString();
                                        for (int c = 0; c < dt_pickup.Rows.Count; c++)
                                        {
                                            if (lot_in == dt_pickup.Rows[c].ItemArray[4].ToString() && boxID == boxID_dbCheck[c])
                                            {
                                                check_lot = true;
                                                selected = false;
                                                boxnotmatch_butlotok = true;
                                                box_lot = boxID_dbCheck[c];
                                                lot = lot_in;
                                            }
                                        }
                                        if (rank != dt_pickupGrid.Rows[j].ItemArray[5].ToString() && !selected)
                                        {
                                            //alert rank wrong
                                            panel26.Visible = true;
                                            label77.Text = "Rank";
                                            tp_alertwrong.Text = rank;
                                            label75.Text = "Not Match";
                                            label76.Text = "หากนำไปใช้มีโอกาสผิดพลาด";
                                            timer_pickupstock.Enabled = true;
                                            checkRank = true;
                                            //     selected = false;
                                        }
                                    }

                                }
                            }
                            if (dt_pickup.Rows[0].ItemArray[2].ToString() == "True" && dt_pickup.Rows[0].ItemArray[3].ToString() == "False")
                            {
                                for (int j = 0; j < dt_pickupGrid.Rows.Count; j++)
                                {
                                    if (boxID == dt_pickupGrid.Rows[j].ItemArray[13].ToString())
                                    {
                                        check_lot = true;
                                    }
                                    if (boxID != dt_pickupGrid.Rows[j].ItemArray[13].ToString() && partNo == dt_pickupGrid.Rows[j].ItemArray[0].ToString() && selected && !box_nogridview)
                                    {

                                        string lot_in = dt_pickupGrid.Rows[j].ItemArray[14].ToString();
                                        for (int c = 0; c < dt_pickup.Rows.Count; c++)
                                        {
                                            if (lot_in == dt_pickup.Rows[c].ItemArray[4].ToString() && boxID == boxID_dbCheck[c])
                                            {
                                                check_lot = true;
                                                selected = false;
                                                boxnotmatch_butlotok = true;
                                                box_lot = boxID_dbCheck[c];
                                                lot = lot_in;
                                            }
                                        }

                                    }
                                }
                            }
                            if (dt_pickup.Rows[0].ItemArray[2].ToString() == "False" && dt_pickup.Rows[0].ItemArray[3].ToString() == "True")
                            {
                                for (int j = 0; j < dt_pickupGrid.Rows.Count; j++)
                                {
                                    if (rank == dt_pickupGrid.Rows[j].ItemArray[5].ToString())
                                    {
                                        check_lot = true;
                                        checkRank = true;
                                    }
                                    if (rank != dt_pickupGrid.Rows[j].ItemArray[5].ToString() && partNo == dt_pickupGrid.Rows[j].ItemArray[0].ToString() && !checkRank)
                                    {
                                        //alert rank enable rank wrong
                                        panel26.Visible = true;
                                        label77.Text = "Rank";
                                        tp_alertwrong.Text = rank;
                                        label75.Text = "Not Match";
                                        label76.Text = "หากนำไปใช้มีโอกาสผิดพลาด";
                                        //tp_alertranknotmach.Text = rank;
                                        timer_pickupstock.Enabled = true;
                                        checkRank = true;
                                        check_lot = true;
                                    }
                                }
                            }
                            if (dt_pickup.Rows[0].ItemArray[2].ToString() == "False" && dt_pickup.Rows[0].ItemArray[3].ToString() == "False")
                            {
                                check_lot = true;
                                //freedom to select
                            }

                            if (check_lot && boxIdCheck)
                            {
                                for (int j = 0; j < dataGridView7.Rows.Count; j++)
                                {
                                    if (boxID == dt_pickupGrid.Rows[j].ItemArray[13].ToString() && partNo == dt_pickupGrid.Rows[j].ItemArray[0].ToString() && dt_pickup.Rows[0].ItemArray[2].ToString() == "True")
                                    {
                                        int rowIndex = j;
                                        try
                                        {
                                            if (rowIndex >= 0)
                                            {
                                                string mcline = dataGridView7.Rows[rowIndex].Cells[2].Value.ToString();
                                                string order_time = dt_pickupGrid.Rows[rowIndex].ItemArray[15].ToString();
                                                DateTime pickupTime = DateTime.Now;
                                                dt_pickupCut.Rows.Add(rowIndex, boxID, mcline, partNo, order_time, qty, pickupTime);
                                                con.Close();
                                                check_pickup_scan(partNo, boxID, mcline, rank, order_time, qty, pickupTime);
                                                // dataGridView7.Rows.RemoveAt(rowIndex);
                                                dataGridView7.Rows[rowIndex].Cells[18].Value = "PickupStock";
                                                // dt_pickupGrid.Rows.RemoveAt(rowIndex);

                                            }
                                        }
                                        catch (Exception ab)
                                        {
                                            MessageBox.Show(ab.Message);
                                        }
                                        finally
                                        {
                                            con.Close();

                                        }

                                    }
                                    //box not match but lot okay
                                    else if (boxID != dt_pickupGrid.Rows[j].ItemArray[13].ToString() && dt_pickup.Rows[0].ItemArray[2].ToString() == "True" && boxnotmatch_butlotok && lot == dt_pickupGrid.Rows[j].ItemArray[14].ToString() && partNo == dt_pickupGrid.Rows[j].ItemArray[0].ToString())
                                    {
                                        int rowIndex = j;
                                        try
                                        {
                                            if (rowIndex >= 0)
                                            {
                                                string mcline = dataGridView7.Rows[rowIndex].Cells[2].Value.ToString();
                                                string order_time = dt_pickupGrid.Rows[rowIndex].ItemArray[15].ToString();
                                                DateTime pickupTime = DateTime.Now;
                                                dt_pickupCut.Rows.Add(rowIndex, boxID, mcline, partNo, order_time, qty, pickupTime);
                                                con.Close();
                                                check_pickup_scan(partNo, box_lot, mcline, rank, order_time, qty, pickupTime);
                                                //  dataGridView7.Rows.RemoveAt(rowIndex);
                                                dataGridView7.Rows[rowIndex].Cells[18].Value = "PickupStock";
                                                //   dt_pickupGrid.Rows.RemoveAt(rowIndex);
                                                dataGridView7.Rows[rowIndex].Cells[15].Value = box_lot;

                                            }
                                        }
                                        catch (Exception ab)
                                        {
                                            MessageBox.Show(ab.Message);
                                        }
                                        finally
                                        {
                                            con.Close();
                                            boxnotmatch_butlotok = false;
                                        }
                                    }

                                    else if (dt_pickup.Rows[0].ItemArray[2].ToString() == "False")
                                    { //part fifo not enable
                                      //  for (j = 0; j < boxID_dbCheck.Count; j++)
                                       // {
                                            if (boxID == dt_pickupGrid.Rows[j].ItemArray[13].ToString() && partNo == dt_pickupGrid.Rows[j].ItemArray[0].ToString() && check_box_grid)
                                            {
                                                int rowIndex = j;

                                                try
                                                {
                                                    if (rowIndex >= 0)
                                                    {

                                                        string mcline = dataGridView7.Rows[rowIndex].Cells[2].Value.ToString();
                                                        string order_time = dt_pickupGrid.Rows[rowIndex].ItemArray[15].ToString();
                                                        DateTime pickupTime = DateTime.Now;
                                                        dt_pickupCut.Rows.Add(rowIndex, boxID, mcline, partNo, order_time, qty, pickupTime);
                                                        con.Close();
                                                        check_pickup_scan(partNo, boxID, mcline, rank, order_time, qty, pickupTime);
                                                        //  dataGridView7.Rows.RemoveAt(rowIndex);
                                                        dataGridView7.Rows[rowIndex].Cells[18].Value = "PickupStock";
                                                        //  dt_pickupGrid.Rows.RemoveAt(rowIndex);

                                                    }
                                                }
                                                catch (Exception ab)
                                                {
                                                    MessageBox.Show(ab.Message);
                                                }
                                                finally
                                                {
                                                    con.Close();
                                                }

                                            }
                                            for (int p = 0; p < boxID_dbCheck.Count; p++)
                                            {
                                                if (boxID == boxID_dbCheck[p] && partNo == dt_pickupGrid.Rows[j].ItemArray[0].ToString() && pick_box && dataGridView7.Rows[j].Cells[18].Value.ToString() != "PickupStock" && !check_box_grid)
                                                {
                                                    int rowIndex = j;

                                                    try
                                                    {
                                                        if (rowIndex >= 0)
                                                        {

                                                            string mcline = dataGridView7.Rows[rowIndex].Cells[2].Value.ToString();
                                                            string order_time = dt_pickupGrid.Rows[rowIndex].ItemArray[15].ToString();
                                                            DateTime pickupTime = DateTime.Now;
                                                            dt_pickupCut.Rows.Add(rowIndex, boxID, mcline, partNo, order_time, qty, pickupTime);
                                                            con.Close();
                                                            check_pickup_scan(partNo, boxID, mcline, rank, order_time, qty, pickupTime);
                                                            //  dataGridView7.Rows.RemoveAt(rowIndex);
                                                            dataGridView7.Rows[rowIndex].Cells[18].Value = "PickupStock";
                                                            dataGridView7.Rows[rowIndex].Cells[15].Value = boxID;
                                                            dataGridView7.Rows[rowIndex].Cells[16].Value = dt_pickup.Rows[p].ItemArray[4].ToString();
                                                            //  dt_pickupGrid.Rows.RemoveAt(rowIndex);

                                                        }
                                                    }
                                                    catch (Exception ab)
                                                    {
                                                        MessageBox.Show(ab.Message);
                                                    }
                                                    finally
                                                    {
                                                        con.Close();
                                                    }
                                                    pick_box = false;
                                                }
                                           // }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (!boxIdCheck && check_lot)
                                {
                                    //has been take out
                                    // panel27.Visible = true;
                                    panel26.Visible = true;
                                    label77.Text = "BOXID";
                                    tp_alertwrong.Text = boxID;
                                    label75.Text = "Not Found";
                                    label76.Text = "";
                                    // tp_alertboxid.Text = boxID;
                                    timer_pickupstock.Enabled = true;


                                }
                                if (!check_lot && boxIdCheck && !checkRank)
                                {
                                    //alert fifo enable and box id not found please recheck boxid in store management
                                    pap_lotidwrong.Visible = true;
                                    timer_pickupstock.Enabled = true;
                                }
                                if (!check_lot && !boxIdCheck)
                                {
                                    //alert not found please recheck boxid in store management
                                    // panel27.Visible = true;
                                    panel26.Visible = true;
                                    label77.Text = "BOXID";
                                    tp_alertwrong.Text = boxID;
                                    label75.Text = "Not Found";
                                    label76.Text = "";
                                    // tp_alertboxid.Text = boxID;
                                    timer_pickupstock.Enabled = true;

                                }

                            }
                        }
                        else
                        {
                            //incorect part
                            panel26.Visible = true;
                            label77.Text = "Part ";
                            tp_alertwrong.Text = partNo;
                            label75.Text = "Wrong parts";
                            label76.Text = "Please choose the corect parts!!";
                            timer_pickupstock.Enabled = true;
                        }
                    }
                    else
                    {
                        panel25.Visible = true;
                        tp_alertwrong.Text = partNo;
                        timer_pickupstock.Enabled = true;
                    }
                }
                catch (Exception ab)
                {
                    MessageBox.Show(ab.Message);
                }
                finally
                {
                    reader.Close();
                    con.Close();
                }
            }
            if (select == 3)
            {
                cmd = new SqlCommand();
                cmd.Connection = con;
                int no = 0;
                int j = 0;
                string rank_en = "";
                string rank = "";
                bool boxIdCheck = false;
                bool check_lot = false;
                bool check_part = false;
                bool checkRank = false;
                bool pick_box = true;
                bool check_box_grid = false;
                bool boxnotmatch_butlotok = false;
                bool box_nogridview = false;
               
                string lot = "";
                string box_lot = "";

                string model = "";
                string part_name = "";
                string shelf = "";
                string zone = "";
                string floor = "";
                string side = "";
                string slot = "";
                string statusM = "";
                string statusDB = "INUSE";
                int stock = 0;
                string boxID = sub_barcode[0].Substring(2, sub_barcode[0].Length - 2);
                string MPN = sub_barcode[2];
                string maker = "";
                string qty = sub_barcode[3];
                string ccode = "";
            //    string dcode = sub_barcode[4];
                tas_scan.Text = partNo;
                string userIn = Login.userID;
                string TimeIn = DateTime.Now.ToString();
                string partcode = sub_barcode[0] + " " + sub_barcode[1] + " " + sub_barcode[2] + " " + sub_barcode[3];
                List<string> box_idCheck = new List<string>();
                List<string> boxID_dbCheck = new List<string>();
                int max = 0;
                int min = 0;
                bool selected = true;
                dt_pickup.Rows.Clear();
                try
                {
                    if (dataGridView7.Rows.Count > 0)
                    {
                        con.Open();
                        for (int k = 0; k < dt_pickupGrid.Rows.Count; k++)
                        {
                            if (partNo == dt_pickupGrid.Rows[k].ItemArray[0].ToString() && !check_part)
                            {
                                check_part = true;
                            }
                        }
                        if (check_part)
                        {
                            cmd.CommandText = "SELECT a.box_id,a.rank,a.stock_lot_id,a.pickup_line,b.FIFO_enable,b.rank_enable,a.part_no,a.stock_lot_id  FROM [THAIARROW_PART_STORE].[dbo].[Stock_part] AS a INNER JOIN [THAIARROW_PART_STORE].[dbo].[Master_part_data] AS b ON a.part_no = b.part_no where pickup_transaction IS NULL AND a.part_no ='" + partNo + "'AND pickup_transaction IS NULL";
                            reader = cmd.ExecuteReader();
                            if (reader.HasRows)
                            {
                                while (reader.Read())
                                {
                                    boxID_dbCheck.Add(reader.GetValue(0).ToString());
                                    //box_idCheck.Add(reader.GetValue(4).ToString(), reader.GetValue(5).ToString());
                                    dt_pickup.Rows.Add(reader.GetValue(6).ToString(), reader.GetValue(0).ToString(), reader.GetValue(4).ToString(), reader.GetValue(5).ToString(),reader.GetValue(7).ToString());
                                }
                            }
                            reader.Close();
                            //check boxid 
                            for (j = 0; j < dt_pickupGrid.Rows.Count; j++)
                            {
                                if (boxID == dt_pickupGrid.Rows[j].ItemArray[13].ToString() && partNo == dt_pickupGrid.Rows[j].ItemArray[0].ToString())
                                {
                                    boxIdCheck = true;
                                    check_box_grid = true;
                                    box_nogridview = true;

                                }
                            }
                            for (j = 0; j < boxID_dbCheck.Count; j++)
                            {
                                if (boxID == boxID_dbCheck[j].ToString() && !boxIdCheck)
                                {
                                    boxIdCheck = true;

                                }
                            }

                            if (dt_pickup.Rows[0].ItemArray[2].ToString() == "True" && dt_pickup.Rows[0].ItemArray[3].ToString() == "True")
                            {
                                for (j = 0; j < dt_pickupGrid.Rows.Count; j++)
                                {
                                    if (boxID == dt_pickupGrid.Rows[j].ItemArray[13].ToString() && rank == dt_pickupGrid.Rows[j].ItemArray[5].ToString())
                                    {
                                        //boxid and rank match
                                        check_lot = true;

                                    }
                                    if (boxID == dt_pickupGrid.Rows[j].ItemArray[13].ToString() && rank != dt_pickupGrid.Rows[j].ItemArray[5].ToString())
                                    {
                                        //alert rank wrong
                                      //  pap_ranknotmach.Visible = true;
                                        panel26.Visible = true;
                                        label77.Text = "Rank";
                                        tp_alertwrong.Text = rank;
                                        label75.Text = "Not Match";
                                        label76.Text = "หากนำไปใช้มีโอกาสผิดพลาด";
                                        //tp_alertranknotmach.Text = rank;
                                        timer_pickupstock.Enabled = true;
                                        checkRank = true;
                                        check_lot = true;

                                    }
                                  //  if (boxID != dt_pickupGrid.Rows[j].ItemArray[13].ToString() && rank == dt_pickupGrid.Rows[j].ItemArray[5].ToString())
                                  //  {
                                        //alert fifo enable you must choost sort by lot id please select corect boxid
                                  //      check_lot = false;
                                  //  }
                                    if (boxID != dt_pickupGrid.Rows[j].ItemArray[13].ToString() && partNo == dt_pickupGrid.Rows[j].ItemArray[0].ToString() && selected && !box_nogridview)
                                    {

                                        string lot_in = dt_pickupGrid.Rows[j].ItemArray[14].ToString();
                                        for (int c = 0; c < dt_pickup.Rows.Count; c++)
                                        {
                                            if (lot_in == dt_pickup.Rows[c].ItemArray[4].ToString() && boxID == boxID_dbCheck[c])
                                            {
                                                check_lot = true;
                                                selected = false;
                                                boxnotmatch_butlotok = true;
                                                box_lot = boxID_dbCheck[c];
                                                lot = lot_in;
                                            }
                                        }
                                        if (rank != dt_pickupGrid.Rows[j].ItemArray[5].ToString() && !selected)
                                        {
                                            //alert rank wrong
                                            panel26.Visible = true;
                                            label77.Text = "Rank";
                                            tp_alertwrong.Text = rank;
                                            label75.Text = "Not Match";
                                            label76.Text = "หากนำไปใช้มีโอกาสผิดพลาด";
                                            timer_pickupstock.Enabled = true;
                                            checkRank = true;
                                            //     selected = false;
                                        }
                                    }

                                }
                            }
                            if (dt_pickup.Rows[0].ItemArray[2].ToString() == "True" && dt_pickup.Rows[0].ItemArray[3].ToString() == "False")
                            {
                                for (j = 0; j < dt_pickupGrid.Rows.Count; j++)
                                {
                                    if (boxID == dt_pickupGrid.Rows[j].ItemArray[13].ToString())
                                    {
                                        check_lot = true;
                                    }
                                    if (boxID != dt_pickupGrid.Rows[j].ItemArray[13].ToString() && partNo == dt_pickupGrid.Rows[j].ItemArray[0].ToString() && selected && !box_nogridview)
                                    {

                                        string lot_in = dt_pickupGrid.Rows[j].ItemArray[14].ToString();
                                        for (int c = 0; c < dt_pickup.Rows.Count; c++)
                                        {
                                            if (lot_in == dt_pickup.Rows[c].ItemArray[4].ToString() && boxID == boxID_dbCheck[c])
                                            {
                                                check_lot = true;
                                                selected = false;
                                                boxnotmatch_butlotok = true;
                                                box_lot = boxID_dbCheck[c];
                                                lot = lot_in;
                                            }
                                        }

                                    }
                                }
                            }
                            if (dt_pickup.Rows[0].ItemArray[2].ToString() == "False" && dt_pickup.Rows[0].ItemArray[3].ToString() == "True")
                            {
                                for (j = 0; j < dt_pickupGrid.Rows.Count; j++)
                                {
                                    if (rank == dt_pickupGrid.Rows[j].ItemArray[5].ToString())
                                    {
                                        check_lot = true;
                                    }
                                    if (rank != dt_pickupGrid.Rows[j].ItemArray[5].ToString())
                                    {
                                        //alert rank enable rank wrong
                                        panel26.Visible = true;
                                        label77.Text = "Rank";
                                        tp_alertwrong.Text = rank;
                                        label75.Text = "Not Match";
                                        label76.Text = "หากนำไปใช้มีโอกาสผิดพลาด";
                                        timer_pickupstock.Enabled = true;
                                        checkRank = true;
                                        check_lot = true;

                                    }
                                }
                            }
                            if (dt_pickup.Rows[0].ItemArray[2].ToString() == "False" && dt_pickup.Rows[0].ItemArray[3].ToString() == "False")
                            {
                                check_lot = true;
                                //freedom to select
                            }

                            if (check_lot && boxIdCheck)
                            {
                                for (j = 0; j < dataGridView7.Rows.Count; j++)
                                {
                                    if (boxID == dt_pickupGrid.Rows[j].ItemArray[13].ToString() && partNo == dt_pickupGrid.Rows[j].ItemArray[0].ToString() && dt_pickup.Rows[0].ItemArray[2].ToString() == "True")
                                    {
                                        int rowIndex = j;

                                        try
                                        {
                                            if (rowIndex >= 0)
                                            {

                                                string mcline = dataGridView7.Rows[rowIndex].Cells[2].Value.ToString();
                                                string order_time = dt_pickupGrid.Rows[rowIndex].ItemArray[15].ToString();
                                                DateTime pickupTime = DateTime.Now;
                                                dt_pickupCut.Rows.Add(rowIndex, boxID, mcline, partNo, order_time, qty, pickupTime);
                                                con.Close();
                                                check_pickup_scan(partNo, boxID, mcline, rank, order_time, qty, pickupTime);
                                                //  dataGridView7.Rows.RemoveAt(rowIndex);
                                                dataGridView7.Rows[rowIndex].Cells[18].Value = "PickupStock";
                                                //  dt_pickupGrid.Rows.RemoveAt(rowIndex);

                                            }
                                        }
                                        catch (Exception ab)
                                        {
                                            MessageBox.Show(ab.Message);
                                        }
                                        finally
                                        {
                                            con.Close();
                                        }

                                    }
                                    //box not match but lot okay
                                    else if (boxID != dt_pickupGrid.Rows[j].ItemArray[13].ToString() && dt_pickup.Rows[0].ItemArray[2].ToString() == "True" && boxnotmatch_butlotok && lot == dt_pickupGrid.Rows[j].ItemArray[14].ToString() && partNo == dt_pickupGrid.Rows[j].ItemArray[0].ToString())
                                    {
                                        int rowIndex = j;
                                        try
                                        {
                                            if (rowIndex >= 0)
                                            {
                                                string mcline = dataGridView7.Rows[rowIndex].Cells[2].Value.ToString();
                                                string order_time = dt_pickupGrid.Rows[rowIndex].ItemArray[15].ToString();
                                                DateTime pickupTime = DateTime.Now;
                                                dt_pickupCut.Rows.Add(rowIndex, boxID, mcline, partNo, order_time, qty, pickupTime);
                                                con.Close();
                                                check_pickup_scan(partNo, box_lot, mcline, rank, order_time, qty, pickupTime);
                                                //  dataGridView7.Rows.RemoveAt(rowIndex);
                                                dataGridView7.Rows[rowIndex].Cells[18].Value = "PickupStock";
                                                //   dt_pickupGrid.Rows.RemoveAt(rowIndex);
                                                dataGridView7.Rows[rowIndex].Cells[15].Value = box_lot;

                                            }
                                        }
                                        catch (Exception ab)
                                        {
                                            MessageBox.Show(ab.Message);
                                        }
                                        finally
                                        {
                                            con.Close();
                                            boxnotmatch_butlotok = false;
                                        }
                                    }

                                    else if (dt_pickup.Rows[0].ItemArray[2].ToString() == "False")
                                    { //part fifo not enable
                                        if (boxID == dt_pickupGrid.Rows[j].ItemArray[13].ToString() && partNo == dt_pickupGrid.Rows[j].ItemArray[0].ToString() && check_box_grid)
                                        {
                                            int rowIndex = j;

                                            try
                                            {
                                                if (rowIndex >= 0)
                                                {

                                                    string mcline = dataGridView7.Rows[rowIndex].Cells[2].Value.ToString();
                                                    string order_time = dt_pickupGrid.Rows[rowIndex].ItemArray[15].ToString();
                                                    DateTime pickupTime = DateTime.Now;
                                                    dt_pickupCut.Rows.Add(rowIndex, boxID, mcline, partNo, order_time, qty, pickupTime);
                                                    con.Close();
                                                    check_pickup_scan(partNo, boxID, mcline, rank, order_time, qty, pickupTime);
                                                    //  dataGridView7.Rows.RemoveAt(rowIndex);
                                                    dataGridView7.Rows[rowIndex].Cells[18].Value = "PickupStock";
                                                    //  dt_pickupGrid.Rows.RemoveAt(rowIndex);

                                                }
                                            }
                                            catch (Exception ab)
                                            {
                                                MessageBox.Show(ab.Message);
                                            }
                                            finally
                                            {
                                                con.Close();
                                                //  pick_box = false;

                                            }

                                        }
                                        for (int p = 0; p < boxID_dbCheck.Count; p++)
                                        {
                                            if (boxID == boxID_dbCheck[p] && partNo == dt_pickupGrid.Rows[j].ItemArray[0].ToString() && pick_box && dataGridView7.Rows[j].Cells[18].Value.ToString() != "PickupStock" && !check_box_grid)
                                            {
                                                int rowIndex = j;

                                                try
                                                {
                                                    if (rowIndex >= 0)
                                                    {

                                                        string mcline = dataGridView7.Rows[rowIndex].Cells[2].Value.ToString();
                                                        string order_time = dt_pickupGrid.Rows[rowIndex].ItemArray[15].ToString();
                                                        DateTime pickupTime = DateTime.Now;
                                                        dt_pickupCut.Rows.Add(rowIndex, boxID, mcline, partNo, order_time, qty, pickupTime);
                                                        con.Close();
                                                        check_pickup_scan(partNo, boxID, mcline, rank, order_time, qty, pickupTime);
                                                        //  dataGridView7.Rows.RemoveAt(rowIndex);
                                                        dataGridView7.Rows[rowIndex].Cells[18].Value = "PickupStock";
                                                        dataGridView7.Rows[rowIndex].Cells[15].Value = boxID;
                                                        dataGridView7.Rows[rowIndex].Cells[16].Value = dt_pickup.Rows[p].ItemArray[4].ToString();
                                                        //  dt_pickupGrid.Rows.RemoveAt(rowIndex);

                                                    }
                                                }
                                                catch (Exception ab)
                                                {
                                                    MessageBox.Show(ab.Message);
                                                }
                                                finally
                                                {
                                                    con.Close();
                                                }
                                                pick_box = false;
                                            }

                                        }
                                    }

                                }

                            }
                            else
                            {
                                if (!boxIdCheck && check_lot)
                                {
                                    //has been take out
                                    panel26.Visible = true;
                                    label77.Text = "BOXID";
                                    tp_alertwrong.Text = boxID;
                                    label75.Text = "Not Found";
                                    label76.Text = "";
                                    timer_pickupstock.Enabled = true;

                                }
                                if (!check_lot && boxIdCheck && !checkRank)
                                {
                                    //alert fifo enable and box id not found please recheck boxid in store management
                                    pap_lotidwrong.Visible = true;
                                    timer_pickupstock.Enabled = true;
                                }
                                if (!check_lot && !boxIdCheck)
                                {
                                    //alert not found please recheck boxid in store management
                                   // panel27.Visible = true;
                                    panel26.Visible = true;
                                    label77.Text = "BOXID";
                                    tp_alertwrong.Text = boxID;
                                    label75.Text = "Not Found";             
                                    label76.Text = "";
                                   // tp_alertboxid.Text = boxID;
                                    timer_pickupstock.Enabled = true;
                                }

                            }
                        }
                        else
                        {
                            //incorect part
                            panel26.Visible = true;
                            label77.Text = "Part ";     
                            tp_alertwrong.Text = partNo;
                            label75.Text = "Wrong parts";
                            label76.Text = "Please choose the corect parts!!";
                            timer_pickupstock.Enabled = true;
                        }
                    }
                    else
                    {
                        panel25.Visible = true;
                       
                        tp_alertwrong.Text = partNo;
                        timer_pickupstock.Enabled = true;
                    }
                }


                catch (Exception ab)
                {
                    MessageBox.Show(ab.Message);
                }
                finally
                {
                    reader.Close();
                    con.Close();
                }
            }
        }
        private void fill_dtpickupgrid()
        {
            dt_pickupGrid.Rows.Clear();
            dt_pickupGrid.Columns.Clear();
            dt_pickup.Columns.Clear();
            dt_pickupCut.Columns.Clear();
            dt_pickupCut.Rows.Clear();

            dt_pickupCut.Columns.Add("part_no");
            dt_pickupCut.Columns.Add("box_id");
            dt_pickupCut.Columns.Add("mcLine");
            dt_pickupCut.Columns.Add("rank");
            dt_pickupCut.Columns.Add("order_time");
            dt_pickupCut.Columns.Add("qty");
            dt_pickupCut.Columns.Add("pickuptime");


            dt_pickup.Columns.Add("part_no");
            dt_pickup.Columns.Add("boxID");
            dt_pickup.Columns.Add("fifo_enable");
            dt_pickup.Columns.Add("rank_enable");
            dt_pickup.Columns.Add("lot_id");


            dt_pickupGrid.Columns.Add("part_no");
            dt_pickupGrid.Columns.Add("mcline");
            dt_pickupGrid.Columns.Add("model");
            dt_pickupGrid.Columns.Add("fifo_ena");
            dt_pickupGrid.Columns.Add("rank_ena");
            dt_pickupGrid.Columns.Add("rank");
            dt_pickupGrid.Columns.Add("shelf");
            dt_pickupGrid.Columns.Add("zone");
            dt_pickupGrid.Columns.Add("floor");
           
            dt_pickupGrid.Columns.Add("slot");
            dt_pickupGrid.Columns.Add("min_stock");
            dt_pickupGrid.Columns.Add("quantity");
            dt_pickupGrid.Columns.Add("qty_pcs");
            dt_pickupGrid.Columns.Add("box_id");
            dt_pickupGrid.Columns.Add("stock_lot_id");
            dt_pickupGrid.Columns.Add("order_time");
            dt_pickupGrid.Columns.Add("supplier");
            dt_pickupGrid.Columns.Add("maxstock");
            dt_pickupGrid.Columns.Add("side");
            dt_pickupGrid.Columns.Add("statusM");

            dt_transaction_pickup.Columns.Clear();
            dt_transaction_pickup.Rows.Clear();
            dt_transaction_pickup.Columns.Add("transaction_id");
            dt_transaction_pickup.Columns.Add("mcline");

        }

        private void Timer_pickupstock_Tick(object sender, EventArgs e)
        {
            panel25.Visible = false;
            panel26.Visible = false;
          //  panel27.Visible = false;
          //  pap_ranknotmach.Visible = false;
            pap_lotidwrong.Visible = false;
            timer_pickupstock.Enabled = false;
        }
        private void BtEdit_delLocate_Click(object sender, EventArgs e)
        {
            btEdit_delLocate.Visible = true;
            btEdit_cancelLocal.Visible = false;
            btEdit_addLocal.Visible = false;
            btEdit_add.Visible = true;

            int rowIndex = dataGridView5.CurrentCell.RowIndex;
            try
            {
                if (rowIndex >= 0)
                {
                    dataGridView5.Rows.RemoveAt(rowIndex);
                    dt_local.Rows.RemoveAt(rowIndex);
                    load_Editlocation();
                }
            }
            catch (Exception ab)
            {
                MessageBox.Show(ab.Message);
            }
        }

        private void DataGridView7_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            fill_detail_pickup();
        }
        private void fill_detail_pickup()
        {
            try
            {
                int index = dataGridView7.CurrentCell.RowIndex;
                tp_boxid.Text = dt_pickupGrid.Rows[index].ItemArray[13].ToString();
                tp_maker.Text = dt_pickupGrid.Rows[index].ItemArray[16].ToString();
                tp_area.Text = dt_pickupGrid.Rows[index].ItemArray[6].ToString();
                tp_slot.Text = dt_pickupGrid.Rows[index].ItemArray[9].ToString();
                tp_userout.Text = "";
                tp_qty.Text = dt_pickupGrid.Rows[index].ItemArray[12].ToString();
                tp_mpn.Text = "";
                tp_shelf.Text = dt_pickupGrid.Rows[index].ItemArray[8].ToString();
                tp_statusM.Text = "";
                tp_ordertime.Text = dt_pickupGrid.Rows[index].ItemArray[15].ToString();
                tp_rank.Text = dt_pickupGrid.Rows[index].ItemArray[5].ToString();
                tp_dcode.Text = "";
                tp_zone.Text = dt_pickupGrid.Rows[index].ItemArray[7].ToString();
                tp_statusDB.Text = "";
                tp_stock.Text = dt_pickupGrid.Rows[index].ItemArray[11].ToString();
                tp_ccode.Text = dt_pickupGrid.Rows[index].ItemArray[13].ToString();
                tp_maxstock.Text = dt_pickupGrid.Rows[index].ItemArray[17].ToString();
                tp_minstock.Text = dt_pickupGrid.Rows[index].ItemArray[10].ToString();
                tp_side.Text = dt_pickupGrid.Rows[index].ItemArray[18].ToString();
            }
            catch(Exception ab)
            {
                MessageBox.Show(ab.Message);
            }
        }

        private void check_pickup_scan(string partNo,string boxID,string mcline,string rank,string order_time,string qty,DateTime pickupTime)
        {
            //Let's start process when scanpart in page pickup part
            //1.if first scan (yes->insert new transaction , no ->kept transaction id and reuse until user click ok button)
            //2.kept transaction id then applier in db.transaction detail record part boxid and mcline
            //3.update db.stockpart eg.stock_status(out stock),pickup_time,pickup_transection,pickup_user,pickup_line
            //4.update db.order_event eg.order_status(complete),transaction_id
            cmd = new SqlCommand();
            cmd.Connection = con;
            string transaction_type = "PickupStock";
            string user = Login.userName;
           
            bool check_transaction = true;
            int transaction_id = 0;
            try
            {
                con.Open();
                cmd.CommandText = "SELECT transaction_id FROM [THAIARROW_PART_STORE].[dbo].[Order_event] where order_status ='Pickup' AND order_user ='"+user+"'";
                reader = cmd.ExecuteReader();
                if(reader.HasRows)
                {
                    while (reader.Read())
                    {
                        check_first_pickup = false;
                        transaction_id = int.Parse(reader.GetValue(0).ToString());
                    }
                }
                else
                {
                    check_first_pickup = true;
                }
                reader.Close();
                con.Close();
            }
            catch(Exception ab)
            {
                MessageBox.Show(ab.Message);
            }
            if (check_first_pickup)
            {
                DateTime eventTime = DateTime.Now;
                eventPickup = eventTime;
                try
                {
                    con.Open();
                    cmd.CommandText = "INSERT INTO [THAIARROW_PART_STORE].[dbo].[Transaction] (transaction_type,transaction_user,event_time) VALUES('" + transaction_type + "','" + user + "','" + eventTime + "')";
                    int b = cmd.ExecuteNonQuery();
                    if (b == 0)
                    {
                        check_transaction = false;
                        check_first_pickup = true;
                        MessageBox.Show("เกิดข้อผิดที่ฐานข้อมูลรายการ");
                    }
                    else
                    {
                        check_first_pickup = false;
                        cmd.CommandText = "SELECT [transaction_id] FROM [THAIARROW_PART_STORE].[dbo].[Transaction] where transaction_type ='PickupStock' AND transaction_user ='" + user + "' AND event_time ='" + eventTime + "'";
                        reader = cmd.ExecuteReader();
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                transaction_id = int.Parse(reader.GetValue(0).ToString());

                            }
                        }
                        reader.Close();
                    }
                }
                catch (Exception a)
                {
                    MessageBox.Show(a.Message);
                }
                finally
                {
                    con.Close();
                    
                    if(check_transaction)
                    {
                        if (pickup_transaction_detail(eventTime, transaction_id, partNo, boxID, mcline, rank, order_time, qty, pickupTime) && pickup_stock_part(eventTime, transaction_id, partNo, boxID, mcline, rank, order_time, qty, pickupTime) && pickup_order_event(eventTime, transaction_id, partNo, boxID, mcline, rank, order_time, qty, pickupTime) && count_stock_pickup(partNo,qty) && update_dbpartData_pickup(partNo,qty))
                        {
                            MessageBox.Show("Success");

                        }
                    }
                }
            }
            else
            {
                try
                {
                    con.Close();
                    if (check_transaction)
                    {
                        if (pickup_transaction_detail(eventPickup, transaction_id, partNo, boxID, mcline, rank, order_time, qty, pickupTime) && pickup_stock_part(eventPickup, transaction_id, partNo, boxID, mcline, rank, order_time, qty, pickupTime) && pickup_order_event(eventPickup, transaction_id, partNo, boxID, mcline, rank, order_time, qty, pickupTime) && count_stock_pickup(partNo, qty) && update_dbpartData_pickup(partNo, qty))
                        {
                            MessageBox.Show("Success");

                        }
                    }
                }
                catch(Exception ab)
                {
                    MessageBox.Show(ab.Message);
                }
                finally
                {
                    
                }
            }
        }
    

        private void BtPickup_OK_Click(object sender, EventArgs e)
        {
            //Let's start process when you click OK button
            //1.sent data into db.transaction
            //2.kept transaction id then applier in db.transaction detail record part boxid and mcline
            //3.update db.stockpart eg.stock_status(out stock),pickup_time,pickup_transection,pickup_user,pickup_line
            //4.update db.order_event eg.order_status(complete),transaction_id

            //pickup_transaction
            cmd = new SqlCommand();
            cmd.Connection = con;
            string user = Login.userName;
            string order_status = "PickupStock";
            int transaction_id = 0;
            try
            {
                con.Open();
                cmd.CommandText = "SELECT transaction_id FROM [THAIARROW_PART_STORE].[dbo].[Order_event] where order_status = 'Pickup' AND order_user ='" + user + "'";
                reader = cmd.ExecuteReader();
                if(reader.HasRows)
                {
                    while(reader.Read())
                    {
                        transaction_id = int.Parse(reader.GetValue(0).ToString());
                    }
                }
                reader.Close();
                for (int i = 0; i < dt_pickupCut.Rows.Count; i++)
                {
                  //  int index = int.Parse(dt_pickupCut.Rows[i].ItemArray[0].ToString());
                  //  dataGridView7.Rows.RemoveAt(index);
                    cmd.CommandText = "UPDATE [THAIARROW_PART_STORE].[dbo].[Order_event] SET order_status ='" + order_status + "' where part_no ='" + dt_pickupCut.Rows[i].ItemArray[3].ToString() + "' AND mcline ='" + dt_pickupCut.Rows[i].ItemArray[2].ToString() + "' AND order_status ='Pickup'";
                    int b = cmd.ExecuteNonQuery();
                    if(b==0)
                    {
                        //MessageBox.Show("เกิดข้อผิดพลาดขณะยืนยันการ pickup ที่ db order event");
                    }
                }

            }
            catch(Exception ab)
            {
                MessageBox.Show(ab.Message);
            }
            finally
            {
                con.Close();
                MessageBox.Show("Pickup Part Complete");
                check_first_pickup = true;
                writeto_monitor_pickup(transaction_id);
                fill_dtpickupgrid();
                fill_tablepickup();
            }

        }
        private void writeto_monitor_pickup(int transaction_id)
        {
            cmd = new SqlCommand();
            cmd.Connection = con;
            string username = Login.userName;
            string type = "PickupStock";
           
            try
            {
                
                con.Open(); 
              
                   // int transaction_id = int.Parse(dt_transaction_pickup.Rows[0].ItemArray[0].ToString());
                    

                    cmd.CommandText = "DELETE FROM [THAIARROW_PART_STORE].[dbo].[Monitor]";
                    int a = cmd.ExecuteNonQuery();
                    //  if(a==0)
                    //  {
                    //      MessageBox.Show("เกิดข้อผิดพลาดในการเตรียมการส่งข้อมูลไป db monitor");
                    //   }
                    cmd.CommandText = "INSERT INTO [THAIARROW_PART_STORE].[dbo].[Monitor] (transaction_id,event_type,username) VALUES ('" + transaction_id + "','" + type + "','" + username + "')";
                    int b = cmd.ExecuteNonQuery();
                    if (b == 0)
                    {
                        MessageBox.Show("เกิดข้อผิดพลาดขณะส่งข้อมูลไป db monitor");
                    }
                
            }
            catch (Exception ab)
            {
                MessageBox.Show(ab.Message);
            }
            finally
            {
                con.Close();
            }
        }
        private bool pickup_transaction_detail(DateTime eventTime,int transaction_id,string partNo,string boxID,string mcline,string rank,string order_time,string qty,DateTime pickupTime)
        {
            bool result = true;

            dt_transaction_pickup.Rows.Add(transaction_id);
           
            string user = Login.userName;
            int j = 0;
            cmd = new SqlCommand();
            cmd.Connection = con;
            try
            {
                con.Open();

                    cmd.CommandText = "INSERT INTO [THAIARROW_PART_STORE].[dbo].[Transaction_detail] (transaction_id,part_no,mcline,box_id) VALUES ('" + transaction_id+"','" + partNo +"','"+mcline+"','"+boxID+"')";
                    int b = cmd.ExecuteNonQuery();
                    
                    if (b == 0)
                    {
                        result = false;
                        MessageBox.Show("เกิดข้อผิดพลาดที่ฐานข้อมูลรายละเอียดรายการ");
                    }
                

            }
            catch(Exception ab)
            {
                MessageBox.Show(ab.Message);
            }
            finally
            {
                con.Close();
            }

            return result;
        }
        private bool pickup_stock_part(DateTime eventTime,int transaction_id, string partNo, string boxID, string mcline, string rank, string order_time, string qty, DateTime pickupTime)
        {
            bool result = true;
            
            string user = Login.userName;
           
            try
            {
                con.Open();
          
                    cmd.CommandText = "UPDATE [THAIARROW_PART_STORE].[dbo].[Stock_part] SET stock_status ='PickupStock' , pickup_time ='"+ pickupTime + "', pickup_transaction ='"+transaction_id+"',pickup_user ='"+user+"',pickup_line ='"+mcline+"' where part_no ='"+ partNo + "' AND box_id ='"+boxID+"'";
                    int b = cmd.ExecuteNonQuery();
                    if (b == 0)
                    {
                        result = false;
                        MessageBox.Show("เกิดข้อผิดพลาดที่ฐานข้อมูล Stock Part");
                    }

            }
            catch(Exception ab)
            {
                MessageBox.Show(ab.Message);
            }
            finally
            {
                con.Close();
            }
            return result;
        }
        private bool pickup_order_event(DateTime eventTime ,int transaction_id, string partNo, string boxID, string mcline, string rank, string order_time, string qty, DateTime pickupTime)
        {
            bool result = true;
            string user = Login.userName;
            string order_status = "Pickup";
            List<string> order_id = new List<string>();
            cmd = new SqlCommand();
            cmd.Connection = con;
            try
            {
                con.Open();
                cmd.CommandText = "SELECT order_id FROM [THAIARROW_PART_STORE].[dbo].[Order_event] where order_user ='"+user+"' AND part_no ='"+partNo+"' AND mcline ='"+mcline+"' AND order_status IS NULL AND transaction_id IS NULL";
                reader = cmd.ExecuteReader();
                if(reader.HasRows)
                {
                    while(reader.Read())
                    {
                        order_id.Add(reader.GetValue(0).ToString());
                    }
                }
                reader.Close();
 
                    cmd.CommandText = "UPDATE [THAIARROW_PART_STORE].[dbo].[Order_event] SET order_status ='" + order_status + "',transaction_id ='" + transaction_id + "',pickup_box_id ='"+boxID+"' where part_no ='" + partNo + "' AND mcline ='" + mcline + "' AND order_id ='" + order_id[0] + "'";
                    int b = cmd.ExecuteNonQuery();
                    if (b == 0)
                    {
                        result = false;
                        MessageBox.Show("เกิดข้อผิดพลาดที่ฐานข้อมูล Order event ในขณะ Picking");
                    }

            }
            catch(Exception ab)
            {
                MessageBox.Show(ab.Message);
            }
            finally
            {
                con.Close();
            }
            return result;
        }
        private bool count_stock_pickup(string part,string qty)
        {
            bool result = false;
            cmd = new SqlCommand();
            cmd.Connection = con;
            int qty_in = int.Parse(qty);
            int minstock = 0;
      
           // List<string> check_part = new List<string>();
            dt_countstock.Rows.Clear();
            dt_countstock.Columns.Clear();
            dt_countstock.Columns.Add("part");
            dt_countstock.Columns.Add("sum");
          //  for (int q = 0; q < dt_pickupCut.Rows.Count; q++)
          //  {
          //      check_part.Add(dt_pickupCut.Rows[q].ItemArray[0].ToString());
          //  }
          //  List<string> sub_part = check_part.Distinct().ToList();
            int sum = 0;
            try
            {
                con.Open();

                    cmd.CommandText = "SELECT [qty_pcs],[min_stock] FROM [THAIARROW_PART_STORE].[dbo].[Master_part_data] where part_no ='" + part + "'";
                    reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            sum = int.Parse(reader.GetValue(0).ToString());
                            minstock = int.Parse(reader.GetValue(1).ToString());
                        sum -= qty_in;
                        }

                    }

                    dt_countstock.Rows.Add(part, sum);
                    result = true;
            }
            catch (Exception ab)
            {
                MessageBox.Show(ab.Message);
            }
            finally
            {
                reader.Close();
                con.Close();
            }

            return result;
        }
        private bool update_dbpartData_pickup(string partNo,string qty)
        {
            bool result = false;
            string user = Login.userName;
            string box_id = "";
            int count = 0;
            DateTime updateDate = DateTime.Now;
            cmd = new SqlCommand();
            cmd.Connection = con;
            try
            {
                con.Open();
  
                    string part = dt_countstock.Rows[0].ItemArray[0].ToString();
                    int sum = int.Parse(dt_countstock.Rows[0].ItemArray[1].ToString());
                    cmd.CommandText = "SELECT box_id FROM [THAIARROW_PART_STORE].[dbo].[Stock_part] where part_no ='" + part + "' AND pickup_line IS NULL AND pickup_transaction IS NULL AND pickup_line IS NULL";
                    reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            box_id = reader.GetValue(0).ToString();
                            count++;
                        }
                    }
                    reader.Close();
                    cmd.CommandText = "UPDATE [THAIARROW_PART_STORE].[dbo].[Master_part_data] SET qty_pcs ='" + sum + "', update_user ='" + user + "',update_date ='" + updateDate + "' ,stock_available ='"+count+"' where part_no ='" + part + "'";
                    int b = cmd.ExecuteNonQuery();
                    if (b > 0)
                    {
                        result = true;
                    }
                

            }
            catch (Exception ab)
            {
                MessageBox.Show(ab.Message);
            }
            finally
            {
                con.Close();
            }

            return result;
        }



        #endregion
        //edit part region
        #region
        //edit part save button
        private void BtEdit_save_Click(object sender, EventArgs e)
        {
            string partNo = tEdit_partno.Text;
            string partName = tEdit_partname.Text;
            string supplier = tEdit_supplier.Text;
            uint qty = Convert.ToUInt32(numEdit_qty.Value);
            string model = tEdit_model.Text;
            uint maxStock = Convert.ToUInt32(numEdit_max.Value);
            uint minStock = Convert.ToUInt32(numEdit_min.Value);
            int Local = dt_local.Rows.Count;
            int Line = dt_line.Rows.Count;
            bool fifo = checkEdit_FIFO.Checked;
            bool rankEnabled = checkEdit_rank.Checked;
            string createUser = Login.name;
            string userID = Login.userName;
            bool checkPart = false;
            bool checkLocal = false;
            bool checkLine = false;
            string detail = "";
            string rank_detail = "";
            string local_detail = "";
            string line_detail = "";
            string eventAction = "";
            string propertyPart = comEdit_partpackage.Text.Substring(2, comEdit_partpackage.Text.Length - 2);
            string typePart = comEdit_parttype.Text.Substring(2, comEdit_parttype.Text.Length - 2);

            DateTime eventTime = DateTime.Now;


            if (partNo != "" && partName != "" && supplier != "" && qty != 0 && model != "" && maxStock != 0 && minStock != 0 && propertyPart != "" && typePart != "")
            {
                checkPart = Edit_part();
                if (!checkPart)
                {
                    checkLocal = Edit_location();
                    checkLine = Edit_line();
                    //  if (!checkPart && !checkLocal && !checkLine)
                    //    {
                    MessageBox.Show("ทำการเพิ่มข้อมูล Part :" + partNo + "เสร็จสมบูรณ์");

                    for (int j = 0; j < dt_local.Rows.Count; j++)
                    {
                        local_detail = local_detail + dt_local.Rows[j]["Shelf"].ToString() + "|" + dt_local.Rows[j]["Zone"].ToString() + "|" + dt_local.Rows[j]["Floor"].ToString() + "|" + dt_local.Rows[j]["Side"].ToString() + "|" + dt_local.Rows[j]["Slot"].ToString() + ",";
                    }
                    for (int k = 0; k < dt_line.Rows.Count; k++)
                    {
                        line_detail = line_detail + dt_line.Rows[k]["Line No"].ToString() + "|";
                    }
                    eventAction = "EditPart";
                    detail = "part_no =" + partNo + ":part_name =" + partName + ":supplier =" + supplier + ":Qty =" + qty.ToString() + ":model =" + model + ":maxStock =" + maxStock + ":minStock =" + minStock + ":FIFO =" + fifo.ToString() + ":RankEnabled =" + rankEnabled.ToString() + ":location =" + local_detail + ":lineNo =" + line_detail + ":part_package =" + propertyPart + ":type_part =" + typePart;
                    addEdit_event(eventAction, partNo, userID, eventTime, detail);
                    dt_local.Rows.Clear();
                    dt_rank.Rows.Clear();
                    dt_line.Rows.Clear();
                    tEdit_partno.Text = "";
                    tEdit_partname.Text = "";
                    tEdit_model.Text = "";
                    numEdit_max.Value = 0;
                    numEdit_min.Value = 0;
                    numEdit_qty.Value = 0;
                    tEdit_supplier.Text = "";
                    checkEdit_FIFO.Checked = false;
                    checkEdit_rank.Checked = false;
                    comEdit_partpackage.Text = "";
                    comEdit_parttype.Text = "";
                }
                else
                {
                    MessageBox.Show("เกิดข้อผิดพลาดในการแก้ไข Part", "Error Part No : '" + partNo + "'", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("กรุณากรอกข้อมูลให้ครบถ้วน", "Error Part No : '" + partNo + "'", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private bool Edit_part()
        {
            string partNo_old = dataGridView1[0, dataGridView1.CurrentCell.RowIndex].Value.ToString();
            string partNo = tEdit_partno.Text;
            string partName = tEdit_partname.Text;
            string supplier = tEdit_supplier.Text;
            uint qty = Convert.ToUInt32(numEdit_qty.Value);
            string model = tEdit_model.Text;
            uint maxStock = Convert.ToUInt32(numEdit_max.Value);
            uint minStock = Convert.ToUInt32(numEdit_min.Value);
            //   string rankAction = textAdd_propertyPart.Text;
            string shelf = comEdit_shelf.Text;
            string zone = comEdit_zone.Text;
            string floor = comEdit_floor.Text;
            string side = comEdit_side.Text;
            string slot = comEdit_slot.Text;
            string lineDB = comEdit_line.Text;
            bool fifo = checkEdit_FIFO.Checked;
            bool rankEnabled = checkEdit_rank.Checked;
            string updateUser = Login.name;
            DateTime createDate = new DateTime();
            createDate = DateTime.Now;
            bool result = false;
            string propertyPart = comEdit_partpackage.Text.Substring(2, comEdit_partpackage.Text.Length - 2);
            string typePart = comEdit_parttype.Text.Substring(2, comEdit_parttype.Text.Length - 2);
            cmd = new SqlCommand();
            cmd.Connection = con;
            if (partNo != "" || partName != "" || qty != 0 || maxStock != 0 || minStock != 0 || shelf != "" || zone != "" || floor != "" || side != "" ||slot != "" || lineDB != "" && propertyPart != "" && typePart != "")
            {
                try
                {
                    con.Open();
                    cmd.CommandText = "UPDATE [THAIARROW_PART_STORE].[dbo].[Master_part_data] SET part_name='" + partName + "',model='" + model + "',FIFO_enable='" + fifo + "',rank_enable='" + rankEnabled + "',max_stock=" + maxStock + ",min_stock=" + minStock + ",qty_pcs=" + qty + ",update_user='" + updateUser + "',update_date='" + createDate + "',supplier='" + supplier + "',part_type='" + typePart + "',part_package=N'" + propertyPart + "' WHERE part_no = '" + partNo_old + "'";
                    int b = cmd.ExecuteNonQuery();
                    if (b == 0)
                    {
                        MessageBox.Show("เพิ่มข้อมูล Part No ไม่สำเร็จ");
                        result = true;
                    }
                    else
                    {
                        // MessageBox.Show("เพิ่มข้อมูลสำเร็จ");
                        result = false;
                    }
                }
                catch (Exception a)
                {
                    MessageBox.Show(a.Message);
                }
                finally
                {
                    con.Close();
                }
            }
            else
            {
                MessageBox.Show("WARNING", "กรุณากรอกข้อมูลให้ครบถ้วน", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                result = true;
            }
            return result;
        }
        private bool Edit_location()
        {
            cmd = new SqlCommand();
            cmd.Connection = con;
            string shelf = "";
            string zone = "";
            string floor = "";
            string side = "";
            string slot = "";
            string partNo_old = dataGridView1.Rows[dataGridView1.CurrentCell.RowIndex].Cells[0].Value.ToString();
            string partNo = tEdit_partno.Text;
            string user = Login.userName;
            bool result = false;
            DateTime regisTime = new DateTime();
            regisTime = DateTime.Now;
            DataTable temp_local = new DataTable();
            temp_local.Columns.Clear();
            temp_local.Rows.Clear();
            temp_local.Columns.Add("Shelf");
            temp_local.Columns.Add("Zone");
            temp_local.Columns.Add("Floor");
            temp_local.Columns.Add("Side");
            temp_local.Columns.Add("Slot");
            try
            {
                con.Open();
                cmd.CommandText = "SELECT * FROM [THAIARROW_PART_STORE].[dbo].[Master_location] where [part_no_registered] ='" + partNo_old + "'";
                reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        string shelf_ = reader.GetValue(1).ToString();
                        string zone_ = reader.GetValue(2).ToString();
                        string floor_ = reader.GetValue(3).ToString();
                        string side_ = reader.GetValue(4).ToString();
                        string slot_ = reader.GetValue(5).ToString();
                        temp_local.Rows.Add(shelf_, zone_, floor_, side_, slot_);
                    }
                }
                reader.Close();
                for (int i = 0; i < temp_local.Rows.Count; i++)
                {
                    cmd.CommandText = "UPDATE [THAIARROW_PART_STORE].[dbo].[Master_location] SET part_no_registered =NULL, part_registered_user =NULL,part_registered_time =NULL where shelf ='" + temp_local.Rows[i]["Shelf"].ToString() + "'AND zone ='" + temp_local.Rows[i]["Zone"].ToString() + "'AND floor ='" + temp_local.Rows[i]["Floor"].ToString() + "' AND side ='"+ temp_local.Rows[i]["Side"].ToString() + "'AND slot ='" + temp_local.Rows[i]["Slot"].ToString() + "'";
                    int b = cmd.ExecuteNonQuery();
                    if (b == 0) { MessageBox.Show("เกิดข้อผิดพลาดขณะแก้ไขสถานที่จัดเก็บ"); return true; }
                }


                for (int i = 0; i < dt_local.Rows.Count; i++)
                {
                    shelf = dt_local.Rows[i]["Shelf"].ToString();
                    zone = dt_local.Rows[i]["Zone"].ToString();
                    floor = dt_local.Rows[i]["Floor"].ToString();
                    side = dt_local.Rows[i]["Side"].ToString();
                    slot = dt_local.Rows[i]["Slot"].ToString();
                    cmd.CommandText = "UPDATE [THAIARROW_PART_STORE].[dbo].[Master_location] SET part_no_registered ='" + partNo + "', part_registered_user ='" + user + "',part_registered_time ='" + regisTime + "'where shelf ='" + shelf + "'AND zone ='" + zone + "'AND floor ='" + floor + "'AND side ='"+side+"' AND slot ='" + slot + "'";
                    int a = cmd.ExecuteNonQuery();
                    if (a == 0)
                    {
                        MessageBox.Show("เกิดข้อผิดพลาดในการเพิ่มสถานที่จัดเก็บลงฐานข้อมูล");
                        result = true;
                    }

                }

            }
            catch (Exception ab)
            {
                MessageBox.Show(ab.Message);
            }
            finally
            {

                con.Close();
                dataGridView5.Rows.Clear();
            }
            return result;
        }
        private void BtEdit_cancelLine_Click(object sender, EventArgs e)
        {
            btEdit_addLine.Visible = true;
            btEdit_delLine.Visible = true;
            btEdit_addLineNo.Visible = false;
            btEdit_cancelLine.Visible = false;
        }
        private void BtEdit_addLineNo_Click(object sender, EventArgs e)
        {
            string partNo = tEdit_partno.Text;
            string line = comEdit_line.Text;
            if (!sameLine(partNo, line))
            {

                try
                {

                    dataGridView4.Rows.Add(line);
                    dt_line.Rows.Add(line);
                    dataGridView4.DefaultCellStyle.Font = new Font("Tahoma", 12);

                }
                catch (Exception ab)
                {
                    MessageBox.Show(ab.Message);
                }
                finally
                {

                }

            }
            else
            {
                MessageBox.Show("มี Part No :" + partNo + "ได้ถูกเพิ่มใน Line นี้แล้ว");
            }

        }
        private bool Edit_line()
        {
            string lineNo = "";
            string partNo = tEdit_partno.Text;
            bool result = false;
            cmd = new SqlCommand();
            cmd.Connection = con;
            try
            {
                con.Open();

                //delete data line old
                cmd.CommandText = "DELETE FROM [THAIARROW_PART_STORE].[dbo].[Master_line_part] WHERE part_no ='" + partNo + "'";
                int b = cmd.ExecuteNonQuery();
               // if (b == 0) { MessageBox.Show("ไม่มีข้อมูล Line No ในฐานข้อมูล Line-Part จะทำการเพิ่มข้อมูลในลำดับถัดไป"); result = true; }
                //  if (b > 0)
                //  {
                for (int i = 0; i < dt_line.Rows.Count; i++)
                {
                    lineNo = dt_line.Rows[i]["Line No"].ToString();
                    cmd.CommandText = "INSERT INTO [THAIARROW_PART_STORE].[dbo].[Master_line_part] (line_no,part_no) VALUES ('" + lineNo + "','" + partNo + "')";
                    int c = cmd.ExecuteNonQuery();
                    if (c == 0) { MessageBox.Show("เกิดข้อผิดพลาดในการแก้ไขข้อมูล Line"); result = true; }
                }
                //   }

            }
            catch (Exception ab)
            {
                MessageBox.Show(ab.Message);
            }
            finally
            {
                con.Close();
                dataGridView4.Rows.Clear();
            }
            return result;
        }
        private void addEdit_event(string eventAction, string partNo, string userID, DateTime eventTime, string detail)
        {
            cmd = new SqlCommand();
            cmd.Connection = con;
            try
            {
                con.Open();
                cmd.CommandText = "INSERT INTO [THAIARROW_PART_STORE].[dbo].[data_event] (event_action,part_no,event_user,event_time,detail) VALUES ('" + eventAction + "','" + partNo + "','" + userID + "','" + eventTime + "','" + detail + "')";
                int b = cmd.ExecuteNonQuery();
                if (b == 0) { MessageBox.Show("เกิดข้อผิดพลาดในการส่งข้อมูลเข้าสู่ฐานข้อมูลเหตุการณ์"); }
            }
            catch (Exception ab)
            {
                MessageBox.Show(ab.Message);
            }
            finally
            {
                con.Close();
            }
        }

        private void BtEdit_addLine_Click(object sender, EventArgs e)
        {
            btEdit_cancelLine.Visible = true;
            btEdit_addLineNo.Visible = true;
            btEdit_addLine.Visible = false;
            btEdit_delLine.Visible = false;
            load_editLine();
        }
        //create dropdown line in edit part page
        private void load_editLine()
        {
            cmd = new SqlCommand();
            cmd.Connection = con;
            comEdit_line.Text = "";
            comEdit_line.Items.Clear();
            try
            {
                con.Open();
                cmd.CommandText = "SELECT [line_no] FROM [THAIARROW_PART_STORE].[dbo].[Master_line] ORDER BY line_no ASC";
                reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        string line = reader.GetValue(0).ToString();
                        comEdit_line.Items.Add(line);
                    }
                }
            }
            catch (Exception ab)
            {
                MessageBox.Show(ab.Message);
            }
            finally
            {
                con.Close();
                reader.Close();
            }
        }
        //edit button when click add page edit part
        private void BtEdit_addLocal_Click(object sender, EventArgs e)
        {
            string shelf = comEdit_shelf.Text;
            string zone = comEdit_zone.Text;
            string floor = comEdit_floor.Text;
            string side = comEdit_side.Text;
            string slot = comEdit_slot.Text;
            if (!EditsameLocation(shelf, zone, floor, side, slot))
            {
                dataGridView5.Rows.Add(shelf, zone, floor, side, slot);
                dt_local.Rows.Add(shelf, zone, floor, side, slot);
                load_Editlocation();
            }
            else
            {
                MessageBox.Show("สถานที่จัดเก็บนี้มีข้อมูลแล้วกรุณาเลือกใหม่");
            }
        }
        private bool EditsameLocation(string shelf, string zone, string floor,string side, string slot)
        {
            bool result = false;

            if (dataGridView5.Rows.Count > 1)
            {
                try
                {

                    for (int i = 0; i < dataGridView5.Rows.Count - 1; i++)
                    {
                        if (dataGridView5.Rows[i].Cells[0].Value.ToString() == shelf && dataGridView5.Rows[i].Cells[1].Value.ToString() == zone && dataGridView5.Rows[i].Cells[2].Value.ToString() == floor && dataGridView5.Rows[i].Cells[3].Value.ToString() == side && dataGridView5.Rows[i].Cells[4].Value.ToString() == slot)
                        {
                            result = true;
                        }
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }

            return result;
        }
        //dropdown editpage
        private void load_Editlocation()
        {
            cmd = new SqlCommand();
            cmd.Connection = con;
            int index = dataGridView5.Rows.Count;
            comEdit_shelf.Text = "";
            comEdit_shelf.Items.Clear();
            comEdit_zone.Text = "";
            comEdit_zone.Items.Clear();
            comEdit_floor.Text = "";
            comEdit_floor.Items.Clear();
            comEdit_side.Text = "";
            comEdit_side.Items.Clear();
            comEdit_slot.Text = "";
            comEdit_slot.Items.Clear();
            if (index == 1)
            {
                try
                {
                    con.Open();
                    cmd.CommandText = " SELECT DISTINCT [shelf] FROM [THAIARROW_PART_STORE].[dbo].[Master_location] where part_no_registered Is NULL AND part_registered_user IS NULL AND part_registered_time IS NULL ";
                    reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            string shelf_ = reader.GetValue(0).ToString();
                            comEdit_shelf.Items.Add(shelf_);
                        }
                    }
                }
                catch (Exception ab)
                {
                    MessageBox.Show(ab.Message);
                }
                finally
                {
                    con.Close();
                    reader.Close();
                    //  load_Editzone();
                }
            }
            if (index > 1)
            {
                try
                {
                    string shelf = dataGridView5.Rows[0].Cells[0].Value.ToString();
                    con.Open();
                    cmd.CommandText = " SELECT DISTINCT [shelf] FROM [THAIARROW_PART_STORE].[dbo].[Master_location] where part_no_registered Is NULL AND part_registered_user IS NULL AND part_registered_time IS NULL AND shelf ='" + shelf + "'";
                    reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            string area_ = reader.GetValue(0).ToString();
                            comEdit_shelf.Items.Add(area_);
                        }
                    }
                }

                catch (Exception ab)
                {
                    MessageBox.Show(ab.Message);
                }
                finally
                {
                    con.Close();
                    reader.Close();
                    //  load_Editzone();
                }
            }
        }
        //dropdown editpage
        private void load_Editzone()
        {
            string shelf = comEdit_shelf.Text;
            int index = dataGridView5.Rows.Count;
            comEdit_zone.Text = "";
            comEdit_zone.Items.Clear();
            comEdit_floor.Text = "";
            comEdit_floor.Items.Clear();
            comEdit_side.Text = "";
            comEdit_side.Items.Clear();
            comEdit_slot.Text = "";
            comEdit_slot.Items.Clear();
            if (index == 1)
            {
                try
                {
                    con.Open();
                    cmd.CommandText = " SELECT DISTINCT [zone] FROM [THAIARROW_PART_STORE].[dbo].[Master_location] where shelf ='" + shelf + "' AND part_no_registered IS NULL AND part_registered_user IS NULL AND part_registered_time IS NULL";

                    reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            string zone_ = reader.GetValue(0).ToString();
                            comEdit_zone.Items.Add(zone_);

                        }
                    }
                }
                catch (Exception ab)
                {
                    MessageBox.Show(ab.Message);
                }
                finally
                {
                    con.Close();
                    reader.Close();
                    //    load_Editshelf();

                }
            }
            if (index > 1)
            {
                try
                {
                    string zone = dataGridView5.Rows[0].Cells[1].Value.ToString();
                    con.Open();
                    cmd.CommandText = " SELECT DISTINCT [zone] FROM [THAIARROW_PART_STORE].[dbo].[Master_location] where shelf ='" + shelf + "' AND part_no_registered IS NULL AND part_registered_user IS NULL AND part_registered_time IS NULL AND zone ='" + zone + "'";

                    reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            string zone_ = reader.GetValue(0).ToString();
                            comEdit_zone.Items.Add(zone_);

                        }
                    }
                }
                catch (Exception ab)
                {
                    MessageBox.Show(ab.Message);
                }
                finally
                {
                    con.Close();
                    reader.Close();
                    
                }
            }
        }
        //edit floor
        private void load_Editshelf()
        {
            string zone = comEdit_zone.Text;
            string shelf = comEdit_shelf.Text;
            int index = dataGridView5.Rows.Count;
            comEdit_floor.Text = "";
            comEdit_floor.Items.Clear();
            comEdit_side.Text = "";
            comEdit_side.Items.Clear();
            comEdit_slot.Text = "";
            comEdit_slot.Items.Clear();
            if (index == 1)
            {
                try
                {
                    con.Open();
                    cmd.CommandText = " SELECT DISTINCT [floor] FROM [THAIARROW_PART_STORE].[dbo].[Master_location] where shelf ='" + shelf + "' AND zone='" + zone + "'AND part_no_registered IS NULL AND part_registered_user IS NULL AND part_registered_time IS NULL ";

                    reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            string shelf_ = reader.GetValue(0).ToString();
                            comEdit_floor.Items.Add(shelf_);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Shelf และ Zone นี้ไม่มี Floor ที่ว่าง");
                    }
                }
                catch (Exception ab)
                {
                    MessageBox.Show(ab.Message);
                }
                finally
                {
                    con.Close();
                    reader.Close();
                    //    load_Editslot();
                }
            }
            if (index > 1)
            {
                try
                {
                    string floor = dataGridView5.Rows[0].Cells[2].Value.ToString();
                    con.Open();
                    cmd.CommandText = " SELECT DISTINCT [floor] FROM [THAIARROW_PART_STORE].[dbo].[Master_location] where shelf ='" + shelf + "' AND zone='" + zone + "'AND part_no_registered IS NULL AND part_registered_user IS NULL AND part_registered_time IS NULL AND floor ='" + floor + "'";

                    reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            string shelf_ = reader.GetValue(0).ToString();
                            comEdit_floor.Items.Add(shelf_);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Shelf และ Zone นี้ไม่มี Floor ที่ว่าง");
                    }
                }
                catch (Exception ab)
                {
                    MessageBox.Show(ab.Message);
                }
                finally
                {
                    con.Close();
                    reader.Close();
             
                }
            }
        }
        private void load_Editside()
        {
            string zone = comEdit_zone.Text;
            string shelf = comEdit_shelf.Text;
            string floor = comEdit_floor.Text;
            int index = dataGridView5.Rows.Count;
            
            comEdit_side.Text = "";
            comEdit_side.Items.Clear();
            comEdit_slot.Text = "";
            comEdit_slot.Items.Clear();
            if (index == 1)
            {
                try
                {
                    con.Open();
                    cmd.CommandText = " SELECT DISTINCT [side] FROM [THAIARROW_PART_STORE].[dbo].[Master_location] where shelf ='" + shelf + "' AND zone='" + zone + "'AND floor ='"+floor+"'AND part_no_registered IS NULL AND part_registered_user IS NULL AND part_registered_time IS NULL ";

                    reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            string shelf_ = reader.GetValue(0).ToString();
                            comEdit_side.Items.Add(shelf_);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Shelf ,Zone , Floor นี้ไม่มี Side ที่ว่าง");
                    }
                }
                catch (Exception ab)
                {
                    MessageBox.Show(ab.Message);
                }
                finally
                {
                    con.Close();
                    reader.Close();
                    //    load_Editslot();
                }
            }
            if (index > 1)
            {
                try
                {
                    string side_in = dataGridView5.Rows[0].Cells[3].Value.ToString();
                    con.Open();
                    cmd.CommandText = " SELECT DISTINCT [side] FROM [THAIARROW_PART_STORE].[dbo].[Master_location] where shelf ='" + shelf + "' AND zone='" + zone + "'AND floor ='"+floor+"' AND part_no_registered IS NULL AND part_registered_user IS NULL AND part_registered_time IS NULL AND side ='" + side_in + "'";

                    reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            string shelf_ = reader.GetValue(0).ToString();
                            comEdit_side.Items.Add(shelf_);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Shelf ,Zone , Floor นี้ไม่มี Side ที่ว่าง");
                    }
                }
                catch (Exception ab)
                {
                    MessageBox.Show(ab.Message);
                }
                finally
                {
                    con.Close();
                    reader.Close();

                }
            }
        }

        private void load_Editslot()
        {

            string shelf = comEdit_shelf.Text;
            string zone = comEdit_zone.Text;
            string floor = comEdit_floor.Text;
            string side = comEdit_side.Text;
            int index = dataGridView5.Rows.Count;
            List<string> slot_ina = new List<string>();
            List<string> check_slot = new List<string>();
            string temp_slot = "";
            slot_ina.Clear();
            int k = -1;
            int d = 0;
            bool hasValue = false;
            bool count_one = true;
            List<string> slot_duplicate = new List<string>();

            if (index == 1)
            {
                try
                {
                    con.Open();
                    cmd.CommandText = " SELECT DISTINCT [slot] FROM [THAIARROW_PART_STORE].[dbo].[Master_location] where shelf ='" + shelf + "' AND zone='" + zone + "' AND floor='" + floor + "' AND side ='"+side+"'AND part_no_registered IS NULL AND part_registered_user IS NULL AND part_registered_time IS NULL";

                    reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            string slot_ = reader.GetValue(0).ToString();
                            comEdit_slot.Items.Add(slot_);

                        }
                    }
                }
                catch (Exception ab)
                {
                    MessageBox.Show(ab.Message);
                }
                finally
                {
                    con.Close();
                    reader.Close();
                }
            }
            if (index > 1)
            {
                try
                {
                    string slot_in = dt_local.Rows[0].ItemArray[4].ToString();
                    con.Open();
                    cmd.CommandText = " SELECT DISTINCT [slot] FROM [THAIARROW_PART_STORE].[dbo].[Master_location] where shelf ='" + shelf + "' AND zone='" + zone + "' AND floor='" + floor + "'AND side='"+side+"'";

                    reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            slot_ina.Add(reader.GetValue(0).ToString());

                        }
                    }
                    
                    reader.Close();
                    for (int j = 0; j < slot_ina.Count; j++)
                    {
                        if (slot_ina[j] == slot_in) { k = j; hasValue = true; }

                        if (hasValue)
                        {

                            for (int l = 1; l < dataGridView5.Rows.Count; l++)
                            {
                                if (k - l < 0 || k + l > slot_ina.Count - 1 && k != -1) { }
                                if (k - l >= 0 && k - l < slot_ina.Count - 1 && k != -1) { check_slot.Add(slot_ina[k - 1]); }

                                if (k + l <= slot_ina.Count - 1 && k != -1) { check_slot.Add(slot_ina[k + 1]); temp_slot = slot_ina[k + 1]; }
                                //  if(k+l <=slot_ina.Count - 1 && k == 0) { textAdd_Slot.Items.Add(slot_ina[k + 1]); }
                                if (k - l < 0 && k != -1) { }
                            }
                            hasValue = false;
                        }
                    }
                    // filter duplicate string array
                    for(int j=0; j< check_slot.Count;j++)
                    {
                        
                        for(int r = 1; r<check_slot.Count;r++)
                        {
                            if(check_slot[j] == check_slot[r] && j!=r)
                            {
                                check_slot[r] = "";
                            }
                        }
                    }
                    for (int j = 0; j < check_slot.Count; j++)
                    {
                        check_slot[j].Distinct();
                        cmd.CommandText = "SELECT DISTINCT [slot] FROM [THAIARROW_PART_STORE].[dbo].[Master_location] where shelf ='" + shelf + "' AND zone='" + zone + "' AND floor='" + floor + "'AND side='"+side+"'AND slot ='"+ check_slot[j]+ "'AND part_no_registered IS NULL AND part_registered_user IS NULL AND part_registered_time IS NULL ";
                        reader = cmd.ExecuteReader();
                        if(reader.HasRows)
                        {
                            comEdit_slot.Items.Add(check_slot[j]);
                        }
                        reader.Close();

                    }
                  //  comEdit_slot.Items.Clear();
                }
                catch (Exception ab)
                {
                    MessageBox.Show(ab.Message);
                }
                finally
                {
                    con.Close();
                    reader.Close();
                }
            }

        }

        private void BtEdit_add_Click(object sender, EventArgs e)
        {
            comEdit_shelf.Text = "";
            comEdit_shelf.Items.Clear();
            comEdit_zone.Text = "";
            comEdit_zone.Items.Clear();
            comEdit_floor.Text = "";
            comEdit_floor.Items.Clear();
            comEdit_side.Text = "";
            comEdit_side.Items.Clear();
            comEdit_slot.Text = "";
            comEdit_slot.Items.Clear();
            load_Editlocation();
            btEdit_add.Visible = false;
            btEdit_addLocal.Visible = true;
            btEdit_cancelLocal.Visible = true;
            btEdit_delLocate.Visible = false;
        }

        private void ComEdit_area_SelectedIndexChanged(object sender, EventArgs e)
        {
            load_Editzone();
        }

        private void ComEdit_zone_SelectedIndexChanged(object sender, EventArgs e)
        {
            load_Editshelf();
        }
        private void ComEdit_floor_SelectedIndexChanged(object sender, EventArgs e)
        {
            load_Editside();
        }

        private void ComEdit_shelf_SelectedIndexChanged(object sender, EventArgs e)
        {
            load_Editslot();
        }

        private void BtEdit_cancelLocal_Click(object sender, EventArgs e)
        {
            btEdit_addLocal.Visible = false;
            btEdit_cancelLocal.Visible = false;

            btEdit_delLocate.Visible = true;
            btEdit_add.Visible = true;

            comEdit_shelf.Text = "";
            comEdit_shelf.Items.Clear();
            comEdit_zone.Text = "";
            comEdit_zone.Items.Clear();
            comEdit_floor.Text = "";
            comEdit_floor.Items.Clear();
            comEdit_side.Text = "";
            comEdit_side.Items.Clear();
            comEdit_slot.Text = "";
            comEdit_slot.Items.Clear();
        }
        private void BtEdit_delLine_Click(object sender, EventArgs e)
        {
            int rowIndex = dataGridView4.CurrentCell.RowIndex;
            try
            {
                if (rowIndex >= 0)
                {
                    dataGridView4.Rows.RemoveAt(rowIndex);
                    dt_line.Rows.RemoveAt(rowIndex);
                    load_editLine();
                }
            }
            catch (Exception ab)
            {
                MessageBox.Show(ab.Message);
            }
        }

        private void BtEdit_cancel_Click(object sender, EventArgs e)
        {
            dt_local.Rows.Clear();
            dt_rank.Rows.Clear();
            dt_line.Rows.Clear();
            tEdit_partno.Text = "";
            tEdit_partname.Text = "";
            tEdit_model.Text = "";
            numEdit_max.Value = 0;
            numEdit_min.Value = 0;
            numEdit_qty.Value = 0;
            tEdit_supplier.Text = "";
            checkEdit_FIFO.Checked = false;
            checkEdit_rank.Checked = false;
            comEdit_partpackage.Text = "";
            comEdit_parttype.Text = "";
            //  comAdd_partpackage.Enabled = false;
            dt_local.Rows.Clear();
            dt_line.Rows.Clear();
            //clear datagridview befor use
            dataGridView5.Rows.Clear();
            dataGridView4.Rows.Clear();
            dataGridView5.Columns.Clear();
            dataGridView4.Columns.Clear();
            //datatable location
            dt_local.Columns.Clear();
            dt_local.Columns.Add("Shelf");
            dt_local.Columns.Add("Zone");
            dt_local.Columns.Add("Floor");
            dt_local.Columns.Add("Side");
            dt_local.Columns.Add("Slot");
            //datatable rank
            dt_rank.Columns.Clear();
            dt_rank.Columns.Add("Rank");
            dt_rank.Columns.Add("Action");
            //datatable line
            dt_line.Columns.Clear();
            dt_line.Columns.Add("Line No");
            load_editLine();
            load_Editlocation();
            load_propertyPart();
            load_typePart();

            TableLocation();
            TableLine();
            bt_Pickup.BackColor = menuButtonUnSelectColor;
            bt_addStock.BackColor = menuButtonUnSelectColor;
            bt_manage.BackColor = menuButtonSelectColor;
            bt_userSetup.BackColor = menuButtonUnSelectColor;
        }
        #endregion

        //log and history
        #region
            private void create_dtlog()
        {
            dt_log.Columns.Clear();
            dt_log.Rows.Clear();
            dt_log.Columns.Add("no");
            dt_log.Columns.Add("transaction_id");
            dt_log.Columns.Add("part_no");
            dt_log.Columns.Add("mcline");
            dt_log.Columns.Add("transaction_type");
            dt_log.Columns.Add("box_id");
            dt_log.Columns.Add("transaction_user");
            dt_log.Columns.Add("eventTime");
            dt_log.Columns.Add("waitTime");
            dataGridView13.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridView13.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridView13.ColumnHeadersDefaultCellStyle.Font = new Font("Tahoma", 12F, FontStyle.Bold);
            dataGridView13.DefaultCellStyle.Font = new Font("Tahoma", 10F);

            dataGridView8.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridView8.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridView8.ColumnHeadersDefaultCellStyle.Font = new Font("Tahoma", 12F, FontStyle.Bold);
            dataGridView8.DefaultCellStyle.Font = new Font("Tahoma", 10F);
        }
        private void log_dropdown()
        {
            cmd = new SqlCommand();
            cmd.Connection = con;
            try
            {
                con.Open();

            }
            catch(Exception ab)
            {

            }
        }
        private void load_tableLog ()
        {
            DataTable temp_logs = new DataTable();
            temp_logs.Columns.Clear();
            temp_logs.Rows.Clear();
            cmd = new SqlCommand();
            cmd.Connection = con;
            dataGridView8.Rows.Clear();
            clog_event.Items.Clear();
             clog_event.Items.Add("AddStock");
             clog_event.Items.Add("PickupStock");
            dt_log_transaction.Columns.Clear();
            dt_log_transaction.Rows.Clear();
            temp_logs.Columns.Add("no");
            temp_logs.Columns.Add("transaction_id");
            temp_logs.Columns.Add("part_no");
            temp_logs.Columns.Add("mcline");
            temp_logs.Columns.Add("transaction_type");
            temp_logs.Columns.Add("box_id");
            temp_logs.Columns.Add("transaction_user");
            temp_logs.Columns.Add("eventTime");
            temp_logs.Columns.Add("waitTime");
            dt_log_transaction.Columns.Add("transaction");

            

            datelog_form.Enabled = false;
            datelog_to.Enabled = false;
            btlog_back.Enabled = false;
            int no = 0;
            count_log = 0;
            count_page_log = 0;
            
            List<Int64> transac = new List<long>();
          //  List<Int64> transaction_id_ = new List<long>();
            List<DateTime> order_time = new List<DateTime>();
            List<DateTime> pickup_time = new List<DateTime>();
        //    temp_logs.Columns.Clear();
        //    temp_logs.Rows.Clear();
           // temp_logs.Columns.Add("order_time");
            TimeSpan wait_time = TimeSpan.Zero;
            try
            {
                con.Open();
                cmd.CommandText = "SELECT DISTINCT transaction_id FROM [THAIARROW_PART_STORE].[dbo].[Transaction]";
                reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        //  transaction_id_.Add(reader.GetInt64(0));
                        string transaction_id_ = reader.GetValue(0).ToString();
                        dt_log_transaction.Rows.Add(transaction_id_);
                    }
                }
                reader.Close();
                cmd.CommandText = "SELECT  a.transaction_id,a.transaction_type,a.transaction_user,a.event_time,b.part_no,b.mcline,b.box_id FROM [THAIARROW_PART_STORE].[dbo].[Transaction] AS a INNER JOIN [THAIARROW_PART_STORE].[dbo].[Transaction_detail] AS b ON a.transaction_id = b.transaction_id ORDER BY event_time ASC ";
                reader = cmd.ExecuteReader();
                if(reader.HasRows)
                {
                    while(reader.Read())
                    {
                        no++;
                       Int64 transaction_id = reader.GetInt64(0);
                        string transaction_type = reader.GetString(1);
                        string transaction_user = reader.GetString(2);
                        DateTime event_time = Convert.ToDateTime(reader.GetValue(3).ToString());
                        string part_no = reader.GetString(4);
                        string mcline = reader.GetValue(5).ToString();
                        string box_id = reader.GetString(6);
                        //dataGridView8.Rows.Add(no,transaction_id, part_no, mcline, transaction_type, box_id, transaction_user, event_time);
                       // dt_log.Rows.Add(no,transaction_id, part_no, mcline, transaction_type, box_id, transaction_user, event_time,wait_time);
                        temp_logs.Rows.Add(no, transaction_id, part_no, mcline, transaction_type, box_id, transaction_user, event_time,wait_time);
                        
                    }
                }
                reader.Close();
                cmd.CommandText = "SELECT a.order_time,a.transaction_id,b.pickup_time FROM [THAIARROW_PART_STORE].[dbo].[Order_event] AS a INNER JOIN [THAIARROW_PART_STORE].[dbo].[Stock_part] AS b ON a.transaction_id = b.pickup_transaction where order_status = 'PickupStock'";
                reader = cmd.ExecuteReader();
                if(reader.HasRows)
                {
                    while(reader.Read())
                    {
                        order_time.Add(reader.GetDateTime(0));
                        transac.Add(reader.GetInt64(1));
                        pickup_time.Add(reader.GetDateTime(2));
                    }
                }
                reader.Close();

                for (int i = 0; i < temp_logs.Rows.Count; i++)
                {
                    int num = int.Parse(temp_logs.Rows[i].ItemArray[0].ToString());

                    Int64 transaction_id = Int64.Parse(temp_logs.Rows[i].ItemArray[1].ToString());

                    string transaction_type = temp_logs.Rows[i].ItemArray[4].ToString();
                    string transaction_user = temp_logs.Rows[i].ItemArray[6].ToString();
                    DateTime event_time = Convert.ToDateTime(temp_logs.Rows[i].ItemArray[7].ToString());
                    string part_no = temp_logs.Rows[i].ItemArray[2].ToString();
                    string mcline = temp_logs.Rows[i].ItemArray[3].ToString();
                    string box_id = temp_logs.Rows[i].ItemArray[5].ToString();
                    wait_time = TimeSpan.Zero;
                    for (int j = 0; j < transac.Count; j++)
                    {
                        if (transac[j] == int.Parse(temp_logs.Rows[i].ItemArray[1].ToString()))
                        {
                            wait_time = (DateTime)pickup_time[j] - (DateTime)order_time[j];
                        }
                    }
                    dt_log.Rows.Add(num, transaction_id, part_no, mcline, transaction_type, box_id, transaction_user, event_time, wait_time.TotalMinutes);
                }
            }
            catch(Exception ab)
            {
                MessageBox.Show(ab.Message);
            }
            finally
            {
                reader.Close();
                con.Close();
                manage_page_log(1,1);    
            }
        }
        private void manage_page_log(int type,int number_start)
        {
           // int i = count_log;
            
            if(number_start == 0)
            {
                count_log = 0;
            }
            int k = count_log;
            try
            {
                
            if(type == 1)
            {
                    if (k + 30 <= dt_log_transaction.Rows.Count && check_firstforward)
                    {
                        dataGridView13.Rows.Clear();
                        btlog_back.Enabled = true;
                        btlog_next.Enabled = true;
                        for (int i = count_log +30; i < k + 60; i++)
                        {
                           // string no = dt_log.Rows[i].ItemArray[0].ToString();
                            string transaction_id = dt_log_transaction.Rows[i].ItemArray[0].ToString();
                          //  string part_no = dt_log.Rows[i].ItemArray[2].ToString();
                            //string mcline = dt_log.Rows[i].ItemArray[3].ToString();
                            //string transaction_type = dt_log.Rows[i].ItemArray[4].ToString();
                            //string box_id = dt_log.Rows[i].ItemArray[5].ToString();
                            //string transaction_user = dt_log.Rows[i].ItemArray[6].ToString();
                            //string eventTime = dt_log.Rows[i].ItemArray[7].ToString();
                            //string wait_time = dt_log.Rows[i].ItemArray[8].ToString();
                           // dataGridView8.Rows.Add(part_no, mcline, transaction_type, box_id, transaction_user, eventTime, wait_time);
                            dataGridView13.Rows.Add(transaction_id);
                        }
                        count_log += 60;
                        count_page_log += 30;
                        check_firstbackward = true;
                        check_firstforward = false;
                        page_log(number_start);
                    }
                    else if (k + 30 <= dt_log_transaction.Rows.Count && !check_firstforward)
                    {
                        dataGridView13.Rows.Clear();
                        btlog_back.Enabled = true;
                        btlog_next.Enabled = true;
                        for (int i = count_log; i < k + 30; i++)
                        {                       
                            string transaction_id = dt_log_transaction.Rows[i].ItemArray[0].ToString();
                            dataGridView13.Rows.Add(transaction_id);
                        }
                        count_log += 30;
                        count_page_log += 30;
                        check_firstbackward = true;
                        check_firstforward = false;
                        page_log(number_start);
                    }
                    if (k+30 > dt_log_transaction.Rows.Count && count_page_log > 30)
                {
                    dataGridView13.Rows.Clear();
                    btlog_next.Enabled = false;
                    btlog_back.Enabled = true;
                    for(int i = count_log; i< dt_log_transaction.Rows.Count; i++)
                    {
                            string transaction_id = dt_log_transaction.Rows[i].ItemArray[0].ToString();
                            dataGridView13.Rows.Add(transaction_id);
                        }
                    count_log +=30;
                    count_page_log += 30;
                    page_log(number_start);
                }
               if(k + 30 > dt_log_transaction.Rows.Count && count_page_log <= 30)
                {
                    dataGridView13.Rows.Clear();
                    btlog_next.Enabled = false;
                    btlog_back.Enabled = true;
                    for (int i = count_log; i < dt_log_transaction.Rows.Count; i++)
                    {
                            string transaction_id = dt_log_transaction.Rows[i].ItemArray[0].ToString();
                            dataGridView13.Rows.Add(transaction_id);
                        }
                    count_log += 30;
                    count_page_log += 30;
                    page_log(number_start);
                }
            }
                if (type == 2)
                {
                    dataGridView13.Rows.Clear();

                    if (count_log > 0)
                    {
                        if (count_log - 30 < dt_log_transaction.Rows.Count && count_log < dt_log_transaction.Rows.Count && check_firstbackward)
                        {
                            dataGridView8.Rows.Clear();
                            btlog_next.Enabled = true;
                            btlog_back.Enabled = true;
                            for (int i = count_log - 60; i < count_log-30; i++)
                            {
                                string transaction_id = dt_log_transaction.Rows[i].ItemArray[0].ToString();
                                dataGridView13.Rows.Add(transaction_id);
                            }
                            count_log -= 60;
                            count_page_log -= 30;
                            check_firstbackward = false;
                            check_firstforward = true;
                            page_log(number_start);
                        }
                        else if (count_log - 30 < dt_log_transaction.Rows.Count && count_log < dt_log_transaction.Rows.Count && !check_firstbackward)
                        {
                            dataGridView13.Rows.Clear();
                            btlog_next.Enabled = true;
                            btlog_back.Enabled = true;
                            for (int i = count_log - 30; i < count_log; i++)
                            {
                                string transaction_id = dt_log_transaction.Rows[i].ItemArray[0].ToString();
                                dataGridView13.Rows.Add(transaction_id);
                            }
                            count_log -= 30;
                            count_page_log -= 30;
                            check_firstbackward = false;
                            check_firstforward = true;
                            page_log(number_start);
                        }
                    }
                    if (count_log > dt_log_transaction.Rows.Count)
                    {
                        dataGridView13.Rows.Clear();
                        btlog_next.Enabled = true;
                        btlog_back.Enabled = true;
                        for (int i = count_log - 60; i < count_log - 30; i++)
                        {
                            string transaction_id = dt_log_transaction.Rows[i].ItemArray[0].ToString();
                            dataGridView13.Rows.Add(transaction_id);
                        }
                        count_log -= 60;
                        count_page_log -= 30;
                        check_firstbackward = false;
                        check_firstforward = false;
                        page_log(number_start);
                    }

                    if (count_log == 0)
                    {
                        if (dt_log_transaction.Rows.Count < 30)
                        {
                            dataGridView13.Rows.Clear();
                            btlog_back.Enabled = false;
                            btlog_next.Enabled = true;
                            for (int i = 0; i < (dt_log_transaction.Rows.Count % 30); i++)
                            {
                                string transaction_id = dt_log_transaction.Rows[i].ItemArray[0].ToString();
                                dataGridView13.Rows.Add(transaction_id);
                            }
                            count_log += 30;
                            count_page_log += 30;
                            check_firstforward = false;
                            check_firstbackward = false;
                            page_log(number_start);
                        }
                        else
                        {
                            btlog_back.Enabled = false;
                            btlog_next.Enabled = true;
                            check_firstforward = false;
                            check_firstbackward = false;
                            count_page_log = 0;
                        }

                    }
                }
            }
            catch (Exception ab)
            {
                MessageBox.Show(ab.Message);
            }
        }
        private void page_log(int number_start)
        {
            int page = dt_log_transaction.Rows.Count / 30;
            int page_plus = dt_log_transaction.Rows.Count % 30;
            int current_page = 1;
            int maxpage = 1;
            if (count_page_log == 0)
            {
                current_page = 1;
            }
            else
            {
                current_page = count_page_log / 30;
            }
            if(page_plus == 0)
            {
                maxpage = page;
            }
            else
            {
                maxpage = page + 1;
            }
            if (number_start == 1)
            {
                tlog_page.Text = "Page :" + current_page.ToString() + "/" + maxpage.ToString();
            }
            else
            {
                if (dt_log_transaction.Rows.Count <= 30)
                {
                    tlog_page.Text = "Page :" + "1" + "/" + "1";
                    btlog_next.Enabled = false;
                    btlog_back.Enabled = false;
                }
                else
                {
                    tlog_page.Text = "Page :" + "1" + "/" + maxpage.ToString();
                    count_page_log = 30;
                    btlog_back.Enabled = false;
                }
            }
        }
        private void Btlog_next_Click(object sender, EventArgs e)
        {
            manage_page_log(1,1);
        }
        private void Btlog_back_Click(object sender, EventArgs e)
        {
            manage_page_log(2,1);
        }
        //gridview transaction
        private void DataGridView13_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            fill_detail_loggrid();
        }
        private void fill_detail_loggrid()
        {
            cmd = new SqlCommand();
            cmd.Connection = con;
            try
            {
                dataGridView8.Rows.Clear();
           
                int index = dataGridView13.CurrentRow.Index;
                string transaction_id = dataGridView13.Rows[index].Cells[0].Value.ToString();
                for(int i = 0; i < dt_log.Rows.Count; i++)
                {
                    if(transaction_id == dt_log.Rows[i].ItemArray[1].ToString())
                    {
                        string part_no = dt_log.Rows[i].ItemArray[2].ToString();
                        string mcline = dt_log.Rows[i].ItemArray[3].ToString();
                        string transaction_type = dt_log.Rows[i].ItemArray[4].ToString();
                        string box_id = dt_log.Rows[i].ItemArray[5].ToString();
                        string transaction_user = dt_log.Rows[i].ItemArray[6].ToString();
                        string eventTime = dt_log.Rows[i].ItemArray[7].ToString();
                        string wait_time = dt_log.Rows[i].ItemArray[8].ToString();
                        dataGridView8.Rows.Add(part_no, mcline, transaction_type, box_id, transaction_user, eventTime, wait_time);
                    }
                }

            }
            catch (Exception ab)
            {
                MessageBox.Show(ab.Message);
            }
        }

        private void Btlog_search_Click(object sender, EventArgs e)
        {
            string event_id = tlog_eventid.Text;
            string event_ = "";
            string part_no = tlog_partno.Text;
            string part = "";
            string mcline = tlog_qty.Text;
            string mcline_in = "";
            string eventtype = clog_event.Text;
            string box_id = tlog_boxid.Text;
            DateTime from = datelog_form.Value;
            DateTime to = datelog_to.Value;
            dataGridView8.Rows.Clear();
            int no = 0;
            bool search_transactionid = false;
            bool search_part = false;
            bool search_mcline = false;
            bool search_event = false;
            bool search_boxid = false;
            bool search_user = false;
            //bool searcg_time = false;
            DataTable temp_logs = new DataTable();
            TimeSpan wait_time = TimeSpan.Zero;
            List<Int64> transac = new List<long>();
            List<Int64> tran = new List<long>();
            List<DateTime> order_time = new List<DateTime>();
            List<DateTime> pickup_time = new List<DateTime>();

            temp_logs.Columns.Add("no");
            temp_logs.Columns.Add("transaction_id");
            temp_logs.Columns.Add("part_no");
            temp_logs.Columns.Add("mcline");
            temp_logs.Columns.Add("transaction_type");
            temp_logs.Columns.Add("box_id");
            temp_logs.Columns.Add("transaction_user");
            temp_logs.Columns.Add("eventTime");
            temp_logs.Columns.Add("waitTime");

            bool check_load = true;
            cmd = new SqlCommand();
            cmd.Connection = con;
            try
            {
                con.Open();
                if(event_id !="" || event_id != null)
                {
                    dt_log_transaction.Rows.Clear();
                    for (int i = 0; i <  dt_log_transaction.Rows.Count; i++)
                    {
                        if(event_id == dt_log_transaction.Rows[i].ItemArray[0].ToString())
                        {
                            
                            dt_log_transaction.Rows.Add(event_id);
                        }
                    }
                }
                if (checklog_date.Checked)
                {
                    datelog_form.Enabled = true;
                    datelog_to.Enabled = true;
                }
                else
                {
                    datelog_form.Enabled = false;
                    datelog_to.Enabled = false;
                }
                if (event_id == "" || event_id == null)
                {
                    search_transactionid = false;
                }
                else
                {
                    search_transactionid = true;
                }
                if (part_no == "" || part_no == null)
                {
                    search_part = false;
                }
                else
                {
                    search_part = true;
                }
                if (mcline == "" || mcline == null)
                {
                    search_mcline = false;
                }
                else
                {
                    search_mcline = true;
                }
                if (eventtype == "" || eventtype == null)
                {
                    search_event = false;
                }
                else
                {
                    search_event = true;
                }
                if (box_id == "" || box_id == null)
                {
                    search_boxid = false;
                }
                else
                {
                    search_boxid = true;
                }
                if (search_transactionid || search_part || search_mcline || search_event || search_boxid)
                {
                    string sql = "";
                    sql = "SELECT a.transaction_id,a.transaction_type,a.transaction_user,a.event_time,b.part_no,b.mcline,b.box_id FROM[THAIARROW_PART_STORE].[dbo].[Transaction] AS a INNER JOIN[THAIARROW_PART_STORE].[dbo].[Transaction_detail] AS b ON a.transaction_id = b.transaction_id  ";
                    sql += "where ";
                    if (search_transactionid)
                    {
                        sql += " a.transaction_id ='" + event_id + "' AND";
                    }
                    if (search_part)
                    {
                        sql += " b.part_no ='" + part_no + "' AND";
                    }
                    if (search_mcline)
                    {
                        sql += " b.mcline ='" + mcline + "' AND";
                    }
                    if (search_event)
                    {
                        sql += " a.transaction_type ='" + eventtype + "' AND";
                    }
                    if (checklog_date.Checked)
                    {
                        sql += " a.event_time BETWEEN '" + from.ToString("yyyy-MM-dd HH:mm") + "' AND'" + to.ToString("yyyy-MM-dd HH:mm") + "' AND";
                    }
                    sql = sql.Substring(0, sql.Length - 4);
                    dt_log.Rows.Clear();
                    dataGridView13.Rows.Clear();
                    //dataGridView8.Rows.Clear();
                    
                    cmd.CommandText = sql;
                    reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            no++;
                            Int64 transaction_id = reader.GetInt64(0);
                            string transaction_type = reader.GetString(1);
                            string transaction_user = reader.GetString(2);
                            DateTime event_time = Convert.ToDateTime(reader.GetValue(3).ToString());
                            string partNo = reader.GetString(4);
                            string mcline_ = reader.GetValue(5).ToString();
                            string boxID = reader.GetString(6);
                            //dataGridView8.Rows.Add(no,transaction_id, part_no, mcline, transaction_type, box_id, transaction_user, event_time);
                            // dt_log.Rows.Add(no, transaction_id, partNo, mcline_, transaction_type, boxID, transaction_user, event_time);
                            temp_logs.Rows.Add(no, transaction_id, partNo, mcline_, transaction_type, boxID, transaction_user, event_time);
                        }
                        check_load = true;
                    }
                    else
                    {
                        MessageBox.Show("ไม่พบข้อมูล");
                        reader.Close();
                        con.Close();
                        check_load = false;
                        load_tableLog();
                        
                    }
                    reader.Close();
                    if (check_load)
                    {
                        cmd.CommandText = "SELECT a.order_time,a.transaction_id,b.pickup_time FROM [THAIARROW_PART_STORE].[dbo].[Order_event] AS a INNER JOIN [THAIARROW_PART_STORE].[dbo].[Stock_part] AS b ON a.transaction_id = b.pickup_transaction where order_status = 'PickupStock'";
                        reader = cmd.ExecuteReader();
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                order_time.Add(reader.GetDateTime(0));
                                transac.Add(reader.GetInt64(1));
                                pickup_time.Add(reader.GetDateTime(2));
                            }
                        }
                        reader.Close();

                        for (int i = 0; i < temp_logs.Rows.Count; i++)
                        {
                            int num = int.Parse(temp_logs.Rows[i].ItemArray[0].ToString());

                            Int64 transaction_id = Int64.Parse(temp_logs.Rows[i].ItemArray[1].ToString());

                            string transaction_type = temp_logs.Rows[i].ItemArray[4].ToString();
                            string transaction_user = temp_logs.Rows[i].ItemArray[6].ToString();
                            DateTime event_time = Convert.ToDateTime(temp_logs.Rows[i].ItemArray[7].ToString());
                            string partNO = temp_logs.Rows[i].ItemArray[2].ToString();
                            string mcline_ = temp_logs.Rows[i].ItemArray[3].ToString();
                            string boxid = temp_logs.Rows[i].ItemArray[5].ToString();
                            wait_time = TimeSpan.Zero;
                            for (int j = 0; j < transac.Count; j++)
                            {
                                if (transac[j] == int.Parse(temp_logs.Rows[i].ItemArray[1].ToString()))
                                {
                                    wait_time = (DateTime)pickup_time[j] - (DateTime)order_time[j];
                                }
                            }
                            tran.Add(transaction_id);
                               dt_log.Rows.Add(num, transaction_id, partNO, mcline_, transaction_type, boxid, transaction_user, event_time, wait_time.TotalMinutes);
                          //  dt_log_transaction.Rows.Add(transaction_id);
                            dataGridView8.Rows.Add(partNO, mcline_, transaction_type, boxid, transaction_user, event_time, wait_time.TotalMinutes);
                        }

                        con.Close();
                        Int64[] tran_distinct = tran.Distinct().ToArray();
                        for(int i = 0; i < tran_distinct.Length; i++)
                        {
                            dt_log_transaction.Rows.Add(tran_distinct.GetValue(i));
                        }
                        if (dt_log_transaction.Rows.Count > 20) { manage_page_log(1, 0); }
                        else { manage_page_log(2, 0); }
                    }
                }
                else
                {
                    MessageBox.Show("กำลังเริ่มต้นใหม่");
                    reader.Close();
                    con.Close();
                    load_tableLog();
                }
            }
            catch(Exception ab)
            {
                MessageBox.Show(ab.Message);
            }
            finally
            {
                reader.Close();
                con.Close();
                
            }
        }
    

        private void Checklog_date_CheckedChanged(object sender, EventArgs e)
        {
            if (checklog_date.Checked)
            {
                datelog_form.Enabled = true;
                datelog_to.Enabled = true;
            }
            else
            {
                datelog_form.Enabled = false;
                datelog_to.Enabled = false;
            }
        }


        #endregion

        //edit stock region
        #region
        private void fill_table_editstock()
        {
            cmd = new SqlCommand();
            cmd.Connection = con;
            
            dataGridView9.Rows.Clear();
            dt_editstock.Rows.Clear();
            try
            {
                con.Open();
                cmd.CommandText = "SELECT *  FROM [THAIARROW_PART_STORE].[dbo].[Master_part_data]";
                reader = cmd.ExecuteReader();
                if(reader.HasRows)
                {
                    while(reader.Read())
                    {
                        string part_no = reader.GetValue(1).ToString();
                        string partname = reader.GetValue(2).ToString();
                        string model = reader.GetValue(3).ToString();
                        string maxstock = reader.GetValue(10).ToString();
                        string minstock = reader.GetValue(11).ToString();
                        string qty = reader.GetValue(12).ToString();
                        string create_user = reader.GetValue(13).ToString();
                        string create_date = reader.GetValue(14).ToString();
                        string update_user = reader.GetValue(15).ToString();
                        string update_date = reader.GetValue(16).ToString();

                        string supplier = reader.GetValue(17).ToString();
                        string part_type = reader.GetValue(18).ToString();
                        string part_package = reader.GetValue(19).ToString();
                      

                        string stock = reader.GetValue(8).ToString();
                        dataGridView9.Rows.Add(part_no, stock);
                        dt_editstock.Rows.Add(part_no, partname, model, stock, maxstock, minstock, qty, create_user, create_date, update_user, update_date, supplier, part_type, part_package);
                    }
                }
            }
            catch(Exception ab)
            {
                MessageBox.Show(ab.Message);
            }
            finally
            {
                reader.Close();
                con.Close();
                enable_textEditStock();
                fill_table_editstockpart();//load datagridview 10
            }
        }
        private void editstock_location ()
        {
            int index = dataGridView9.CurrentCell.RowIndex;
            string part_no = dataGridView9.Rows[index].Cells[0].Value.ToString();
            string slot = "";
            dt_editstock_local.Rows.Clear();
            cmd = new SqlCommand();
            cmd.Connection = con;
            try
            {
                con.Open();
                cmd.CommandText = "SELECT shelf,zone,floor,side,slot FROM [THAIARROW_PART_STORE].[dbo].[Master_location] where part_no_registered ='"+part_no +"'";
                reader = cmd.ExecuteReader();
                if(reader.HasRows)
                {
                    while(reader.Read())
                    {
                        string shelf = reader.GetValue(0).ToString();
                        string zone = reader.GetValue(1).ToString();
                        string floor = reader.GetValue(2).ToString();
                        string side = reader.GetValue(3).ToString();
                        slot = slot + "," + reader.GetValue(4).ToString();
                        slot = slot.Substring(1, slot.Length - 1);
                        dt_editstock_local.Rows.Add(shelf, zone, floor,side, slot);
                    }
                }
            }
            catch(Exception ab)
            {
                MessageBox.Show(ab.Message);
            }
            finally
            {
                reader.Close();
                con.Close();
            }
        }
        private void DataGridView9_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            fill_table_editstockpart();
            editstock_location();
        }
        private void fill_table_editstockpart()
        {
            
            cmd = new SqlCommand();
            cmd.Connection = con;
            try
            {
                int index = dataGridView9.CurrentCell.RowIndex;
                string part_no = dataGridView9.Rows[index].Cells[0].Value.ToString();
                dataGridView10.Rows.Clear();

                con.Open();
                cmd.CommandText = "SELECT * FROM [THAIARROW_PART_STORE].[dbo].[Stock_part] where part_no ='"+part_no+"'";
                reader = cmd.ExecuteReader();
                if(reader.HasRows)
                {
                    while(reader.Read())
                    {
                        string box = reader.GetValue(1).ToString();
                        string rank = reader.GetValue(3).ToString();
                        string stock_lot_id = reader.GetValue(11).ToString();
                        string stock_time = reader.GetValue(12).ToString();
                        string stock_transaction = reader.GetValue(13).ToString();
                        string stock_user = reader.GetValue(14).ToString();
                        string stock_status = reader.GetValue(15).ToString();
                        string pickupTime = reader.GetValue(16).ToString();
                        string pickup_transaction = reader.GetValue(17).ToString();
                        string pickup_user = reader.GetValue(18).ToString();
                        string pickup_line = reader.GetValue(19).ToString();
                        dataGridView10.Rows.Add(box, rank, stock_lot_id, stock_time, stock_transaction, stock_user, stock_status, pickupTime, pickup_transaction, pickup_user, pickup_line);
                       
                    }
                }
            }
            catch(Exception ac)
            {
                MessageBox.Show(ac.Message);
            }
            finally
            {
                reader.Close();
                con.Close();
               
                
            }
        }
        //datatable editstock header
        private void create_dtEditstock()
        {
         
            dt_editstock.Rows.Clear();
            dt_editstock.Columns.Clear();
            dt_editstock.Columns.Add("partno");
            dt_editstock.Columns.Add("partname");
            dt_editstock.Columns.Add("model");
            dt_editstock.Columns.Add("stock");
            dt_editstock.Columns.Add("max");
            dt_editstock.Columns.Add("min");
            dt_editstock.Columns.Add("qty");
            dt_editstock.Columns.Add("cerateuser");
            dt_editstock.Columns.Add("createdate");
            dt_editstock.Columns.Add("updateuser");
            dt_editstock.Columns.Add("updatedate");
            dt_editstock.Columns.Add("supplier");
            dt_editstock.Columns.Add("part_type");
            dt_editstock.Columns.Add("part_package");

            dt_editstock_local.Columns.Clear();
            dt_editstock_local.Rows.Clear();
            dt_editstock_local.Columns.Add("shelf");
            dt_editstock_local.Columns.Add("zone");
            dt_editstock_local.Columns.Add("floor");
            dt_editstock_local.Columns.Add("side");
            dt_editstock_local.Columns.Add("slot");

        }
        private void enable_textEditStock()
        {
            ces_stockstatus.Items.Clear();
            ces_stockstatus.Items.Add("InStock");
            ces_stockstatus.Items.Add("PickupStock");
            tes_boxid.Enabled = false;
            tes_maker.Enabled = false;
            tes_area.Enabled = false;
            tes_slot.Enabled = false;
            tes_userin.Enabled = false;
            tes_qty.Enabled = false;
            tes_shelf.Enabled = false;
            tes_maxstock.Enabled = false;
            tes_timein.Enabled = false;
            tes_rank.Enabled = false;
            tes_zone.Enabled = false;
            tes_minstock.Enabled = false;
            tes_stock.Enabled = false;
            tes_side.Enabled = false;

            ces_pickupuser.Enabled = false;
            ces_pickupline.Enabled = false;
            des_pickuptime.Enabled = false;
        }

        private void DataGridView10_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            fill_textbox_editstock();
        }
        private void fill_textbox_editstock()
        {
            try
            {
                int index = dataGridView10.CurrentCell.RowIndex;
                int index_9 = dataGridView9.CurrentCell.RowIndex;

                tes_boxid.Text = dataGridView10.Rows[index].Cells[0].Value.ToString();
                tes_maker.Text = dt_editstock.Rows[index_9].ItemArray[11].ToString();
                tes_area.Text = dt_editstock_local.Rows[0].ItemArray[0].ToString(); // shelf
                tes_slot.Text = dt_editstock_local.Rows[0].ItemArray[4].ToString();
                tes_userin.Text = dt_editstock.Rows[index_9].ItemArray[7].ToString();
                tes_qty.Text = dt_editstock.Rows[index_9].ItemArray[6].ToString();
                tes_shelf.Text = dt_editstock_local.Rows[0].ItemArray[3].ToString(); //floor
                tes_maxstock.Text = dt_editstock.Rows[index_9].ItemArray[4].ToString();
                tes_timein.Text = dt_editstock.Rows[index_9].ItemArray[9].ToString();
                tes_rank.Text = dataGridView10.Rows[index].Cells[1].Value.ToString();
                tes_zone.Text = dt_editstock_local.Rows[0].ItemArray[1].ToString();
                tes_minstock.Text = dt_editstock.Rows[index].ItemArray[5].ToString();
                tes_stock.Text = dt_editstock.Rows[index_9].ItemArray[3].ToString();
                tes_partname.Text = dt_editstock.Rows[index_9].ItemArray[1].ToString();
                ces_stockstatus.Text = dataGridView10.Rows[index].Cells[6].Value.ToString();
                tes_side.Text = dt_editstock_local.Rows[0].ItemArray[3].ToString();
            }
            catch(Exception ab)
            {
                MessageBox.Show(ab.Message);

            }
          
        }
        //fill text box search part 
        private void textbox_searchpart()
        {
            string box_id = dataGridView10.Rows[dataGridView10.CurrentCell.RowIndex].Cells[0].Value.ToString();
            cmd = new SqlCommand();
            cmd.Connection = con;
            string part_no = "";
            if(box_id != "")
            {
                try
                {
                    con.Open();
                    cmd.CommandText = "SELECT part_no FROM [THAIARROW_PART_STORE].[dbo].[Stock_part] where box_id ='"+box_id+"'";
                    reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while(reader.Read())
                        {
                            part_no = reader.GetValue(0).ToString();
                        }
                    }
                    tes_searchpart.Text = part_no;
                    reader.Close();
                    con.Close();

                }
                catch(Exception ab)
                {
                    MessageBox.Show(ab.Message);
                }
            }
            else
            {

            }
        }
        // ok button in edit stock page
        private void BtEditstock_OK_Click(object sender, EventArgs e)
        {
            //Process edit stock OK
            //1.alert yes/no question
            //2.update db stock part -> stock_status
            //3.in db transaction fill column detail and transactintype edit
            //4.fill data in db transaction_detail
            DialogResult result = MessageBox.Show("Do you want to edit stock ?", "Edit Stock", MessageBoxButtons.YesNo);
            if(result == DialogResult.Yes)
            {
                if (dataGridView10.Rows.Count > 1)
                {
                    string part_no = "";
                    try
                    {
                        textbox_searchpart();
                        DateTime time = DateTime.Now;
                        string user = Login.userName;
                        string stock_status = ces_stockstatus.Text;
                        if (stock_status == "") { stock_status = dataGridView10.Rows[dataGridView10.CurrentCell.RowIndex].Cells[6].Value.ToString(); }
                        string box_id = dataGridView10.Rows[dataGridView10.CurrentCell.RowIndex].Cells[0].Value.ToString();
                        part_no = tes_searchpart.Text;
                        string pickup_line = ces_pickupline.Text;
                        string pickup_user = ces_pickupuser.Text;
                        DateTime pickup_time = des_pickuptime.Value;


                        string transaction_id = transaction_editstock_ok(1, time, user, box_id, part_no);
                        int count_part = adjust_amount_editstock(1, part_no);
                        cmd = new SqlCommand();
                        cmd.Connection = con;
                        con.Open();
                        // update db part stock
                        if (stock_status == "InStock")
                        {
                            cmd.CommandText = "UPDATE [THAIARROW_PART_STORE].[dbo].[Stock_part] SET stock_status ='" + stock_status + "', pickup_time = null,  pickup_transaction = null ,pickup_user = null ,pickup_line = null where box_id ='" + box_id + "'";
                        }
                        if (stock_status == "PickupStock")
                        {
                            cmd.CommandText = "UPDATE [THAIARROW_PART_STORE].[dbo].[Stock_part] SET stock_status ='" + stock_status + "', pickup_time ='" + Convert.ToDateTime(pickup_time) + "',  pickup_transaction ='" + transaction_id + "',pickup_user ='" + pickup_user + "' ,pickup_line ='" + pickup_line + "' where box_id ='" + box_id + "'";
                        }
                        int b = cmd.ExecuteNonQuery();
                        if (b == 0)
                        {
                            MessageBox.Show("เกิดข้อผิดพลาดขณะแก้ stock");
                        }
                        //update db partdata -> stock_available
                        cmd.CommandText = "UPDATE [THAIARROW_PART_STORE].[dbo].[Master_part_data] SET stock_available ='" + count_part + "' where part_no ='" + part_no + "'";
                        b = cmd.ExecuteNonQuery();
                        if (b == 0)
                        {
                            MessageBox.Show("เกิดข้อผิดพลาดขณะแก้ stock_available");
                        }


                    }
                    catch (Exception ab)
                    {
                        MessageBox.Show(ab.Message);
                    }
                    finally
                    {
                        reader.Close();
                        con.Close();

                        fill_table_editstockpart();//load datagridview 10
                    }
                }
                else
                {
                    MessageBox.Show("ไม่มีรายการที่จะแก้ไข","Error",MessageBoxButtons.OK,MessageBoxIcon.Error);
                }
            }
            else if (result == DialogResult.No)
            {

            }
        }
        // del button in edit stock page
        private void BtEditstock_del_Click(object sender, EventArgs e)
        {
            //Process edit stock DELETE
            //1.alert yes/no question
            //2.update db stock part -> stock_status
            //3.in db transaction fill column detail and transactintype edit
            //4.fill data in db transaction_detail
            DialogResult result = MessageBox.Show("Do you want to delete stock ?", "Edit Stock", MessageBoxButtons.YesNo);
            if (result == DialogResult.Yes)
            {
            try
            {
                    DateTime time = DateTime.Now;
                    string user = Login.userName;
                    string stock_status = ces_stockstatus.Text;
                    string box_id = tes_boxid.Text;
                    string part_no = dataGridView9.Rows[dataGridView9.CurrentCell.RowIndex].Cells[0].Value.ToString();
                    string pickup_line = ces_pickupline.Text;
                    string pickup_user = ces_pickupuser.Text;
                    DateTime pickup_time = des_pickuptime.Value;
                    
                    string transaction_id = transaction_editstock_ok(2, time, user, box_id, part_no);

                    cmd = new SqlCommand();
                    cmd.Connection = con;
                    con.Open();
                    cmd.CommandText = "DELETE  FROM [THAIARROW_PART_STORE].[dbo].[Stock_part] where box_id ='"+box_id+"'";
                    int b = cmd.ExecuteNonQuery();
                    if (b == 0)
                    {
                        MessageBox.Show("เกิดข้อผิดพลาดขณะลบ stock");
                    }
                    con.Close();
                    //update db partdata -> stock_available
                    int count_part = adjust_amount_editstock(1, part_no);
                    con.Open();
                    cmd.CommandText = "UPDATE [THAIARROW_PART_STORE].[dbo].[Master_part_data] SET stock_available ='" + count_part + "' where part_no ='" + part_no + "'";
                    b = cmd.ExecuteNonQuery();
                    if (b == 0)
                    {
                        MessageBox.Show("เกิดข้อผิดพลาดขณะแก้ stock_available");
                    }
                }
                catch (Exception ab)
                {
                    MessageBox.Show(ab.Message);
                }
                finally
                {
                    con.Close();
                    fill_table_editstockpart();//load datagridview 10
                }

            }
            else if (result == DialogResult.No)
            {

            }
        }
        private string transaction_editstock_ok(int d,DateTime time,string user,string boxid,string part_no)
        {
            //update db transaction and db transaction detail
            string transaction_id = "";
            string pickup_line = ces_pickupline.Text;
            string detail = "";
            cmd = new SqlCommand();
            cmd.Connection = con;
            if (d == 1)
            {
                try
                {
                    con.Open();
                    //prepare column detail in transaction
                    cmd.CommandText = "SELECT * FROM [THAIARROW_PART_STORE].[dbo].[Stock_part] where box_id ='"+boxid+"'";
                    reader = cmd.ExecuteReader();
                    if(reader.HasRows)
                    {
                        while(reader.Read())
                        {
                            string stock_id = reader.GetValue(0).ToString();
                            string rank = reader.GetValue(3).ToString();
                            string fullcode = reader.GetValue(7).ToString();
                            string first_scan = reader.GetValue(8).ToString();
                            string first_scan_date = reader.GetValue(9).ToString();
                            string qty = reader.GetValue(10).ToString();
                            string stock_lot = reader.GetValue(11).ToString();
                            string stock_time = reader.GetValue(12).ToString();
                            string stock_transaction = reader.GetValue(13).ToString();
                            string stock_user = reader.GetValue(14).ToString();
                            string stock_status = reader.GetValue(15).ToString();
                            string pickup_time = reader.GetValue(16).ToString();
                            string pickup_tran = reader.GetValue(17).ToString();
                            string pickup_user = reader.GetValue(18).ToString();
                            detail = stock_id + "," + boxid + "," + part_no + "," + rank + "," + fullcode + "," + first_scan + "," + first_scan_date + "," + qty + "," + stock_lot + "," + stock_time + "," + stock_transaction + "," + stock_user + "," + stock_status + "," + pickup_time + "," + pickup_tran + "," + pickup_user + "," + pickup_line;
                        }
                    }
                    reader.Close();

                    //insert transaction db
                    
                    cmd.CommandText = "INSERT INTO [THAIARROW_PART_STORE].[dbo].[Transaction] (transaction_type,transaction_user,event_time,detail) VALUES ('EditStock','" + user + "','" + time + "','"+detail+"')";
                    int b = cmd.ExecuteNonQuery();
                    if (b == 0)
                    {
                        MessageBox.Show("เกิดข้อผิดพลาดที่ db.transaction");
                    }
                    cmd.CommandText = "SELECT transaction_id FROM [THAIARROW_PART_STORE].[dbo].[Transaction] where event_time ='" + time + "'";
                    reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            transaction_id = reader.GetValue(0).ToString();
                        }
                    }
                    reader.Close();
                    cmd.CommandText = "INSERT INTO [THAIARROW_PART_STORE].[dbo].[Transaction_detail] (transaction_id,part_no,mcline,box_id) VALUES ('" + transaction_id + "','" + part_no + "','" + pickup_line + "','" + boxid + "')";
                    int a = cmd.ExecuteNonQuery();
                    if (b == 0)
                    {
                        MessageBox.Show("เกิดข้อผิดพลาดขณะทำการแก้ไข stock db.transaction_detail");
                    }
                }
                catch (Exception ab)
                {
                    MessageBox.Show(ab.Message);
                }
                finally
                {
                    con.Close();
                }
            }
            if(d == 2)
            {
                try
                {
                    con.Open();
                    //prepare column detail in transaction
                    cmd.CommandText = "SELECT * FROM [THAIARROW_PART_STORE].[dbo].[Stock_part] where box_id ='" + boxid + "'";
                    reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            string stock_id = reader.GetValue(0).ToString();
                            string rank = reader.GetValue(3).ToString();
                            string fullcode = reader.GetValue(7).ToString();
                            string first_scan = reader.GetValue(8).ToString();
                            string first_scan_date = reader.GetValue(9).ToString();
                            string qty = reader.GetValue(10).ToString();
                            string stock_lot = reader.GetValue(11).ToString();
                            string stock_time = reader.GetValue(12).ToString();
                            string stock_transaction = reader.GetValue(13).ToString();
                            string stock_user = reader.GetValue(14).ToString();
                            string stock_status = reader.GetValue(15).ToString();
                            string pickup_time = reader.GetValue(16).ToString();
                            string pickup_tran = reader.GetValue(17).ToString();
                            string pickup_user = reader.GetValue(18).ToString();
                            detail = stock_id + "," + boxid + "," + part_no + "," + rank + "," + fullcode + "," + first_scan + "," + first_scan_date + "," + qty + "," + stock_lot + "," + stock_time + "," + stock_transaction + "," + stock_user + "," + stock_status + "," + pickup_time + "," + pickup_tran + "," + pickup_user + "," + pickup_line;
                        }
                    }
                    reader.Close();
                    //insert transaction db
       
                    cmd.CommandText = "INSERT INTO [THAIARROW_PART_STORE].[dbo].[Transaction] (transaction_type,transaction_user,event_time,detail) VALUES ('DeleteStock','" + user + "','" + time + "','"+detail+"')";
                    int b = cmd.ExecuteNonQuery();
                    if (b == 0)
                    {
                        MessageBox.Show("เกิดข้อผิดพลาดที่ db.transaction");
                    }
                    cmd.CommandText = "SELECT transaction_id FROM [THAIARROW_PART_STORE].[dbo].[Transaction] where event_time ='" + time + "'";
                    reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            transaction_id = reader.GetValue(0).ToString();
                        }
                    }
                    reader.Close();
                    cmd.CommandText = "INSERT INTO [THAIARROW_PART_STORE].[dbo].[Transaction_detail] (transaction_id,part_no,mcline,box_id) VALUES ('" + transaction_id + "','" + part_no + "','" + pickup_line + "','" + boxid + "')";
                    int a = cmd.ExecuteNonQuery();
                    if (b == 0)
                    {
                        MessageBox.Show("เกิดข้อผิดพลาดขณะทำการลบ stock db.transaction_detail");
                    }
                }
                catch (Exception ab)
                {
                    MessageBox.Show(ab.Message);
                }
                finally
                {
                    con.Close();
                }
            }
            return transaction_id;

        }
        private void Ces_stockstatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            check_stockstatus();
        }
        private void check_stockstatus()
        {
            if(ces_stockstatus.Text == "PickupStock")
            {
                ces_pickupline.Enabled = true;
                ces_pickupuser.Enabled = true;
                des_pickuptime.Enabled = true;
                dropdown_editstock_status();
            }
            if(ces_stockstatus.Text == "InStock")
            {
                ces_pickupline.Enabled = false;
                ces_pickupuser.Enabled = false;
                des_pickuptime.Enabled = false;
                ces_pickupline.Items.Clear();
                ces_pickupuser.Items.Clear();
               // ces_pickupuser.Text = "";
                
                
            }
        }
        private void dropdown_editstock_status()
        {
            string partno = dataGridView9.Rows[dataGridView9.CurrentCell.RowIndex].Cells[0].Value.ToString();
            ces_pickupline.Items.Clear();
            ces_pickupuser.Items.Clear();
            cmd = new SqlCommand();
            cmd.Connection = con;
            try
            {
                con.Open();
                cmd.CommandText = "SELECT line_no FROM [THAIARROW_PART_STORE].[dbo].[Master_line_part] where part_no ='"+partno+"'";
                reader = cmd.ExecuteReader();
                if(reader.HasRows)
                {
                    while(reader.Read())
                    {
                        string line = reader.GetValue(0).ToString();
                        ces_pickupline.Items.Add(line);
                    }
                }
                reader.Close();
                cmd.CommandText = "SELECT username FROM [THAIARROW_PART_STORE].[dbo].[User_data]";
                reader = cmd.ExecuteReader();
                if(reader.HasRows)
                {
                    ces_pickupuser.Text = Login.userName;
                    while(reader.Read())
                    {
                        string user = reader.GetValue(0).ToString();
                        ces_pickupuser.Items.Add(user);
                    }
                }
                reader.Close();
            }
            catch(Exception ab)
            {
                MessageBox.Show(ab.Message);
            }
            finally
            {
                
                con.Close();
            }
        }
        private int adjust_amount_editstock(int d,string part_no)
        {
            int count = 0;
            string box_id = "";
            cmd = new SqlCommand();
            cmd.Connection = con;
            try
            {
                con.Open();
                cmd.CommandText = "SELECT box_id FROM [THAIARROW_PART_STORE].[dbo].[Stock_part] where part_no ='"+part_no+"'";
                reader = cmd.ExecuteReader();
                if(reader.HasRows)
                {
                    while(reader.Read())
                    {
                        box_id = reader.GetValue(0).ToString();
                        count++;
                    }
                }
            }
            catch(Exception ab)
            {
                MessageBox.Show(ab.Message);
            }
            finally
            {
                reader.Close();
                con.Close();
            }

            return count;
        }
        //search in table
        private void BtEditstock_search_Click(object sender, EventArgs e)
        {
            cmd = new SqlCommand();
            cmd.Connection = con;
            string search = tes_searchetc.Text;
            string search_part = tes_searchpart.Text;
            if (search != ""  || search_part != "")
            {
                if (search_part != "" && search =="")
                {
                    try
                    {
                        con.Open();
                        cmd.CommandText = "SELECT * FROM [THAIARROW_PART_STORE].[dbo].[Master_part_data] where part_no Like '%"+search_part+"%'";
                        reader = cmd.ExecuteReader();
                        if (reader.HasRows)
                        {
                            dataGridView9.Rows.Clear();
                            dt_editstock.Rows.Clear();
                            while (reader.Read())
                            {
                                string partNo = reader.GetValue(1).ToString();
                                string partname = reader.GetValue(2).ToString();
                                string model = reader.GetValue(3).ToString();
                                string maxstock = reader.GetValue(10).ToString();
                                string minstock = reader.GetValue(11).ToString();
                                string qty = reader.GetValue(12).ToString();
                                string create_user = reader.GetValue(13).ToString();
                                string create_date = reader.GetValue(14).ToString();
                                string update_user = reader.GetValue(15).ToString();
                                string update_date = reader.GetValue(16).ToString();

                                string supplier = reader.GetValue(17).ToString();
                                string part_type = reader.GetValue(18).ToString();
                                string part_package = reader.GetValue(19).ToString();


                                int stock_available = int.Parse(reader.GetValue(8).ToString());

                                // dt_partData.Rows.Add(partNo, partName, model, fifo, rank, max, min, qty, createUser, createDate, updateUser, supplier);
                                dataGridView9.Rows.Add(partNo, stock_available);
                                dt_editstock.Rows.Add(partNo, partname, model, stock_available, maxstock, minstock, qty, create_user, create_date, update_user, update_date, supplier, part_type, part_package);
                                
                            }
                            reader.Close();
                            con.Close();
                            editstock_location();
                        }
                        else
                        {
                            reader.Close();
                            con.Close();
                            MessageBox.Show("ไม่พบข้อมูล Part", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    catch (Exception ab)
                    {
                        MessageBox.Show(ab.Message);
                    }
                    finally
                    {
                        reader.Close();
                        con.Close();
                    }
                }
                else if(search_part != "" && search != "")
                {
                    try
                    {
                        con.Open();
                        cmd.CommandText = "SELECT * FROM [THAIARROW_PART_STORE].[dbo].[Master_part_data] where part_no Like '%" + search_part + "%'";
                        reader = cmd.ExecuteReader();
                        if (reader.HasRows)
                        {
                            dataGridView9.Rows.Clear();
                            dt_editstock.Rows.Clear();
                            while (reader.Read())
                            {                                        
                                string partNo = reader.GetValue(1).ToString();
                                string partname = reader.GetValue(2).ToString();
                                string model = reader.GetValue(3).ToString();
                                string maxstock = reader.GetValue(10).ToString();
                                string minstock = reader.GetValue(11).ToString();
                                string qty = reader.GetValue(12).ToString();
                                string create_user = reader.GetValue(13).ToString();
                                string create_date = reader.GetValue(14).ToString();
                                string update_user = reader.GetValue(15).ToString();
                                string update_date = reader.GetValue(16).ToString();

                                string supplier = reader.GetValue(17).ToString();
                                string part_type = reader.GetValue(18).ToString();
                                string part_package = reader.GetValue(19).ToString();


                                int stock_available = int.Parse(reader.GetValue(8).ToString());

                                // dt_partData.Rows.Add(partNo, partName, model, fifo, rank, max, min, qty, createUser, createDate, updateUser, supplier);
                                dataGridView9.Rows.Add(partNo, stock_available);
                                dt_editstock.Rows.Add(partNo, partname, model, stock_available, maxstock, minstock, qty, create_user, create_date, update_user, update_date, supplier, part_type, part_package);
                                
                            }
                            reader.Close();
                            con.Close();
                            editstock_location();
                        }
                        else
                        {
                            reader.Close();
                            con.Close();
                            MessageBox.Show("ไม่พบข้อมูล Part", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        con.Open();
                        cmd.CommandText = "SELECT box_id,rank,stock_lot_id,stock_time,stock_transaction,stock_user,stock_status,pickup_time,pickup_transaction,pickup_user,pickup_line FROM [THAIARROW_PART_STORE].[dbo].[Stock_part] where part_no ='"+search_part+"' AND (box_id LIKE '%" + search + "%' OR rank LIKE '%" + search + "%' OR stock_lot_id LIKE '%" + search + "%' OR stock_time LIKE '%" + search + "%' OR stock_transaction LIKE '%" + search + "%' OR stock_user LIKE '%" + search + "%' OR stock_status LIKE '%" + search + "%' OR pickup_time LIKE '%" + search + "%' OR pickup_transaction LIKE '%" + search + "%' OR pickup_user LIKE '%" + search + "%' OR pickup_line LIKE '%" + search + "%')";
                        reader = cmd.ExecuteReader();
                        if(reader.HasRows)
                        {
                            dataGridView10.Rows.Clear();
                            while(reader.Read())
                            {
                                string box_id = reader.GetValue(0).ToString();
                                string rank = reader.GetValue(1).ToString();
                                string stock_lot_id = reader.GetValue(2).ToString();
                                string stock_time = reader.GetValue(3).ToString();
                                string stock_transaction = reader.GetValue(4).ToString();
                                string stock_user = reader.GetValue(5).ToString();
                                string stock_status = reader.GetValue(6).ToString();
                                string pickup_time = reader.GetValue(7).ToString();
                                string pickup_transaction = reader.GetValue(8).ToString();
                                string pickup_user = reader.GetValue(9).ToString();
                                string pickup_line = reader.GetValue(10).ToString();
                                dataGridView10.Rows.Add(box_id, rank, stock_lot_id, stock_time, stock_transaction, stock_user, stock_status, pickup_time, pickup_transaction, pickup_user,pickup_line);
                            }
                        }
                        else
                        {
                            reader.Close();
                            con.Close();
                            MessageBox.Show("ไม่พบข้อมูล", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        reader.Close();
                    }
                    catch (Exception ab)
                    {
                        MessageBox.Show(ab.Message);
                    }
                    finally
                    {
                        reader.Close();
                        con.Close();
                    }
                }
                else if(search_part == "" && search != "")
                {
                    try
                    {
                        List<string> part = new List<string>();
                        con.Open();
                        cmd.CommandText = "SELECT box_id,rank,stock_lot_id,stock_time,stock_transaction,stock_user,stock_status,pickup_time,pickup_transaction,pickup_user,pickup_line,part_no FROM [THAIARROW_PART_STORE].[dbo].[Stock_part] where box_id LIKE '%" + search + "%' OR rank LIKE '%" + search + "%' OR stock_lot_id LIKE '%" + search + "%' OR stock_time LIKE '%" + search + "%' OR stock_transaction LIKE '%" + search + "%' OR stock_user LIKE '%" + search + "%' OR stock_status LIKE '%" + search + "%' OR pickup_time LIKE '%" + search + "%' OR pickup_transaction LIKE '%" + search + "%' OR pickup_user LIKE '%" + search + "%' OR pickup_line LIKE '%" + search + "%'";
                        reader = cmd.ExecuteReader();
                        if (reader.HasRows)
                        {
                            dataGridView10.Rows.Clear();
                            while (reader.Read())
                            {
                                string box_id = reader.GetValue(0).ToString();
                                string rank = reader.GetValue(1).ToString();
                                string stock_lot_id = reader.GetValue(2).ToString();
                                string stock_time = reader.GetValue(3).ToString();
                                string stock_transaction = reader.GetValue(4).ToString();
                                string stock_user = reader.GetValue(5).ToString();
                                string stock_status = reader.GetValue(6).ToString();
                                string pickup_time = reader.GetValue(7).ToString();
                                string pickup_transaction = reader.GetValue(8).ToString();
                                string pickup_user = reader.GetValue(9).ToString();
                                string pickup_line = reader.GetValue(10).ToString();
                                part.Add(reader.GetValue(11).ToString());
                                dataGridView10.Rows.Add(box_id, rank, stock_lot_id, stock_time, stock_transaction, stock_user, stock_status, pickup_time, pickup_transaction, pickup_user, pickup_line);
                            }
                        }
                        else
                        {
                            reader.Close();
                            //con.Close();
                            MessageBox.Show("ไม่พบข้อมูล Part", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        reader.Close();
                        dataGridView9.Rows.Clear();
                        IEnumerable<string> distin_part = part.Distinct();
                        foreach (string part_in in distin_part)
                        {
                             cmd.CommandText = "SELECT part_no,stock_available FROM [THAIARROW_PART_STORE].[dbo].[Master_part_data] where part_no ='" + part_in + "'";
                            reader = cmd.ExecuteReader();
                            if(reader.HasRows)
                            {                  
                                while(reader.Read())
                                {
                                    string part_no = reader.GetValue(0).ToString();
                                    int stock_available = int.Parse(reader.GetValue(1).ToString());
                                    dataGridView9.Rows.Add(part_no, stock_available);
                                }
                            }
                            else
                            {
                                reader.Close();
                                //con.Close();
                                MessageBox.Show("ไม่พบข้อมูล", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                            reader.Close();
                        }
                        reader.Close();
                        con.Close();
                    }
                    catch (Exception ab)
                    {
                        MessageBox.Show(ab.Message);
                    }
                    finally
                    {
                        reader.Close();
                        con.Close();
                    }
                }
            }
            else
            {
                fill_table_editstock();
            }

        }


        #endregion

        // mcline region
        #region
        private void fill_mclineTable ()
        {
            cmd = new SqlCommand();
            cmd.Connection = con;
            string ctext = "";
            string part = "";
            bool select_mc = false;
            string line_temp = "";
            dataGridView11.Rows.Clear();
            dataGridView12.Rows.Clear();
            dataGridView11.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridView11.DefaultCellStyle.Font = new Font("Tahoma", 10F);
            dataGridView11.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridView11.ColumnHeadersDefaultCellStyle.Font = new Font("Tahoma", 12F, FontStyle.Bold);

            dataGridView12.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridView12.DefaultCellStyle.Font = new Font("Tahoma", 10F);
            dataGridView12.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridView12.ColumnHeadersDefaultCellStyle.Font = new Font("Tahoma", 12F, FontStyle.Bold);
            try
            {
                if(cmc_line.SelectedIndex == -1 && cmc_part.SelectedIndex == -1)
                {
                    ctext = "SELECT DISTINCT line_no FROM [THAIARROW_PART_STORE].[dbo].[Master_line]";
                }
                if(cmc_line.SelectedIndex !=-1 && cmc_part.SelectedIndex == -1)
                {
                    ctext = "SELECT DISTINCT line_no FROM [THAIARROW_PART_STORE].[dbo].[Master_line] where line_no ='" + cmc_line.Text+"'";
                }
                if(cmc_line.SelectedIndex ==-1 && cmc_part.SelectedIndex !=-1)
                {
                    ctext = "SELECT DISTINCT line_no,part_no FROM [THAIARROW_PART_STORE].[dbo].[Master_line_part] where line_no ='" + cmc_line.Text + "' AND part_no ='" + cmc_part.Text + "'";
                    select_mc = true;
                }
                if(cmc_line.SelectedIndex !=-1 && cmc_part.SelectedIndex !=-1)
                {
                    ctext = "SELECT DISTINCT line_no,part_no FROM [THAIARROW_PART_STORE].[dbo].[Master_line_part] where line_no ='" + cmc_line.Text+"'";
                    select_mc = true;
                }
                con.Open();
                cmd.CommandText = ctext;
                reader = cmd.ExecuteReader();
                if(reader.HasRows)
                {
                    while(reader.Read())
                    {
                        string line = reader.GetValue(0).ToString();
                        if (select_mc)
                        {
                            part = reader.GetValue(1).ToString();
                            dataGridView12.Rows.Add(part);
                        }
                        if (line_temp != line) { dataGridView11.Rows.Add(line); }
                        line_temp = line;
                    }
                }
            }
            catch(Exception ab)
            {
                MessageBox.Show(ab.Message);
            }
            finally
            {
                reader.Close();
                con.Close();
            }
        }
        private void filltable_add_mc_part()
        {
            cmd = new SqlCommand();
            cmd.Connection = con;
            dataGridView11.Rows.Clear();
            dataGridView12.Rows.Clear();
          //  bool select_mc = false;
            string part = "";
            string line_temp = "";

            dataGridView12.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridView12.DefaultCellStyle.Font = new Font("Tahoma", 10F);
            dataGridView12.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridView12.ColumnHeadersDefaultCellStyle.Font = new Font("Tahoma", 12F, FontStyle.Bold);
            try
            {
                con.Open();
                cmd.CommandText = "SELECT DISTINCT line_no,part_no FROM [THAIARROW_PART_STORE].[dbo].[Master_line_part] where line_no ='" + cmc_line.Text +"'";
                
                reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        string line = reader.GetValue(0).ToString();
                       
                            part = reader.GetValue(1).ToString();
                            dataGridView12.Rows.Add(part);
                        
                        if (line_temp != line) { dataGridView11.Rows.Add(line); }
                        line_temp = line;
                    }
                }
            }
            catch(Exception ab)
            {
                MessageBox.Show(ab.Message);
            }

        }
        private void filltable_gridpart()
        {
            cmd = new SqlCommand();
            cmd.Connection = con;
          
            dataGridView12.Rows.Clear();
            string part = "";
            string line = dataGridView11.Rows[dataGridView11.CurrentCell.RowIndex].Cells[0].Value.ToString();
            dataGridView12.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridView12.DefaultCellStyle.Font = new Font("Tahoma", 10F);
            dataGridView12.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridView12.ColumnHeadersDefaultCellStyle.Font = new Font("Tahoma", 12F, FontStyle.Bold);
            try
            {
                con.Open();
                cmd.CommandText = "SELECT DISTINCT part_no FROM [THAIARROW_PART_STORE].[dbo].[Master_line_part] where line_no ='" + line + "'";

                reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                       

                        part = reader.GetValue(0).ToString();
                        dataGridView12.Rows.Add(part);

                       
                    }
                }
                reader.Close();
                con.Close();
            }
            catch (Exception ab)
            {
                MessageBox.Show(ab.Message);
            }
        }
        private void dropdown_mcline()
        {
            cmd = new SqlCommand();
            cmd.Connection = con;
            cmc_line.Items.Clear();
            cmc_part.Items.Clear();
            try
            {
                  
                con.Open();
                cmd.CommandText = "SELECT DISTINCT line_no FROM [THAIARROW_PART_STORE].[dbo].[Master_line_part]";
                reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        string line = reader.GetValue(0).ToString();
              
                        cmc_line.Items.Add(line);
                 
                    }
                }
            }
            catch (Exception ab)
            {
                MessageBox.Show(ab.Message);
            }
            finally
            {
                reader.Close();
                con.Close();
            }
        }
        private void dropdown_mcline_part()
        {
            cmd = new SqlCommand();
            cmd.Connection = con;
            cmc_part.Items.Clear();
            try
            {

                con.Open();
                cmd.CommandText = "SELECT DISTINCT part_no FROM [THAIARROW_PART_STORE].[dbo].[Master_line_part] where line_no ='"+cmc_line.Text+"'";
                reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        string part = reader.GetValue(0).ToString();
                        cmc_part.Items.Add(part);
                    }
                }
            }
            catch (Exception ab)
            {
                MessageBox.Show(ab.Message);
            }
            finally
            {
                reader.Close();
                con.Close();
            }
        }
        private void dropdown_mc_part()
        {
            cmd = new SqlCommand();
            cmd.Connection = con;
            cmc_part.Items.Clear();
            try
            {

                con.Open();
                cmd.CommandText = "SELECT DISTINCT part_no FROM [THAIARROW_PART_STORE].[dbo].[Master_part_data] ";
                reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        string part = reader.GetValue(0).ToString();
                        cmc_part.Items.Add(part);
                    }
                }
            }
            catch (Exception ab)
            {
                MessageBox.Show(ab.Message);
            }
            finally
            {
                reader.Close();
                con.Close();
            }
        }
        private void Cmc_line_SelectedIndexChanged(object sender, EventArgs e)
        {
            dropdown_mcline_part();
        }
        private void Btmc_add_Click(object sender, EventArgs e)
        {
            
            btmc_okadd.Visible = true;
            btmc_canceladd.Visible = true;
            btmc_del.Visible = false;
            btmc_search.Visible = false;
            btmc_add.Visible = false;
            cmc_line.Visible = true;
            cmc_part.Visible = true;
            dropdown_mc_part();

        }

        private void Btmc_okadd_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Do you want to add line", "Add Line", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                if (cmc_line.Text != "" && cmc_part.Text != "")
                {
                    cmd = new SqlCommand();
                    cmd.Connection = con;
                    string line = cmc_line.Text;
                    string part = cmc_part.Text;
                    try
                    {
                        if (mccheck_line(line) && mccheck_part(part,line) && mccheck_partdata())
                        {
                            
                            con.Open();
                            cmd.CommandText = "INSERT INTO [THAIARROW_PART_STORE].[dbo].[Master_line] (line_no) VALUES ('" + line + "')";
                            int b = cmd.ExecuteNonQuery();
                            if (b == 0)
                            {
                                MessageBox.Show("เกิดข้อผิดพลาดขณะทำการเพิ่มข้อมูล line");
                            }
                            cmd.CommandText = "INSERT INTO [THAIARROW_PART_STORE].[dbo].[Master_line_part] (line_no,part_no) VALUES ('" + line + "','" + part + "')";
                            int c = cmd.ExecuteNonQuery();
                            if (c == 0)
                            {
                                MessageBox.Show("เกิดข้อผิดพลาดขณะทำการเพิ่มข้อมูล line");
                            }
                            con.Close();
                            fill_mclineTable();
                        }
                        else if(!mccheck_line(line) && mccheck_part(part,line) && mccheck_partdata())
                        {
                           
                            con.Open();
                            cmd.CommandText = "INSERT INTO [THAIARROW_PART_STORE].[dbo].[Master_line_part] (line_no,part_no) VALUES ('" + line + "','" + part + "')";
                            int c = cmd.ExecuteNonQuery();
                            if (c == 0)
                            {
                                MessageBox.Show("เกิดข้อผิดพลาดขณะทำการเพิ่มข้อมูล line");
                            }
                        }
                        else if(!mccheck_partdata())
                        {
                            MessageBox.Show("ไม่มีข้อมูลใน Master part data");
                        }
                        else
                        {
                            MessageBox.Show("มีข้อมูล Line นี้แล้ว");
                        }
                        con.Close();
                        filltable_add_mc_part();
                    }
                    catch(Exception ab)
                    {
                        MessageBox.Show(ab.Message);
                    }
                    finally
                    {
                        con.Close();
                        
                    }
                }
                else if(cmc_line.Text != "" && cmc_part.Text == "")
                {
                    cmd = new SqlCommand();
                    cmd.Connection = con;
                    string line = cmc_line.Text;
                    string part = cmc_part.Text;
                    try
                    {
                        if (mccheck_line(line))
                        {
                            con.Open();
                            cmd.CommandText = "INSERT INTO [THAIARROW_PART_STORE].[dbo].[Master_line] (line_no) VALUES ('" + line + "')";
                            int b = cmd.ExecuteNonQuery();
                            if (b == 0)
                            {
                                MessageBox.Show("เกิดข้อผิดพลาดขณะทำการเพิ่มข้อมูล line");
                            }
                        }
                        else
                        {
                            MessageBox.Show("มีข้อมูล Line นี้แล้ว");
                        }

                    }
                    catch (Exception ab)
                    {
                        MessageBox.Show(ab.Message);
                    }
                    finally
                    {
                        con.Close();
                        fill_mclineTable();
                    }
                }
                else
                {
                    MessageBox.Show("กรุณากรอกข้อมูลให้ครบถ้วน");
                }
            }
            else if (dialogResult == DialogResult.No)
            {
                //do something else
            }
        }
        private bool mccheck_line (string line)
        {
            bool result = true;

            cmd = new SqlCommand();
            cmd.Connection = con;
                try
                {
                con.Open();
                cmd.CommandText = "SELECT line_no  FROM [THAIARROW_PART_STORE].[dbo].[Master_line] where line_no ='"+line+"'";
                reader = cmd.ExecuteReader();
                if(reader.HasRows)
                {
                    result = false;
                }
                reader.Close();
                con.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            

            return result;
        }
        private bool mccheck_partdata()
        {
            bool result = false;
            cmd = new SqlCommand();
            cmd.Connection = con;
            string part = cmc_part.Text;
            try
            {
                con.Open();
                cmd.CommandText = "SELECT part_no FROM [THAIARROW_PART_STORE].[dbo].[Master_part_data] where part_no = '" + part + "'";
                reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    result = true;
                }
                reader.Close();
                con.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            return result;
        }
        private bool mccheck_part(string part,string line)
        {
            bool result = true;
            cmd = new SqlCommand();
            cmd.Connection = con;

            try
            {
                con.Open();
                cmd.CommandText = "SELECT part_no FROM [THAIARROW_PART_STORE].[dbo].[Master_line_part] where part_no = '" + part +"' AND line_no ='"+line+"'";
                reader = cmd.ExecuteReader();
                if(reader.HasRows)
                {
                    result = false;
                }
                reader.Close();
                con.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }     

            return result;
        }
        private void Btmc_canceladd_Click(object sender, EventArgs e)
        {
            btmc_okadd.Visible = false;
            btmc_canceladd.Visible = false;
            btmc_del.Visible = true;
            btmc_search.Visible = true;
            btmc_add.Visible = true;
            cmc_line.Visible = true;
            cmc_part.Visible = true;
         
        }
        private void Btmc_del_Click(object sender, EventArgs e)
        {
            button form1 = new button();
            // form1.Location = new Point(960, 540);
            form1.StartPosition = FormStartPosition.CenterParent;
            //form1.Location.Y = 540;
            form1.ShowDialog();
            if(form1.DialogResult == DialogResult.OK)
            {//delete line
                MessageBox.Show("Select Delete Line");
                form1.Hide();
                int index = dataGridView11.CurrentCell.RowIndex;
                string line = dataGridView11.Rows[index].Cells[0].Value.ToString();
               
                cmd = new SqlCommand();
                cmd.Connection = con;
                try
                {
                    con.Open();
                    cmd.CommandText = "DELETE FROM [THAIARROW_PART_STORE].[dbo].[Master_line_part] where line_no = '" + line + "'";
                    int b = cmd.ExecuteNonQuery();
                    if (b == 0)
                    {
                        MessageBox.Show("เกิดข้อผิดพลาดขณะทำการลบรายการ line db line part");
                    }
                    cmd.CommandText = "DELETE FROM [THAIARROW_PART_STORE].[dbo].[Master_line] where line_no = '" + line + "'";
                    int c = cmd.ExecuteNonQuery();
                    if(c==0)
                    {
                        MessageBox.Show("เกิดข้อผิดพลาดขณะทำการลบรายการ line");
                    }
                }
                catch (Exception ab)
                {
                    MessageBox.Show(ab.Message);
                }
                finally
                {
                    con.Close();
                    fill_mclineTable();
                }

            }
            else if (form1.DialogResult == DialogResult.Cancel)
            {//delete part
                MessageBox.Show("Select Delete Part");
                form1.Hide();
                int index = dataGridView11.CurrentCell.RowIndex;
                string line = dataGridView11.Rows[index].Cells[0].Value.ToString();
                int index12 = dataGridView12.CurrentCell.RowIndex;
                string part = dataGridView12.Rows[index12].Cells[0].Value.ToString();
                cmd = new SqlCommand();
                cmd.Connection = con;
                try
                {
                    con.Open();
                    cmd.CommandText = "DELETE FROM [THAIARROW_PART_STORE].[dbo].[Master_line_part] where line_no = '" + line + "'AND part_no ='"+part+"'";
                    int b = cmd.ExecuteNonQuery();
                    if (b == 0)
                    {
                        MessageBox.Show("เกิดข้อผิดพลาดขณะทำการลบรายการ line db line part");
                    }
                    
                }
                catch (Exception ab)
                {
                    MessageBox.Show(ab.Message);
                }
                finally
                {
                    con.Close();
                    filltable_gridpart();
                }

            }
            else if (form1.DialogResult == DialogResult.Ignore)
            {
                MessageBox.Show("Cancel");
                form1.Hide();

            }
        
        }
        private void Btmc_search_Click(object sender, EventArgs e)
        {
            if(cmc_line.Text != "" || cmc_part.Text !="")
            {
                fill_mclineTable();
            }
            else
            {
                fill_mclineTable();
            }
        }
        private void DataGridView11_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            cmd = new SqlCommand();
            cmd.Connection = con;
            try
            {
                dataGridView12.Rows.Clear();
                int index = dataGridView11.CurrentCell.RowIndex;
                string mc = dataGridView11.Rows[index].Cells[0].Value.ToString();
                string part = "";
                con.Open();
                cmd.CommandText = "SELECT DISTINCT part_no FROM [THAIARROW_PART_STORE].[dbo].[Master_line_part] where line_no ='"+mc+"'";
                reader = cmd.ExecuteReader();
                if(reader.HasRows)
                {
                    while (reader.Read())
                    {
                        part = reader.GetValue(0).ToString();
                        dataGridView12.Rows.Add(part);
                    }
                }
            }
            catch(Exception ab)
            {
                MessageBox.Show(ab.Message);
            }
            finally
            {
                con.Close();
            }

        }




        #endregion
    }
}






