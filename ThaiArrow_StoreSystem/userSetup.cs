﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace ThaiArrow_StoreSystem
{
    public partial class userSetup : Form
    {
        private SqlConnection con = new SqlConnection(ConfigurationSettings.AppSettings["SQLConnection"]);
        private SqlCommand cmd;
        private SqlDataReader reader;
        public userSetup()
        {
            InitializeComponent();
            // fill_tableuser();
            dataGridView_user_Load();
            comboBox_position_Load();
        }
   
      
        private void DataGridView_user_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (dataGridView_user.Rows.Count > 0)
                {
                    checkBox1.Checked = false;
                    checkBox2.Checked = false;
                    checkBox3.Checked = false;
                    checkBox4.Checked = false;
                    checkBox5.Checked = false;
                    checkBox6.Checked = false;
                    checkBox7.Checked = false;
                    checkBox8.Checked = false;
                    string user_id = dataGridView_user.Rows[dataGridView_user.CurrentRow.Index].Cells[0].Value.ToString();
                    string username = dataGridView_user.Rows[dataGridView_user.CurrentRow.Index].Cells[1].Value.ToString();
                    string pass = dataGridView_user.Rows[dataGridView_user.CurrentRow.Index].Cells[2].Value.ToString();
                    string name = dataGridView_user.Rows[dataGridView_user.CurrentRow.Index].Cells[3].Value.ToString();
                    string sure = dataGridView_user.Rows[dataGridView_user.CurrentRow.Index].Cells[4].Value.ToString();
                    string position = dataGridView_user.Rows[dataGridView_user.CurrentRow.Index].Cells[5].Value.ToString();
                    
                    tbworkerID.Text = user_id;
                    tbUsername.Text = username;
                    tbPassword.Text = pass;
                    tbName.Text = name;
                    tbSurname.Text = sure;
                    tbposition.Text = position;
                    tickBok_permission(username);
                }
            }
            catch(Exception ab)
            {
                MessageBox.Show(ab.Message);
            }
        }

        private void ButtonClose_Click(object sender, EventArgs e)
        {
            this.Hide();
            Main fm = new Main();
            fm.Show();
        }
        private void fill_tableuser()
        {
            cmd = new SqlCommand();
            cmd.Connection = con;
            dataGridView_user.Rows.Clear();
            try
            {
                con.Open();
                cmd.CommandText = "SELECT * FROM [THAIARROW_PART_STORE].[dbo].[User_data]" ;
                reader = cmd.ExecuteReader();
                if(reader.HasRows)
                {
                    while(reader.Read())
                    {
                        string user_id = reader.GetValue(0).ToString();
                        string username = reader.GetValue(1).ToString();
                        string name = reader.GetValue(2).ToString();
                        string surename = reader.GetValue(3).ToString();
                        string password = reader.GetValue(4).ToString();
                        string position = reader.GetValue(5).ToString();
                        dataGridView_user.Rows.Add(user_id, username, name, surename, password, position);
                    }
                }
            }
            catch(Exception ab)
            {
                MessageBox.Show(ab.Message);
            }
            finally
            {
                reader.Close();
                con.Close();
            }
        }
        private void comboBox_position_Load()
        {

            tbposition.AutoCompleteMode = AutoCompleteMode.Suggest;
            tbposition.AutoCompleteSource = AutoCompleteSource.CustomSource;
            AutoCompleteStringCollection col = new AutoCompleteStringCollection();
            comboBox_position.Items.Clear();
            cmd = new SqlCommand();
            cmd.Connection = con;
            try
            {
                con.Open();
                cmd.CommandText = "SELECT DISTINCT Position FROM [THAIARROW_PART_STORE].[dbo].[User_position] ORDER BY Position ASC";
                reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        string sName = reader.GetValue(0).ToString().Replace("'", "'");
                        if (sName != "Admin")
                        {
                            comboBox_position.Items.Add(sName);
                            col.Add(sName);
                        }
                    }
                }
                tbposition.AutoCompleteCustomSource = col;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                reader.Close();
                con.Close();
            }
        }
        private void comboBox_name_Load()
        {
            comboBox_name.Items.Clear();
            cmd = new SqlCommand();
            cmd.Connection = con;
            try
            {
                con.Open();
                cmd.CommandText = "SELECT DISTINCT name, surename FROM [THAIARROW_PART_STORE].[dbo].[User_data] WHERE position = '" + comboBox_position.SelectedItem.ToString() + "' ORDER BY name ASC";
                reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        string Name = reader.GetValue(0).ToString().Replace("'", "'");
                        string SurName = reader.GetValue(1).ToString().Replace("'", "'");
                        string fullname = Name + " " + SurName;
                        comboBox_name.Items.Add(fullname);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                reader.Close();
                con.Close();
            }
        }
        private void dataGridView_user_Load()
        {
            string cmdcode = "";
            string cmd_permission = "";
            cmd = new SqlCommand();
            cmd.Connection = con;
            char[] splitchar = { ' ' };
            string[] name = null;
            dataGridView_user.Rows.Clear();
            //  comboBox_name.SelectedItem.ToString().Split()
            if (comboBox_position.SelectedIndex == -1)
            {
                cmdcode = "SELECT DISTINCT a.user_id ,a.username,a.name ,a.surename ,a.password,a.position FROM [THAIARROW_PART_STORE].[dbo].[User_data] AS a ORDER BY user_id ASC";
            }
            if (comboBox_position.SelectedIndex != -1 && comboBox_name.SelectedIndex != -1)
            {
                name = comboBox_name.SelectedItem.ToString().Split(splitchar);
                cmdcode = "SELECT DISTINCT a.user_id ,a.username,a.name ,a.surename ,a.password,a.position FROM [THAIARROW_PART_STORE].[dbo].[User_data] AS a WHERE a.position = '" + comboBox_position.SelectedItem.ToString().Replace("'", "''") + "' AND a.name ='" + name[0] + "' AND a.surename ='" + name[1] + "'  ORDER BY [user_id] ASC";
            }
            else if (comboBox_position.SelectedIndex != -1)
            {
                cmdcode = "SELECT DISTINCT a.user_id ,a.username,a.name ,a.surename ,a.password,a.position FROM [THAIARROW_PART_STORE].[dbo].[User_data] AS a WHERE a.position = '" + comboBox_position.SelectedItem.ToString().Replace("'", "''") + "' ORDER BY [user_id] ASC";
            }

          
            try
            {
                con.Open();
                cmd.CommandText = cmdcode;
                reader = cmd.ExecuteReader();
                if(reader.HasRows)
                {
                    while(reader.Read())
                    {
                        string user_id = reader.GetValue(0).ToString();
                        string username = reader.GetValue(1).ToString();
                        string name_ = reader.GetValue(2).ToString();
                        string sure = reader.GetValue(3).ToString();
                        string pass = reader.GetValue(4).ToString();
                        string position = reader.GetValue(5).ToString();
                        dataGridView_user.Rows.Add(user_id, username, name_, sure, pass, position);
                    }
                }
                tbworkerID.Enabled = false;
                tbUsername.Enabled = false;
                tbPassword.Enabled = false;
                tbName.Enabled = false;
                tbSurname.Enabled = false;
                tbposition.Enabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                con.Close();
            }
        }

        private void add_userpermiss()
        {
            cmd = new SqlCommand();
            cmd.Connection = con;
            string username = tbUsername.Text;
            List<string> check = new List<string>();
            if (checkBox1.Checked == true) { check.Add("UserSetup"); }
            if (checkBox2.Checked == true) { check.Add("PickupPart"); }
            if (checkBox3.Checked == true) { check.Add("AddStock"); }
            if (checkBox4.Checked == true) { check.Add("PermissionSetup"); }
            if (checkBox5.Checked == true) { check.Add("PartsData"); }
            if (checkBox6.Checked == true) { check.Add("EditStock"); }
            if (checkBox7.Checked == true) { check.Add("Logs"); }
            if (checkBox8.Checked == true) { check.Add("MCLine"); }
            try
            {
                con.Open();
                for (int i = 0; i < check.Count; i++)
                {
                    cmd.CommandText = "INSERT INTO [THAIARROW_PART_STORE].[dbo].[User_permission] (username,permission) VALUES ('" + username + "','" + check[i] + "')";
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception a)
            {
                MessageBox.Show(a.Message);
            }
            finally
            {
                con.Close();
               // dataGridView_user_Load();
                comboBox_position_Load();
            }
        }
        private void add_userData()
        {
            cmd = new SqlCommand();
            cmd.Connection = con;
            string userID = tbworkerID.Text;
            string username = tbUsername.Text;
            string password = tbPassword.Text;
            string name = tbName.Text;
            string surename = tbSurname.Text;
            string position = tbposition.Text;
            try
            {
                con.Open();
                cmd.CommandText = "SET IDENTITY_INSERT [THAIARROW_PART_STORE].[dbo].[User_data] ON INSERT INTO[THAIARROW_PART_STORE].[dbo].[User_data](user_id,username,name,surename,password,position) VALUES('" + userID + "','" + username + "','" + name + "','" + surename + "','" + password + "','" + position + "')SET IDENTITY_INSERT  [THAIARROW_PART_STORE].[dbo].[User_data] OFF ";
                cmd.ExecuteNonQuery();
            }
            catch (Exception a)
            {
                MessageBox.Show(a.Message);
            }
            finally
            {
                con.Close();
               // dataGridView_user_Load();
                comboBox_position_Load();
            }

        }


        private void Panel4_Paint(object sender, PaintEventArgs e)
        {

        }

        private void Bt_search_Click(object sender, EventArgs e)
        {
            dataGridView_user_Load();
        }

        private void ComboBox_position_SelectedIndexChanged(object sender, EventArgs e)
        {
            comboBox_name_Load();
        }

        private void Newbtn1_Click(object sender, EventArgs e)
        {
            btnADD.Visible = true;
            btnUPDATE.Visible = false;
            btnDELETE.Visible = false;
            tbworkerID.Enabled = true;
            tbUsername.Enabled = true;
            tbPassword.Enabled = true;
            tbName.Enabled = true;
            tbSurname.Enabled = true;
            tbworkerID.Text = "";
            tbUsername.Text = "";
            tbPassword.Text = "";
            tbName.Text = "";
            tbSurname.Text = "";
            //  comboBox_permission.Enabled = true;
            tbposition.Enabled = true;
            tbposition.Text = "";
            comboBox_permission_Load();
            tbposition_load();
            clear_permis();
        }
        private void clear_permis()
        {
            checkBox1.Checked = false;
            checkBox2.Checked = false;
            checkBox3.Checked = false;
            checkBox4.Checked = false;
            checkBox5.Checked = false;
            checkBox6.Checked = false;
            checkBox7.Checked = false;
            checkBox8.Checked = false;
        }
        private void comboBox_permission_Load()
        {

            cmd = new SqlCommand();
            cmd.Connection = con;
            try
            {
                con.Open();
                cmd.CommandText = "SELECT DISTINCT permission FROM [THAIARROW_PART_STORE].[dbo].[User_permission] ORDER BY permission ASC";
                reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        string sName = reader.GetValue(0).ToString().Replace("'", "'");
                        if (sName != "Admin")
                        {
                            //  comboBox_permission.Items.Add(sName);
                            // col.Add(sName);
                        }
                    }
                }
                //tbposition.AutoCompleteCustomSource = col;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                reader.Close();
                con.Close();
            }

        }
        private void tbposition_load()
        {
            tbposition.Items.Clear();
            cmd = new SqlCommand();
            cmd.Connection = con;
            try
            {
                con.Open();
                cmd.CommandText = "SELECT DISTINCT position FROM [THAIARROW_PART_STORE].[dbo].[User_position] ORDER BY position ASC";
                reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        string sName = reader.GetValue(0).ToString().Replace("'", "'");
                        if (sName != "Admin")
                        {
                            tbposition.Items.Add(sName);
                            // col.Add(sName);
                        }
                    }
                }
                //tbposition.AutoCompleteCustomSource = col;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                reader.Close();
                con.Close();
            }
        }

        private void Editbtn1_Click(object sender, EventArgs e)
        {
            if (comboBox_name.SelectedIndex != -1 && comboBox_position.SelectedIndex != -1 || dataGridView_user.SelectedRows.Count > 0)
            {
                btnADD.Visible = false;
                btnUPDATE.Visible = true;
                btnDELETE.Visible = false;
                tbposition.Enabled = true;
                // getValue();
                tbworkerID.Enabled = true;
                tbUsername.Enabled = true;
                tbPassword.Enabled = true;
                tbName.Enabled = true;
                tbSurname.Enabled = true;
                tbposition.Enabled = true;
                tbposition_load();
                //comboBox_permission.Enabled = true;
            }
            else
            {
                MessageBox.Show("กรุณาเลือก Name ที่ต้องการแก้ไขก่อน", "ผิดพลาด", MessageBoxButtons.OK);
            }
        }

        private void Delbtn1_Click(object sender, EventArgs e)
        {
            if (comboBox_name.SelectedIndex != -1 && comboBox_position.SelectedIndex != -1 || dataGridView_user.SelectedRows.Count > 0)
            {
                btnADD.Visible = false;
                btnUPDATE.Visible = false;
                btnDELETE.Visible = true;
                // getValue();
                tbworkerID.Enabled = false;
                tbUsername.Enabled = false;
                tbPassword.Enabled = false;
                tbName.Enabled = false;
                tbSurname.Enabled = false;
                tbposition.Enabled = false;
                // comboBox_permission.Enabled = false;
            }
            else
            {
                MessageBox.Show("กรุณาเลือก Name ที่ต้องการลบก่อน", "ผิดพลาด", MessageBoxButtons.OK);
            }
        }

        private void BtnUPDATE_Click(object sender, EventArgs e)
        {
            edit_user();
        }
        private void edit_user()
        {
            cmd = new SqlCommand();
            cmd.Connection = con;
            List<string> check = new List<string>();
            DialogResult dialogResult = MessageBox.Show("Edit", "Do you want to edit ?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dialogResult == DialogResult.Yes)
            {
                if (checkBox1.Checked == true) { check.Add("UserSetup"); }
                if (checkBox2.Checked == true) { check.Add("PickupPart"); }
                if (checkBox3.Checked == true) { check.Add("AddStock"); }
                if (checkBox4.Checked == true) { check.Add("PermissionSetup"); }
                if (checkBox5.Checked == true) { check.Add("PartsData"); }
                if (checkBox6.Checked == true) { check.Add("EditStock"); }
                if (checkBox7.Checked == true) { check.Add("Logs"); }
                if (checkBox8.Checked == true) { check.Add("MCLine"); }
                string username = dataGridView_user.Rows[dataGridView_user.CurrentCell.RowIndex].Cells[1].Value.ToString();
                try
                {
                    con.Open();
                    {
                        for (int i = 0; i < check.Count; i++)
                        {
                            cmd.CommandText = "DELETE FROM [THAIARROW_PART_STORE].[dbo].[User_data] where username ='" + username + "'";
                            int row = cmd.ExecuteNonQuery();
                        }
                    }
                }
                catch (Exception a)
                {
                    con.Close();
                    MessageBox.Show(a.Message);
                }
                finally
                {
                    con.Close();
                    clear_userpermiss(username, check.Count);
                    add_userData();
                    dataGridView_user_Load();
                    btnUPDATE.Visible = false;

                }
            }
            else if (dialogResult == DialogResult.No)
            {
                this.Hide();
                userSetup pm = new userSetup();
                pm.Show();
            }
        }
        private void clear_userpermiss(string username, int check)
        {
            cmd = new SqlCommand();
            cmd.Connection = con;
            try
            {
                con.Open();
                for (int i = 0; i < check; i++)
                {
                    cmd.CommandText = "DELETE FROM [THAIARROW_PART_STORE].[dbo].[User_permission] where username ='" + username + "'";
                    int row = cmd.ExecuteNonQuery();
                }
            }
            catch (Exception a)
            {
                MessageBox.Show(a.Message);
            }
            finally
            {
                con.Close();
                add_userpermiss();
            }

        }
        private void tickBok_permission(string UserName)
        {
            cmd = new SqlCommand();
            cmd.Connection = con;
            try
            {
                con.Open();
                cmd.CommandText = "SELECT [permission] FROM [THAIARROW_PART_STORE].[dbo].[User_permission] where username ='" + UserName + "' ORDER BY permission ASC";
                reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {

                    while (reader.Read())
                    {
                        string userPermis = reader.GetValue(0).ToString();
                        if (userPermis == "UserSetup") { checkBox1.Checked = true; }
                        if (userPermis == "PickupPart") { checkBox2.Checked = true; }
                        if (userPermis == "AddStock") { checkBox3.Checked = true; }
                        if (userPermis == "PermissionSetup") { checkBox4.Checked = true; }
                        if (userPermis == "PartsData") { checkBox5.Checked = true; }
                        if (userPermis == "EditStock") { checkBox6.Checked = true; }
                        if (userPermis == "Logs") { checkBox7.Checked = true; }
                        if (userPermis == "MCLine") { checkBox8.Checked = true; }
                    }

                }
            }
            catch (Exception a)
            {
                MessageBox.Show(a.Message);
            }
            finally
            {
                con.Close();
            }
        }
     

        private void BtnADD_Click(object sender, EventArgs e)
        {
            if (!UserName(tbUsername.Text.ToString()))
            {
                cmd = new SqlCommand();
                cmd.Connection = con;
                try
                {
                    string mes = "UserID : " + tbworkerID.Text.ToString() + "\r\nUsername : " + tbUsername.Text.ToString() + "\r\nPassword :" +
                                 tbPassword.Text.ToString() + "\r\nName : " + tbName.Text.ToString() + "\r\nSurname : " + tbSurname.Text.ToString() +
                                 "\r\nPosition : " + tbposition.Text.ToString();
                    if (MessageBox.Show(mes, "NEW", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        ///yes

                        con.Open();
                        cmd.CommandText = "SET IDENTITY_INSERT [THAIARROW_PART_STORE].[dbo].[User_data] ON INSERT INTO [THAIARROW_PART_STORE].[dbo].[User_data] ([user_id] ,[username] ,[name] ,[surename] ,[password] ,[position] ) " +
                            " VALUES (@user_id,@username, @name, @surename, @password,@position) SET IDENTITY_INSERT [THAIARROW_PART_STORE].[dbo].[User_data] OFF";


                        cmd.Parameters.AddWithValue("@user_id", int.Parse(tbworkerID.Text));
                        cmd.Parameters.AddWithValue("@username", tbUsername.Text);
                        cmd.Parameters.AddWithValue("@name", tbName.Text);
                        cmd.Parameters.AddWithValue("@surename", tbSurname.Text);
                        cmd.Parameters.AddWithValue("@password", tbPassword.Text);
                        cmd.Parameters.AddWithValue("@position", tbposition.Text.ToString());


                        int a = cmd.ExecuteNonQuery();
                        if (a == 0)
                        {
                            MessageBox.Show("การเพิ่ม User ไม่สำเร็จ", "ผิดพลาด", MessageBoxButtons.OK);
                        }
                        else
                        {
                            MessageBox.Show("การเพิ่ม User สำเร็จ", "สำเร็จ", MessageBoxButtons.OK);
                        }
                    }
                    else
                    {
                        ///no
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    con.Close();
                    dataGridView_user_Load();
                    sentDB_permission();
                    if (comboBox_position.SelectedIndex != -1)
                        comboBox_name_Load();
                }
            }
            else
            {
                MessageBox.Show("ไม่สามารถใช้ WorkerID นี้ได้", "ผิดพลาด", MessageBoxButtons.OK);
            }
           
        }
        //insert to user_permission db
        private void sentDB_permission()
        {
            cmd = new SqlCommand();
            cmd.Connection = con;
            List<string> userPermission = new List<string>();
            try
            {
                string userName = tbUsername.Text;
                if (checkBox1.Checked == true) { userPermission.Add("UserSetup"); }
                if (checkBox2.Checked == true) { userPermission.Add("PickupPart"); }
                if (checkBox3.Checked == true) { userPermission.Add("AddStock"); }
                if (checkBox4.Checked == true) { userPermission.Add("PermissionSetup"); }
                if (checkBox5.Checked == true) { userPermission.Add("PartsData"); }
                if (checkBox6.Checked == true) { userPermission.Add("EditStock"); }
                if (checkBox7.Checked == true) { userPermission.Add("Logs"); }
                if (checkBox8.Checked == true) { userPermission.Add("MCLine"); }
                con.Open();
                for (int i = 0; i < userPermission.Count; i++)
                {
                    cmd.CommandText = "INSERT INTO [THAIARROW_PART_STORE].[dbo].[User_permission] ([username] ,[permission] ) " +
                                    " VALUES ('" + userName + "','" + userPermission[i] + "')";
                    int b = cmd.ExecuteNonQuery();
                }
            }
            catch (Exception a)
            {
                MessageBox.Show(a.Message);
            }
            finally
            {
                con.Close();
                dataGridView_user_Load();
            }
        }
        private bool UserName(string UserName)
        {
            bool result = false;
            cmd = new SqlCommand();
            cmd.Connection = con;
            try
            {
                con.Open();
                cmd.CommandText = "SELECT DISTINCT * FROM [THAIARROW_PART_STORE].[dbo].[User_data] WHERE username = '" + UserName + "' ";
                reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    result = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            con.Close();
            return result;
        }

        //process of del 1)del in user_data 2) del in user_permission
        private void BtnDELETE_Click(object sender, EventArgs e)
        {
            cmd = new SqlCommand();
            cmd.Connection = con;
            string username = tbUsername.Text;
            try
            {
                string mes = "WorkerID : " + tbworkerID.Text.ToString() + "\r\nUsername : " + tbUsername.Text.ToString() + "\r\nPassword :" +
                                      tbPassword.Text.ToString() + "\r\nName : " + tbName.Text.ToString() + "\r\nSurname : " + tbSurname.Text.ToString() +
                                      "\r\nPosition : " + tbposition.Text.ToString();
                if (MessageBox.Show(mes, "DELETE", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    ///yes
                    con.Open();
                    cmd.CommandText = "DELETE FROM [THAIARROW_PART_STORE].[dbo].[User_data] where username ='" + username + "'";
                    int a = cmd.ExecuteNonQuery();
                    if (a == 0)
                    {
                        MessageBox.Show("การลบ User ไม่สำเร็จ", "ผิดพลาด", MessageBoxButtons.OK);
                    }
                    else
                    {
                        MessageBox.Show("การลบ User สำเร็จ", "สำเร็จ", MessageBoxButtons.OK);
                    }
                }
                else
                {
                    ///no
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                con.Close();
                del_userpermission();
                dataGridView_user_Load();
                //comboBox_name_Load();
                tbposition_load();
                btnDELETE.Visible = false;
            }
        }
        private void del_userpermission()
        {
            cmd = new SqlCommand();
            cmd.Connection = con;
            string username = tbUsername.Text;
            int check = 0;
            if (checkBox1.Checked == true) { check++; }
            if (checkBox2.Checked == true) { check++; }
            if (checkBox3.Checked == true) { check++; }
            if (checkBox4.Checked == true) { check++; }
            if (checkBox5.Checked == true) { check++; }
            if (checkBox6.Checked == true) { check++; }
            if (checkBox7.Checked == true) { check++; }
            if (checkBox8.Checked == true) { check++; }
            try
            {
                con.Open();
                for (int i = 0; i < check; i++)
                {
                    cmd.CommandText = "DELETE FROM [THAIARROW_PART_STORE].[dbo].[User_permission] where username ='" + username + "'";
                    int row = cmd.ExecuteNonQuery();
                }
            }
            catch (Exception a)
            {
                MessageBox.Show(a.Message);
            }
            finally
            {
                con.Close();
                dataGridView_user_Load();
                tbposition_load();
                tbworkerID.Text = "";
                tbUsername.Text = "";
                tbPassword.Text = "";
                tbName.Text = "";
                tbSurname.Text = "";
                tbposition.Text = "";
            }
        }

        private void BtPermissSetup_Click(object sender, EventArgs e)
        {
            this.Hide();
            PermissionSetup fm1 = new PermissionSetup();
            fm1.Show();
        }


        private void txtType1_KeyPress(object sender, KeyPressEventArgs e)
        {
            int isNumber = 0;
            e.Handled = !int.TryParse(e.KeyChar.ToString(), out isNumber);
        }
    }
}

