//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ThaiArrow_StoreSystem
{
    using System;
    using System.Collections.Generic;
    
    public partial class Transaction_detail
    {
        public long record_id { get; set; }
        public Nullable<long> transaction_id { get; set; }
        public string part_no { get; set; }
        public string mcline { get; set; }
        public string box_id { get; set; }
    }
}
