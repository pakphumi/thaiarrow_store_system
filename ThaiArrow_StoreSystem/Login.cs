﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Windows.Forms;



namespace ThaiArrow_StoreSystem
{
    public partial class Login : Form
    {
        private SqlConnection con = new SqlConnection(ConfigurationSettings.AppSettings["SQLConnection"]);
        private SqlConnection con_permission = new SqlConnection(ConfigurationSettings.AppSettings["SQLConnection"]);
        private SqlCommand cmd_permission;
        private SqlCommand cmd;
        private SqlDataReader reader;
        private SqlDataReader reader_permission;
        public static string frmSrl, userID, userName, name, surename, position, permission, workerID;

        private void ButtonLogin_Click(object sender, EventArgs e)
        {
            cmd = new SqlCommand();
            cmd.Connection = con;
            cmd_permission = new SqlCommand();
            cmd_permission.Connection = con_permission;
            // List<string> permiss = new List<string>();
            try
            {
                con.Open();
                cmd.CommandText = " SELECT [user_id],[username],[name],[surename],[position]" +
                    " FROM [THAIARROW_PART_STORE].[dbo].[User_data]" +
                    " WHERE username = '" + textBoxUsername.Text + "' COLLATE SQL_Latin1_General_CP1_CS_AS" +
                    " AND password = '" + textBoxPassword.Text + "' COLLATE SQL_Latin1_General_CP1_CS_AS ";

                con_permission.Open();
                cmd_permission.CommandText = " SELECT [user_permission_id],[username],[permission]" +
                    " FROM [THAIARROW_PART_STORE].[dbo].[User_permission]" +
                    " WHERE username = '" + textBoxUsername.Text + "'";

                reader = cmd.ExecuteReader();
                reader_permission = cmd_permission.ExecuteReader();
                permiss.Clear();
                if (reader.HasRows && reader_permission.HasRows)
                {

                    while (reader.Read())
                    {
                        while (reader_permission.Read())
                        {
                            labelAlert.Enabled = false;
                            labelAlert.Visible = false;
                            userID = reader.GetValue(0).ToString();
                            userName = reader.GetValue(1).ToString();
                            name = reader.GetValue(2).ToString();
                            Program.name_login = name;
                            surename = reader.GetValue(3).ToString();
                            Program.surname_login = surename;
                            position = reader.GetValue(4).ToString();
                            permission = reader_permission.GetValue(2).ToString();
                            if (permission == "AddStock") { permiss.Add("AddStock"); }
                            if (permission == "EditStock") { permiss.Add("EditStock"); }
                            if (permission == "Logs") { permiss.Add("Logs"); }
                            if (permission == "MCLine") { permiss.Add("MCLine"); }
                            if (permission == "PartsData") { permiss.Add("PartsData"); }
                            if (permission == "PermissionSetup") { permiss.Add("PermissionSetup"); }
                            if (permission == "PickupPart") { permiss.Add("PickupPart"); }
                            if (permission == "UserSetup") { permiss.Add("UserSetup"); }
                        }
                    }
                    this.Hide();
                    Main fm = new Main();
                    fm.Show();
                }
                else
                {
                    labelAlert.Enabled = true;
                    labelAlert.Visible = true;
                }

            }
            finally
            {
                reader.Close();
                con.Close();
                reader_permission.Close();
                con_permission.Close();
            }
        }

        public static List<string> permiss = new List<string>();
        private void Login_KeyPress(object sender, KeyPressEventArgs e)
        {
            string keyPressed = e.KeyChar.ToString();
            if (keyPressed == "\r")
            {
                cmd = new SqlCommand();
                cmd.Connection = con;
                cmd_permission = new SqlCommand();
                cmd_permission.Connection = con_permission;
                List<string> permis = new List<string>();
                try
                {
                    con.Open();
                    cmd.CommandText = " SELECT [user_id],[username],[name],[surename],[position]" +
                        " FROM [THAIARROW_PART_STORE].[dbo].[User_data]" +
                        " WHERE username = '" + textBoxUsername.Text + "' COLLATE SQL_Latin1_General_CP1_CS_AS" +
                        " AND password = '" + textBoxPassword.Text + "' COLLATE SQL_Latin1_General_CP1_CS_AS";

                    con_permission.Open();
                    cmd_permission.CommandText = " SELECT [user_permission_id],[username],[permission]" +
                        " FROM [THAIARROW_PART_STORE].[dbo].[User_permission]" +
                        " WHERE username = '" + textBoxUsername.Text + "'";

                    reader = cmd.ExecuteReader();
                    reader_permission = cmd_permission.ExecuteReader();
                    if (reader.HasRows && reader_permission.HasRows)
                    {
                        while (reader.Read())
                        {
                            while (reader_permission.Read())
                            {
                                labelAlert.Enabled = false;
                                labelAlert.Visible = false;
                                userID = reader.GetValue(0).ToString();
                                userName = reader.GetValue(1).ToString();
                                name = reader.GetValue(2).ToString();
                                Program.name_login = name;
                                surename = reader.GetValue(3).ToString();
                                Program.surname_login = surename;
                                position = reader.GetValue(4).ToString();
                                permission = reader_permission.GetValue(2).ToString();
                                if (permission == "AddStock") { permiss.Add("AddStock"); }
                                if (permission == "EditStock") { permiss.Add("EditStock"); }
                                if (permission == "Logs") { permiss.Add("Logs"); }
                                if (permission == "MCLine") { permiss.Add("MCLine"); }
                                if (permission == "PartsData") { permiss.Add("PartsData"); }
                                if (permission == "PermissionSetup") { permiss.Add("PermissionSetup"); }
                                if (permission == "PickupPart") { permiss.Add("PickupPart"); }
                                if (permission == "UserSetup") { permiss.Add("UserSetup"); }
                            }
                        }
                        //     if(permission != "admin")
                        //      {
                        //         this.Hide();
                        //        MainForm fm1 = new MainForm();
                        //       fm1.Show();
                        //  }
                        // this.Hide();
                        //  LineSetup fm = new LineSetup();
                        // fm.Show();
                        this.Hide();
                        Main fm = new Main();
                        fm.Show();
                    }
                    else
                    {
                        labelAlert.Enabled = true;
                        labelAlert.Visible = true;
                    }

                }
                finally
                {
                    reader.Close();
                    con.Close();
                    reader_permission.Close();
                    con_permission.Close();
                }
            }
        }

        public Login()
        {
            InitializeComponent();
        }

        private void Login_Load(object sender, EventArgs e)
        {

        }

        private void ButtonCancel_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
