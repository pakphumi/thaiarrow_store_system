﻿namespace ThaiArrow_StoreSystem
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label30 = new System.Windows.Forms.Label();
            this.labelLineUserNameTopPanel = new System.Windows.Forms.Label();
            this.label66 = new System.Windows.Forms.Label();
            this.labelLevel = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.buttonClose = new System.Windows.Forms.Button();
            this.buttonMinimize = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.bt_Pickup = new System.Windows.Forms.Button();
            this.bt_addStock = new System.Windows.Forms.Button();
            this.bt_manage = new System.Windows.Forms.Button();
            this.bt_userSetup = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabMain = new System.Windows.Forms.TabPage();
            this.button8 = new System.Windows.Forms.Button();
            this.bt_LAddStock = new System.Windows.Forms.Button();
            this.bt_LPickupPart = new System.Windows.Forms.Button();
            this.tabPickup = new System.Windows.Forms.TabPage();
            this.pap_lotidwrong = new System.Windows.Forms.Panel();
            this.label85 = new System.Windows.Forms.Label();
            this.label84 = new System.Windows.Forms.Label();
            this.panel25 = new System.Windows.Forms.Panel();
            this.label73 = new System.Windows.Forms.Label();
            this.label74 = new System.Windows.Forms.Label();
            this.panel26 = new System.Windows.Forms.Panel();
            this.tp_alertwrong = new System.Windows.Forms.TextBox();
            this.label75 = new System.Windows.Forms.Label();
            this.label76 = new System.Windows.Forms.Label();
            this.label77 = new System.Windows.Forms.Label();
            this.panel24 = new System.Windows.Forms.Panel();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.label118 = new System.Windows.Forms.Label();
            this.tp_side = new System.Windows.Forms.TextBox();
            this.label52 = new System.Windows.Forms.Label();
            this.tp_maxstock = new System.Windows.Forms.TextBox();
            this.label53 = new System.Windows.Forms.Label();
            this.tp_ccode = new System.Windows.Forms.TextBox();
            this.label54 = new System.Windows.Forms.Label();
            this.tp_minstock = new System.Windows.Forms.TextBox();
            this.label55 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.label65 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.label69 = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.tp_stock = new System.Windows.Forms.TextBox();
            this.tp_ordertime = new System.Windows.Forms.TextBox();
            this.tp_userout = new System.Windows.Forms.TextBox();
            this.tp_statusDB = new System.Windows.Forms.TextBox();
            this.tp_statusM = new System.Windows.Forms.TextBox();
            this.tp_slot = new System.Windows.Forms.TextBox();
            this.tp_zone = new System.Windows.Forms.TextBox();
            this.tp_shelf = new System.Windows.Forms.TextBox();
            this.tp_area = new System.Windows.Forms.TextBox();
            this.tp_dcode = new System.Windows.Forms.TextBox();
            this.tp_mpn = new System.Windows.Forms.TextBox();
            this.tp_maker = new System.Windows.Forms.TextBox();
            this.tp_rank = new System.Windows.Forms.TextBox();
            this.tp_qty = new System.Windows.Forms.TextBox();
            this.tp_boxid = new System.Windows.Forms.TextBox();
            this.bt_pickup_cancel = new System.Windows.Forms.Button();
            this.btPickup_OK = new System.Windows.Forms.Button();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.label71 = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this.tp_partname = new System.Windows.Forms.TextBox();
            this.tp_scan = new System.Windows.Forms.TextBox();
            this.panel23 = new System.Windows.Forms.Panel();
            this.label51 = new System.Windows.Forms.Label();
            this.dataGridView7 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MCline = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fifo_enable = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rank_enable = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn22 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.side_pickup = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn23 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.minstock = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Qty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.qty_stock = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pu_boxID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn26 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StatusM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel20 = new System.Windows.Forms.Panel();
            this.button2 = new System.Windows.Forms.Button();
            this.label50 = new System.Windows.Forms.Label();
            this.tabAddStock = new System.Windows.Forms.TabPage();
            this.panel22 = new System.Windows.Forms.Panel();
            this.tas_alertoverstock = new System.Windows.Forms.TextBox();
            this.label47 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.pas_duplicate = new System.Windows.Forms.Panel();
            this.tas_alertboxid = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.panel52 = new System.Windows.Forms.Panel();
            this.tas_alertadd = new System.Windows.Forms.TextBox();
            this.label171 = new System.Windows.Forms.Label();
            this.panel21 = new System.Windows.Forms.Panel();
            this.tas_alert_addStockcom = new System.Windows.Forms.TextBox();
            this.label44 = new System.Windows.Forms.Label();
            this.panel19 = new System.Windows.Forms.Panel();
            this.label25 = new System.Windows.Forms.Label();
            this.panel18 = new System.Windows.Forms.Panel();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.label117 = new System.Windows.Forms.Label();
            this.tas_side = new System.Windows.Forms.TextBox();
            this.labellll = new System.Windows.Forms.Label();
            this.tas_maxstock = new System.Windows.Forms.TextBox();
            this.label43 = new System.Windows.Forms.Label();
            this.tas_ccode = new System.Windows.Forms.TextBox();
            this.label42 = new System.Windows.Forms.Label();
            this.tas_spare = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.tas_stock = new System.Windows.Forms.TextBox();
            this.tas_timeIn = new System.Windows.Forms.TextBox();
            this.tas_userIn = new System.Windows.Forms.TextBox();
            this.tas_statusDB = new System.Windows.Forms.TextBox();
            this.tas_statusM = new System.Windows.Forms.TextBox();
            this.tas_slot = new System.Windows.Forms.TextBox();
            this.tas_zone = new System.Windows.Forms.TextBox();
            this.tas_shelf = new System.Windows.Forms.TextBox();
            this.tas_area = new System.Windows.Forms.TextBox();
            this.tas_dcode = new System.Windows.Forms.TextBox();
            this.tas_mpn = new System.Windows.Forms.TextBox();
            this.tas_maker = new System.Windows.Forms.TextBox();
            this.tas_rank = new System.Windows.Forms.TextBox();
            this.tas_qty = new System.Windows.Forms.TextBox();
            this.tas_boxID = new System.Windows.Forms.TextBox();
            this.btas_cancel = new System.Windows.Forms.Button();
            this.btas_ok = new System.Windows.Forms.Button();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.tas_partname = new System.Windows.Forms.TextBox();
            this.tas_scan = new System.Windows.Forms.TextBox();
            this.panel37 = new System.Windows.Forms.Panel();
            this.tas_alertnotfound = new System.Windows.Forms.TextBox();
            this.label136 = new System.Windows.Forms.Label();
            this.label179 = new System.Windows.Forms.Label();
            this.label180 = new System.Windows.Forms.Label();
            this.dataGridView6 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.side = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel16 = new System.Windows.Forms.Panel();
            this.buttonLM_Refresh = new System.Windows.Forms.Button();
            this.label20 = new System.Windows.Forms.Label();
            this.tabManage = new System.Windows.Forms.TabPage();
            this.panel7 = new System.Windows.Forms.Panel();
            this.label144 = new System.Windows.Forms.Label();
            this.label145 = new System.Windows.Forms.Label();
            this.bt_lmcline = new System.Windows.Forms.Button();
            this.bt_Lusersetup = new System.Windows.Forms.Button();
            this.bt_LLogs = new System.Windows.Forms.Button();
            this.bt_leditstock = new System.Windows.Forms.Button();
            this.bt_partdata = new System.Windows.Forms.Button();
            this.tabPart = new System.Windows.Forms.TabPage();
            this.panel42 = new System.Windows.Forms.Panel();
            this.label121 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.groupBox15 = new System.Windows.Forms.GroupBox();
            this.bt_pddelpart = new System.Windows.Forms.Button();
            this.label124 = new System.Windows.Forms.Label();
            this.label123 = new System.Windows.Forms.Label();
            this.dataGridView17 = new System.Windows.Forms.DataGridView();
            this.dataGridView16 = new System.Windows.Forms.DataGridView();
            this.button13 = new System.Windows.Forms.Button();
            this.bt_pdaddpart = new System.Windows.Forms.Button();
            this.GroupAddPartControl = new System.Windows.Forms.GroupBox();
            this.button29 = new System.Windows.Forms.Button();
            this.label131 = new System.Windows.Forms.Label();
            this.label178 = new System.Windows.Forms.Label();
            this.tpd_partname = new System.Windows.Forms.TextBox();
            this.tpd_partno = new System.Windows.Forms.TextBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.tpd_search = new System.Windows.Forms.TextBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.pd_partno = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pd_partname = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pd_model = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pd_fifo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pd_rank = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pd_maxstock = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pd_minstock = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pd_qty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pd_createuser = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pd_createdate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pd_updateuser = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pd_updatedate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pd_supplier = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pd_packagepart = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pd_parttype = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label120 = new System.Windows.Forms.Label();
            this.tabEditStock = new System.Windows.Forms.TabPage();
            this.dataGridView10 = new System.Windows.Forms.DataGridView();
            this.es_boxid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.es_rank = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.es_stocklotid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.es_stocktime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.es_stock_transaction = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.es_stock_user = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.es_stock_status = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.es_pickup_time = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.es_pickup_transaction = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.es_pickup_user = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.es_pickup_line = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridView9 = new System.Windows.Forms.DataGridView();
            this.es_partno = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.es_stockava = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel30 = new System.Windows.Forms.Panel();
            this.label114 = new System.Windows.Forms.Label();
            this.panel28 = new System.Windows.Forms.Panel();
            this.label83 = new System.Windows.Forms.Label();
            this.panel29 = new System.Windows.Forms.Panel();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.label122 = new System.Windows.Forms.Label();
            this.tes_side = new System.Windows.Forms.TextBox();
            this.label96 = new System.Windows.Forms.Label();
            this.des_pickuptime = new System.Windows.Forms.DateTimePicker();
            this.ces_pickupline = new System.Windows.Forms.ComboBox();
            this.ces_pickupuser = new System.Windows.Forms.ComboBox();
            this.label87 = new System.Windows.Forms.Label();
            this.ces_stockstatus = new System.Windows.Forms.ComboBox();
            this.label86 = new System.Windows.Forms.Label();
            this.tes_maxstock = new System.Windows.Forms.TextBox();
            this.label88 = new System.Windows.Forms.Label();
            this.tes_minstock = new System.Windows.Forms.TextBox();
            this.label89 = new System.Windows.Forms.Label();
            this.label91 = new System.Windows.Forms.Label();
            this.label92 = new System.Windows.Forms.Label();
            this.label93 = new System.Windows.Forms.Label();
            this.label97 = new System.Windows.Forms.Label();
            this.label102 = new System.Windows.Forms.Label();
            this.label103 = new System.Windows.Forms.Label();
            this.label104 = new System.Windows.Forms.Label();
            this.label107 = new System.Windows.Forms.Label();
            this.label108 = new System.Windows.Forms.Label();
            this.label109 = new System.Windows.Forms.Label();
            this.label110 = new System.Windows.Forms.Label();
            this.label111 = new System.Windows.Forms.Label();
            this.tes_stock = new System.Windows.Forms.TextBox();
            this.tes_timein = new System.Windows.Forms.TextBox();
            this.tes_userin = new System.Windows.Forms.TextBox();
            this.tes_slot = new System.Windows.Forms.TextBox();
            this.tes_zone = new System.Windows.Forms.TextBox();
            this.tes_shelf = new System.Windows.Forms.TextBox();
            this.tes_area = new System.Windows.Forms.TextBox();
            this.tes_maker = new System.Windows.Forms.TextBox();
            this.tes_rank = new System.Windows.Forms.TextBox();
            this.tes_qty = new System.Windows.Forms.TextBox();
            this.tes_boxid = new System.Windows.Forms.TextBox();
            this.btEditstock_del = new System.Windows.Forms.Button();
            this.btEditstock_OK = new System.Windows.Forms.Button();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.label129 = new System.Windows.Forms.Label();
            this.tes_searchetc = new System.Windows.Forms.TextBox();
            this.btEditstock_search = new System.Windows.Forms.Button();
            this.label112 = new System.Windows.Forms.Label();
            this.label113 = new System.Windows.Forms.Label();
            this.tes_partname = new System.Windows.Forms.TextBox();
            this.tes_searchpart = new System.Windows.Forms.TextBox();
            this.tabLogs = new System.Windows.Forms.TabPage();
            this.panel48 = new System.Windows.Forms.Panel();
            this.label146 = new System.Windows.Forms.Label();
            this.panel47 = new System.Windows.Forms.Panel();
            this.groupBox19 = new System.Windows.Forms.GroupBox();
            this.checklog_date = new System.Windows.Forms.CheckBox();
            this.label81 = new System.Windows.Forms.Label();
            this.tlog_boxid = new System.Windows.Forms.TextBox();
            this.clog_event = new System.Windows.Forms.ComboBox();
            this.btlog_search = new System.Windows.Forms.Button();
            this.datelog_to = new System.Windows.Forms.DateTimePicker();
            this.datelog_form = new System.Windows.Forms.DateTimePicker();
            this.label152 = new System.Windows.Forms.Label();
            this.label151 = new System.Windows.Forms.Label();
            this.label150 = new System.Windows.Forms.Label();
            this.label149 = new System.Windows.Forms.Label();
            this.label148 = new System.Windows.Forms.Label();
            this.tlog_qty = new System.Windows.Forms.TextBox();
            this.tlog_partno = new System.Windows.Forms.TextBox();
            this.label147 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.tlog_eventid = new System.Windows.Forms.TextBox();
            this.panel27 = new System.Windows.Forms.Panel();
            this.label78 = new System.Windows.Forms.Label();
            this.label79 = new System.Windows.Forms.Label();
            this.panel34 = new System.Windows.Forms.Panel();
            this.dataGridView13 = new System.Windows.Forms.DataGridView();
            this.transaction_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tlog_page = new System.Windows.Forms.TextBox();
            this.btlog_back = new System.Windows.Forms.Button();
            this.btlog_next = new System.Windows.Forms.Button();
            this.panel49 = new System.Windows.Forms.Panel();
            this.label153 = new System.Windows.Forms.Label();
            this.dataGridView8 = new System.Windows.Forms.DataGridView();
            this.PartsNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.User_Out = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Spare_ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.User_In = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Time_In = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.waiting_in = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel33 = new System.Windows.Forms.Panel();
            this.button23 = new System.Windows.Forms.Button();
            this.label90 = new System.Windows.Forms.Label();
            this.tabMCLine = new System.Windows.Forms.TabPage();
            this.panel32 = new System.Windows.Forms.Panel();
            this.label130 = new System.Windows.Forms.Label();
            this.label132 = new System.Windows.Forms.Label();
            this.panel51 = new System.Windows.Forms.Panel();
            this.label115 = new System.Windows.Forms.Label();
            this.panel43 = new System.Windows.Forms.Panel();
            this.groupBox18 = new System.Windows.Forms.GroupBox();
            this.label140 = new System.Windows.Forms.Label();
            this.panel45 = new System.Windows.Forms.Panel();
            this.btmc_canceladd = new System.Windows.Forms.Button();
            this.btmc_okadd = new System.Windows.Forms.Button();
            this.btmc_search = new System.Windows.Forms.Button();
            this.btmc_del = new System.Windows.Forms.Button();
            this.btmc_add = new System.Windows.Forms.Button();
            this.label139 = new System.Windows.Forms.Label();
            this.cmc_part = new System.Windows.Forms.ComboBox();
            this.label138 = new System.Windows.Forms.Label();
            this.cmc_line = new System.Windows.Forms.ComboBox();
            this.panel46 = new System.Windows.Forms.Panel();
            this.dataGridView12 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn24 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridView11 = new System.Windows.Forms.DataGridView();
            this.LineNo_MCLine = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel50 = new System.Windows.Forms.Panel();
            this.label141 = new System.Windows.Forms.Label();
            this.panel44 = new System.Windows.Forms.Panel();
            this.label106 = new System.Windows.Forms.Label();
            this.tabAddPart = new System.Windows.Forms.TabPage();
            this.panel11 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btAdd_delLocal = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.label174 = new System.Windows.Forms.Label();
            this.comAdd_Line = new System.Windows.Forms.ComboBox();
            this.dataGridView3 = new System.Windows.Forms.DataGridView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label116 = new System.Windows.Forms.Label();
            this.comAdd_floor = new System.Windows.Forms.ComboBox();
            this.btAdd_del = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.label175 = new System.Windows.Forms.Label();
            this.label176 = new System.Windows.Forms.Label();
            this.label168 = new System.Windows.Forms.Label();
            this.label172 = new System.Windows.Forms.Label();
            this.comAdd_Slot = new System.Windows.Forms.ComboBox();
            this.comAdd_side = new System.Windows.Forms.ComboBox();
            this.comAdd_Zone = new System.Windows.Forms.ComboBox();
            this.comAdd_shelf = new System.Windows.Forms.ComboBox();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.panel10 = new System.Windows.Forms.Panel();
            this.btAdd_cancel = new System.Windows.Forms.Button();
            this.btAdd_save = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.checkAdd_Rank = new System.Windows.Forms.CheckBox();
            this.checkAdd_FIFO = new System.Windows.Forms.CheckBox();
            this.label126 = new System.Windows.Forms.Label();
            this.label125 = new System.Windows.Forms.Label();
            this.label101 = new System.Windows.Forms.Label();
            this.label82 = new System.Windows.Forms.Label();
            this.label94 = new System.Windows.Forms.Label();
            this.label95 = new System.Windows.Forms.Label();
            this.label98 = new System.Windows.Forms.Label();
            this.label99 = new System.Windows.Forms.Label();
            this.label100 = new System.Windows.Forms.Label();
            this.comAdd_parttype = new System.Windows.Forms.ComboBox();
            this.comAdd_partpackage = new System.Windows.Forms.ComboBox();
            this.numAdd_minstock = new System.Windows.Forms.NumericUpDown();
            this.textAdd_model = new System.Windows.Forms.TextBox();
            this.numAdd_maxstock = new System.Windows.Forms.NumericUpDown();
            this.numAdd_qty = new System.Windows.Forms.NumericUpDown();
            this.textAdd_supplier = new System.Windows.Forms.TextBox();
            this.textAdd_partname = new System.Windows.Forms.TextBox();
            this.textAdd_partno = new System.Windows.Forms.TextBox();
            this.panel9 = new System.Windows.Forms.Panel();
            this.label105 = new System.Windows.Forms.Label();
            this.panel17 = new System.Windows.Forms.Panel();
            this.label80 = new System.Windows.Forms.Label();
            this.tabEditPart = new System.Windows.Forms.TabPage();
            this.panel31 = new System.Windows.Forms.Panel();
            this.label127 = new System.Windows.Forms.Label();
            this.label128 = new System.Windows.Forms.Label();
            this.panel13 = new System.Windows.Forms.Panel();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.btEdit_cancelLine = new System.Windows.Forms.Button();
            this.btEdit_addLineNo = new System.Windows.Forms.Button();
            this.btEdit_delLine = new System.Windows.Forms.Button();
            this.btEdit_addLine = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.comEdit_line = new System.Windows.Forms.ComboBox();
            this.dataGridView4 = new System.Windows.Forms.DataGridView();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label119 = new System.Windows.Forms.Label();
            this.comEdit_floor = new System.Windows.Forms.ComboBox();
            this.btEdit_delLocate = new System.Windows.Forms.Button();
            this.btEdit_add = new System.Windows.Forms.Button();
            this.btEdit_cancelLocal = new System.Windows.Forms.Button();
            this.btEdit_addLocal = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.comEdit_slot = new System.Windows.Forms.ComboBox();
            this.comEdit_side = new System.Windows.Forms.ComboBox();
            this.comEdit_zone = new System.Windows.Forms.ComboBox();
            this.comEdit_shelf = new System.Windows.Forms.ComboBox();
            this.dataGridView5 = new System.Windows.Forms.DataGridView();
            this.panel14 = new System.Windows.Forms.Panel();
            this.btEdit_cancel = new System.Windows.Forms.Button();
            this.btEdit_save = new System.Windows.Forms.Button();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.checkEdit_rank = new System.Windows.Forms.CheckBox();
            this.checkEdit_FIFO = new System.Windows.Forms.CheckBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.comEdit_parttype = new System.Windows.Forms.ComboBox();
            this.comEdit_partpackage = new System.Windows.Forms.ComboBox();
            this.numEdit_min = new System.Windows.Forms.NumericUpDown();
            this.tEdit_model = new System.Windows.Forms.TextBox();
            this.numEdit_max = new System.Windows.Forms.NumericUpDown();
            this.numEdit_qty = new System.Windows.Forms.NumericUpDown();
            this.tEdit_supplier = new System.Windows.Forms.TextBox();
            this.tEdit_partname = new System.Windows.Forms.TextBox();
            this.tEdit_partno = new System.Windows.Forms.TextBox();
            this.panel15 = new System.Windows.Forms.Panel();
            this.label19 = new System.Windows.Forms.Label();
            this.panel12 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btmain_back = new System.Windows.Forms.Button();
            this.btmain_logout = new System.Windows.Forms.Button();
            this.timer_addstock = new System.Windows.Forms.Timer(this.components);
            this.timer_pickupstock = new System.Windows.Forms.Timer(this.components);
            this.timer_logout = new System.Windows.Forms.Timer(this.components);
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabMain.SuspendLayout();
            this.tabPickup.SuspendLayout();
            this.pap_lotidwrong.SuspendLayout();
            this.panel25.SuspendLayout();
            this.panel26.SuspendLayout();
            this.panel24.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.panel23.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView7)).BeginInit();
            this.panel20.SuspendLayout();
            this.tabAddStock.SuspendLayout();
            this.panel22.SuspendLayout();
            this.pas_duplicate.SuspendLayout();
            this.panel52.SuspendLayout();
            this.panel21.SuspendLayout();
            this.panel19.SuspendLayout();
            this.panel18.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.panel37.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView6)).BeginInit();
            this.panel16.SuspendLayout();
            this.tabManage.SuspendLayout();
            this.panel7.SuspendLayout();
            this.tabPart.SuspendLayout();
            this.panel42.SuspendLayout();
            this.panel6.SuspendLayout();
            this.groupBox15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView16)).BeginInit();
            this.GroupAddPartControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panel5.SuspendLayout();
            this.tabEditStock.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView9)).BeginInit();
            this.panel30.SuspendLayout();
            this.panel28.SuspendLayout();
            this.panel29.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.tabLogs.SuspendLayout();
            this.panel48.SuspendLayout();
            this.panel47.SuspendLayout();
            this.groupBox19.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.panel27.SuspendLayout();
            this.panel34.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView13)).BeginInit();
            this.panel49.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView8)).BeginInit();
            this.panel33.SuspendLayout();
            this.tabMCLine.SuspendLayout();
            this.panel32.SuspendLayout();
            this.panel51.SuspendLayout();
            this.panel43.SuspendLayout();
            this.groupBox18.SuspendLayout();
            this.panel45.SuspendLayout();
            this.panel46.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView11)).BeginInit();
            this.panel50.SuspendLayout();
            this.panel44.SuspendLayout();
            this.tabAddPart.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel8.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.panel10.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numAdd_minstock)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numAdd_maxstock)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numAdd_qty)).BeginInit();
            this.panel9.SuspendLayout();
            this.panel17.SuspendLayout();
            this.tabEditPart.SuspendLayout();
            this.panel31.SuspendLayout();
            this.panel13.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView4)).BeginInit();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView5)).BeginInit();
            this.panel14.SuspendLayout();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numEdit_min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numEdit_max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numEdit_qty)).BeginInit();
            this.panel15.SuspendLayout();
            this.panel12.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(57)))), ((int)(((byte)(108)))));
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(242, 69);
            this.panel1.TabIndex = 0;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::ThaiArrow_StoreSystem.Properties.Resources.icons8_menu_filled_100;
            this.pictureBox1.Location = new System.Drawing.Point(43, 19);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(30, 30);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(91)))), ((int)(((byte)(150)))));
            this.panel2.Controls.Add(this.label30);
            this.panel2.Location = new System.Drawing.Point(241, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(661, 69);
            this.panel2.TabIndex = 0;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label30.ForeColor = System.Drawing.Color.White;
            this.label30.Location = new System.Drawing.Point(39, 14);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(427, 31);
            this.label30.TabIndex = 18;
            this.label30.Text = "STORE MANAGEMENT SYSTEM";
            // 
            // labelLineUserNameTopPanel
            // 
            this.labelLineUserNameTopPanel.AutoSize = true;
            this.labelLineUserNameTopPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.labelLineUserNameTopPanel.ForeColor = System.Drawing.Color.White;
            this.labelLineUserNameTopPanel.Location = new System.Drawing.Point(1082, 26);
            this.labelLineUserNameTopPanel.Name = "labelLineUserNameTopPanel";
            this.labelLineUserNameTopPanel.Size = new System.Drawing.Size(225, 20);
            this.labelLineUserNameTopPanel.TabIndex = 28;
            this.labelLineUserNameTopPanel.Text = "________________________";
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label66.ForeColor = System.Drawing.Color.White;
            this.label66.Location = new System.Drawing.Point(979, 26);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(97, 20);
            this.label66.TabIndex = 27;
            this.label66.Text = "User Name :";
            // 
            // labelLevel
            // 
            this.labelLevel.AutoSize = true;
            this.labelLevel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.labelLevel.ForeColor = System.Drawing.Color.White;
            this.labelLevel.Location = new System.Drawing.Point(1442, 26);
            this.labelLevel.Name = "labelLevel";
            this.labelLevel.Size = new System.Drawing.Size(99, 20);
            this.labelLevel.TabIndex = 26;
            this.labelLevel.Text = "__________";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label31.ForeColor = System.Drawing.Color.White;
            this.label31.Location = new System.Drawing.Point(1382, 26);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(54, 20);
            this.label31.TabIndex = 25;
            this.label31.Text = "Level :";
            // 
            // buttonClose
            // 
            this.buttonClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(27)))), ((int)(((byte)(17)))));
            this.buttonClose.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.buttonClose.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(35)))), ((int)(((byte)(23)))));
            this.buttonClose.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(194)))), ((int)(((byte)(199)))));
            this.buttonClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.buttonClose.ForeColor = System.Drawing.Color.White;
            this.buttonClose.Location = new System.Drawing.Point(1877, 12);
            this.buttonClose.Name = "buttonClose";
            this.buttonClose.Size = new System.Drawing.Size(25, 25);
            this.buttonClose.TabIndex = 29;
            this.buttonClose.Text = "X";
            this.buttonClose.UseVisualStyleBackColor = false;
            this.buttonClose.Click += new System.EventHandler(this.ButtonClose_Click);
            // 
            // buttonMinimize
            // 
            this.buttonMinimize.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(27)))), ((int)(((byte)(17)))));
            this.buttonMinimize.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.buttonMinimize.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(35)))), ((int)(((byte)(23)))));
            this.buttonMinimize.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(194)))), ((int)(((byte)(199)))));
            this.buttonMinimize.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonMinimize.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.buttonMinimize.ForeColor = System.Drawing.Color.White;
            this.buttonMinimize.Location = new System.Drawing.Point(1848, 12);
            this.buttonMinimize.Name = "buttonMinimize";
            this.buttonMinimize.Size = new System.Drawing.Size(25, 25);
            this.buttonMinimize.TabIndex = 30;
            this.buttonMinimize.Text = "_";
            this.buttonMinimize.UseVisualStyleBackColor = false;
            this.buttonMinimize.Click += new System.EventHandler(this.ButtonMinimize_Click);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(31)))), ((int)(((byte)(75)))));
            this.panel3.Controls.Add(this.label1);
            this.panel3.Location = new System.Drawing.Point(0, 69);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(242, 69);
            this.panel3.TabIndex = 31;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(47, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(149, 29);
            this.label1.TabIndex = 0;
            this.label1.Text = "MAIN MENU";
            this.label1.Click += new System.EventHandler(this.Label1_Click);
            // 
            // bt_Pickup
            // 
            this.bt_Pickup.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.bt_Pickup.FlatAppearance.BorderSize = 0;
            this.bt_Pickup.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.bt_Pickup.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_Pickup.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt_Pickup.ForeColor = System.Drawing.Color.White;
            this.bt_Pickup.Location = new System.Drawing.Point(0, 144);
            this.bt_Pickup.Name = "bt_Pickup";
            this.bt_Pickup.Size = new System.Drawing.Size(242, 69);
            this.bt_Pickup.TabIndex = 32;
            this.bt_Pickup.Text = "PICKUP PART";
            this.bt_Pickup.UseVisualStyleBackColor = false;
            this.bt_Pickup.Click += new System.EventHandler(this.Bt_Pickup_Click);
            // 
            // bt_addStock
            // 
            this.bt_addStock.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.bt_addStock.FlatAppearance.BorderSize = 0;
            this.bt_addStock.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.bt_addStock.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_addStock.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt_addStock.ForeColor = System.Drawing.Color.White;
            this.bt_addStock.Location = new System.Drawing.Point(0, 219);
            this.bt_addStock.Name = "bt_addStock";
            this.bt_addStock.Size = new System.Drawing.Size(242, 69);
            this.bt_addStock.TabIndex = 33;
            this.bt_addStock.Text = "ADD STOCK";
            this.bt_addStock.UseVisualStyleBackColor = false;
            this.bt_addStock.Click += new System.EventHandler(this.Bt_addStock_Click);
            // 
            // bt_manage
            // 
            this.bt_manage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.bt_manage.FlatAppearance.BorderSize = 0;
            this.bt_manage.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.bt_manage.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_manage.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt_manage.ForeColor = System.Drawing.Color.White;
            this.bt_manage.Location = new System.Drawing.Point(0, 294);
            this.bt_manage.Name = "bt_manage";
            this.bt_manage.Size = new System.Drawing.Size(242, 69);
            this.bt_manage.TabIndex = 34;
            this.bt_manage.Text = "MANAGEMENT";
            this.bt_manage.UseVisualStyleBackColor = false;
            this.bt_manage.Click += new System.EventHandler(this.Bt_manage_Click);
            // 
            // bt_userSetup
            // 
            this.bt_userSetup.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.bt_userSetup.FlatAppearance.BorderSize = 0;
            this.bt_userSetup.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.bt_userSetup.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_userSetup.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt_userSetup.ForeColor = System.Drawing.Color.White;
            this.bt_userSetup.Location = new System.Drawing.Point(0, 369);
            this.bt_userSetup.Name = "bt_userSetup";
            this.bt_userSetup.Size = new System.Drawing.Size(242, 69);
            this.bt_userSetup.TabIndex = 35;
            this.bt_userSetup.Text = "USER SETUP";
            this.bt_userSetup.UseVisualStyleBackColor = false;
            this.bt_userSetup.Click += new System.EventHandler(this.Bt_userSetup_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabMain);
            this.tabControl1.Controls.Add(this.tabPickup);
            this.tabControl1.Controls.Add(this.tabAddStock);
            this.tabControl1.Controls.Add(this.tabManage);
            this.tabControl1.Controls.Add(this.tabPart);
            this.tabControl1.Controls.Add(this.tabEditStock);
            this.tabControl1.Controls.Add(this.tabLogs);
            this.tabControl1.Controls.Add(this.tabMCLine);
            this.tabControl1.Controls.Add(this.tabAddPart);
            this.tabControl1.Controls.Add(this.tabEditPart);
            this.tabControl1.Location = new System.Drawing.Point(241, 70);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1678, 1011);
            this.tabControl1.TabIndex = 36;
            // 
            // tabMain
            // 
            this.tabMain.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.tabMain.Controls.Add(this.button8);
            this.tabMain.Controls.Add(this.bt_LAddStock);
            this.tabMain.Controls.Add(this.bt_LPickupPart);
            this.tabMain.Location = new System.Drawing.Point(4, 22);
            this.tabMain.Name = "tabMain";
            this.tabMain.Padding = new System.Windows.Forms.Padding(3);
            this.tabMain.Size = new System.Drawing.Size(1670, 985);
            this.tabMain.TabIndex = 0;
            this.tabMain.Text = "Main Menu";
            // 
            // button8
            // 
            this.button8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(91)))), ((int)(((byte)(150)))));
            this.button8.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.button8.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(124)))), ((int)(((byte)(131)))), ((int)(((byte)(139)))));
            this.button8.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(41)))), ((int)(((byte)(43)))));
            this.button8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button8.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button8.ForeColor = System.Drawing.Color.White;
            this.button8.Image = global::ThaiArrow_StoreSystem.Properties.Resources.icons8_bad_decision_100;
            this.button8.ImageAlign = System.Drawing.ContentAlignment.BottomRight;
            this.button8.Location = new System.Drawing.Point(1116, 6);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(540, 973);
            this.button8.TabIndex = 2;
            this.button8.Text = "MANAGMENT";
            this.button8.UseVisualStyleBackColor = false;
            this.button8.Click += new System.EventHandler(this.Button8_Click);
            // 
            // bt_LAddStock
            // 
            this.bt_LAddStock.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(31)))), ((int)(((byte)(75)))));
            this.bt_LAddStock.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.bt_LAddStock.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(124)))), ((int)(((byte)(131)))), ((int)(((byte)(139)))));
            this.bt_LAddStock.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(41)))), ((int)(((byte)(43)))));
            this.bt_LAddStock.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_LAddStock.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt_LAddStock.ForeColor = System.Drawing.Color.White;
            this.bt_LAddStock.Image = global::ThaiArrow_StoreSystem.Properties.Resources.icons8_monitor_100;
            this.bt_LAddStock.ImageAlign = System.Drawing.ContentAlignment.BottomRight;
            this.bt_LAddStock.Location = new System.Drawing.Point(565, 10);
            this.bt_LAddStock.Name = "bt_LAddStock";
            this.bt_LAddStock.Size = new System.Drawing.Size(540, 973);
            this.bt_LAddStock.TabIndex = 1;
            this.bt_LAddStock.Text = "ADD STOCK";
            this.bt_LAddStock.UseVisualStyleBackColor = false;
            this.bt_LAddStock.Click += new System.EventHandler(this.Bt_LAddStock_Click);
            // 
            // bt_LPickupPart
            // 
            this.bt_LPickupPart.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(57)))), ((int)(((byte)(108)))));
            this.bt_LPickupPart.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.bt_LPickupPart.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(124)))), ((int)(((byte)(131)))), ((int)(((byte)(139)))));
            this.bt_LPickupPart.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(41)))), ((int)(((byte)(43)))));
            this.bt_LPickupPart.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_LPickupPart.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt_LPickupPart.ForeColor = System.Drawing.Color.White;
            this.bt_LPickupPart.Image = global::ThaiArrow_StoreSystem.Properties.Resources.Plan;
            this.bt_LPickupPart.ImageAlign = System.Drawing.ContentAlignment.BottomRight;
            this.bt_LPickupPart.Location = new System.Drawing.Point(6, 6);
            this.bt_LPickupPart.Name = "bt_LPickupPart";
            this.bt_LPickupPart.Size = new System.Drawing.Size(540, 973);
            this.bt_LPickupPart.TabIndex = 0;
            this.bt_LPickupPart.Text = "PICKUP PART";
            this.bt_LPickupPart.UseVisualStyleBackColor = false;
            this.bt_LPickupPart.Click += new System.EventHandler(this.Bt_LPickupPart_Click);
            // 
            // tabPickup
            // 
            this.tabPickup.Controls.Add(this.pap_lotidwrong);
            this.tabPickup.Controls.Add(this.panel25);
            this.tabPickup.Controls.Add(this.panel26);
            this.tabPickup.Controls.Add(this.panel24);
            this.tabPickup.Controls.Add(this.panel23);
            this.tabPickup.Controls.Add(this.dataGridView7);
            this.tabPickup.Controls.Add(this.panel20);
            this.tabPickup.Location = new System.Drawing.Point(4, 22);
            this.tabPickup.Name = "tabPickup";
            this.tabPickup.Padding = new System.Windows.Forms.Padding(3);
            this.tabPickup.Size = new System.Drawing.Size(1670, 985);
            this.tabPickup.TabIndex = 1;
            this.tabPickup.Text = "PickupPart";
            this.tabPickup.UseVisualStyleBackColor = true;
            // 
            // pap_lotidwrong
            // 
            this.pap_lotidwrong.BackColor = System.Drawing.Color.Red;
            this.pap_lotidwrong.Controls.Add(this.label85);
            this.pap_lotidwrong.Controls.Add(this.label84);
            this.pap_lotidwrong.Location = new System.Drawing.Point(531, 108);
            this.pap_lotidwrong.Name = "pap_lotidwrong";
            this.pap_lotidwrong.Size = new System.Drawing.Size(690, 294);
            this.pap_lotidwrong.TabIndex = 144;
            this.pap_lotidwrong.Visible = false;
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label85.ForeColor = System.Drawing.SystemColors.Control;
            this.label85.Location = new System.Drawing.Point(35, 213);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(619, 31);
            this.label85.TabIndex = 7;
            this.label85.Text = "Please pickup the correct pieces in order by Lot ID";
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Font = new System.Drawing.Font("Microsoft Sans Serif", 50F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label84.ForeColor = System.Drawing.SystemColors.Control;
            this.label84.Location = new System.Drawing.Point(116, 71);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(425, 76);
            this.label84.TabIndex = 6;
            this.label84.Text = "Lot ID Wrong";
            // 
            // panel25
            // 
            this.panel25.BackColor = System.Drawing.Color.Red;
            this.panel25.Controls.Add(this.label73);
            this.panel25.Controls.Add(this.label74);
            this.panel25.Location = new System.Drawing.Point(531, 108);
            this.panel25.Name = "panel25";
            this.panel25.Size = new System.Drawing.Size(690, 294);
            this.panel25.TabIndex = 140;
            this.panel25.Visible = false;
            // 
            // label73
            // 
            this.label73.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label73.ForeColor = System.Drawing.Color.White;
            this.label73.Location = new System.Drawing.Point(42, 137);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(629, 118);
            this.label73.TabIndex = 5;
            this.label73.Text = "The part list you requested is not available. ";
            this.label73.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label74
            // 
            this.label74.Font = new System.Drawing.Font("Microsoft Sans Serif", 35F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label74.ForeColor = System.Drawing.Color.White;
            this.label74.Location = new System.Drawing.Point(41, 34);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(629, 118);
            this.label74.TabIndex = 4;
            this.label74.Text = "Please make a request first.";
            this.label74.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel26
            // 
            this.panel26.BackColor = System.Drawing.Color.Red;
            this.panel26.Controls.Add(this.tp_alertwrong);
            this.panel26.Controls.Add(this.label75);
            this.panel26.Controls.Add(this.label76);
            this.panel26.Controls.Add(this.label77);
            this.panel26.Location = new System.Drawing.Point(531, 106);
            this.panel26.Name = "panel26";
            this.panel26.Size = new System.Drawing.Size(690, 294);
            this.panel26.TabIndex = 141;
            this.panel26.Visible = false;
            // 
            // tp_alertwrong
            // 
            this.tp_alertwrong.BackColor = System.Drawing.Color.Red;
            this.tp_alertwrong.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tp_alertwrong.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tp_alertwrong.ForeColor = System.Drawing.Color.White;
            this.tp_alertwrong.Location = new System.Drawing.Point(297, 20);
            this.tp_alertwrong.Name = "tp_alertwrong";
            this.tp_alertwrong.Size = new System.Drawing.Size(371, 46);
            this.tp_alertwrong.TabIndex = 8;
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Font = new System.Drawing.Font("Microsoft Sans Serif", 50F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label75.ForeColor = System.Drawing.SystemColors.Control;
            this.label75.Location = new System.Drawing.Point(148, 91);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(404, 76);
            this.label75.TabIndex = 6;
            this.label75.Text = "Wrong Parts";
            // 
            // label76
            // 
            this.label76.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label76.ForeColor = System.Drawing.Color.White;
            this.label76.Location = new System.Drawing.Point(36, 156);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(629, 118);
            this.label76.TabIndex = 4;
            this.label76.Text = "Please choose the correct parts!!!";
            this.label76.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label77
            // 
            this.label77.Font = new System.Drawing.Font("Microsoft Sans Serif", 50F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label77.ForeColor = System.Drawing.Color.White;
            this.label77.Location = new System.Drawing.Point(3, 2);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(276, 74);
            this.label77.TabIndex = 3;
            this.label77.Text = "Part.";
            this.label77.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel24
            // 
            this.panel24.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(227)))), ((int)(((byte)(238)))));
            this.panel24.Controls.Add(this.groupBox9);
            this.panel24.Controls.Add(this.groupBox10);
            this.panel24.Location = new System.Drawing.Point(0, 607);
            this.panel24.Name = "panel24";
            this.panel24.Size = new System.Drawing.Size(1664, 382);
            this.panel24.TabIndex = 138;
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.label118);
            this.groupBox9.Controls.Add(this.tp_side);
            this.groupBox9.Controls.Add(this.label52);
            this.groupBox9.Controls.Add(this.tp_maxstock);
            this.groupBox9.Controls.Add(this.label53);
            this.groupBox9.Controls.Add(this.tp_ccode);
            this.groupBox9.Controls.Add(this.label54);
            this.groupBox9.Controls.Add(this.tp_minstock);
            this.groupBox9.Controls.Add(this.label55);
            this.groupBox9.Controls.Add(this.label56);
            this.groupBox9.Controls.Add(this.label57);
            this.groupBox9.Controls.Add(this.label58);
            this.groupBox9.Controls.Add(this.label59);
            this.groupBox9.Controls.Add(this.label60);
            this.groupBox9.Controls.Add(this.label61);
            this.groupBox9.Controls.Add(this.label62);
            this.groupBox9.Controls.Add(this.label63);
            this.groupBox9.Controls.Add(this.label64);
            this.groupBox9.Controls.Add(this.label65);
            this.groupBox9.Controls.Add(this.label67);
            this.groupBox9.Controls.Add(this.label68);
            this.groupBox9.Controls.Add(this.label69);
            this.groupBox9.Controls.Add(this.label70);
            this.groupBox9.Controls.Add(this.tp_stock);
            this.groupBox9.Controls.Add(this.tp_ordertime);
            this.groupBox9.Controls.Add(this.tp_userout);
            this.groupBox9.Controls.Add(this.tp_statusDB);
            this.groupBox9.Controls.Add(this.tp_statusM);
            this.groupBox9.Controls.Add(this.tp_slot);
            this.groupBox9.Controls.Add(this.tp_zone);
            this.groupBox9.Controls.Add(this.tp_shelf);
            this.groupBox9.Controls.Add(this.tp_area);
            this.groupBox9.Controls.Add(this.tp_dcode);
            this.groupBox9.Controls.Add(this.tp_mpn);
            this.groupBox9.Controls.Add(this.tp_maker);
            this.groupBox9.Controls.Add(this.tp_rank);
            this.groupBox9.Controls.Add(this.tp_qty);
            this.groupBox9.Controls.Add(this.tp_boxid);
            this.groupBox9.Controls.Add(this.bt_pickup_cancel);
            this.groupBox9.Controls.Add(this.btPickup_OK);
            this.groupBox9.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox9.Location = new System.Drawing.Point(423, 42);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(1134, 318);
            this.groupBox9.TabIndex = 130;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Details :";
            // 
            // label118
            // 
            this.label118.AutoSize = true;
            this.label118.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label118.Location = new System.Drawing.Point(520, 247);
            this.label118.Name = "label118";
            this.label118.Size = new System.Drawing.Size(49, 20);
            this.label118.TabIndex = 144;
            this.label118.Text = "Side :";
            // 
            // tp_side
            // 
            this.tp_side.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tp_side.Location = new System.Drawing.Point(520, 270);
            this.tp_side.Name = "tp_side";
            this.tp_side.Size = new System.Drawing.Size(173, 26);
            this.tp_side.TabIndex = 143;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label52.Location = new System.Drawing.Point(729, 247);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(91, 20);
            this.label52.TabIndex = 142;
            this.label52.Text = "Max Stock :";
            // 
            // tp_maxstock
            // 
            this.tp_maxstock.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tp_maxstock.Location = new System.Drawing.Point(733, 270);
            this.tp_maxstock.Name = "tp_maxstock";
            this.tp_maxstock.Size = new System.Drawing.Size(173, 26);
            this.tp_maxstock.TabIndex = 141;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label53.Location = new System.Drawing.Point(57, 247);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(74, 20);
            this.label53.TabIndex = 140;
            this.label53.Text = "C. Code :";
            // 
            // tp_ccode
            // 
            this.tp_ccode.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tp_ccode.Location = new System.Drawing.Point(61, 270);
            this.tp_ccode.Name = "tp_ccode";
            this.tp_ccode.Size = new System.Drawing.Size(173, 26);
            this.tp_ccode.TabIndex = 139;
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label54.Location = new System.Drawing.Point(940, 247);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(87, 20);
            this.label54.TabIndex = 138;
            this.label54.Text = "Min Stock :";
            // 
            // tp_minstock
            // 
            this.tp_minstock.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tp_minstock.Location = new System.Drawing.Point(944, 270);
            this.tp_minstock.Name = "tp_minstock";
            this.tp_minstock.Size = new System.Drawing.Size(173, 26);
            this.tp_minstock.TabIndex = 137;
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label55.Location = new System.Drawing.Point(940, 180);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(58, 20);
            this.label55.TabIndex = 136;
            this.label55.Text = "Stock :";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label56.Location = new System.Drawing.Point(940, 115);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(95, 20);
            this.label56.TabIndex = 135;
            this.label56.Text = "Order Time :";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label57.Location = new System.Drawing.Point(940, 47);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(81, 20);
            this.label57.TabIndex = 134;
            this.label57.Text = "User Out :";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label58.Location = new System.Drawing.Point(729, 180);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(101, 20);
            this.label58.TabIndex = 133;
            this.label58.Text = "Status (DB) :";
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label59.Location = new System.Drawing.Point(729, 115);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(91, 20);
            this.label59.TabIndex = 132;
            this.label59.Text = "Status (M) :";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label60.Location = new System.Drawing.Point(729, 47);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(45, 20);
            this.label60.TabIndex = 131;
            this.label60.Text = "Slot :";
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label61.Location = new System.Drawing.Point(516, 180);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(54, 20);
            this.label61.TabIndex = 130;
            this.label61.Text = "Zone :";
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label62.Location = new System.Drawing.Point(516, 115);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(53, 20);
            this.label62.TabIndex = 129;
            this.label62.Text = "Floor :";
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label63.Location = new System.Drawing.Point(516, 47);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(54, 20);
            this.label63.TabIndex = 128;
            this.label63.Text = "Shelf :";
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label64.Location = new System.Drawing.Point(282, 180);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(75, 20);
            this.label64.TabIndex = 127;
            this.label64.Text = "D. Code :";
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label65.Location = new System.Drawing.Point(282, 115);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(51, 20);
            this.label65.TabIndex = 126;
            this.label65.Text = "MPN :";
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label67.Location = new System.Drawing.Point(282, 47);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(61, 20);
            this.label67.TabIndex = 125;
            this.label67.Text = "Maker :";
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label68.Location = new System.Drawing.Point(57, 183);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(55, 20);
            this.label68.TabIndex = 124;
            this.label68.Text = "Rank :";
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label69.Location = new System.Drawing.Point(57, 118);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(41, 20);
            this.label69.TabIndex = 123;
            this.label69.Text = "Qty :";
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label70.Location = new System.Drawing.Point(57, 50);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(65, 20);
            this.label70.TabIndex = 122;
            this.label70.Text = "Box ID :";
            // 
            // tp_stock
            // 
            this.tp_stock.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tp_stock.Location = new System.Drawing.Point(944, 203);
            this.tp_stock.Name = "tp_stock";
            this.tp_stock.Size = new System.Drawing.Size(173, 26);
            this.tp_stock.TabIndex = 121;
            // 
            // tp_ordertime
            // 
            this.tp_ordertime.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tp_ordertime.Location = new System.Drawing.Point(944, 138);
            this.tp_ordertime.Name = "tp_ordertime";
            this.tp_ordertime.Size = new System.Drawing.Size(173, 26);
            this.tp_ordertime.TabIndex = 120;
            // 
            // tp_userout
            // 
            this.tp_userout.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tp_userout.Location = new System.Drawing.Point(944, 70);
            this.tp_userout.Name = "tp_userout";
            this.tp_userout.Size = new System.Drawing.Size(173, 26);
            this.tp_userout.TabIndex = 119;
            // 
            // tp_statusDB
            // 
            this.tp_statusDB.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tp_statusDB.Location = new System.Drawing.Point(733, 203);
            this.tp_statusDB.Name = "tp_statusDB";
            this.tp_statusDB.Size = new System.Drawing.Size(173, 26);
            this.tp_statusDB.TabIndex = 118;
            // 
            // tp_statusM
            // 
            this.tp_statusM.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tp_statusM.Location = new System.Drawing.Point(733, 138);
            this.tp_statusM.Name = "tp_statusM";
            this.tp_statusM.Size = new System.Drawing.Size(173, 26);
            this.tp_statusM.TabIndex = 117;
            // 
            // tp_slot
            // 
            this.tp_slot.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tp_slot.Location = new System.Drawing.Point(733, 70);
            this.tp_slot.Name = "tp_slot";
            this.tp_slot.Size = new System.Drawing.Size(173, 26);
            this.tp_slot.TabIndex = 116;
            // 
            // tp_zone
            // 
            this.tp_zone.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tp_zone.Location = new System.Drawing.Point(520, 206);
            this.tp_zone.Name = "tp_zone";
            this.tp_zone.Size = new System.Drawing.Size(173, 26);
            this.tp_zone.TabIndex = 115;
            // 
            // tp_shelf
            // 
            this.tp_shelf.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tp_shelf.Location = new System.Drawing.Point(520, 138);
            this.tp_shelf.Name = "tp_shelf";
            this.tp_shelf.Size = new System.Drawing.Size(173, 26);
            this.tp_shelf.TabIndex = 114;
            // 
            // tp_area
            // 
            this.tp_area.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tp_area.Location = new System.Drawing.Point(520, 70);
            this.tp_area.Name = "tp_area";
            this.tp_area.Size = new System.Drawing.Size(173, 26);
            this.tp_area.TabIndex = 113;
            // 
            // tp_dcode
            // 
            this.tp_dcode.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tp_dcode.Location = new System.Drawing.Point(286, 203);
            this.tp_dcode.Name = "tp_dcode";
            this.tp_dcode.Size = new System.Drawing.Size(173, 26);
            this.tp_dcode.TabIndex = 112;
            // 
            // tp_mpn
            // 
            this.tp_mpn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tp_mpn.Location = new System.Drawing.Point(286, 138);
            this.tp_mpn.Name = "tp_mpn";
            this.tp_mpn.Size = new System.Drawing.Size(173, 26);
            this.tp_mpn.TabIndex = 111;
            // 
            // tp_maker
            // 
            this.tp_maker.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tp_maker.Location = new System.Drawing.Point(286, 70);
            this.tp_maker.Name = "tp_maker";
            this.tp_maker.Size = new System.Drawing.Size(173, 26);
            this.tp_maker.TabIndex = 110;
            // 
            // tp_rank
            // 
            this.tp_rank.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tp_rank.Location = new System.Drawing.Point(61, 206);
            this.tp_rank.Name = "tp_rank";
            this.tp_rank.Size = new System.Drawing.Size(173, 26);
            this.tp_rank.TabIndex = 109;
            // 
            // tp_qty
            // 
            this.tp_qty.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tp_qty.Location = new System.Drawing.Point(61, 141);
            this.tp_qty.Name = "tp_qty";
            this.tp_qty.Size = new System.Drawing.Size(173, 26);
            this.tp_qty.TabIndex = 108;
            // 
            // tp_boxid
            // 
            this.tp_boxid.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tp_boxid.Location = new System.Drawing.Point(61, 73);
            this.tp_boxid.Name = "tp_boxid";
            this.tp_boxid.Size = new System.Drawing.Size(173, 26);
            this.tp_boxid.TabIndex = 107;
            // 
            // bt_pickup_cancel
            // 
            this.bt_pickup_cancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(67)))), ((int)(((byte)(53)))));
            this.bt_pickup_cancel.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(57)))), ((int)(((byte)(108)))));
            this.bt_pickup_cancel.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(102)))), ((int)(((byte)(185)))));
            this.bt_pickup_cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_pickup_cancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.bt_pickup_cancel.ForeColor = System.Drawing.Color.White;
            this.bt_pickup_cancel.Location = new System.Drawing.Point(380, 262);
            this.bt_pickup_cancel.Name = "bt_pickup_cancel";
            this.bt_pickup_cancel.Size = new System.Drawing.Size(134, 42);
            this.bt_pickup_cancel.TabIndex = 102;
            this.bt_pickup_cancel.Text = "CANCEL";
            this.bt_pickup_cancel.UseVisualStyleBackColor = false;
            // 
            // btPickup_OK
            // 
            this.btPickup_OK.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(91)))), ((int)(((byte)(150)))));
            this.btPickup_OK.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(57)))), ((int)(((byte)(108)))));
            this.btPickup_OK.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(102)))), ((int)(((byte)(185)))));
            this.btPickup_OK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btPickup_OK.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.btPickup_OK.ForeColor = System.Drawing.Color.White;
            this.btPickup_OK.Location = new System.Drawing.Point(240, 262);
            this.btPickup_OK.Name = "btPickup_OK";
            this.btPickup_OK.Size = new System.Drawing.Size(134, 42);
            this.btPickup_OK.TabIndex = 101;
            this.btPickup_OK.Text = "OK";
            this.btPickup_OK.UseVisualStyleBackColor = false;
            this.btPickup_OK.Click += new System.EventHandler(this.BtPickup_OK_Click);
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.label71);
            this.groupBox10.Controls.Add(this.label72);
            this.groupBox10.Controls.Add(this.tp_partname);
            this.groupBox10.Controls.Add(this.tp_scan);
            this.groupBox10.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox10.Location = new System.Drawing.Point(27, 61);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(353, 318);
            this.groupBox10.TabIndex = 0;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Pickup Part Control :";
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label71.Location = new System.Drawing.Point(10, 54);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(91, 20);
            this.label71.TabIndex = 109;
            this.label71.Text = "Scan Part  :";
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label72.Location = new System.Drawing.Point(9, 193);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(92, 20);
            this.label72.TabIndex = 108;
            this.label72.Text = "Part Name :";
            // 
            // tp_partname
            // 
            this.tp_partname.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tp_partname.Location = new System.Drawing.Point(14, 228);
            this.tp_partname.Name = "tp_partname";
            this.tp_partname.Size = new System.Drawing.Size(285, 26);
            this.tp_partname.TabIndex = 106;
            // 
            // tp_scan
            // 
            this.tp_scan.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tp_scan.Location = new System.Drawing.Point(14, 77);
            this.tp_scan.Name = "tp_scan";
            this.tp_scan.Size = new System.Drawing.Size(285, 26);
            this.tp_scan.TabIndex = 0;
            this.tp_scan.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tp_scan_KeyDown);
            // 
            // panel23
            // 
            this.panel23.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(57)))), ((int)(((byte)(108)))));
            this.panel23.Controls.Add(this.label51);
            this.panel23.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel23.Location = new System.Drawing.Point(0, 566);
            this.panel23.Name = "panel23";
            this.panel23.Size = new System.Drawing.Size(1671, 45);
            this.panel23.TabIndex = 137;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label51.ForeColor = System.Drawing.Color.White;
            this.label51.Location = new System.Drawing.Point(723, 8);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(135, 29);
            this.label51.TabIndex = 35;
            this.label51.Text = "Pickup Part";
            // 
            // dataGridView7
            // 
            this.dataGridView7.AllowUserToAddRows = false;
            this.dataGridView7.AllowUserToDeleteRows = false;
            this.dataGridView7.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridView7.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridView7.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn13,
            this.dataGridViewTextBoxColumn18,
            this.MCline,
            this.dataGridViewTextBoxColumn16,
            this.fifo_enable,
            this.rank_enable,
            this.dataGridViewTextBoxColumn17,
            this.dataGridViewTextBoxColumn20,
            this.dataGridViewTextBoxColumn22,
            this.dataGridViewTextBoxColumn21,
            this.side_pickup,
            this.dataGridViewTextBoxColumn23,
            this.minstock,
            this.Qty,
            this.qty_stock,
            this.pu_boxID,
            this.dataGridViewTextBoxColumn26,
            this.dataGridViewTextBoxColumn25,
            this.StatusM});
            this.dataGridView7.Location = new System.Drawing.Point(0, 48);
            this.dataGridView7.Name = "dataGridView7";
            this.dataGridView7.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView7.ShowCellErrors = false;
            this.dataGridView7.ShowCellToolTips = false;
            this.dataGridView7.ShowEditingIcon = false;
            this.dataGridView7.ShowRowErrors = false;
            this.dataGridView7.Size = new System.Drawing.Size(1667, 523);
            this.dataGridView7.TabIndex = 32;
            this.dataGridView7.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridView7_CellContentClick);
            this.dataGridView7.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridView7_CellContentClick);
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.DataPropertyName = "No";
            this.dataGridViewTextBoxColumn13.HeaderText = "No";
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            this.dataGridViewTextBoxColumn13.Width = 50;
            // 
            // dataGridViewTextBoxColumn18
            // 
            this.dataGridViewTextBoxColumn18.DataPropertyName = "partNo";
            this.dataGridViewTextBoxColumn18.HeaderText = "Part No";
            this.dataGridViewTextBoxColumn18.Name = "dataGridViewTextBoxColumn18";
            this.dataGridViewTextBoxColumn18.Width = 125;
            // 
            // MCline
            // 
            this.MCline.DataPropertyName = "mcline";
            this.MCline.HeaderText = "Mc Line";
            this.MCline.Name = "MCline";
            // 
            // dataGridViewTextBoxColumn16
            // 
            this.dataGridViewTextBoxColumn16.DataPropertyName = "Model";
            this.dataGridViewTextBoxColumn16.HeaderText = "Model";
            this.dataGridViewTextBoxColumn16.Name = "dataGridViewTextBoxColumn16";
            // 
            // fifo_enable
            // 
            this.fifo_enable.HeaderText = "FIFO Enable";
            this.fifo_enable.Name = "fifo_enable";
            this.fifo_enable.Width = 75;
            // 
            // rank_enable
            // 
            this.rank_enable.DataPropertyName = "rank_enable";
            this.rank_enable.HeaderText = "Rank Enable";
            this.rank_enable.Name = "rank_enable";
            this.rank_enable.Width = 75;
            // 
            // dataGridViewTextBoxColumn17
            // 
            this.dataGridViewTextBoxColumn17.DataPropertyName = "rank_enabled";
            this.dataGridViewTextBoxColumn17.HeaderText = "Rank";
            this.dataGridViewTextBoxColumn17.Name = "dataGridViewTextBoxColumn17";
            this.dataGridViewTextBoxColumn17.Width = 75;
            // 
            // dataGridViewTextBoxColumn20
            // 
            this.dataGridViewTextBoxColumn20.DataPropertyName = "shelf";
            this.dataGridViewTextBoxColumn20.HeaderText = "Shelf";
            this.dataGridViewTextBoxColumn20.Name = "dataGridViewTextBoxColumn20";
            this.dataGridViewTextBoxColumn20.Width = 50;
            // 
            // dataGridViewTextBoxColumn22
            // 
            this.dataGridViewTextBoxColumn22.DataPropertyName = "Zone";
            this.dataGridViewTextBoxColumn22.HeaderText = "Zone";
            this.dataGridViewTextBoxColumn22.Name = "dataGridViewTextBoxColumn22";
            this.dataGridViewTextBoxColumn22.Width = 70;
            // 
            // dataGridViewTextBoxColumn21
            // 
            this.dataGridViewTextBoxColumn21.DataPropertyName = "floor";
            this.dataGridViewTextBoxColumn21.HeaderText = "Floor";
            this.dataGridViewTextBoxColumn21.Name = "dataGridViewTextBoxColumn21";
            this.dataGridViewTextBoxColumn21.Width = 70;
            // 
            // side_pickup
            // 
            this.side_pickup.HeaderText = "Side";
            this.side_pickup.Name = "side_pickup";
            this.side_pickup.Width = 50;
            // 
            // dataGridViewTextBoxColumn23
            // 
            this.dataGridViewTextBoxColumn23.DataPropertyName = "Slot";
            this.dataGridViewTextBoxColumn23.HeaderText = "Slot";
            this.dataGridViewTextBoxColumn23.Name = "dataGridViewTextBoxColumn23";
            this.dataGridViewTextBoxColumn23.Width = 50;
            // 
            // minstock
            // 
            this.minstock.HeaderText = "Min Stock";
            this.minstock.Name = "minstock";
            // 
            // Qty
            // 
            this.Qty.DataPropertyName = "Qty";
            this.Qty.HeaderText = "Qty ";
            this.Qty.Name = "Qty";
            this.Qty.Width = 75;
            // 
            // qty_stock
            // 
            this.qty_stock.HeaderText = "Qty Stock";
            this.qty_stock.Name = "qty_stock";
            this.qty_stock.Width = 75;
            // 
            // pu_boxID
            // 
            this.pu_boxID.HeaderText = "Box ID";
            this.pu_boxID.Name = "pu_boxID";
            this.pu_boxID.Width = 125;
            // 
            // dataGridViewTextBoxColumn26
            // 
            this.dataGridViewTextBoxColumn26.DataPropertyName = "lot_id";
            this.dataGridViewTextBoxColumn26.HeaderText = "Lot ID";
            this.dataGridViewTextBoxColumn26.Name = "dataGridViewTextBoxColumn26";
            this.dataGridViewTextBoxColumn26.Width = 125;
            // 
            // dataGridViewTextBoxColumn25
            // 
            this.dataGridViewTextBoxColumn25.DataPropertyName = "Order Time";
            this.dataGridViewTextBoxColumn25.HeaderText = "Order Time";
            this.dataGridViewTextBoxColumn25.Name = "dataGridViewTextBoxColumn25";
            this.dataGridViewTextBoxColumn25.Width = 140;
            // 
            // StatusM
            // 
            this.StatusM.HeaderText = "Status(M)";
            this.StatusM.Name = "StatusM";
            // 
            // panel20
            // 
            this.panel20.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(57)))), ((int)(((byte)(108)))));
            this.panel20.Controls.Add(this.button2);
            this.panel20.Controls.Add(this.label50);
            this.panel20.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel20.Location = new System.Drawing.Point(3, 3);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(1664, 45);
            this.panel20.TabIndex = 31;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(91)))), ((int)(((byte)(150)))));
            this.button2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(57)))), ((int)(((byte)(108)))));
            this.button2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(102)))), ((int)(((byte)(185)))));
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.button2.ForeColor = System.Drawing.Color.White;
            this.button2.Image = global::ThaiArrow_StoreSystem.Properties.Resources.Refresh;
            this.button2.Location = new System.Drawing.Point(1619, 2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(40, 40);
            this.button2.TabIndex = 74;
            this.button2.UseVisualStyleBackColor = false;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold);
            this.label50.ForeColor = System.Drawing.Color.White;
            this.label50.Location = new System.Drawing.Point(773, 9);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(135, 26);
            this.label50.TabIndex = 28;
            this.label50.Text = "Pickup Part";
            // 
            // tabAddStock
            // 
            this.tabAddStock.Controls.Add(this.panel22);
            this.tabAddStock.Controls.Add(this.pas_duplicate);
            this.tabAddStock.Controls.Add(this.panel52);
            this.tabAddStock.Controls.Add(this.panel21);
            this.tabAddStock.Controls.Add(this.panel19);
            this.tabAddStock.Controls.Add(this.panel18);
            this.tabAddStock.Controls.Add(this.panel37);
            this.tabAddStock.Controls.Add(this.dataGridView6);
            this.tabAddStock.Controls.Add(this.panel16);
            this.tabAddStock.Location = new System.Drawing.Point(4, 22);
            this.tabAddStock.Name = "tabAddStock";
            this.tabAddStock.Size = new System.Drawing.Size(1670, 985);
            this.tabAddStock.TabIndex = 2;
            this.tabAddStock.Text = "AddStock";
            this.tabAddStock.UseVisualStyleBackColor = true;
            // 
            // panel22
            // 
            this.panel22.BackColor = System.Drawing.Color.Red;
            this.panel22.Controls.Add(this.tas_alertoverstock);
            this.panel22.Controls.Add(this.label47);
            this.panel22.Controls.Add(this.label48);
            this.panel22.Controls.Add(this.label49);
            this.panel22.Location = new System.Drawing.Point(582, 190);
            this.panel22.Name = "panel22";
            this.panel22.Size = new System.Drawing.Size(690, 294);
            this.panel22.TabIndex = 139;
            this.panel22.Visible = false;
            // 
            // tas_alertoverstock
            // 
            this.tas_alertoverstock.BackColor = System.Drawing.Color.Red;
            this.tas_alertoverstock.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tas_alertoverstock.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tas_alertoverstock.ForeColor = System.Drawing.Color.White;
            this.tas_alertoverstock.Location = new System.Drawing.Point(285, 20);
            this.tas_alertoverstock.Name = "tas_alertoverstock";
            this.tas_alertoverstock.Size = new System.Drawing.Size(307, 46);
            this.tas_alertoverstock.TabIndex = 8;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Microsoft Sans Serif", 50F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.ForeColor = System.Drawing.SystemColors.Control;
            this.label47.Location = new System.Drawing.Point(90, 98);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(492, 76);
            this.label47.TabIndex = 6;
            this.label47.Text = "Over MaxStock";
            // 
            // label48
            // 
            this.label48.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.ForeColor = System.Drawing.Color.White;
            this.label48.Location = new System.Drawing.Point(36, 156);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(629, 118);
            this.label48.TabIndex = 4;
            this.label48.Text = "Please recheck quantity in store mangement system!!!";
            this.label48.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label49
            // 
            this.label49.Font = new System.Drawing.Font("Microsoft Sans Serif", 50F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.ForeColor = System.Drawing.Color.White;
            this.label49.Location = new System.Drawing.Point(119, 2);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(160, 74);
            this.label49.TabIndex = 3;
            this.label49.Text = "Part.";
            this.label49.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pas_duplicate
            // 
            this.pas_duplicate.BackColor = System.Drawing.Color.Red;
            this.pas_duplicate.Controls.Add(this.tas_alertboxid);
            this.pas_duplicate.Controls.Add(this.label24);
            this.pas_duplicate.Controls.Add(this.label45);
            this.pas_duplicate.Controls.Add(this.label46);
            this.pas_duplicate.Location = new System.Drawing.Point(585, 185);
            this.pas_duplicate.Name = "pas_duplicate";
            this.pas_duplicate.Size = new System.Drawing.Size(690, 294);
            this.pas_duplicate.TabIndex = 137;
            this.pas_duplicate.Visible = false;
            // 
            // tas_alertboxid
            // 
            this.tas_alertboxid.BackColor = System.Drawing.Color.Red;
            this.tas_alertboxid.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tas_alertboxid.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tas_alertboxid.ForeColor = System.Drawing.Color.White;
            this.tas_alertboxid.Location = new System.Drawing.Point(285, 20);
            this.tas_alertboxid.Name = "tas_alertboxid";
            this.tas_alertboxid.Size = new System.Drawing.Size(307, 46);
            this.tas_alertboxid.TabIndex = 8;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 50F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.SystemColors.Control;
            this.label24.Location = new System.Drawing.Point(90, 98);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(511, 76);
            this.label24.TabIndex = 6;
            this.label24.Text = "BoxID Duplicate";
            // 
            // label45
            // 
            this.label45.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.ForeColor = System.Drawing.Color.White;
            this.label45.Location = new System.Drawing.Point(36, 156);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(629, 118);
            this.label45.TabIndex = 4;
            this.label45.Text = "Please recheck BoxID in store mangement system!!!";
            this.label45.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label46
            // 
            this.label46.Font = new System.Drawing.Font("Microsoft Sans Serif", 50F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.ForeColor = System.Drawing.Color.White;
            this.label46.Location = new System.Drawing.Point(119, 2);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(160, 74);
            this.label46.TabIndex = 3;
            this.label46.Text = "Part.";
            this.label46.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel52
            // 
            this.panel52.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(91)))), ((int)(((byte)(150)))));
            this.panel52.Controls.Add(this.tas_alertadd);
            this.panel52.Controls.Add(this.label171);
            this.panel52.Location = new System.Drawing.Point(588, 180);
            this.panel52.Name = "panel52";
            this.panel52.Size = new System.Drawing.Size(690, 294);
            this.panel52.TabIndex = 134;
            this.panel52.Visible = false;
            // 
            // tas_alertadd
            // 
            this.tas_alertadd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(91)))), ((int)(((byte)(150)))));
            this.tas_alertadd.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tas_alertadd.Font = new System.Drawing.Font("Microsoft Sans Serif", 40F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tas_alertadd.ForeColor = System.Drawing.Color.White;
            this.tas_alertadd.Location = new System.Drawing.Point(174, 163);
            this.tas_alertadd.Name = "tas_alertadd";
            this.tas_alertadd.Size = new System.Drawing.Size(307, 61);
            this.tas_alertadd.TabIndex = 9;
            // 
            // label171
            // 
            this.label171.Font = new System.Drawing.Font("Microsoft Sans Serif", 50F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label171.ForeColor = System.Drawing.Color.White;
            this.label171.Location = new System.Drawing.Point(161, 41);
            this.label171.Name = "label171";
            this.label171.Size = new System.Drawing.Size(350, 74);
            this.label171.TabIndex = 3;
            this.label171.Text = "Add Part";
            this.label171.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel21
            // 
            this.panel21.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(91)))), ((int)(((byte)(150)))));
            this.panel21.Controls.Add(this.tas_alert_addStockcom);
            this.panel21.Controls.Add(this.label44);
            this.panel21.Location = new System.Drawing.Point(578, 192);
            this.panel21.Name = "panel21";
            this.panel21.Size = new System.Drawing.Size(690, 294);
            this.panel21.TabIndex = 138;
            this.panel21.Visible = false;
            // 
            // tas_alert_addStockcom
            // 
            this.tas_alert_addStockcom.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(91)))), ((int)(((byte)(150)))));
            this.tas_alert_addStockcom.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tas_alert_addStockcom.Font = new System.Drawing.Font("Microsoft Sans Serif", 40F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tas_alert_addStockcom.ForeColor = System.Drawing.Color.White;
            this.tas_alert_addStockcom.Location = new System.Drawing.Point(174, 163);
            this.tas_alert_addStockcom.Name = "tas_alert_addStockcom";
            this.tas_alert_addStockcom.Size = new System.Drawing.Size(307, 61);
            this.tas_alert_addStockcom.TabIndex = 9;
            // 
            // label44
            // 
            this.label44.Font = new System.Drawing.Font("Microsoft Sans Serif", 50F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.ForeColor = System.Drawing.Color.White;
            this.label44.Location = new System.Drawing.Point(161, 41);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(350, 74);
            this.label44.TabIndex = 3;
            this.label44.Text = "Add Stock";
            this.label44.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel19
            // 
            this.panel19.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(57)))), ((int)(((byte)(108)))));
            this.panel19.Controls.Add(this.label25);
            this.panel19.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel19.Location = new System.Drawing.Point(0, 566);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(1671, 45);
            this.panel19.TabIndex = 136;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label25.ForeColor = System.Drawing.Color.White;
            this.label25.Location = new System.Drawing.Point(723, 8);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(188, 29);
            this.label25.TabIndex = 35;
            this.label25.Text = "Add Stock Menu";
            // 
            // panel18
            // 
            this.panel18.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(227)))), ((int)(((byte)(238)))));
            this.panel18.Controls.Add(this.groupBox7);
            this.panel18.Controls.Add(this.groupBox8);
            this.panel18.Location = new System.Drawing.Point(0, 607);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(1664, 382);
            this.panel18.TabIndex = 135;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.label117);
            this.groupBox7.Controls.Add(this.tas_side);
            this.groupBox7.Controls.Add(this.labellll);
            this.groupBox7.Controls.Add(this.tas_maxstock);
            this.groupBox7.Controls.Add(this.label43);
            this.groupBox7.Controls.Add(this.tas_ccode);
            this.groupBox7.Controls.Add(this.label42);
            this.groupBox7.Controls.Add(this.tas_spare);
            this.groupBox7.Controls.Add(this.label39);
            this.groupBox7.Controls.Add(this.label40);
            this.groupBox7.Controls.Add(this.label41);
            this.groupBox7.Controls.Add(this.label36);
            this.groupBox7.Controls.Add(this.label37);
            this.groupBox7.Controls.Add(this.label38);
            this.groupBox7.Controls.Add(this.label33);
            this.groupBox7.Controls.Add(this.label34);
            this.groupBox7.Controls.Add(this.label35);
            this.groupBox7.Controls.Add(this.label28);
            this.groupBox7.Controls.Add(this.label29);
            this.groupBox7.Controls.Add(this.label32);
            this.groupBox7.Controls.Add(this.label27);
            this.groupBox7.Controls.Add(this.label26);
            this.groupBox7.Controls.Add(this.label22);
            this.groupBox7.Controls.Add(this.tas_stock);
            this.groupBox7.Controls.Add(this.tas_timeIn);
            this.groupBox7.Controls.Add(this.tas_userIn);
            this.groupBox7.Controls.Add(this.tas_statusDB);
            this.groupBox7.Controls.Add(this.tas_statusM);
            this.groupBox7.Controls.Add(this.tas_slot);
            this.groupBox7.Controls.Add(this.tas_zone);
            this.groupBox7.Controls.Add(this.tas_shelf);
            this.groupBox7.Controls.Add(this.tas_area);
            this.groupBox7.Controls.Add(this.tas_dcode);
            this.groupBox7.Controls.Add(this.tas_mpn);
            this.groupBox7.Controls.Add(this.tas_maker);
            this.groupBox7.Controls.Add(this.tas_rank);
            this.groupBox7.Controls.Add(this.tas_qty);
            this.groupBox7.Controls.Add(this.tas_boxID);
            this.groupBox7.Controls.Add(this.btas_cancel);
            this.groupBox7.Controls.Add(this.btas_ok);
            this.groupBox7.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox7.Location = new System.Drawing.Point(423, 42);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(1134, 318);
            this.groupBox7.TabIndex = 130;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Details :";
            // 
            // label117
            // 
            this.label117.AutoSize = true;
            this.label117.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label117.Location = new System.Drawing.Point(524, 243);
            this.label117.Name = "label117";
            this.label117.Size = new System.Drawing.Size(49, 20);
            this.label117.TabIndex = 144;
            this.label117.Text = "Side :";
            // 
            // tas_side
            // 
            this.tas_side.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tas_side.Location = new System.Drawing.Point(520, 266);
            this.tas_side.Name = "tas_side";
            this.tas_side.Size = new System.Drawing.Size(173, 26);
            this.tas_side.TabIndex = 143;
            // 
            // labellll
            // 
            this.labellll.AutoSize = true;
            this.labellll.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.labellll.Location = new System.Drawing.Point(729, 247);
            this.labellll.Name = "labellll";
            this.labellll.Size = new System.Drawing.Size(91, 20);
            this.labellll.TabIndex = 142;
            this.labellll.Text = "Max Stock :";
            // 
            // tas_maxstock
            // 
            this.tas_maxstock.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tas_maxstock.Location = new System.Drawing.Point(733, 270);
            this.tas_maxstock.Name = "tas_maxstock";
            this.tas_maxstock.Size = new System.Drawing.Size(173, 26);
            this.tas_maxstock.TabIndex = 141;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label43.Location = new System.Drawing.Point(57, 247);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(74, 20);
            this.label43.TabIndex = 140;
            this.label43.Text = "C. Code :";
            // 
            // tas_ccode
            // 
            this.tas_ccode.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tas_ccode.Location = new System.Drawing.Point(61, 270);
            this.tas_ccode.Name = "tas_ccode";
            this.tas_ccode.Size = new System.Drawing.Size(173, 26);
            this.tas_ccode.TabIndex = 139;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label42.Location = new System.Drawing.Point(940, 247);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(87, 20);
            this.label42.TabIndex = 138;
            this.label42.Text = "Min Stock :";
            // 
            // tas_spare
            // 
            this.tas_spare.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tas_spare.Location = new System.Drawing.Point(944, 270);
            this.tas_spare.Name = "tas_spare";
            this.tas_spare.Size = new System.Drawing.Size(173, 26);
            this.tas_spare.TabIndex = 137;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label39.Location = new System.Drawing.Point(940, 180);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(58, 20);
            this.label39.TabIndex = 136;
            this.label39.Text = "Stock :";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label40.Location = new System.Drawing.Point(940, 115);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(69, 20);
            this.label40.TabIndex = 135;
            this.label40.Text = "Time In :";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label41.Location = new System.Drawing.Point(940, 47);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(69, 20);
            this.label41.TabIndex = 134;
            this.label41.Text = "User In :";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label36.Location = new System.Drawing.Point(729, 180);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(101, 20);
            this.label36.TabIndex = 133;
            this.label36.Text = "Status (DB) :";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label37.Location = new System.Drawing.Point(729, 115);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(91, 20);
            this.label37.TabIndex = 132;
            this.label37.Text = "Status (M) :";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label38.Location = new System.Drawing.Point(729, 47);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(45, 20);
            this.label38.TabIndex = 131;
            this.label38.Text = "Slot :";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label33.Location = new System.Drawing.Point(516, 180);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(54, 20);
            this.label33.TabIndex = 130;
            this.label33.Text = "Zone :";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label34.Location = new System.Drawing.Point(516, 115);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(53, 20);
            this.label34.TabIndex = 129;
            this.label34.Text = "Floor :";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label35.Location = new System.Drawing.Point(516, 47);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(54, 20);
            this.label35.TabIndex = 128;
            this.label35.Text = "Shelf :";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label28.Location = new System.Drawing.Point(282, 180);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(75, 20);
            this.label28.TabIndex = 127;
            this.label28.Text = "D. Code :";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label29.Location = new System.Drawing.Point(282, 115);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(51, 20);
            this.label29.TabIndex = 126;
            this.label29.Text = "MPN :";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label32.Location = new System.Drawing.Point(282, 47);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(61, 20);
            this.label32.TabIndex = 125;
            this.label32.Text = "Maker :";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label27.Location = new System.Drawing.Point(57, 183);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(55, 20);
            this.label27.TabIndex = 124;
            this.label27.Text = "Rank :";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label26.Location = new System.Drawing.Point(57, 118);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(41, 20);
            this.label26.TabIndex = 123;
            this.label26.Text = "Qty :";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label22.Location = new System.Drawing.Point(57, 50);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(65, 20);
            this.label22.TabIndex = 122;
            this.label22.Text = "Box ID :";
            // 
            // tas_stock
            // 
            this.tas_stock.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tas_stock.Location = new System.Drawing.Point(944, 203);
            this.tas_stock.Name = "tas_stock";
            this.tas_stock.Size = new System.Drawing.Size(173, 26);
            this.tas_stock.TabIndex = 121;
            // 
            // tas_timeIn
            // 
            this.tas_timeIn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tas_timeIn.Location = new System.Drawing.Point(944, 138);
            this.tas_timeIn.Name = "tas_timeIn";
            this.tas_timeIn.Size = new System.Drawing.Size(173, 26);
            this.tas_timeIn.TabIndex = 120;
            // 
            // tas_userIn
            // 
            this.tas_userIn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tas_userIn.Location = new System.Drawing.Point(944, 70);
            this.tas_userIn.Name = "tas_userIn";
            this.tas_userIn.Size = new System.Drawing.Size(173, 26);
            this.tas_userIn.TabIndex = 119;
            // 
            // tas_statusDB
            // 
            this.tas_statusDB.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tas_statusDB.Location = new System.Drawing.Point(733, 203);
            this.tas_statusDB.Name = "tas_statusDB";
            this.tas_statusDB.Size = new System.Drawing.Size(173, 26);
            this.tas_statusDB.TabIndex = 118;
            // 
            // tas_statusM
            // 
            this.tas_statusM.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tas_statusM.Location = new System.Drawing.Point(733, 138);
            this.tas_statusM.Name = "tas_statusM";
            this.tas_statusM.Size = new System.Drawing.Size(173, 26);
            this.tas_statusM.TabIndex = 117;
            // 
            // tas_slot
            // 
            this.tas_slot.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tas_slot.Location = new System.Drawing.Point(733, 70);
            this.tas_slot.Name = "tas_slot";
            this.tas_slot.Size = new System.Drawing.Size(173, 26);
            this.tas_slot.TabIndex = 116;
            // 
            // tas_zone
            // 
            this.tas_zone.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tas_zone.Location = new System.Drawing.Point(520, 206);
            this.tas_zone.Name = "tas_zone";
            this.tas_zone.Size = new System.Drawing.Size(173, 26);
            this.tas_zone.TabIndex = 115;
            // 
            // tas_shelf
            // 
            this.tas_shelf.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tas_shelf.Location = new System.Drawing.Point(520, 138);
            this.tas_shelf.Name = "tas_shelf";
            this.tas_shelf.Size = new System.Drawing.Size(173, 26);
            this.tas_shelf.TabIndex = 114;
            // 
            // tas_area
            // 
            this.tas_area.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tas_area.Location = new System.Drawing.Point(520, 70);
            this.tas_area.Name = "tas_area";
            this.tas_area.Size = new System.Drawing.Size(173, 26);
            this.tas_area.TabIndex = 113;
            // 
            // tas_dcode
            // 
            this.tas_dcode.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tas_dcode.Location = new System.Drawing.Point(286, 203);
            this.tas_dcode.Name = "tas_dcode";
            this.tas_dcode.Size = new System.Drawing.Size(173, 26);
            this.tas_dcode.TabIndex = 112;
            // 
            // tas_mpn
            // 
            this.tas_mpn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tas_mpn.Location = new System.Drawing.Point(286, 138);
            this.tas_mpn.Name = "tas_mpn";
            this.tas_mpn.Size = new System.Drawing.Size(173, 26);
            this.tas_mpn.TabIndex = 111;
            // 
            // tas_maker
            // 
            this.tas_maker.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tas_maker.Location = new System.Drawing.Point(286, 70);
            this.tas_maker.Name = "tas_maker";
            this.tas_maker.Size = new System.Drawing.Size(173, 26);
            this.tas_maker.TabIndex = 110;
            // 
            // tas_rank
            // 
            this.tas_rank.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tas_rank.Location = new System.Drawing.Point(61, 206);
            this.tas_rank.Name = "tas_rank";
            this.tas_rank.Size = new System.Drawing.Size(173, 26);
            this.tas_rank.TabIndex = 109;
            // 
            // tas_qty
            // 
            this.tas_qty.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tas_qty.Location = new System.Drawing.Point(61, 141);
            this.tas_qty.Name = "tas_qty";
            this.tas_qty.Size = new System.Drawing.Size(173, 26);
            this.tas_qty.TabIndex = 108;
            // 
            // tas_boxID
            // 
            this.tas_boxID.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tas_boxID.Location = new System.Drawing.Point(61, 73);
            this.tas_boxID.Name = "tas_boxID";
            this.tas_boxID.Size = new System.Drawing.Size(173, 26);
            this.tas_boxID.TabIndex = 107;
            // 
            // btas_cancel
            // 
            this.btas_cancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(67)))), ((int)(((byte)(53)))));
            this.btas_cancel.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(57)))), ((int)(((byte)(108)))));
            this.btas_cancel.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(102)))), ((int)(((byte)(185)))));
            this.btas_cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btas_cancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.btas_cancel.ForeColor = System.Drawing.Color.White;
            this.btas_cancel.Location = new System.Drawing.Point(371, 262);
            this.btas_cancel.Name = "btas_cancel";
            this.btas_cancel.Size = new System.Drawing.Size(134, 42);
            this.btas_cancel.TabIndex = 102;
            this.btas_cancel.Text = "CANCEL";
            this.btas_cancel.UseVisualStyleBackColor = false;
            this.btas_cancel.Click += new System.EventHandler(this.Btas_cancel_Click);
            // 
            // btas_ok
            // 
            this.btas_ok.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(91)))), ((int)(((byte)(150)))));
            this.btas_ok.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(57)))), ((int)(((byte)(108)))));
            this.btas_ok.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(102)))), ((int)(((byte)(185)))));
            this.btas_ok.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btas_ok.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.btas_ok.ForeColor = System.Drawing.Color.White;
            this.btas_ok.Location = new System.Drawing.Point(240, 262);
            this.btas_ok.Name = "btas_ok";
            this.btas_ok.Size = new System.Drawing.Size(134, 42);
            this.btas_ok.TabIndex = 101;
            this.btas_ok.Text = "OK";
            this.btas_ok.UseVisualStyleBackColor = false;
            this.btas_ok.Click += new System.EventHandler(this.Btas_ok_Click);
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.label21);
            this.groupBox8.Controls.Add(this.label23);
            this.groupBox8.Controls.Add(this.tas_partname);
            this.groupBox8.Controls.Add(this.tas_scan);
            this.groupBox8.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox8.Location = new System.Drawing.Point(27, 61);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(353, 318);
            this.groupBox8.TabIndex = 0;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Add Part Control :";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label21.Location = new System.Drawing.Point(10, 54);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(91, 20);
            this.label21.TabIndex = 109;
            this.label21.Text = "Scan Part  :";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label23.Location = new System.Drawing.Point(9, 193);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(92, 20);
            this.label23.TabIndex = 108;
            this.label23.Text = "Part Name :";
            // 
            // tas_partname
            // 
            this.tas_partname.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tas_partname.Location = new System.Drawing.Point(14, 228);
            this.tas_partname.Name = "tas_partname";
            this.tas_partname.Size = new System.Drawing.Size(285, 26);
            this.tas_partname.TabIndex = 106;
            // 
            // tas_scan
            // 
            this.tas_scan.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tas_scan.Location = new System.Drawing.Point(14, 77);
            this.tas_scan.Name = "tas_scan";
            this.tas_scan.Size = new System.Drawing.Size(285, 26);
            this.tas_scan.TabIndex = 0;
            this.tas_scan.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox44_KeyDown);
            // 
            // panel37
            // 
            this.panel37.BackColor = System.Drawing.Color.Red;
            this.panel37.Controls.Add(this.tas_alertnotfound);
            this.panel37.Controls.Add(this.label136);
            this.panel37.Controls.Add(this.label179);
            this.panel37.Controls.Add(this.label180);
            this.panel37.Location = new System.Drawing.Point(585, 189);
            this.panel37.Name = "panel37";
            this.panel37.Size = new System.Drawing.Size(690, 294);
            this.panel37.TabIndex = 133;
            this.panel37.Visible = false;
            // 
            // tas_alertnotfound
            // 
            this.tas_alertnotfound.BackColor = System.Drawing.Color.Red;
            this.tas_alertnotfound.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tas_alertnotfound.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tas_alertnotfound.ForeColor = System.Drawing.Color.White;
            this.tas_alertnotfound.Location = new System.Drawing.Point(285, 20);
            this.tas_alertnotfound.Name = "tas_alertnotfound";
            this.tas_alertnotfound.Size = new System.Drawing.Size(307, 46);
            this.tas_alertnotfound.TabIndex = 8;
            // 
            // label136
            // 
            this.label136.AutoSize = true;
            this.label136.Font = new System.Drawing.Font("Microsoft Sans Serif", 50F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label136.ForeColor = System.Drawing.SystemColors.Control;
            this.label136.Location = new System.Drawing.Point(170, 92);
            this.label136.Name = "label136";
            this.label136.Size = new System.Drawing.Size(344, 76);
            this.label136.TabIndex = 6;
            this.label136.Text = "Not Found";
            // 
            // label179
            // 
            this.label179.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label179.ForeColor = System.Drawing.Color.White;
            this.label179.Location = new System.Drawing.Point(36, 156);
            this.label179.Name = "label179";
            this.label179.Size = new System.Drawing.Size(629, 118);
            this.label179.TabIndex = 4;
            this.label179.Text = "Please recheck Part No in store mangement system!!!";
            this.label179.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label180
            // 
            this.label180.Font = new System.Drawing.Font("Microsoft Sans Serif", 50F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label180.ForeColor = System.Drawing.Color.White;
            this.label180.Location = new System.Drawing.Point(119, 2);
            this.label180.Name = "label180";
            this.label180.Size = new System.Drawing.Size(160, 74);
            this.label180.TabIndex = 3;
            this.label180.Text = "Part.";
            this.label180.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dataGridView6
            // 
            this.dataGridView6.AllowUserToAddRows = false;
            this.dataGridView6.AllowUserToDeleteRows = false;
            this.dataGridView6.AllowUserToResizeColumns = false;
            this.dataGridView6.AllowUserToResizeRows = false;
            this.dataGridView6.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridView6.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridView6.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8,
            this.side,
            this.dataGridViewTextBoxColumn9,
            this.dataGridViewTextBoxColumn10,
            this.dataGridViewTextBoxColumn11,
            this.dataGridViewTextBoxColumn12,
            this.dataGridViewTextBoxColumn14,
            this.dataGridViewTextBoxColumn15});
            this.dataGridView6.Location = new System.Drawing.Point(0, 48);
            this.dataGridView6.Name = "dataGridView6";
            this.dataGridView6.RowHeadersVisible = false;
            this.dataGridView6.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView6.ShowCellErrors = false;
            this.dataGridView6.ShowCellToolTips = false;
            this.dataGridView6.ShowEditingIcon = false;
            this.dataGridView6.ShowRowErrors = false;
            this.dataGridView6.Size = new System.Drawing.Size(1667, 523);
            this.dataGridView6.TabIndex = 31;
            this.dataGridView6.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridView6_CellContentClick);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "No";
            this.dataGridViewTextBoxColumn1.HeaderText = "No";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Width = 50;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Model";
            this.dataGridViewTextBoxColumn2.HeaderText = "Model";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "rank_enabled";
            this.dataGridViewTextBoxColumn5.HeaderText = "Rank";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "partNo";
            this.dataGridViewTextBoxColumn3.HeaderText = "Part No";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.Width = 200;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "partName";
            this.dataGridViewTextBoxColumn4.HeaderText = "Part Name";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.Width = 200;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "shelf";
            this.dataGridViewTextBoxColumn6.HeaderText = "Shelf";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.Width = 50;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "zone";
            this.dataGridViewTextBoxColumn7.HeaderText = "Zone";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.Width = 75;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "floor";
            this.dataGridViewTextBoxColumn8.HeaderText = "Floor";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.Width = 70;
            // 
            // side
            // 
            this.side.HeaderText = "Side";
            this.side.Name = "side";
            this.side.Width = 75;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.DataPropertyName = "Slot";
            this.dataGridViewTextBoxColumn9.HeaderText = "Slot";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.Width = 50;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.DataPropertyName = "StatusDB";
            this.dataGridViewTextBoxColumn10.HeaderText = "Status(DB)";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.DataPropertyName = "UserIn";
            this.dataGridViewTextBoxColumn11.HeaderText = "UserIn";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.Width = 80;
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.DataPropertyName = "TimeIn";
            this.dataGridViewTextBoxColumn12.HeaderText = "Time In";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.Width = 125;
            // 
            // dataGridViewTextBoxColumn14
            // 
            this.dataGridViewTextBoxColumn14.DataPropertyName = "Stock";
            this.dataGridViewTextBoxColumn14.HeaderText = "Stock";
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            this.dataGridViewTextBoxColumn14.Width = 75;
            // 
            // dataGridViewTextBoxColumn15
            // 
            this.dataGridViewTextBoxColumn15.DataPropertyName = "PartCode";
            this.dataGridViewTextBoxColumn15.HeaderText = "Part Code";
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            this.dataGridViewTextBoxColumn15.Width = 200;
            // 
            // panel16
            // 
            this.panel16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(57)))), ((int)(((byte)(108)))));
            this.panel16.Controls.Add(this.buttonLM_Refresh);
            this.panel16.Controls.Add(this.label20);
            this.panel16.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel16.Location = new System.Drawing.Point(0, 0);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(1670, 45);
            this.panel16.TabIndex = 30;
            // 
            // buttonLM_Refresh
            // 
            this.buttonLM_Refresh.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(91)))), ((int)(((byte)(150)))));
            this.buttonLM_Refresh.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(57)))), ((int)(((byte)(108)))));
            this.buttonLM_Refresh.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(102)))), ((int)(((byte)(185)))));
            this.buttonLM_Refresh.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonLM_Refresh.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.buttonLM_Refresh.ForeColor = System.Drawing.Color.White;
            this.buttonLM_Refresh.Image = global::ThaiArrow_StoreSystem.Properties.Resources.Refresh;
            this.buttonLM_Refresh.Location = new System.Drawing.Point(1619, 2);
            this.buttonLM_Refresh.Name = "buttonLM_Refresh";
            this.buttonLM_Refresh.Size = new System.Drawing.Size(40, 40);
            this.buttonLM_Refresh.TabIndex = 74;
            this.buttonLM_Refresh.UseVisualStyleBackColor = false;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold);
            this.label20.ForeColor = System.Drawing.Color.White;
            this.label20.Location = new System.Drawing.Point(773, 9);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(121, 26);
            this.label20.TabIndex = 28;
            this.label20.Text = "Add Stock";
            // 
            // tabManage
            // 
            this.tabManage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.tabManage.Controls.Add(this.panel7);
            this.tabManage.Controls.Add(this.bt_lmcline);
            this.tabManage.Controls.Add(this.bt_Lusersetup);
            this.tabManage.Controls.Add(this.bt_LLogs);
            this.tabManage.Controls.Add(this.bt_leditstock);
            this.tabManage.Controls.Add(this.bt_partdata);
            this.tabManage.Location = new System.Drawing.Point(4, 22);
            this.tabManage.Name = "tabManage";
            this.tabManage.Size = new System.Drawing.Size(1670, 985);
            this.tabManage.TabIndex = 3;
            this.tabManage.Text = "Management";
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.panel7.Controls.Add(this.label144);
            this.panel7.Controls.Add(this.label145);
            this.panel7.Location = new System.Drawing.Point(-4, 929);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(1674, 56);
            this.panel7.TabIndex = 6;
            // 
            // label144
            // 
            this.label144.AutoSize = true;
            this.label144.BackColor = System.Drawing.Color.Transparent;
            this.label144.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label144.ForeColor = System.Drawing.Color.White;
            this.label144.Location = new System.Drawing.Point(30, 22);
            this.label144.Name = "label144";
            this.label144.Size = new System.Drawing.Size(263, 20);
            this.label144.TabIndex = 91;
            this.label144.Text = "Store Management System @ 2019";
            this.label144.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label145
            // 
            this.label145.AutoSize = true;
            this.label145.BackColor = System.Drawing.Color.Transparent;
            this.label145.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label145.ForeColor = System.Drawing.Color.White;
            this.label145.Location = new System.Drawing.Point(1261, 15);
            this.label145.Name = "label145";
            this.label145.Size = new System.Drawing.Size(383, 20);
            this.label145.TabIndex = 90;
            this.label145.Text = "Thai Arrow Products Co., Ltd. (149 Bang Phli Factory)";
            this.label145.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // bt_lmcline
            // 
            this.bt_lmcline.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(91)))), ((int)(((byte)(150)))));
            this.bt_lmcline.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.bt_lmcline.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(124)))), ((int)(((byte)(131)))), ((int)(((byte)(139)))));
            this.bt_lmcline.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(41)))), ((int)(((byte)(43)))));
            this.bt_lmcline.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_lmcline.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt_lmcline.ForeColor = System.Drawing.Color.White;
            this.bt_lmcline.Image = global::ThaiArrow_StoreSystem.Properties.Resources.production;
            this.bt_lmcline.ImageAlign = System.Drawing.ContentAlignment.BottomRight;
            this.bt_lmcline.Location = new System.Drawing.Point(1339, 6);
            this.bt_lmcline.Name = "bt_lmcline";
            this.bt_lmcline.Size = new System.Drawing.Size(325, 927);
            this.bt_lmcline.TabIndex = 5;
            this.bt_lmcline.Text = "MC LINE";
            this.bt_lmcline.UseVisualStyleBackColor = false;
            this.bt_lmcline.Click += new System.EventHandler(this.Button15_Click);
            // 
            // bt_Lusersetup
            // 
            this.bt_Lusersetup.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(57)))), ((int)(((byte)(108)))));
            this.bt_Lusersetup.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.bt_Lusersetup.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(124)))), ((int)(((byte)(131)))), ((int)(((byte)(139)))));
            this.bt_Lusersetup.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(41)))), ((int)(((byte)(43)))));
            this.bt_Lusersetup.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_Lusersetup.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt_Lusersetup.ForeColor = System.Drawing.Color.White;
            this.bt_Lusersetup.Image = global::ThaiArrow_StoreSystem.Properties.Resources.teamwork__1_;
            this.bt_Lusersetup.ImageAlign = System.Drawing.ContentAlignment.BottomRight;
            this.bt_Lusersetup.Location = new System.Drawing.Point(1008, 6);
            this.bt_Lusersetup.Name = "bt_Lusersetup";
            this.bt_Lusersetup.Size = new System.Drawing.Size(325, 927);
            this.bt_Lusersetup.TabIndex = 4;
            this.bt_Lusersetup.Text = "USER SETUP";
            this.bt_Lusersetup.UseVisualStyleBackColor = false;
            this.bt_Lusersetup.Click += new System.EventHandler(this.Bt_LUserSetup_Click);
            // 
            // bt_LLogs
            // 
            this.bt_LLogs.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(31)))), ((int)(((byte)(57)))));
            this.bt_LLogs.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.bt_LLogs.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(124)))), ((int)(((byte)(131)))), ((int)(((byte)(139)))));
            this.bt_LLogs.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(41)))), ((int)(((byte)(43)))));
            this.bt_LLogs.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_LLogs.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt_LLogs.ForeColor = System.Drawing.Color.White;
            this.bt_LLogs.Image = global::ThaiArrow_StoreSystem.Properties.Resources.icons8_submit_document_100;
            this.bt_LLogs.ImageAlign = System.Drawing.ContentAlignment.BottomRight;
            this.bt_LLogs.Location = new System.Drawing.Point(677, 3);
            this.bt_LLogs.Name = "bt_LLogs";
            this.bt_LLogs.Size = new System.Drawing.Size(325, 927);
            this.bt_LLogs.TabIndex = 3;
            this.bt_LLogs.Text = "LOGS";
            this.bt_LLogs.UseVisualStyleBackColor = false;
            this.bt_LLogs.Click += new System.EventHandler(this.Bt_LLogs_Click);
            // 
            // bt_leditstock
            // 
            this.bt_leditstock.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(57)))), ((int)(((byte)(108)))));
            this.bt_leditstock.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.bt_leditstock.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(124)))), ((int)(((byte)(131)))), ((int)(((byte)(139)))));
            this.bt_leditstock.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(41)))), ((int)(((byte)(43)))));
            this.bt_leditstock.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_leditstock.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt_leditstock.ForeColor = System.Drawing.Color.White;
            this.bt_leditstock.Image = global::ThaiArrow_StoreSystem.Properties.Resources.industrial_robot__3_;
            this.bt_leditstock.ImageAlign = System.Drawing.ContentAlignment.BottomRight;
            this.bt_leditstock.Location = new System.Drawing.Point(346, 2);
            this.bt_leditstock.Name = "bt_leditstock";
            this.bt_leditstock.Size = new System.Drawing.Size(325, 927);
            this.bt_leditstock.TabIndex = 2;
            this.bt_leditstock.Text = "EDIT STOCK";
            this.bt_leditstock.UseVisualStyleBackColor = false;
            this.bt_leditstock.Click += new System.EventHandler(this.Button10_Click);
            // 
            // bt_partdata
            // 
            this.bt_partdata.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(91)))), ((int)(((byte)(150)))));
            this.bt_partdata.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.bt_partdata.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(124)))), ((int)(((byte)(131)))), ((int)(((byte)(139)))));
            this.bt_partdata.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(41)))), ((int)(((byte)(43)))));
            this.bt_partdata.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_partdata.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt_partdata.ForeColor = System.Drawing.Color.White;
            this.bt_partdata.Image = global::ThaiArrow_StoreSystem.Properties.Resources.icons8_electronics;
            this.bt_partdata.ImageAlign = System.Drawing.ContentAlignment.BottomRight;
            this.bt_partdata.Location = new System.Drawing.Point(15, 3);
            this.bt_partdata.Name = "bt_partdata";
            this.bt_partdata.Size = new System.Drawing.Size(325, 927);
            this.bt_partdata.TabIndex = 1;
            this.bt_partdata.Text = "PARTS DATA";
            this.bt_partdata.UseVisualStyleBackColor = false;
            this.bt_partdata.Click += new System.EventHandler(this.Button9_Click);
            // 
            // tabPart
            // 
            this.tabPart.Controls.Add(this.panel42);
            this.tabPart.Controls.Add(this.panel6);
            this.tabPart.Controls.Add(this.dataGridView1);
            this.tabPart.Controls.Add(this.panel5);
            this.tabPart.Location = new System.Drawing.Point(4, 22);
            this.tabPart.Name = "tabPart";
            this.tabPart.Size = new System.Drawing.Size(1670, 985);
            this.tabPart.TabIndex = 4;
            this.tabPart.Text = "PartsData";
            this.tabPart.UseVisualStyleBackColor = true;
            // 
            // panel42
            // 
            this.panel42.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(57)))), ((int)(((byte)(108)))));
            this.panel42.Controls.Add(this.label121);
            this.panel42.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel42.Location = new System.Drawing.Point(-4, 559);
            this.panel42.Name = "panel42";
            this.panel42.Size = new System.Drawing.Size(1671, 45);
            this.panel42.TabIndex = 131;
            // 
            // label121
            // 
            this.label121.AutoSize = true;
            this.label121.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label121.ForeColor = System.Drawing.Color.White;
            this.label121.Location = new System.Drawing.Point(723, 8);
            this.label121.Name = "label121";
            this.label121.Size = new System.Drawing.Size(170, 29);
            this.label121.TabIndex = 35;
            this.label121.Text = "Edit Part Menu";
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(227)))), ((int)(((byte)(238)))));
            this.panel6.Controls.Add(this.groupBox15);
            this.panel6.Controls.Add(this.GroupAddPartControl);
            this.panel6.Location = new System.Drawing.Point(3, 600);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(1664, 382);
            this.panel6.TabIndex = 2;
            // 
            // groupBox15
            // 
            this.groupBox15.Controls.Add(this.bt_pddelpart);
            this.groupBox15.Controls.Add(this.label124);
            this.groupBox15.Controls.Add(this.label123);
            this.groupBox15.Controls.Add(this.dataGridView17);
            this.groupBox15.Controls.Add(this.dataGridView16);
            this.groupBox15.Controls.Add(this.button13);
            this.groupBox15.Controls.Add(this.bt_pdaddpart);
            this.groupBox15.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox15.Location = new System.Drawing.Point(423, 42);
            this.groupBox15.Name = "groupBox15";
            this.groupBox15.Size = new System.Drawing.Size(1134, 318);
            this.groupBox15.TabIndex = 130;
            this.groupBox15.TabStop = false;
            this.groupBox15.Text = "Details :";
            // 
            // bt_pddelpart
            // 
            this.bt_pddelpart.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.bt_pddelpart.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(57)))), ((int)(((byte)(108)))));
            this.bt_pddelpart.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(102)))), ((int)(((byte)(185)))));
            this.bt_pddelpart.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_pddelpart.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.bt_pddelpart.ForeColor = System.Drawing.Color.White;
            this.bt_pddelpart.Location = new System.Drawing.Point(576, 255);
            this.bt_pddelpart.Name = "bt_pddelpart";
            this.bt_pddelpart.Size = new System.Drawing.Size(134, 42);
            this.bt_pddelpart.TabIndex = 109;
            this.bt_pddelpart.Text = "DELETE";
            this.bt_pddelpart.UseVisualStyleBackColor = false;
            this.bt_pddelpart.Click += new System.EventHandler(this.Bt_pddelpart_Click);
            // 
            // label124
            // 
            this.label124.AutoSize = true;
            this.label124.Location = new System.Drawing.Point(759, 26);
            this.label124.Name = "label124";
            this.label124.Size = new System.Drawing.Size(124, 25);
            this.label124.TabIndex = 108;
            this.label124.Text = "LineNoTable";
            // 
            // label123
            // 
            this.label123.AutoSize = true;
            this.label123.Location = new System.Drawing.Point(258, 26);
            this.label123.Name = "label123";
            this.label123.Size = new System.Drawing.Size(136, 25);
            this.label123.TabIndex = 107;
            this.label123.Text = "LocationTable";
            // 
            // dataGridView17
            // 
            this.dataGridView17.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridView17.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView17.Location = new System.Drawing.Point(672, 54);
            this.dataGridView17.Name = "dataGridView17";
            this.dataGridView17.Size = new System.Drawing.Size(288, 163);
            this.dataGridView17.TabIndex = 105;
            // 
            // dataGridView16
            // 
            this.dataGridView16.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridView16.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView16.Location = new System.Drawing.Point(85, 54);
            this.dataGridView16.Name = "dataGridView16";
            this.dataGridView16.Size = new System.Drawing.Size(486, 163);
            this.dataGridView16.TabIndex = 104;
            // 
            // button13
            // 
            this.button13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.button13.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(57)))), ((int)(((byte)(108)))));
            this.button13.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(102)))), ((int)(((byte)(185)))));
            this.button13.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.button13.ForeColor = System.Drawing.Color.White;
            this.button13.Location = new System.Drawing.Point(404, 255);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(134, 42);
            this.button13.TabIndex = 102;
            this.button13.Text = "EDIT";
            this.button13.UseVisualStyleBackColor = false;
            this.button13.Click += new System.EventHandler(this.Button13_Click);
            // 
            // bt_pdaddpart
            // 
            this.bt_pdaddpart.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(91)))), ((int)(((byte)(150)))));
            this.bt_pdaddpart.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(57)))), ((int)(((byte)(108)))));
            this.bt_pdaddpart.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(102)))), ((int)(((byte)(185)))));
            this.bt_pdaddpart.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_pdaddpart.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.bt_pdaddpart.ForeColor = System.Drawing.Color.White;
            this.bt_pdaddpart.Location = new System.Drawing.Point(226, 254);
            this.bt_pdaddpart.Name = "bt_pdaddpart";
            this.bt_pdaddpart.Size = new System.Drawing.Size(134, 42);
            this.bt_pdaddpart.TabIndex = 101;
            this.bt_pdaddpart.Text = "ADD";
            this.bt_pdaddpart.UseVisualStyleBackColor = false;
            this.bt_pdaddpart.Click += new System.EventHandler(this.Button22_Click);
            // 
            // GroupAddPartControl
            // 
            this.GroupAddPartControl.Controls.Add(this.button29);
            this.GroupAddPartControl.Controls.Add(this.label131);
            this.GroupAddPartControl.Controls.Add(this.label178);
            this.GroupAddPartControl.Controls.Add(this.tpd_partname);
            this.GroupAddPartControl.Controls.Add(this.tpd_partno);
            this.GroupAddPartControl.Controls.Add(this.pictureBox4);
            this.GroupAddPartControl.Controls.Add(this.tpd_search);
            this.GroupAddPartControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GroupAddPartControl.Location = new System.Drawing.Point(27, 61);
            this.GroupAddPartControl.Name = "GroupAddPartControl";
            this.GroupAddPartControl.Size = new System.Drawing.Size(353, 318);
            this.GroupAddPartControl.TabIndex = 0;
            this.GroupAddPartControl.TabStop = false;
            this.GroupAddPartControl.Text = "Add Part Control :";
            // 
            // button29
            // 
            this.button29.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(91)))), ((int)(((byte)(150)))));
            this.button29.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(57)))), ((int)(((byte)(108)))));
            this.button29.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(102)))), ((int)(((byte)(185)))));
            this.button29.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button29.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.button29.ForeColor = System.Drawing.Color.White;
            this.button29.Location = new System.Drawing.Point(100, 104);
            this.button29.Name = "button29";
            this.button29.Size = new System.Drawing.Size(134, 42);
            this.button29.TabIndex = 109;
            this.button29.Text = "SEARCH";
            this.button29.UseVisualStyleBackColor = false;
            this.button29.Click += new System.EventHandler(this.Button29_Click);
            // 
            // label131
            // 
            this.label131.AutoSize = true;
            this.label131.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label131.Location = new System.Drawing.Point(7, 230);
            this.label131.Name = "label131";
            this.label131.Size = new System.Drawing.Size(92, 20);
            this.label131.TabIndex = 108;
            this.label131.Text = "Part Name :";
            // 
            // label178
            // 
            this.label178.AutoSize = true;
            this.label178.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label178.Location = new System.Drawing.Point(7, 164);
            this.label178.Name = "label178";
            this.label178.Size = new System.Drawing.Size(70, 20);
            this.label178.TabIndex = 107;
            this.label178.Text = "Part No :";
            // 
            // tpd_partname
            // 
            this.tpd_partname.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tpd_partname.Location = new System.Drawing.Point(11, 251);
            this.tpd_partname.Name = "tpd_partname";
            this.tpd_partname.Size = new System.Drawing.Size(285, 26);
            this.tpd_partname.TabIndex = 106;
            // 
            // tpd_partno
            // 
            this.tpd_partno.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tpd_partno.Location = new System.Drawing.Point(11, 187);
            this.tpd_partno.Name = "tpd_partno";
            this.tpd_partno.Size = new System.Drawing.Size(285, 26);
            this.tpd_partno.TabIndex = 105;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::ThaiArrow_StoreSystem.Properties.Resources.Webp_net_resizeimage;
            this.pictureBox4.Location = new System.Drawing.Point(6, 54);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(30, 30);
            this.pictureBox4.TabIndex = 104;
            this.pictureBox4.TabStop = false;
            // 
            // tpd_search
            // 
            this.tpd_search.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tpd_search.Location = new System.Drawing.Point(52, 54);
            this.tpd_search.Name = "tpd_search";
            this.tpd_search.Size = new System.Drawing.Size(285, 26);
            this.tpd_search.TabIndex = 0;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.pd_partno,
            this.pd_partname,
            this.pd_model,
            this.pd_fifo,
            this.pd_rank,
            this.pd_maxstock,
            this.pd_minstock,
            this.pd_qty,
            this.pd_createuser,
            this.pd_createdate,
            this.pd_updateuser,
            this.pd_updatedate,
            this.pd_supplier,
            this.pd_packagepart,
            this.pd_parttype});
            this.dataGridView1.Location = new System.Drawing.Point(0, 40);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.ShowCellErrors = false;
            this.dataGridView1.ShowCellToolTips = false;
            this.dataGridView1.ShowEditingIcon = false;
            this.dataGridView1.ShowRowErrors = false;
            this.dataGridView1.Size = new System.Drawing.Size(1667, 523);
            this.dataGridView1.TabIndex = 1;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridView1_CellContentClick);
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridView1_CellContentClick);
            // 
            // pd_partno
            // 
            this.pd_partno.DataPropertyName = "PartNo";
            this.pd_partno.HeaderText = "Part No";
            this.pd_partno.Name = "pd_partno";
            this.pd_partno.Width = 125;
            // 
            // pd_partname
            // 
            this.pd_partname.DataPropertyName = "PartName";
            this.pd_partname.HeaderText = "Part Name";
            this.pd_partname.Name = "pd_partname";
            this.pd_partname.Width = 125;
            // 
            // pd_model
            // 
            this.pd_model.DataPropertyName = "Model";
            this.pd_model.HeaderText = "Model";
            this.pd_model.Name = "pd_model";
            // 
            // pd_fifo
            // 
            this.pd_fifo.DataPropertyName = "FIFO";
            this.pd_fifo.HeaderText = "FIFO";
            this.pd_fifo.Name = "pd_fifo";
            this.pd_fifo.Width = 80;
            // 
            // pd_rank
            // 
            this.pd_rank.DataPropertyName = "Rank";
            this.pd_rank.HeaderText = "Rank";
            this.pd_rank.Name = "pd_rank";
            this.pd_rank.Width = 80;
            // 
            // pd_maxstock
            // 
            this.pd_maxstock.DataPropertyName = "MaxStock";
            this.pd_maxstock.HeaderText = "Max Stock";
            this.pd_maxstock.Name = "pd_maxstock";
            // 
            // pd_minstock
            // 
            this.pd_minstock.DataPropertyName = "MinStock";
            this.pd_minstock.HeaderText = "Min Stock";
            this.pd_minstock.Name = "pd_minstock";
            // 
            // pd_qty
            // 
            this.pd_qty.DataPropertyName = "Qty";
            this.pd_qty.HeaderText = "Qty";
            this.pd_qty.Name = "pd_qty";
            // 
            // pd_createuser
            // 
            this.pd_createuser.DataPropertyName = "CreateUser";
            this.pd_createuser.HeaderText = "Create User";
            this.pd_createuser.Name = "pd_createuser";
            this.pd_createuser.Width = 125;
            // 
            // pd_createdate
            // 
            this.pd_createdate.DataPropertyName = "CreateDate";
            this.pd_createdate.HeaderText = "Create Date";
            this.pd_createdate.Name = "pd_createdate";
            this.pd_createdate.Width = 125;
            // 
            // pd_updateuser
            // 
            this.pd_updateuser.DataPropertyName = "UpdateUser";
            this.pd_updateuser.HeaderText = "Update User";
            this.pd_updateuser.Name = "pd_updateuser";
            this.pd_updateuser.Width = 125;
            // 
            // pd_updatedate
            // 
            this.pd_updatedate.HeaderText = "Update Date";
            this.pd_updatedate.Name = "pd_updatedate";
            this.pd_updatedate.Width = 125;
            // 
            // pd_supplier
            // 
            this.pd_supplier.DataPropertyName = "Supplier";
            this.pd_supplier.HeaderText = "Supplier";
            this.pd_supplier.Name = "pd_supplier";
            // 
            // pd_packagepart
            // 
            this.pd_packagepart.DataPropertyName = "PackagePart";
            this.pd_packagepart.HeaderText = "ลักษณะ Part";
            this.pd_packagepart.Name = "pd_packagepart";
            // 
            // pd_parttype
            // 
            this.pd_parttype.DataPropertyName = "PartType";
            this.pd_parttype.HeaderText = "ประเภท Part";
            this.pd_parttype.Name = "pd_parttype";
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(57)))), ((int)(((byte)(108)))));
            this.panel5.Controls.Add(this.label120);
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(1667, 45);
            this.panel5.TabIndex = 0;
            // 
            // label120
            // 
            this.label120.AutoSize = true;
            this.label120.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label120.ForeColor = System.Drawing.Color.White;
            this.label120.Location = new System.Drawing.Point(771, 8);
            this.label120.Name = "label120";
            this.label120.Size = new System.Drawing.Size(123, 29);
            this.label120.TabIndex = 35;
            this.label120.Text = "Parts Data";
            // 
            // tabEditStock
            // 
            this.tabEditStock.Controls.Add(this.dataGridView10);
            this.tabEditStock.Controls.Add(this.dataGridView9);
            this.tabEditStock.Controls.Add(this.panel30);
            this.tabEditStock.Controls.Add(this.panel28);
            this.tabEditStock.Controls.Add(this.panel29);
            this.tabEditStock.Location = new System.Drawing.Point(4, 22);
            this.tabEditStock.Name = "tabEditStock";
            this.tabEditStock.Size = new System.Drawing.Size(1670, 985);
            this.tabEditStock.TabIndex = 5;
            this.tabEditStock.Text = "EditStock";
            this.tabEditStock.UseVisualStyleBackColor = true;
            // 
            // dataGridView10
            // 
            this.dataGridView10.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridView10.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView10.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.es_boxid,
            this.es_rank,
            this.es_stocklotid,
            this.es_stocktime,
            this.es_stock_transaction,
            this.es_stock_user,
            this.es_stock_status,
            this.es_pickup_time,
            this.es_pickup_transaction,
            this.es_pickup_user,
            this.es_pickup_line});
            this.dataGridView10.Location = new System.Drawing.Point(242, 46);
            this.dataGridView10.Name = "dataGridView10";
            this.dataGridView10.Size = new System.Drawing.Size(1426, 521);
            this.dataGridView10.TabIndex = 140;
            this.dataGridView10.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridView10_CellContentClick);
            this.dataGridView10.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridView10_CellContentClick);
            // 
            // es_boxid
            // 
            this.es_boxid.HeaderText = "Box ID";
            this.es_boxid.Name = "es_boxid";
            this.es_boxid.Width = 125;
            // 
            // es_rank
            // 
            this.es_rank.HeaderText = "Rank";
            this.es_rank.Name = "es_rank";
            this.es_rank.Width = 125;
            // 
            // es_stocklotid
            // 
            this.es_stocklotid.HeaderText = "Stock Lot ID";
            this.es_stocklotid.Name = "es_stocklotid";
            this.es_stocklotid.Width = 125;
            // 
            // es_stocktime
            // 
            this.es_stocktime.HeaderText = "Stock Time";
            this.es_stocktime.Name = "es_stocktime";
            this.es_stocktime.Width = 125;
            // 
            // es_stock_transaction
            // 
            this.es_stock_transaction.HeaderText = "Stock Transaction";
            this.es_stock_transaction.Name = "es_stock_transaction";
            this.es_stock_transaction.Width = 125;
            // 
            // es_stock_user
            // 
            this.es_stock_user.HeaderText = "Stock User";
            this.es_stock_user.Name = "es_stock_user";
            this.es_stock_user.Width = 125;
            // 
            // es_stock_status
            // 
            this.es_stock_status.HeaderText = "Stock Status";
            this.es_stock_status.Name = "es_stock_status";
            this.es_stock_status.Width = 125;
            // 
            // es_pickup_time
            // 
            this.es_pickup_time.HeaderText = "Pickup Time";
            this.es_pickup_time.Name = "es_pickup_time";
            this.es_pickup_time.Width = 125;
            // 
            // es_pickup_transaction
            // 
            this.es_pickup_transaction.HeaderText = "Pickup Transaction";
            this.es_pickup_transaction.Name = "es_pickup_transaction";
            this.es_pickup_transaction.Width = 125;
            // 
            // es_pickup_user
            // 
            this.es_pickup_user.HeaderText = "Pickup User";
            this.es_pickup_user.Name = "es_pickup_user";
            this.es_pickup_user.Width = 125;
            // 
            // es_pickup_line
            // 
            this.es_pickup_line.HeaderText = "Pickup Line";
            this.es_pickup_line.Name = "es_pickup_line";
            this.es_pickup_line.Width = 125;
            // 
            // dataGridView9
            // 
            this.dataGridView9.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridView9.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView9.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.es_partno,
            this.es_stockava});
            this.dataGridView9.Location = new System.Drawing.Point(0, 46);
            this.dataGridView9.Name = "dataGridView9";
            this.dataGridView9.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView9.Size = new System.Drawing.Size(244, 521);
            this.dataGridView9.TabIndex = 36;
            this.dataGridView9.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridView9_CellContentClick);
            this.dataGridView9.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridView9_CellContentClick);
            // 
            // es_partno
            // 
            this.es_partno.HeaderText = "Part No";
            this.es_partno.Name = "es_partno";
            // 
            // es_stockava
            // 
            this.es_stockava.HeaderText = "Stock Available";
            this.es_stockava.Name = "es_stockava";
            // 
            // panel30
            // 
            this.panel30.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(57)))), ((int)(((byte)(108)))));
            this.panel30.Controls.Add(this.label114);
            this.panel30.Location = new System.Drawing.Point(0, 1);
            this.panel30.Name = "panel30";
            this.panel30.Size = new System.Drawing.Size(1667, 45);
            this.panel30.TabIndex = 139;
            // 
            // label114
            // 
            this.label114.AutoSize = true;
            this.label114.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label114.ForeColor = System.Drawing.Color.White;
            this.label114.Location = new System.Drawing.Point(771, 8);
            this.label114.Name = "label114";
            this.label114.Size = new System.Drawing.Size(121, 29);
            this.label114.TabIndex = 35;
            this.label114.Text = "Edit Stock";
            // 
            // panel28
            // 
            this.panel28.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(57)))), ((int)(((byte)(108)))));
            this.panel28.Controls.Add(this.label83);
            this.panel28.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel28.Location = new System.Drawing.Point(0, 563);
            this.panel28.Name = "panel28";
            this.panel28.Size = new System.Drawing.Size(1671, 45);
            this.panel28.TabIndex = 138;
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label83.ForeColor = System.Drawing.Color.White;
            this.label83.Location = new System.Drawing.Point(723, 8);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(187, 29);
            this.label83.TabIndex = 35;
            this.label83.Text = "Edit Stock Menu";
            // 
            // panel29
            // 
            this.panel29.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(227)))), ((int)(((byte)(238)))));
            this.panel29.Controls.Add(this.groupBox11);
            this.panel29.Controls.Add(this.groupBox12);
            this.panel29.Location = new System.Drawing.Point(0, 604);
            this.panel29.Name = "panel29";
            this.panel29.Size = new System.Drawing.Size(1670, 382);
            this.panel29.TabIndex = 137;
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.label122);
            this.groupBox11.Controls.Add(this.tes_side);
            this.groupBox11.Controls.Add(this.label96);
            this.groupBox11.Controls.Add(this.des_pickuptime);
            this.groupBox11.Controls.Add(this.ces_pickupline);
            this.groupBox11.Controls.Add(this.ces_pickupuser);
            this.groupBox11.Controls.Add(this.label87);
            this.groupBox11.Controls.Add(this.ces_stockstatus);
            this.groupBox11.Controls.Add(this.label86);
            this.groupBox11.Controls.Add(this.tes_maxstock);
            this.groupBox11.Controls.Add(this.label88);
            this.groupBox11.Controls.Add(this.tes_minstock);
            this.groupBox11.Controls.Add(this.label89);
            this.groupBox11.Controls.Add(this.label91);
            this.groupBox11.Controls.Add(this.label92);
            this.groupBox11.Controls.Add(this.label93);
            this.groupBox11.Controls.Add(this.label97);
            this.groupBox11.Controls.Add(this.label102);
            this.groupBox11.Controls.Add(this.label103);
            this.groupBox11.Controls.Add(this.label104);
            this.groupBox11.Controls.Add(this.label107);
            this.groupBox11.Controls.Add(this.label108);
            this.groupBox11.Controls.Add(this.label109);
            this.groupBox11.Controls.Add(this.label110);
            this.groupBox11.Controls.Add(this.label111);
            this.groupBox11.Controls.Add(this.tes_stock);
            this.groupBox11.Controls.Add(this.tes_timein);
            this.groupBox11.Controls.Add(this.tes_userin);
            this.groupBox11.Controls.Add(this.tes_slot);
            this.groupBox11.Controls.Add(this.tes_zone);
            this.groupBox11.Controls.Add(this.tes_shelf);
            this.groupBox11.Controls.Add(this.tes_area);
            this.groupBox11.Controls.Add(this.tes_maker);
            this.groupBox11.Controls.Add(this.tes_rank);
            this.groupBox11.Controls.Add(this.tes_qty);
            this.groupBox11.Controls.Add(this.tes_boxid);
            this.groupBox11.Controls.Add(this.btEditstock_del);
            this.groupBox11.Controls.Add(this.btEditstock_OK);
            this.groupBox11.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox11.Location = new System.Drawing.Point(423, 42);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(1134, 318);
            this.groupBox11.TabIndex = 130;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Details :";
            // 
            // label122
            // 
            this.label122.AutoSize = true;
            this.label122.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label122.Location = new System.Drawing.Point(940, 247);
            this.label122.Name = "label122";
            this.label122.Size = new System.Drawing.Size(49, 20);
            this.label122.TabIndex = 151;
            this.label122.Text = "Side :";
            // 
            // tes_side
            // 
            this.tes_side.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tes_side.Location = new System.Drawing.Point(944, 276);
            this.tes_side.Name = "tes_side";
            this.tes_side.Size = new System.Drawing.Size(173, 26);
            this.tes_side.TabIndex = 150;
            // 
            // label96
            // 
            this.label96.AutoSize = true;
            this.label96.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label96.Location = new System.Drawing.Point(282, 250);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(98, 20);
            this.label96.TabIndex = 149;
            this.label96.Text = "Pickup Time:";
            // 
            // des_pickuptime
            // 
            this.des_pickuptime.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.des_pickuptime.Location = new System.Drawing.Point(286, 278);
            this.des_pickuptime.Name = "des_pickuptime";
            this.des_pickuptime.Size = new System.Drawing.Size(267, 26);
            this.des_pickuptime.TabIndex = 148;
            // 
            // ces_pickupline
            // 
            this.ces_pickupline.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ces_pickupline.FormattingEnabled = true;
            this.ces_pickupline.Location = new System.Drawing.Point(286, 207);
            this.ces_pickupline.Name = "ces_pickupline";
            this.ces_pickupline.Size = new System.Drawing.Size(173, 28);
            this.ces_pickupline.TabIndex = 147;
            // 
            // ces_pickupuser
            // 
            this.ces_pickupuser.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ces_pickupuser.FormattingEnabled = true;
            this.ces_pickupuser.Location = new System.Drawing.Point(61, 276);
            this.ces_pickupuser.Name = "ces_pickupuser";
            this.ces_pickupuser.Size = new System.Drawing.Size(173, 28);
            this.ces_pickupuser.TabIndex = 146;
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label87.Location = new System.Drawing.Point(282, 184);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(98, 20);
            this.label87.TabIndex = 145;
            this.label87.Text = "Pickup Line :";
            // 
            // ces_stockstatus
            // 
            this.ces_stockstatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ces_stockstatus.FormattingEnabled = true;
            this.ces_stockstatus.Location = new System.Drawing.Point(61, 207);
            this.ces_stockstatus.Name = "ces_stockstatus";
            this.ces_stockstatus.Size = new System.Drawing.Size(173, 28);
            this.ces_stockstatus.TabIndex = 143;
            this.ces_stockstatus.SelectedIndexChanged += new System.EventHandler(this.Ces_stockstatus_SelectedIndexChanged);
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label86.Location = new System.Drawing.Point(729, 115);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(91, 20);
            this.label86.TabIndex = 142;
            this.label86.Text = "Max Stock :";
            // 
            // tes_maxstock
            // 
            this.tes_maxstock.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tes_maxstock.Location = new System.Drawing.Point(733, 141);
            this.tes_maxstock.Name = "tes_maxstock";
            this.tes_maxstock.Size = new System.Drawing.Size(173, 26);
            this.tes_maxstock.TabIndex = 141;
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label88.Location = new System.Drawing.Point(733, 183);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(87, 20);
            this.label88.TabIndex = 138;
            this.label88.Text = "Min Stock :";
            // 
            // tes_minstock
            // 
            this.tes_minstock.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tes_minstock.Location = new System.Drawing.Point(733, 209);
            this.tes_minstock.Name = "tes_minstock";
            this.tes_minstock.Size = new System.Drawing.Size(173, 26);
            this.tes_minstock.TabIndex = 137;
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label89.Location = new System.Drawing.Point(940, 180);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(58, 20);
            this.label89.TabIndex = 136;
            this.label89.Text = "Stock :";
            // 
            // label91
            // 
            this.label91.AutoSize = true;
            this.label91.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label91.Location = new System.Drawing.Point(940, 115);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(112, 20);
            this.label91.TabIndex = 135;
            this.label91.Text = "Update User  :";
            // 
            // label92
            // 
            this.label92.AutoSize = true;
            this.label92.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label92.Location = new System.Drawing.Point(940, 47);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(103, 20);
            this.label92.TabIndex = 134;
            this.label92.Text = "Create User :";
            // 
            // label93
            // 
            this.label93.AutoSize = true;
            this.label93.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label93.Location = new System.Drawing.Point(57, 181);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(109, 20);
            this.label93.TabIndex = 133;
            this.label93.Text = "Stock Status :";
            // 
            // label97
            // 
            this.label97.AutoSize = true;
            this.label97.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label97.Location = new System.Drawing.Point(729, 47);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(45, 20);
            this.label97.TabIndex = 131;
            this.label97.Text = "Slot :";
            // 
            // label102
            // 
            this.label102.AutoSize = true;
            this.label102.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label102.Location = new System.Drawing.Point(516, 180);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(54, 20);
            this.label102.TabIndex = 130;
            this.label102.Text = "Zone :";
            // 
            // label103
            // 
            this.label103.AutoSize = true;
            this.label103.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label103.Location = new System.Drawing.Point(516, 115);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(53, 20);
            this.label103.TabIndex = 129;
            this.label103.Text = "Floor :";
            // 
            // label104
            // 
            this.label104.AutoSize = true;
            this.label104.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label104.Location = new System.Drawing.Point(516, 47);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(54, 20);
            this.label104.TabIndex = 128;
            this.label104.Text = "Shelf :";
            // 
            // label107
            // 
            this.label107.AutoSize = true;
            this.label107.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label107.Location = new System.Drawing.Point(61, 253);
            this.label107.Name = "label107";
            this.label107.Size = new System.Drawing.Size(99, 20);
            this.label107.TabIndex = 126;
            this.label107.Text = "Pickup user :";
            // 
            // label108
            // 
            this.label108.AutoSize = true;
            this.label108.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label108.Location = new System.Drawing.Point(282, 47);
            this.label108.Name = "label108";
            this.label108.Size = new System.Drawing.Size(61, 20);
            this.label108.TabIndex = 125;
            this.label108.Text = "Maker :";
            // 
            // label109
            // 
            this.label109.AutoSize = true;
            this.label109.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label109.Location = new System.Drawing.Point(57, 118);
            this.label109.Name = "label109";
            this.label109.Size = new System.Drawing.Size(55, 20);
            this.label109.TabIndex = 124;
            this.label109.Text = "Rank :";
            // 
            // label110
            // 
            this.label110.AutoSize = true;
            this.label110.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label110.Location = new System.Drawing.Point(282, 118);
            this.label110.Name = "label110";
            this.label110.Size = new System.Drawing.Size(41, 20);
            this.label110.TabIndex = 123;
            this.label110.Text = "Qty :";
            // 
            // label111
            // 
            this.label111.AutoSize = true;
            this.label111.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label111.Location = new System.Drawing.Point(57, 50);
            this.label111.Name = "label111";
            this.label111.Size = new System.Drawing.Size(65, 20);
            this.label111.TabIndex = 122;
            this.label111.Text = "Box ID :";
            // 
            // tes_stock
            // 
            this.tes_stock.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tes_stock.Location = new System.Drawing.Point(944, 203);
            this.tes_stock.Name = "tes_stock";
            this.tes_stock.Size = new System.Drawing.Size(173, 26);
            this.tes_stock.TabIndex = 121;
            // 
            // tes_timein
            // 
            this.tes_timein.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tes_timein.Location = new System.Drawing.Point(944, 138);
            this.tes_timein.Name = "tes_timein";
            this.tes_timein.Size = new System.Drawing.Size(173, 26);
            this.tes_timein.TabIndex = 120;
            // 
            // tes_userin
            // 
            this.tes_userin.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tes_userin.Location = new System.Drawing.Point(944, 70);
            this.tes_userin.Name = "tes_userin";
            this.tes_userin.Size = new System.Drawing.Size(173, 26);
            this.tes_userin.TabIndex = 119;
            // 
            // tes_slot
            // 
            this.tes_slot.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tes_slot.Location = new System.Drawing.Point(733, 70);
            this.tes_slot.Name = "tes_slot";
            this.tes_slot.Size = new System.Drawing.Size(173, 26);
            this.tes_slot.TabIndex = 116;
            // 
            // tes_zone
            // 
            this.tes_zone.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tes_zone.Location = new System.Drawing.Point(520, 206);
            this.tes_zone.Name = "tes_zone";
            this.tes_zone.Size = new System.Drawing.Size(173, 26);
            this.tes_zone.TabIndex = 115;
            // 
            // tes_shelf
            // 
            this.tes_shelf.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tes_shelf.Location = new System.Drawing.Point(520, 138);
            this.tes_shelf.Name = "tes_shelf";
            this.tes_shelf.Size = new System.Drawing.Size(173, 26);
            this.tes_shelf.TabIndex = 114;
            // 
            // tes_area
            // 
            this.tes_area.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tes_area.Location = new System.Drawing.Point(520, 70);
            this.tes_area.Name = "tes_area";
            this.tes_area.Size = new System.Drawing.Size(173, 26);
            this.tes_area.TabIndex = 113;
            // 
            // tes_maker
            // 
            this.tes_maker.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tes_maker.Location = new System.Drawing.Point(286, 70);
            this.tes_maker.Name = "tes_maker";
            this.tes_maker.Size = new System.Drawing.Size(173, 26);
            this.tes_maker.TabIndex = 110;
            // 
            // tes_rank
            // 
            this.tes_rank.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tes_rank.Location = new System.Drawing.Point(61, 141);
            this.tes_rank.Name = "tes_rank";
            this.tes_rank.Size = new System.Drawing.Size(173, 26);
            this.tes_rank.TabIndex = 109;
            // 
            // tes_qty
            // 
            this.tes_qty.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tes_qty.Location = new System.Drawing.Point(286, 141);
            this.tes_qty.Name = "tes_qty";
            this.tes_qty.Size = new System.Drawing.Size(173, 26);
            this.tes_qty.TabIndex = 108;
            // 
            // tes_boxid
            // 
            this.tes_boxid.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tes_boxid.Location = new System.Drawing.Point(61, 73);
            this.tes_boxid.Name = "tes_boxid";
            this.tes_boxid.Size = new System.Drawing.Size(173, 26);
            this.tes_boxid.TabIndex = 107;
            // 
            // btEditstock_del
            // 
            this.btEditstock_del.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(67)))), ((int)(((byte)(53)))));
            this.btEditstock_del.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(57)))), ((int)(((byte)(108)))));
            this.btEditstock_del.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(102)))), ((int)(((byte)(185)))));
            this.btEditstock_del.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btEditstock_del.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.btEditstock_del.ForeColor = System.Drawing.Color.White;
            this.btEditstock_del.Location = new System.Drawing.Point(762, 253);
            this.btEditstock_del.Name = "btEditstock_del";
            this.btEditstock_del.Size = new System.Drawing.Size(134, 42);
            this.btEditstock_del.TabIndex = 102;
            this.btEditstock_del.Text = "DELETE";
            this.btEditstock_del.UseVisualStyleBackColor = false;
            this.btEditstock_del.Click += new System.EventHandler(this.BtEditstock_del_Click);
            // 
            // btEditstock_OK
            // 
            this.btEditstock_OK.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(91)))), ((int)(((byte)(150)))));
            this.btEditstock_OK.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(57)))), ((int)(((byte)(108)))));
            this.btEditstock_OK.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(102)))), ((int)(((byte)(185)))));
            this.btEditstock_OK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btEditstock_OK.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.btEditstock_OK.ForeColor = System.Drawing.Color.White;
            this.btEditstock_OK.Location = new System.Drawing.Point(592, 253);
            this.btEditstock_OK.Name = "btEditstock_OK";
            this.btEditstock_OK.Size = new System.Drawing.Size(134, 42);
            this.btEditstock_OK.TabIndex = 101;
            this.btEditstock_OK.Text = "OK";
            this.btEditstock_OK.UseVisualStyleBackColor = false;
            this.btEditstock_OK.Click += new System.EventHandler(this.BtEditstock_OK_Click);
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.label129);
            this.groupBox12.Controls.Add(this.tes_searchetc);
            this.groupBox12.Controls.Add(this.btEditstock_search);
            this.groupBox12.Controls.Add(this.label112);
            this.groupBox12.Controls.Add(this.label113);
            this.groupBox12.Controls.Add(this.tes_partname);
            this.groupBox12.Controls.Add(this.tes_searchpart);
            this.groupBox12.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox12.Location = new System.Drawing.Point(27, 61);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(353, 318);
            this.groupBox12.TabIndex = 0;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "Part Control :";
            // 
            // label129
            // 
            this.label129.AutoSize = true;
            this.label129.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label129.Location = new System.Drawing.Point(13, 125);
            this.label129.Name = "label129";
            this.label129.Size = new System.Drawing.Size(98, 20);
            this.label129.TabIndex = 112;
            this.label129.Text = "Search etc. :";
            // 
            // tes_searchetc
            // 
            this.tes_searchetc.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tes_searchetc.Location = new System.Drawing.Point(14, 155);
            this.tes_searchetc.Name = "tes_searchetc";
            this.tes_searchetc.Size = new System.Drawing.Size(285, 26);
            this.tes_searchetc.TabIndex = 111;
            // 
            // btEditstock_search
            // 
            this.btEditstock_search.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(91)))), ((int)(((byte)(150)))));
            this.btEditstock_search.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(57)))), ((int)(((byte)(108)))));
            this.btEditstock_search.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(102)))), ((int)(((byte)(185)))));
            this.btEditstock_search.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btEditstock_search.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.btEditstock_search.ForeColor = System.Drawing.Color.White;
            this.btEditstock_search.Location = new System.Drawing.Point(83, 196);
            this.btEditstock_search.Name = "btEditstock_search";
            this.btEditstock_search.Size = new System.Drawing.Size(134, 42);
            this.btEditstock_search.TabIndex = 110;
            this.btEditstock_search.Text = "SEARCH";
            this.btEditstock_search.UseVisualStyleBackColor = false;
            this.btEditstock_search.Click += new System.EventHandler(this.BtEditstock_search_Click);
            // 
            // label112
            // 
            this.label112.AutoSize = true;
            this.label112.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label112.Location = new System.Drawing.Point(10, 54);
            this.label112.Name = "label112";
            this.label112.Size = new System.Drawing.Size(101, 20);
            this.label112.TabIndex = 109;
            this.label112.Text = "Search Part :";
            // 
            // label113
            // 
            this.label113.AutoSize = true;
            this.label113.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label113.Location = new System.Drawing.Point(10, 256);
            this.label113.Name = "label113";
            this.label113.Size = new System.Drawing.Size(92, 20);
            this.label113.TabIndex = 108;
            this.label113.Text = "Part Name :";
            // 
            // tes_partname
            // 
            this.tes_partname.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tes_partname.Location = new System.Drawing.Point(14, 285);
            this.tes_partname.Name = "tes_partname";
            this.tes_partname.Size = new System.Drawing.Size(285, 26);
            this.tes_partname.TabIndex = 106;
            // 
            // tes_searchpart
            // 
            this.tes_searchpart.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tes_searchpart.Location = new System.Drawing.Point(14, 77);
            this.tes_searchpart.Name = "tes_searchpart";
            this.tes_searchpart.Size = new System.Drawing.Size(285, 26);
            this.tes_searchpart.TabIndex = 0;
            // 
            // tabLogs
            // 
            this.tabLogs.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.tabLogs.Controls.Add(this.panel48);
            this.tabLogs.Controls.Add(this.panel47);
            this.tabLogs.Controls.Add(this.panel27);
            this.tabLogs.Controls.Add(this.panel34);
            this.tabLogs.Controls.Add(this.panel33);
            this.tabLogs.Location = new System.Drawing.Point(4, 22);
            this.tabLogs.Name = "tabLogs";
            this.tabLogs.Size = new System.Drawing.Size(1670, 985);
            this.tabLogs.TabIndex = 6;
            this.tabLogs.Text = "Logs";
            // 
            // panel48
            // 
            this.panel48.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(57)))), ((int)(((byte)(108)))));
            this.panel48.Controls.Add(this.label146);
            this.panel48.Location = new System.Drawing.Point(1290, 75);
            this.panel48.Name = "panel48";
            this.panel48.Size = new System.Drawing.Size(378, 107);
            this.panel48.TabIndex = 5;
            // 
            // label146
            // 
            this.label146.AutoSize = true;
            this.label146.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label146.ForeColor = System.Drawing.Color.White;
            this.label146.Location = new System.Drawing.Point(127, 35);
            this.label146.Name = "label146";
            this.label146.Size = new System.Drawing.Size(124, 29);
            this.label146.TabIndex = 3;
            this.label146.Text = "Command";
            // 
            // panel47
            // 
            this.panel47.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(91)))), ((int)(((byte)(150)))));
            this.panel47.Controls.Add(this.groupBox19);
            this.panel47.Location = new System.Drawing.Point(1290, 75);
            this.panel47.Name = "panel47";
            this.panel47.Size = new System.Drawing.Size(378, 849);
            this.panel47.TabIndex = 34;
            // 
            // groupBox19
            // 
            this.groupBox19.Controls.Add(this.checklog_date);
            this.groupBox19.Controls.Add(this.label81);
            this.groupBox19.Controls.Add(this.tlog_boxid);
            this.groupBox19.Controls.Add(this.clog_event);
            this.groupBox19.Controls.Add(this.btlog_search);
            this.groupBox19.Controls.Add(this.datelog_to);
            this.groupBox19.Controls.Add(this.datelog_form);
            this.groupBox19.Controls.Add(this.label152);
            this.groupBox19.Controls.Add(this.label151);
            this.groupBox19.Controls.Add(this.label150);
            this.groupBox19.Controls.Add(this.label149);
            this.groupBox19.Controls.Add(this.label148);
            this.groupBox19.Controls.Add(this.tlog_qty);
            this.groupBox19.Controls.Add(this.tlog_partno);
            this.groupBox19.Controls.Add(this.label147);
            this.groupBox19.Controls.Add(this.pictureBox2);
            this.groupBox19.Controls.Add(this.tlog_eventid);
            this.groupBox19.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox19.ForeColor = System.Drawing.SystemColors.Control;
            this.groupBox19.Location = new System.Drawing.Point(19, 176);
            this.groupBox19.Name = "groupBox19";
            this.groupBox19.Size = new System.Drawing.Size(350, 670);
            this.groupBox19.TabIndex = 4;
            this.groupBox19.TabStop = false;
            this.groupBox19.Text = "SEARCH";
            // 
            // checklog_date
            // 
            this.checklog_date.AutoSize = true;
            this.checklog_date.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checklog_date.Location = new System.Drawing.Point(23, 579);
            this.checklog_date.Name = "checklog_date";
            this.checklog_date.Size = new System.Drawing.Size(122, 29);
            this.checklog_date.TabIndex = 121;
            this.checklog_date.Text = "Date/Time";
            this.checklog_date.UseVisualStyleBackColor = true;
            this.checklog_date.CheckedChanged += new System.EventHandler(this.Checklog_date_CheckedChanged);
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label81.ForeColor = System.Drawing.Color.White;
            this.label81.Location = new System.Drawing.Point(16, 392);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(110, 31);
            this.label81.TabIndex = 120;
            this.label81.Text = "Box ID :";
            // 
            // tlog_boxid
            // 
            this.tlog_boxid.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.tlog_boxid.Location = new System.Drawing.Point(160, 397);
            this.tlog_boxid.Name = "tlog_boxid";
            this.tlog_boxid.Size = new System.Drawing.Size(186, 26);
            this.tlog_boxid.TabIndex = 119;
            this.tlog_boxid.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // clog_event
            // 
            this.clog_event.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clog_event.FormattingEnabled = true;
            this.clog_event.Location = new System.Drawing.Point(160, 326);
            this.clog_event.Name = "clog_event";
            this.clog_event.Size = new System.Drawing.Size(186, 28);
            this.clog_event.TabIndex = 35;
            // 
            // btlog_search
            // 
            this.btlog_search.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(57)))), ((int)(((byte)(108)))));
            this.btlog_search.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(57)))), ((int)(((byte)(108)))));
            this.btlog_search.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(102)))), ((int)(((byte)(185)))));
            this.btlog_search.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btlog_search.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.btlog_search.ForeColor = System.Drawing.Color.White;
            this.btlog_search.Location = new System.Drawing.Point(115, 622);
            this.btlog_search.Name = "btlog_search";
            this.btlog_search.Size = new System.Drawing.Size(134, 42);
            this.btlog_search.TabIndex = 59;
            this.btlog_search.Text = "Search";
            this.btlog_search.UseVisualStyleBackColor = false;
            this.btlog_search.Click += new System.EventHandler(this.Btlog_search_Click);
            // 
            // datelog_to
            // 
            this.datelog_to.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.datelog_to.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.datelog_to.Location = new System.Drawing.Point(160, 528);
            this.datelog_to.Name = "datelog_to";
            this.datelog_to.Size = new System.Drawing.Size(186, 26);
            this.datelog_to.TabIndex = 118;
            // 
            // datelog_form
            // 
            this.datelog_form.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.datelog_form.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.datelog_form.Location = new System.Drawing.Point(160, 459);
            this.datelog_form.Name = "datelog_form";
            this.datelog_form.Size = new System.Drawing.Size(186, 26);
            this.datelog_form.TabIndex = 117;
            // 
            // label152
            // 
            this.label152.AutoSize = true;
            this.label152.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label152.ForeColor = System.Drawing.Color.White;
            this.label152.Location = new System.Drawing.Point(16, 523);
            this.label152.Name = "label152";
            this.label152.Size = new System.Drawing.Size(61, 31);
            this.label152.TabIndex = 115;
            this.label152.Text = "To :";
            // 
            // label151
            // 
            this.label151.AutoSize = true;
            this.label151.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label151.ForeColor = System.Drawing.Color.White;
            this.label151.Location = new System.Drawing.Point(11, 454);
            this.label151.Name = "label151";
            this.label151.Size = new System.Drawing.Size(92, 31);
            this.label151.TabIndex = 114;
            this.label151.Text = "Form :";
            // 
            // label150
            // 
            this.label150.AutoSize = true;
            this.label150.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label150.ForeColor = System.Drawing.Color.White;
            this.label150.Location = new System.Drawing.Point(11, 323);
            this.label150.Name = "label150";
            this.label150.Size = new System.Drawing.Size(106, 31);
            this.label150.TabIndex = 111;
            this.label150.Text = "Event  :";
            // 
            // label149
            // 
            this.label149.AutoSize = true;
            this.label149.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label149.ForeColor = System.Drawing.Color.White;
            this.label149.Location = new System.Drawing.Point(11, 241);
            this.label149.Name = "label149";
            this.label149.Size = new System.Drawing.Size(122, 31);
            this.label149.TabIndex = 110;
            this.label149.Text = "MCLine :";
            // 
            // label148
            // 
            this.label148.AutoSize = true;
            this.label148.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label148.ForeColor = System.Drawing.Color.White;
            this.label148.Location = new System.Drawing.Point(11, 158);
            this.label148.Name = "label148";
            this.label148.Size = new System.Drawing.Size(121, 31);
            this.label148.TabIndex = 109;
            this.label148.Text = "Part No :";
            // 
            // tlog_qty
            // 
            this.tlog_qty.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.tlog_qty.Location = new System.Drawing.Point(160, 248);
            this.tlog_qty.Name = "tlog_qty";
            this.tlog_qty.Size = new System.Drawing.Size(186, 26);
            this.tlog_qty.TabIndex = 107;
            this.tlog_qty.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tlog_partno
            // 
            this.tlog_partno.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.tlog_partno.Location = new System.Drawing.Point(160, 163);
            this.tlog_partno.Name = "tlog_partno";
            this.tlog_partno.Size = new System.Drawing.Size(186, 26);
            this.tlog_partno.TabIndex = 106;
            this.tlog_partno.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label147
            // 
            this.label147.AutoSize = true;
            this.label147.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label147.ForeColor = System.Drawing.Color.White;
            this.label147.Location = new System.Drawing.Point(11, 77);
            this.label147.Name = "label147";
            this.label147.Size = new System.Drawing.Size(134, 31);
            this.label147.TabIndex = 105;
            this.label147.Text = "Event ID :";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::ThaiArrow_StoreSystem.Properties.Resources.Webp_net_resizeimage;
            this.pictureBox2.Location = new System.Drawing.Point(17, 44);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(30, 30);
            this.pictureBox2.TabIndex = 104;
            this.pictureBox2.TabStop = false;
            // 
            // tlog_eventid
            // 
            this.tlog_eventid.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.tlog_eventid.Location = new System.Drawing.Point(160, 84);
            this.tlog_eventid.Name = "tlog_eventid";
            this.tlog_eventid.Size = new System.Drawing.Size(186, 26);
            this.tlog_eventid.TabIndex = 87;
            this.tlog_eventid.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel27
            // 
            this.panel27.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.panel27.Controls.Add(this.label78);
            this.panel27.Controls.Add(this.label79);
            this.panel27.Location = new System.Drawing.Point(0, 929);
            this.panel27.Name = "panel27";
            this.panel27.Size = new System.Drawing.Size(1674, 56);
            this.panel27.TabIndex = 33;
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.BackColor = System.Drawing.Color.Transparent;
            this.label78.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label78.ForeColor = System.Drawing.Color.White;
            this.label78.Location = new System.Drawing.Point(30, 22);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(263, 20);
            this.label78.TabIndex = 91;
            this.label78.Text = "Store Management System @ 2019";
            this.label78.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.BackColor = System.Drawing.Color.Transparent;
            this.label79.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label79.ForeColor = System.Drawing.Color.White;
            this.label79.Location = new System.Drawing.Point(1261, 15);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(383, 20);
            this.label79.TabIndex = 90;
            this.label79.Text = "Thai Arrow Products Co., Ltd. (149 Bang Phli Factory)";
            this.label79.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel34
            // 
            this.panel34.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(91)))), ((int)(((byte)(150)))));
            this.panel34.Controls.Add(this.dataGridView13);
            this.panel34.Controls.Add(this.tlog_page);
            this.panel34.Controls.Add(this.btlog_back);
            this.panel34.Controls.Add(this.btlog_next);
            this.panel34.Controls.Add(this.panel49);
            this.panel34.Controls.Add(this.dataGridView8);
            this.panel34.Location = new System.Drawing.Point(6, 75);
            this.panel34.Name = "panel34";
            this.panel34.Size = new System.Drawing.Size(1278, 849);
            this.panel34.TabIndex = 32;
            // 
            // dataGridView13
            // 
            this.dataGridView13.AllowUserToAddRows = false;
            this.dataGridView13.AllowUserToDeleteRows = false;
            this.dataGridView13.AllowUserToResizeColumns = false;
            this.dataGridView13.AllowUserToResizeRows = false;
            this.dataGridView13.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridView13.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView13.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.transaction_id});
            this.dataGridView13.Location = new System.Drawing.Point(53, 70);
            this.dataGridView13.MultiSelect = false;
            this.dataGridView13.Name = "dataGridView13";
            this.dataGridView13.ReadOnly = true;
            this.dataGridView13.RowHeadersVisible = false;
            this.dataGridView13.Size = new System.Drawing.Size(162, 714);
            this.dataGridView13.TabIndex = 130;
            this.dataGridView13.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridView13_CellContentClick);
            this.dataGridView13.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridView13_CellContentClick);
            // 
            // transaction_id
            // 
            this.transaction_id.HeaderText = "Transaction ID";
            this.transaction_id.Name = "transaction_id";
            this.transaction_id.ReadOnly = true;
            this.transaction_id.Width = 158;
            // 
            // tlog_page
            // 
            this.tlog_page.Location = new System.Drawing.Point(634, 790);
            this.tlog_page.Name = "tlog_page";
            this.tlog_page.Size = new System.Drawing.Size(100, 20);
            this.tlog_page.TabIndex = 129;
            // 
            // btlog_back
            // 
            this.btlog_back.Location = new System.Drawing.Point(576, 819);
            this.btlog_back.Name = "btlog_back";
            this.btlog_back.Size = new System.Drawing.Size(75, 23);
            this.btlog_back.TabIndex = 128;
            this.btlog_back.Text = "BACK";
            this.btlog_back.UseVisualStyleBackColor = true;
            this.btlog_back.Click += new System.EventHandler(this.Btlog_back_Click);
            // 
            // btlog_next
            // 
            this.btlog_next.Location = new System.Drawing.Point(714, 819);
            this.btlog_next.Name = "btlog_next";
            this.btlog_next.Size = new System.Drawing.Size(75, 23);
            this.btlog_next.TabIndex = 127;
            this.btlog_next.Text = "NEXT";
            this.btlog_next.UseVisualStyleBackColor = true;
            this.btlog_next.Click += new System.EventHandler(this.Btlog_next_Click);
            // 
            // panel49
            // 
            this.panel49.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(57)))), ((int)(((byte)(108)))));
            this.panel49.Controls.Add(this.label153);
            this.panel49.Location = new System.Drawing.Point(0, 0);
            this.panel49.Name = "panel49";
            this.panel49.Size = new System.Drawing.Size(1278, 64);
            this.panel49.TabIndex = 2;
            // 
            // label153
            // 
            this.label153.AutoSize = true;
            this.label153.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold);
            this.label153.ForeColor = System.Drawing.Color.White;
            this.label153.Location = new System.Drawing.Point(527, 22);
            this.label153.Name = "label153";
            this.label153.Size = new System.Drawing.Size(191, 26);
            this.label153.TabIndex = 29;
            this.label153.Text = "Logs and History";
            // 
            // dataGridView8
            // 
            this.dataGridView8.AllowUserToAddRows = false;
            this.dataGridView8.AllowUserToDeleteRows = false;
            this.dataGridView8.AllowUserToResizeColumns = false;
            this.dataGridView8.AllowUserToResizeRows = false;
            this.dataGridView8.BackgroundColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView8.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this.dataGridView8.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView8.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PartsNo,
            this.dataGridViewTextBoxColumn19,
            this.User_Out,
            this.Spare_,
            this.User_In,
            this.Time_In,
            this.waiting_in});
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView8.DefaultCellStyle = dataGridViewCellStyle11;
            this.dataGridView8.Location = new System.Drawing.Point(212, 70);
            this.dataGridView8.MultiSelect = false;
            this.dataGridView8.Name = "dataGridView8";
            this.dataGridView8.ReadOnly = true;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView8.RowHeadersDefaultCellStyle = dataGridViewCellStyle12;
            this.dataGridView8.RowHeadersVisible = false;
            this.dataGridView8.Size = new System.Drawing.Size(1029, 714);
            this.dataGridView8.TabIndex = 0;
            // 
            // PartsNo
            // 
            this.PartsNo.DataPropertyName = "PartsNo";
            this.PartsNo.HeaderText = "Part No";
            this.PartsNo.Name = "PartsNo";
            this.PartsNo.ReadOnly = true;
            this.PartsNo.Width = 150;
            // 
            // dataGridViewTextBoxColumn19
            // 
            this.dataGridViewTextBoxColumn19.DataPropertyName = "mcLine";
            this.dataGridViewTextBoxColumn19.HeaderText = "MCLine";
            this.dataGridViewTextBoxColumn19.Name = "dataGridViewTextBoxColumn19";
            this.dataGridViewTextBoxColumn19.ReadOnly = true;
            this.dataGridViewTextBoxColumn19.Width = 125;
            // 
            // User_Out
            // 
            this.User_Out.DataPropertyName = "User_Out";
            this.User_Out.HeaderText = "Event Type";
            this.User_Out.Name = "User_Out";
            this.User_Out.ReadOnly = true;
            this.User_Out.Width = 150;
            // 
            // Spare_
            // 
            this.Spare_.DataPropertyName = "boxid";
            this.Spare_.HeaderText = "Box ID";
            this.Spare_.Name = "Spare_";
            this.Spare_.ReadOnly = true;
            this.Spare_.Width = 150;
            // 
            // User_In
            // 
            this.User_In.DataPropertyName = "User_In";
            this.User_In.HeaderText = "User";
            this.User_In.Name = "User_In";
            this.User_In.ReadOnly = true;
            this.User_In.Width = 125;
            // 
            // Time_In
            // 
            this.Time_In.DataPropertyName = "Time_In";
            this.Time_In.HeaderText = "Event Time";
            this.Time_In.Name = "Time_In";
            this.Time_In.ReadOnly = true;
            this.Time_In.Width = 175;
            // 
            // waiting_in
            // 
            this.waiting_in.HeaderText = "Waiting Time (Min)";
            this.waiting_in.Name = "waiting_in";
            this.waiting_in.ReadOnly = true;
            this.waiting_in.Width = 150;
            // 
            // panel33
            // 
            this.panel33.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(57)))), ((int)(((byte)(108)))));
            this.panel33.Controls.Add(this.button23);
            this.panel33.Controls.Add(this.label90);
            this.panel33.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel33.Location = new System.Drawing.Point(0, 0);
            this.panel33.Name = "panel33";
            this.panel33.Size = new System.Drawing.Size(1670, 45);
            this.panel33.TabIndex = 31;
            // 
            // button23
            // 
            this.button23.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(91)))), ((int)(((byte)(150)))));
            this.button23.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(57)))), ((int)(((byte)(108)))));
            this.button23.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(102)))), ((int)(((byte)(185)))));
            this.button23.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button23.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.button23.ForeColor = System.Drawing.Color.White;
            this.button23.Location = new System.Drawing.Point(1619, 2);
            this.button23.Name = "button23";
            this.button23.Size = new System.Drawing.Size(40, 40);
            this.button23.TabIndex = 74;
            this.button23.UseVisualStyleBackColor = false;
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold);
            this.label90.ForeColor = System.Drawing.Color.White;
            this.label90.Location = new System.Drawing.Point(773, 9);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(63, 26);
            this.label90.TabIndex = 28;
            this.label90.Text = "Logs";
            // 
            // tabMCLine
            // 
            this.tabMCLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.tabMCLine.Controls.Add(this.panel32);
            this.tabMCLine.Controls.Add(this.panel51);
            this.tabMCLine.Controls.Add(this.panel43);
            this.tabMCLine.Controls.Add(this.panel46);
            this.tabMCLine.Controls.Add(this.panel44);
            this.tabMCLine.Location = new System.Drawing.Point(4, 22);
            this.tabMCLine.Name = "tabMCLine";
            this.tabMCLine.Size = new System.Drawing.Size(1670, 985);
            this.tabMCLine.TabIndex = 8;
            this.tabMCLine.Text = "MCLine";
            // 
            // panel32
            // 
            this.panel32.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.panel32.Controls.Add(this.label130);
            this.panel32.Controls.Add(this.label132);
            this.panel32.Location = new System.Drawing.Point(0, 943);
            this.panel32.Name = "panel32";
            this.panel32.Size = new System.Drawing.Size(1674, 56);
            this.panel32.TabIndex = 106;
            // 
            // label130
            // 
            this.label130.AutoSize = true;
            this.label130.BackColor = System.Drawing.Color.Transparent;
            this.label130.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label130.ForeColor = System.Drawing.Color.White;
            this.label130.Location = new System.Drawing.Point(30, 22);
            this.label130.Name = "label130";
            this.label130.Size = new System.Drawing.Size(263, 20);
            this.label130.TabIndex = 91;
            this.label130.Text = "Store Management System @ 2019";
            this.label130.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label132
            // 
            this.label132.AutoSize = true;
            this.label132.BackColor = System.Drawing.Color.Transparent;
            this.label132.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label132.ForeColor = System.Drawing.Color.White;
            this.label132.Location = new System.Drawing.Point(1261, 15);
            this.label132.Name = "label132";
            this.label132.Size = new System.Drawing.Size(383, 20);
            this.label132.TabIndex = 90;
            this.label132.Text = "Thai Arrow Products Co., Ltd. (149 Bang Phli Factory)";
            this.label132.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel51
            // 
            this.panel51.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(57)))), ((int)(((byte)(108)))));
            this.panel51.Controls.Add(this.label115);
            this.panel51.Location = new System.Drawing.Point(1213, 51);
            this.panel51.Name = "panel51";
            this.panel51.Size = new System.Drawing.Size(454, 77);
            this.panel51.TabIndex = 105;
            // 
            // label115
            // 
            this.label115.AutoSize = true;
            this.label115.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label115.ForeColor = System.Drawing.Color.White;
            this.label115.Location = new System.Drawing.Point(138, 21);
            this.label115.Name = "label115";
            this.label115.Size = new System.Drawing.Size(178, 31);
            this.label115.TabIndex = 90;
            this.label115.Text = "Control Panel";
            // 
            // panel43
            // 
            this.panel43.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(91)))), ((int)(((byte)(150)))));
            this.panel43.Controls.Add(this.groupBox18);
            this.panel43.Location = new System.Drawing.Point(1213, 52);
            this.panel43.Name = "panel43";
            this.panel43.Size = new System.Drawing.Size(457, 889);
            this.panel43.TabIndex = 92;
            // 
            // groupBox18
            // 
            this.groupBox18.Controls.Add(this.label140);
            this.groupBox18.Controls.Add(this.panel45);
            this.groupBox18.Controls.Add(this.label139);
            this.groupBox18.Controls.Add(this.cmc_part);
            this.groupBox18.Controls.Add(this.label138);
            this.groupBox18.Controls.Add(this.cmc_line);
            this.groupBox18.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox18.ForeColor = System.Drawing.SystemColors.Control;
            this.groupBox18.Location = new System.Drawing.Point(37, 115);
            this.groupBox18.Name = "groupBox18";
            this.groupBox18.Size = new System.Drawing.Size(382, 581);
            this.groupBox18.TabIndex = 0;
            this.groupBox18.TabStop = false;
            this.groupBox18.Text = "Menu";
            // 
            // label140
            // 
            this.label140.AutoSize = true;
            this.label140.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label140.ForeColor = System.Drawing.Color.White;
            this.label140.Location = new System.Drawing.Point(27, 284);
            this.label140.Name = "label140";
            this.label140.Size = new System.Drawing.Size(138, 31);
            this.label140.TabIndex = 89;
            this.label140.Text = "Command";
            // 
            // panel45
            // 
            this.panel45.BackColor = System.Drawing.Color.Silver;
            this.panel45.Controls.Add(this.btmc_canceladd);
            this.panel45.Controls.Add(this.btmc_okadd);
            this.panel45.Controls.Add(this.btmc_search);
            this.panel45.Controls.Add(this.btmc_del);
            this.panel45.Controls.Add(this.btmc_add);
            this.panel45.Location = new System.Drawing.Point(33, 318);
            this.panel45.Name = "panel45";
            this.panel45.Size = new System.Drawing.Size(320, 193);
            this.panel45.TabIndex = 91;
            // 
            // btmc_canceladd
            // 
            this.btmc_canceladd.BackColor = System.Drawing.Color.DarkRed;
            this.btmc_canceladd.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Maroon;
            this.btmc_canceladd.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Red;
            this.btmc_canceladd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btmc_canceladd.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.btmc_canceladd.ForeColor = System.Drawing.Color.White;
            this.btmc_canceladd.Location = new System.Drawing.Point(84, 109);
            this.btmc_canceladd.Name = "btmc_canceladd";
            this.btmc_canceladd.Size = new System.Drawing.Size(150, 42);
            this.btmc_canceladd.TabIndex = 92;
            this.btmc_canceladd.Text = "CANCEL";
            this.btmc_canceladd.UseVisualStyleBackColor = false;
            this.btmc_canceladd.Visible = false;
            this.btmc_canceladd.Click += new System.EventHandler(this.Btmc_canceladd_Click);
            // 
            // btmc_okadd
            // 
            this.btmc_okadd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(91)))), ((int)(((byte)(150)))));
            this.btmc_okadd.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(57)))), ((int)(((byte)(108)))));
            this.btmc_okadd.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(102)))), ((int)(((byte)(185)))));
            this.btmc_okadd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btmc_okadd.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.btmc_okadd.ForeColor = System.Drawing.Color.White;
            this.btmc_okadd.Location = new System.Drawing.Point(84, 51);
            this.btmc_okadd.Name = "btmc_okadd";
            this.btmc_okadd.Size = new System.Drawing.Size(150, 42);
            this.btmc_okadd.TabIndex = 91;
            this.btmc_okadd.Text = "OK";
            this.btmc_okadd.UseVisualStyleBackColor = false;
            this.btmc_okadd.Visible = false;
            this.btmc_okadd.Click += new System.EventHandler(this.Btmc_okadd_Click);
            // 
            // btmc_search
            // 
            this.btmc_search.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(91)))), ((int)(((byte)(150)))));
            this.btmc_search.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(57)))), ((int)(((byte)(108)))));
            this.btmc_search.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(102)))), ((int)(((byte)(185)))));
            this.btmc_search.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btmc_search.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.btmc_search.ForeColor = System.Drawing.Color.White;
            this.btmc_search.Location = new System.Drawing.Point(84, 136);
            this.btmc_search.Name = "btmc_search";
            this.btmc_search.Size = new System.Drawing.Size(150, 42);
            this.btmc_search.TabIndex = 90;
            this.btmc_search.Text = "Search";
            this.btmc_search.UseVisualStyleBackColor = false;
            this.btmc_search.Click += new System.EventHandler(this.Btmc_search_Click);
            // 
            // btmc_del
            // 
            this.btmc_del.BackColor = System.Drawing.Color.DarkRed;
            this.btmc_del.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Maroon;
            this.btmc_del.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Red;
            this.btmc_del.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btmc_del.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.btmc_del.ForeColor = System.Drawing.Color.White;
            this.btmc_del.Location = new System.Drawing.Point(84, 78);
            this.btmc_del.Name = "btmc_del";
            this.btmc_del.Size = new System.Drawing.Size(150, 42);
            this.btmc_del.TabIndex = 89;
            this.btmc_del.Text = "DELETE";
            this.btmc_del.UseVisualStyleBackColor = false;
            this.btmc_del.Click += new System.EventHandler(this.Btmc_del_Click);
            // 
            // btmc_add
            // 
            this.btmc_add.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(91)))), ((int)(((byte)(150)))));
            this.btmc_add.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(57)))), ((int)(((byte)(108)))));
            this.btmc_add.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(102)))), ((int)(((byte)(185)))));
            this.btmc_add.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btmc_add.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.btmc_add.ForeColor = System.Drawing.Color.White;
            this.btmc_add.Location = new System.Drawing.Point(84, 21);
            this.btmc_add.Name = "btmc_add";
            this.btmc_add.Size = new System.Drawing.Size(150, 42);
            this.btmc_add.TabIndex = 88;
            this.btmc_add.Text = "ADD";
            this.btmc_add.UseVisualStyleBackColor = false;
            this.btmc_add.Click += new System.EventHandler(this.Btmc_add_Click);
            // 
            // label139
            // 
            this.label139.AutoSize = true;
            this.label139.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label139.ForeColor = System.Drawing.Color.White;
            this.label139.Location = new System.Drawing.Point(27, 142);
            this.label139.Name = "label139";
            this.label139.Size = new System.Drawing.Size(121, 31);
            this.label139.TabIndex = 104;
            this.label139.Text = "Part No :";
            // 
            // cmc_part
            // 
            this.cmc_part.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.cmc_part.FormattingEnabled = true;
            this.cmc_part.Location = new System.Drawing.Point(182, 145);
            this.cmc_part.Name = "cmc_part";
            this.cmc_part.Size = new System.Drawing.Size(152, 28);
            this.cmc_part.TabIndex = 103;
            // 
            // label138
            // 
            this.label138.AutoSize = true;
            this.label138.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label138.ForeColor = System.Drawing.Color.White;
            this.label138.Location = new System.Drawing.Point(27, 54);
            this.label138.Name = "label138";
            this.label138.Size = new System.Drawing.Size(122, 31);
            this.label138.TabIndex = 88;
            this.label138.Text = "Line No :";
            // 
            // cmc_line
            // 
            this.cmc_line.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.cmc_line.FormattingEnabled = true;
            this.cmc_line.Location = new System.Drawing.Point(182, 57);
            this.cmc_line.Name = "cmc_line";
            this.cmc_line.Size = new System.Drawing.Size(152, 28);
            this.cmc_line.TabIndex = 102;
            this.cmc_line.SelectedIndexChanged += new System.EventHandler(this.Cmc_line_SelectedIndexChanged);
            // 
            // panel46
            // 
            this.panel46.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(91)))), ((int)(((byte)(150)))));
            this.panel46.Controls.Add(this.dataGridView12);
            this.panel46.Controls.Add(this.dataGridView11);
            this.panel46.Controls.Add(this.panel50);
            this.panel46.Location = new System.Drawing.Point(3, 51);
            this.panel46.Name = "panel46";
            this.panel46.Size = new System.Drawing.Size(1207, 889);
            this.panel46.TabIndex = 91;
            // 
            // dataGridView12
            // 
            this.dataGridView12.AllowUserToAddRows = false;
            this.dataGridView12.AllowUserToDeleteRows = false;
            this.dataGridView12.AllowUserToResizeColumns = false;
            this.dataGridView12.AllowUserToResizeRows = false;
            this.dataGridView12.BackgroundColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle13.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle13.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView12.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle13;
            this.dataGridView12.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView12.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn24});
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle14.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView12.DefaultCellStyle = dataGridViewCellStyle14;
            this.dataGridView12.Location = new System.Drawing.Point(519, 112);
            this.dataGridView12.MultiSelect = false;
            this.dataGridView12.Name = "dataGridView12";
            this.dataGridView12.ReadOnly = true;
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle15.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle15.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView12.RowHeadersDefaultCellStyle = dataGridViewCellStyle15;
            this.dataGridView12.RowHeadersVisible = false;
            this.dataGridView12.Size = new System.Drawing.Size(360, 680);
            this.dataGridView12.TabIndex = 92;
            // 
            // dataGridViewTextBoxColumn24
            // 
            this.dataGridViewTextBoxColumn24.DataPropertyName = "MC_PartNo";
            this.dataGridViewTextBoxColumn24.HeaderText = "Part No.";
            this.dataGridViewTextBoxColumn24.Name = "dataGridViewTextBoxColumn24";
            this.dataGridViewTextBoxColumn24.ReadOnly = true;
            this.dataGridViewTextBoxColumn24.Width = 360;
            // 
            // dataGridView11
            // 
            this.dataGridView11.AllowUserToAddRows = false;
            this.dataGridView11.AllowUserToDeleteRows = false;
            this.dataGridView11.AllowUserToResizeColumns = false;
            this.dataGridView11.AllowUserToResizeRows = false;
            this.dataGridView11.BackgroundColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle16.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle16.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle16.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle16.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView11.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle16;
            this.dataGridView11.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView11.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.LineNo_MCLine});
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle17.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle17.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle17.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle17.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle17.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView11.DefaultCellStyle = dataGridViewCellStyle17;
            this.dataGridView11.Location = new System.Drawing.Point(279, 112);
            this.dataGridView11.MultiSelect = false;
            this.dataGridView11.Name = "dataGridView11";
            this.dataGridView11.ReadOnly = true;
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle18.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle18.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle18.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle18.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle18.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView11.RowHeadersDefaultCellStyle = dataGridViewCellStyle18;
            this.dataGridView11.RowHeadersVisible = false;
            this.dataGridView11.Size = new System.Drawing.Size(243, 680);
            this.dataGridView11.TabIndex = 0;
            this.dataGridView11.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridView11_CellContentClick);
            this.dataGridView11.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridView11_CellContentClick);
            // 
            // LineNo_MCLine
            // 
            this.LineNo_MCLine.HeaderText = "Line No";
            this.LineNo_MCLine.Name = "LineNo_MCLine";
            this.LineNo_MCLine.ReadOnly = true;
            this.LineNo_MCLine.Width = 238;
            // 
            // panel50
            // 
            this.panel50.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(57)))), ((int)(((byte)(108)))));
            this.panel50.Controls.Add(this.label141);
            this.panel50.Location = new System.Drawing.Point(0, 0);
            this.panel50.Name = "panel50";
            this.panel50.Size = new System.Drawing.Size(1207, 77);
            this.panel50.TabIndex = 91;
            // 
            // label141
            // 
            this.label141.AutoSize = true;
            this.label141.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label141.ForeColor = System.Drawing.Color.White;
            this.label141.Location = new System.Drawing.Point(552, 21);
            this.label141.Name = "label141";
            this.label141.Size = new System.Drawing.Size(245, 31);
            this.label141.TabIndex = 89;
            this.label141.Text = "Line No(DB) Table ";
            // 
            // panel44
            // 
            this.panel44.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(57)))), ((int)(((byte)(108)))));
            this.panel44.Controls.Add(this.label106);
            this.panel44.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel44.Location = new System.Drawing.Point(0, 0);
            this.panel44.Name = "panel44";
            this.panel44.Size = new System.Drawing.Size(1670, 45);
            this.panel44.TabIndex = 32;
            // 
            // label106
            // 
            this.label106.AutoSize = true;
            this.label106.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold);
            this.label106.ForeColor = System.Drawing.Color.White;
            this.label106.Location = new System.Drawing.Point(773, 9);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(93, 26);
            this.label106.TabIndex = 28;
            this.label106.Text = "MCLine";
            // 
            // tabAddPart
            // 
            this.tabAddPart.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.tabAddPart.Controls.Add(this.panel11);
            this.tabAddPart.Controls.Add(this.panel8);
            this.tabAddPart.Controls.Add(this.panel17);
            this.tabAddPart.Location = new System.Drawing.Point(4, 22);
            this.tabAddPart.Name = "tabAddPart";
            this.tabAddPart.Size = new System.Drawing.Size(1670, 985);
            this.tabAddPart.TabIndex = 9;
            this.tabAddPart.Text = "AddPart";
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.panel11.Controls.Add(this.label2);
            this.panel11.Controls.Add(this.label3);
            this.panel11.Location = new System.Drawing.Point(0, 933);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(1674, 56);
            this.panel11.TabIndex = 10;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(30, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(263, 20);
            this.label2.TabIndex = 91;
            this.label2.Text = "Store Management System @ 2019";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(1261, 15);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(383, 20);
            this.label3.TabIndex = 90;
            this.label3.Text = "Thai Arrow Products Co., Ltd. (149 Bang Phli Factory)";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(91)))), ((int)(((byte)(150)))));
            this.panel8.Controls.Add(this.groupBox2);
            this.panel8.Controls.Add(this.groupBox1);
            this.panel8.Controls.Add(this.panel10);
            this.panel8.Controls.Add(this.panel9);
            this.panel8.Location = new System.Drawing.Point(3, 51);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(1653, 885);
            this.panel8.TabIndex = 9;
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(227)))), ((int)(((byte)(238)))));
            this.groupBox2.Controls.Add(this.btAdd_delLocal);
            this.groupBox2.Controls.Add(this.button3);
            this.groupBox2.Controls.Add(this.label174);
            this.groupBox2.Controls.Add(this.comAdd_Line);
            this.groupBox2.Controls.Add(this.dataGridView3);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(537, 179);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(420, 604);
            this.groupBox2.TabIndex = 24;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Location";
            // 
            // btAdd_delLocal
            // 
            this.btAdd_delLocal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(67)))), ((int)(((byte)(53)))));
            this.btAdd_delLocal.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btAdd_delLocal.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btAdd_delLocal.ForeColor = System.Drawing.Color.White;
            this.btAdd_delLocal.Location = new System.Drawing.Point(224, 509);
            this.btAdd_delLocal.Name = "btAdd_delLocal";
            this.btAdd_delLocal.Size = new System.Drawing.Size(135, 66);
            this.btAdd_delLocal.TabIndex = 138;
            this.btAdd_delLocal.Text = "DELETE";
            this.btAdd_delLocal.UseVisualStyleBackColor = false;
            this.btAdd_delLocal.Click += new System.EventHandler(this.BtAdd_delLocal_Click);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(91)))), ((int)(((byte)(150)))));
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.ForeColor = System.Drawing.Color.White;
            this.button3.Location = new System.Drawing.Point(42, 509);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(135, 66);
            this.button3.TabIndex = 137;
            this.button3.Text = "ADD";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.Button3_Click);
            // 
            // label174
            // 
            this.label174.AutoSize = true;
            this.label174.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label174.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(79)))), ((int)(((byte)(91)))), ((int)(((byte)(102)))));
            this.label174.Location = new System.Drawing.Point(28, 362);
            this.label174.Name = "label174";
            this.label174.Size = new System.Drawing.Size(136, 25);
            this.label174.TabIndex = 135;
            this.label174.Text = "Line No(DB) : ";
            // 
            // comAdd_Line
            // 
            this.comAdd_Line.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comAdd_Line.FormattingEnabled = true;
            this.comAdd_Line.Location = new System.Drawing.Point(183, 359);
            this.comAdd_Line.Name = "comAdd_Line";
            this.comAdd_Line.Size = new System.Drawing.Size(203, 28);
            this.comAdd_Line.TabIndex = 3;
            // 
            // dataGridView3
            // 
            this.dataGridView3.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView3.Location = new System.Drawing.Point(33, 34);
            this.dataGridView3.Name = "dataGridView3";
            this.dataGridView3.Size = new System.Drawing.Size(353, 149);
            this.dataGridView3.TabIndex = 1;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(227)))), ((int)(((byte)(238)))));
            this.groupBox1.Controls.Add(this.label116);
            this.groupBox1.Controls.Add(this.comAdd_floor);
            this.groupBox1.Controls.Add(this.btAdd_del);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.label175);
            this.groupBox1.Controls.Add(this.label176);
            this.groupBox1.Controls.Add(this.label168);
            this.groupBox1.Controls.Add(this.label172);
            this.groupBox1.Controls.Add(this.comAdd_Slot);
            this.groupBox1.Controls.Add(this.comAdd_side);
            this.groupBox1.Controls.Add(this.comAdd_Zone);
            this.groupBox1.Controls.Add(this.comAdd_shelf);
            this.groupBox1.Controls.Add(this.dataGridView2);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(27, 179);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(450, 604);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Location";
            // 
            // label116
            // 
            this.label116.AutoSize = true;
            this.label116.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label116.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(79)))), ((int)(((byte)(91)))), ((int)(((byte)(102)))));
            this.label116.Location = new System.Drawing.Point(58, 366);
            this.label116.Name = "label116";
            this.label116.Size = new System.Drawing.Size(72, 25);
            this.label116.TabIndex = 144;
            this.label116.Text = "Floor : ";
            // 
            // comAdd_floor
            // 
            this.comAdd_floor.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comAdd_floor.FormattingEnabled = true;
            this.comAdd_floor.Location = new System.Drawing.Point(172, 363);
            this.comAdd_floor.Name = "comAdd_floor";
            this.comAdd_floor.Size = new System.Drawing.Size(203, 28);
            this.comAdd_floor.TabIndex = 143;
            this.comAdd_floor.SelectedIndexChanged += new System.EventHandler(this.ComAdd_floor_SelectedIndexChanged);
            // 
            // btAdd_del
            // 
            this.btAdd_del.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(67)))), ((int)(((byte)(53)))));
            this.btAdd_del.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btAdd_del.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btAdd_del.ForeColor = System.Drawing.Color.White;
            this.btAdd_del.Location = new System.Drawing.Point(240, 523);
            this.btAdd_del.Name = "btAdd_del";
            this.btAdd_del.Size = new System.Drawing.Size(135, 66);
            this.btAdd_del.TabIndex = 142;
            this.btAdd_del.Text = "DELETE";
            this.btAdd_del.UseVisualStyleBackColor = false;
            this.btAdd_del.Click += new System.EventHandler(this.BtAdd_del_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(91)))), ((int)(((byte)(150)))));
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(63, 523);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(135, 66);
            this.button1.TabIndex = 136;
            this.button1.Text = "ADD";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // label175
            // 
            this.label175.AutoSize = true;
            this.label175.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label175.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(79)))), ((int)(((byte)(91)))), ((int)(((byte)(102)))));
            this.label175.Location = new System.Drawing.Point(57, 474);
            this.label175.Name = "label175";
            this.label175.Size = new System.Drawing.Size(62, 25);
            this.label175.TabIndex = 141;
            this.label175.Text = "Slot : ";
            // 
            // label176
            // 
            this.label176.AutoSize = true;
            this.label176.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label176.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(79)))), ((int)(((byte)(91)))), ((int)(((byte)(102)))));
            this.label176.Location = new System.Drawing.Point(57, 422);
            this.label176.Name = "label176";
            this.label176.Size = new System.Drawing.Size(63, 25);
            this.label176.TabIndex = 140;
            this.label176.Text = "Side :";
            // 
            // label168
            // 
            this.label168.AutoSize = true;
            this.label168.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label168.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(79)))), ((int)(((byte)(91)))), ((int)(((byte)(102)))));
            this.label168.Location = new System.Drawing.Point(57, 309);
            this.label168.Name = "label168";
            this.label168.Size = new System.Drawing.Size(73, 25);
            this.label168.TabIndex = 139;
            this.label168.Text = "Zone : ";
            // 
            // label172
            // 
            this.label172.AutoSize = true;
            this.label172.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label172.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(79)))), ((int)(((byte)(91)))), ((int)(((byte)(102)))));
            this.label172.Location = new System.Drawing.Point(57, 253);
            this.label172.Name = "label172";
            this.label172.Size = new System.Drawing.Size(68, 25);
            this.label172.TabIndex = 138;
            this.label172.Text = "Shelf :";
            // 
            // comAdd_Slot
            // 
            this.comAdd_Slot.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comAdd_Slot.FormattingEnabled = true;
            this.comAdd_Slot.Location = new System.Drawing.Point(172, 475);
            this.comAdd_Slot.Name = "comAdd_Slot";
            this.comAdd_Slot.Size = new System.Drawing.Size(203, 28);
            this.comAdd_Slot.TabIndex = 5;
            // 
            // comAdd_side
            // 
            this.comAdd_side.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comAdd_side.FormattingEnabled = true;
            this.comAdd_side.Location = new System.Drawing.Point(172, 419);
            this.comAdd_side.Name = "comAdd_side";
            this.comAdd_side.Size = new System.Drawing.Size(203, 28);
            this.comAdd_side.TabIndex = 4;
            this.comAdd_side.SelectedIndexChanged += new System.EventHandler(this.ComAdd_Shelf_SelectedIndexChanged);
            // 
            // comAdd_Zone
            // 
            this.comAdd_Zone.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comAdd_Zone.FormattingEnabled = true;
            this.comAdd_Zone.Location = new System.Drawing.Point(172, 306);
            this.comAdd_Zone.Name = "comAdd_Zone";
            this.comAdd_Zone.Size = new System.Drawing.Size(203, 28);
            this.comAdd_Zone.TabIndex = 3;
            this.comAdd_Zone.SelectedIndexChanged += new System.EventHandler(this.ComAdd_Zone_SelectedIndexChanged);
            // 
            // comAdd_shelf
            // 
            this.comAdd_shelf.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comAdd_shelf.FormattingEnabled = true;
            this.comAdd_shelf.Location = new System.Drawing.Point(172, 250);
            this.comAdd_shelf.Name = "comAdd_shelf";
            this.comAdd_shelf.Size = new System.Drawing.Size(203, 28);
            this.comAdd_shelf.TabIndex = 2;
            this.comAdd_shelf.SelectedIndexChanged += new System.EventHandler(this.ComAdd_Area_SelectedIndexChanged);
            // 
            // dataGridView2
            // 
            this.dataGridView2.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Location = new System.Drawing.Point(3, 34);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.Size = new System.Drawing.Size(434, 149);
            this.dataGridView2.TabIndex = 0;
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(57)))), ((int)(((byte)(108)))));
            this.panel10.Controls.Add(this.btAdd_cancel);
            this.panel10.Controls.Add(this.btAdd_save);
            this.panel10.Controls.Add(this.groupBox3);
            this.panel10.Location = new System.Drawing.Point(991, 78);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(662, 807);
            this.panel10.TabIndex = 23;
            // 
            // btAdd_cancel
            // 
            this.btAdd_cancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(67)))), ((int)(((byte)(53)))));
            this.btAdd_cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btAdd_cancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btAdd_cancel.ForeColor = System.Drawing.Color.White;
            this.btAdd_cancel.Location = new System.Drawing.Point(366, 702);
            this.btAdd_cancel.Name = "btAdd_cancel";
            this.btAdd_cancel.Size = new System.Drawing.Size(214, 66);
            this.btAdd_cancel.TabIndex = 145;
            this.btAdd_cancel.Text = "CANCEL";
            this.btAdd_cancel.UseVisualStyleBackColor = false;
            this.btAdd_cancel.Click += new System.EventHandler(this.Button7_Click);
            // 
            // btAdd_save
            // 
            this.btAdd_save.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(91)))), ((int)(((byte)(150)))));
            this.btAdd_save.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btAdd_save.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btAdd_save.ForeColor = System.Drawing.Color.White;
            this.btAdd_save.Location = new System.Drawing.Point(88, 702);
            this.btAdd_save.Name = "btAdd_save";
            this.btAdd_save.Size = new System.Drawing.Size(214, 66);
            this.btAdd_save.TabIndex = 144;
            this.btAdd_save.Text = "SAVE";
            this.btAdd_save.UseVisualStyleBackColor = false;
            this.btAdd_save.Click += new System.EventHandler(this.BtAdd_save_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(227)))), ((int)(((byte)(228)))));
            this.groupBox3.Controls.Add(this.checkAdd_Rank);
            this.groupBox3.Controls.Add(this.checkAdd_FIFO);
            this.groupBox3.Controls.Add(this.label126);
            this.groupBox3.Controls.Add(this.label125);
            this.groupBox3.Controls.Add(this.label101);
            this.groupBox3.Controls.Add(this.label82);
            this.groupBox3.Controls.Add(this.label94);
            this.groupBox3.Controls.Add(this.label95);
            this.groupBox3.Controls.Add(this.label98);
            this.groupBox3.Controls.Add(this.label99);
            this.groupBox3.Controls.Add(this.label100);
            this.groupBox3.Controls.Add(this.comAdd_parttype);
            this.groupBox3.Controls.Add(this.comAdd_partpackage);
            this.groupBox3.Controls.Add(this.numAdd_minstock);
            this.groupBox3.Controls.Add(this.textAdd_model);
            this.groupBox3.Controls.Add(this.numAdd_maxstock);
            this.groupBox3.Controls.Add(this.numAdd_qty);
            this.groupBox3.Controls.Add(this.textAdd_supplier);
            this.groupBox3.Controls.Add(this.textAdd_partname);
            this.groupBox3.Controls.Add(this.textAdd_partno);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.ForeColor = System.Drawing.Color.Black;
            this.groupBox3.Location = new System.Drawing.Point(138, 37);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(414, 604);
            this.groupBox3.TabIndex = 143;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Add Part";
            // 
            // checkAdd_Rank
            // 
            this.checkAdd_Rank.AutoSize = true;
            this.checkAdd_Rank.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkAdd_Rank.Location = new System.Drawing.Point(36, 546);
            this.checkAdd_Rank.Name = "checkAdd_Rank";
            this.checkAdd_Rank.Size = new System.Drawing.Size(153, 29);
            this.checkAdd_Rank.TabIndex = 151;
            this.checkAdd_Rank.Text = "Rank Enabled";
            this.checkAdd_Rank.UseVisualStyleBackColor = true;
            // 
            // checkAdd_FIFO
            // 
            this.checkAdd_FIFO.AutoSize = true;
            this.checkAdd_FIFO.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkAdd_FIFO.Location = new System.Drawing.Point(36, 501);
            this.checkAdd_FIFO.Name = "checkAdd_FIFO";
            this.checkAdd_FIFO.Size = new System.Drawing.Size(153, 29);
            this.checkAdd_FIFO.TabIndex = 152;
            this.checkAdd_FIFO.Text = "FIFO Enabled";
            this.checkAdd_FIFO.UseVisualStyleBackColor = true;
            // 
            // label126
            // 
            this.label126.AutoSize = true;
            this.label126.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label126.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(79)))), ((int)(((byte)(91)))), ((int)(((byte)(102)))));
            this.label126.Location = new System.Drawing.Point(31, 451);
            this.label126.Name = "label126";
            this.label126.Size = new System.Drawing.Size(119, 25);
            this.label126.TabIndex = 150;
            this.label126.Text = "ประเภท Part :";
            // 
            // label125
            // 
            this.label125.AutoSize = true;
            this.label125.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label125.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(79)))), ((int)(((byte)(91)))), ((int)(((byte)(102)))));
            this.label125.Location = new System.Drawing.Point(31, 397);
            this.label125.Name = "label125";
            this.label125.Size = new System.Drawing.Size(118, 25);
            this.label125.TabIndex = 149;
            this.label125.Text = "ลักษณะ Part :";
            // 
            // label101
            // 
            this.label101.AutoSize = true;
            this.label101.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label101.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(79)))), ((int)(((byte)(91)))), ((int)(((byte)(102)))));
            this.label101.Location = new System.Drawing.Point(31, 342);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(115, 25);
            this.label101.TabIndex = 148;
            this.label101.Text = "Min Stock : ";
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label82.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(79)))), ((int)(((byte)(91)))), ((int)(((byte)(102)))));
            this.label82.Location = new System.Drawing.Point(31, 289);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(121, 25);
            this.label82.TabIndex = 147;
            this.label82.Text = "Max Stock : ";
            // 
            // label94
            // 
            this.label94.AutoSize = true;
            this.label94.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label94.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(79)))), ((int)(((byte)(91)))), ((int)(((byte)(102)))));
            this.label94.Location = new System.Drawing.Point(31, 235);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(82, 25);
            this.label94.TabIndex = 146;
            this.label94.Text = "Model : ";
            // 
            // label95
            // 
            this.label95.AutoSize = true;
            this.label95.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label95.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(79)))), ((int)(((byte)(91)))), ((int)(((byte)(102)))));
            this.label95.Location = new System.Drawing.Point(31, 183);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(104, 25);
            this.label95.TabIndex = 145;
            this.label95.Text = "Q\'ty pcs  : ";
            // 
            // label98
            // 
            this.label98.AutoSize = true;
            this.label98.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label98.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(79)))), ((int)(((byte)(91)))), ((int)(((byte)(102)))));
            this.label98.Location = new System.Drawing.Point(31, 130);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(98, 25);
            this.label98.TabIndex = 144;
            this.label98.Text = "Supllier  : ";
            // 
            // label99
            // 
            this.label99.AutoSize = true;
            this.label99.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label99.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(79)))), ((int)(((byte)(91)))), ((int)(((byte)(102)))));
            this.label99.Location = new System.Drawing.Point(31, 82);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(120, 25);
            this.label99.TabIndex = 143;
            this.label99.Text = "Part Name : ";
            // 
            // label100
            // 
            this.label100.AutoSize = true;
            this.label100.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label100.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(79)))), ((int)(((byte)(91)))), ((int)(((byte)(102)))));
            this.label100.Location = new System.Drawing.Point(31, 38);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(93, 25);
            this.label100.TabIndex = 142;
            this.label100.Text = "Part No : ";
            // 
            // comAdd_parttype
            // 
            this.comAdd_parttype.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comAdd_parttype.FormattingEnabled = true;
            this.comAdd_parttype.Location = new System.Drawing.Point(166, 452);
            this.comAdd_parttype.Name = "comAdd_parttype";
            this.comAdd_parttype.Size = new System.Drawing.Size(203, 28);
            this.comAdd_parttype.TabIndex = 9;
            // 
            // comAdd_partpackage
            // 
            this.comAdd_partpackage.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comAdd_partpackage.FormattingEnabled = true;
            this.comAdd_partpackage.Location = new System.Drawing.Point(166, 398);
            this.comAdd_partpackage.Name = "comAdd_partpackage";
            this.comAdd_partpackage.Size = new System.Drawing.Size(203, 28);
            this.comAdd_partpackage.TabIndex = 8;
            // 
            // numAdd_minstock
            // 
            this.numAdd_minstock.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numAdd_minstock.Location = new System.Drawing.Point(166, 344);
            this.numAdd_minstock.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.numAdd_minstock.Name = "numAdd_minstock";
            this.numAdd_minstock.Size = new System.Drawing.Size(203, 26);
            this.numAdd_minstock.TabIndex = 7;
            // 
            // textAdd_model
            // 
            this.textAdd_model.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textAdd_model.Location = new System.Drawing.Point(166, 236);
            this.textAdd_model.Name = "textAdd_model";
            this.textAdd_model.Size = new System.Drawing.Size(203, 26);
            this.textAdd_model.TabIndex = 5;
            // 
            // numAdd_maxstock
            // 
            this.numAdd_maxstock.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numAdd_maxstock.Location = new System.Drawing.Point(166, 291);
            this.numAdd_maxstock.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.numAdd_maxstock.Name = "numAdd_maxstock";
            this.numAdd_maxstock.Size = new System.Drawing.Size(203, 26);
            this.numAdd_maxstock.TabIndex = 4;
            this.numAdd_maxstock.ValueChanged += new System.EventHandler(this.NumericUpDown2_ValueChanged);
            // 
            // numAdd_qty
            // 
            this.numAdd_qty.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numAdd_qty.Location = new System.Drawing.Point(166, 182);
            this.numAdd_qty.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.numAdd_qty.Name = "numAdd_qty";
            this.numAdd_qty.Size = new System.Drawing.Size(203, 26);
            this.numAdd_qty.TabIndex = 3;
            // 
            // textAdd_supplier
            // 
            this.textAdd_supplier.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textAdd_supplier.Location = new System.Drawing.Point(166, 131);
            this.textAdd_supplier.Name = "textAdd_supplier";
            this.textAdd_supplier.Size = new System.Drawing.Size(203, 26);
            this.textAdd_supplier.TabIndex = 2;
            // 
            // textAdd_partname
            // 
            this.textAdd_partname.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textAdd_partname.Location = new System.Drawing.Point(166, 81);
            this.textAdd_partname.Name = "textAdd_partname";
            this.textAdd_partname.Size = new System.Drawing.Size(203, 26);
            this.textAdd_partname.TabIndex = 1;
            // 
            // textAdd_partno
            // 
            this.textAdd_partno.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textAdd_partno.Location = new System.Drawing.Point(166, 37);
            this.textAdd_partno.Name = "textAdd_partno";
            this.textAdd_partno.Size = new System.Drawing.Size(203, 26);
            this.textAdd_partno.TabIndex = 0;
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(57)))), ((int)(((byte)(108)))));
            this.panel9.Controls.Add(this.label105);
            this.panel9.Location = new System.Drawing.Point(0, 0);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(1653, 79);
            this.panel9.TabIndex = 0;
            // 
            // label105
            // 
            this.label105.AutoSize = true;
            this.label105.BackColor = System.Drawing.Color.Transparent;
            this.label105.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label105.ForeColor = System.Drawing.Color.White;
            this.label105.Location = new System.Drawing.Point(767, 20);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(118, 39);
            this.label105.TabIndex = 22;
            this.label105.Text = "MENU";
            // 
            // panel17
            // 
            this.panel17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(57)))), ((int)(((byte)(108)))));
            this.panel17.Controls.Add(this.label80);
            this.panel17.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel17.Location = new System.Drawing.Point(0, 0);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(1670, 45);
            this.panel17.TabIndex = 8;
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label80.ForeColor = System.Drawing.Color.White;
            this.label80.Location = new System.Drawing.Point(759, 8);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(105, 29);
            this.label80.TabIndex = 34;
            this.label80.Text = "Add Part";
            // 
            // tabEditPart
            // 
            this.tabEditPart.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.tabEditPart.Controls.Add(this.panel31);
            this.tabEditPart.Controls.Add(this.panel13);
            this.tabEditPart.Controls.Add(this.panel12);
            this.tabEditPart.Location = new System.Drawing.Point(4, 22);
            this.tabEditPart.Name = "tabEditPart";
            this.tabEditPart.Size = new System.Drawing.Size(1670, 985);
            this.tabEditPart.TabIndex = 10;
            this.tabEditPart.Text = "EditPart";
            // 
            // panel31
            // 
            this.panel31.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.panel31.Controls.Add(this.label127);
            this.panel31.Controls.Add(this.label128);
            this.panel31.Location = new System.Drawing.Point(0, 933);
            this.panel31.Name = "panel31";
            this.panel31.Size = new System.Drawing.Size(1674, 56);
            this.panel31.TabIndex = 11;
            // 
            // label127
            // 
            this.label127.AutoSize = true;
            this.label127.BackColor = System.Drawing.Color.Transparent;
            this.label127.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label127.ForeColor = System.Drawing.Color.White;
            this.label127.Location = new System.Drawing.Point(30, 22);
            this.label127.Name = "label127";
            this.label127.Size = new System.Drawing.Size(263, 20);
            this.label127.TabIndex = 91;
            this.label127.Text = "Store Management System @ 2019";
            this.label127.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label128
            // 
            this.label128.AutoSize = true;
            this.label128.BackColor = System.Drawing.Color.Transparent;
            this.label128.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label128.ForeColor = System.Drawing.Color.White;
            this.label128.Location = new System.Drawing.Point(1261, 15);
            this.label128.Name = "label128";
            this.label128.Size = new System.Drawing.Size(383, 20);
            this.label128.TabIndex = 90;
            this.label128.Text = "Thai Arrow Products Co., Ltd. (149 Bang Phli Factory)";
            this.label128.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel13
            // 
            this.panel13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(91)))), ((int)(((byte)(150)))));
            this.panel13.Controls.Add(this.groupBox4);
            this.panel13.Controls.Add(this.groupBox5);
            this.panel13.Controls.Add(this.panel14);
            this.panel13.Controls.Add(this.panel15);
            this.panel13.Location = new System.Drawing.Point(9, 50);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(1653, 885);
            this.panel13.TabIndex = 10;
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(227)))), ((int)(((byte)(238)))));
            this.groupBox4.Controls.Add(this.btEdit_cancelLine);
            this.groupBox4.Controls.Add(this.btEdit_addLineNo);
            this.groupBox4.Controls.Add(this.btEdit_delLine);
            this.groupBox4.Controls.Add(this.btEdit_addLine);
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Controls.Add(this.comEdit_line);
            this.groupBox4.Controls.Add(this.dataGridView4);
            this.groupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.Location = new System.Drawing.Point(537, 179);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(420, 604);
            this.groupBox4.TabIndex = 24;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Location";
            // 
            // btEdit_cancelLine
            // 
            this.btEdit_cancelLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(67)))), ((int)(((byte)(53)))));
            this.btEdit_cancelLine.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(57)))), ((int)(((byte)(108)))));
            this.btEdit_cancelLine.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(102)))), ((int)(((byte)(185)))));
            this.btEdit_cancelLine.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btEdit_cancelLine.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.btEdit_cancelLine.ForeColor = System.Drawing.Color.White;
            this.btEdit_cancelLine.Location = new System.Drawing.Point(224, 434);
            this.btEdit_cancelLine.Name = "btEdit_cancelLine";
            this.btEdit_cancelLine.Size = new System.Drawing.Size(135, 66);
            this.btEdit_cancelLine.TabIndex = 144;
            this.btEdit_cancelLine.Text = "CANCEL";
            this.btEdit_cancelLine.UseVisualStyleBackColor = false;
            this.btEdit_cancelLine.Visible = false;
            this.btEdit_cancelLine.Click += new System.EventHandler(this.BtEdit_cancelLine_Click);
            // 
            // btEdit_addLineNo
            // 
            this.btEdit_addLineNo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(91)))), ((int)(((byte)(150)))));
            this.btEdit_addLineNo.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(57)))), ((int)(((byte)(108)))));
            this.btEdit_addLineNo.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(102)))), ((int)(((byte)(185)))));
            this.btEdit_addLineNo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btEdit_addLineNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.btEdit_addLineNo.ForeColor = System.Drawing.Color.White;
            this.btEdit_addLineNo.Location = new System.Drawing.Point(46, 438);
            this.btEdit_addLineNo.Name = "btEdit_addLineNo";
            this.btEdit_addLineNo.Size = new System.Drawing.Size(135, 66);
            this.btEdit_addLineNo.TabIndex = 143;
            this.btEdit_addLineNo.Text = "ADD";
            this.btEdit_addLineNo.UseVisualStyleBackColor = false;
            this.btEdit_addLineNo.Visible = false;
            this.btEdit_addLineNo.Click += new System.EventHandler(this.BtEdit_addLineNo_Click);
            // 
            // btEdit_delLine
            // 
            this.btEdit_delLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(67)))), ((int)(((byte)(53)))));
            this.btEdit_delLine.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btEdit_delLine.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btEdit_delLine.ForeColor = System.Drawing.Color.White;
            this.btEdit_delLine.Location = new System.Drawing.Point(224, 509);
            this.btEdit_delLine.Name = "btEdit_delLine";
            this.btEdit_delLine.Size = new System.Drawing.Size(135, 66);
            this.btEdit_delLine.TabIndex = 138;
            this.btEdit_delLine.Text = "DELETE";
            this.btEdit_delLine.UseVisualStyleBackColor = false;
            this.btEdit_delLine.Click += new System.EventHandler(this.BtEdit_delLine_Click);
            // 
            // btEdit_addLine
            // 
            this.btEdit_addLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(91)))), ((int)(((byte)(150)))));
            this.btEdit_addLine.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btEdit_addLine.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btEdit_addLine.ForeColor = System.Drawing.Color.White;
            this.btEdit_addLine.Location = new System.Drawing.Point(46, 509);
            this.btEdit_addLine.Name = "btEdit_addLine";
            this.btEdit_addLine.Size = new System.Drawing.Size(135, 66);
            this.btEdit_addLine.TabIndex = 137;
            this.btEdit_addLine.Text = "ADD";
            this.btEdit_addLine.UseVisualStyleBackColor = false;
            this.btEdit_addLine.Click += new System.EventHandler(this.BtEdit_addLine_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(79)))), ((int)(((byte)(91)))), ((int)(((byte)(102)))));
            this.label5.Location = new System.Drawing.Point(28, 362);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(136, 25);
            this.label5.TabIndex = 135;
            this.label5.Text = "Line No(DB) : ";
            // 
            // comEdit_line
            // 
            this.comEdit_line.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comEdit_line.FormattingEnabled = true;
            this.comEdit_line.Location = new System.Drawing.Point(183, 359);
            this.comEdit_line.Name = "comEdit_line";
            this.comEdit_line.Size = new System.Drawing.Size(203, 28);
            this.comEdit_line.TabIndex = 3;
            // 
            // dataGridView4
            // 
            this.dataGridView4.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridView4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView4.Location = new System.Drawing.Point(33, 34);
            this.dataGridView4.Name = "dataGridView4";
            this.dataGridView4.Size = new System.Drawing.Size(353, 149);
            this.dataGridView4.TabIndex = 1;
            // 
            // groupBox5
            // 
            this.groupBox5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(227)))), ((int)(((byte)(238)))));
            this.groupBox5.Controls.Add(this.label119);
            this.groupBox5.Controls.Add(this.comEdit_floor);
            this.groupBox5.Controls.Add(this.btEdit_delLocate);
            this.groupBox5.Controls.Add(this.btEdit_add);
            this.groupBox5.Controls.Add(this.btEdit_cancelLocal);
            this.groupBox5.Controls.Add(this.btEdit_addLocal);
            this.groupBox5.Controls.Add(this.label6);
            this.groupBox5.Controls.Add(this.label7);
            this.groupBox5.Controls.Add(this.label8);
            this.groupBox5.Controls.Add(this.label9);
            this.groupBox5.Controls.Add(this.comEdit_slot);
            this.groupBox5.Controls.Add(this.comEdit_side);
            this.groupBox5.Controls.Add(this.comEdit_zone);
            this.groupBox5.Controls.Add(this.comEdit_shelf);
            this.groupBox5.Controls.Add(this.dataGridView5);
            this.groupBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox5.Location = new System.Drawing.Point(27, 179);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(450, 604);
            this.groupBox5.TabIndex = 0;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Location";
            // 
            // label119
            // 
            this.label119.AutoSize = true;
            this.label119.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label119.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(79)))), ((int)(((byte)(91)))), ((int)(((byte)(102)))));
            this.label119.Location = new System.Drawing.Point(57, 363);
            this.label119.Name = "label119";
            this.label119.Size = new System.Drawing.Size(67, 25);
            this.label119.TabIndex = 146;
            this.label119.Text = "Floor :";
            // 
            // comEdit_floor
            // 
            this.comEdit_floor.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comEdit_floor.FormattingEnabled = true;
            this.comEdit_floor.Location = new System.Drawing.Point(172, 363);
            this.comEdit_floor.Name = "comEdit_floor";
            this.comEdit_floor.Size = new System.Drawing.Size(203, 28);
            this.comEdit_floor.TabIndex = 145;
            this.comEdit_floor.SelectedIndexChanged += new System.EventHandler(this.ComEdit_floor_SelectedIndexChanged);
            // 
            // btEdit_delLocate
            // 
            this.btEdit_delLocate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(67)))), ((int)(((byte)(53)))));
            this.btEdit_delLocate.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(57)))), ((int)(((byte)(108)))));
            this.btEdit_delLocate.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(102)))), ((int)(((byte)(185)))));
            this.btEdit_delLocate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btEdit_delLocate.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.btEdit_delLocate.ForeColor = System.Drawing.Color.White;
            this.btEdit_delLocate.Location = new System.Drawing.Point(240, 523);
            this.btEdit_delLocate.Name = "btEdit_delLocate";
            this.btEdit_delLocate.Size = new System.Drawing.Size(135, 66);
            this.btEdit_delLocate.TabIndex = 144;
            this.btEdit_delLocate.Text = "DELETE";
            this.btEdit_delLocate.UseVisualStyleBackColor = false;
            this.btEdit_delLocate.Click += new System.EventHandler(this.BtEdit_delLocate_Click);
            // 
            // btEdit_add
            // 
            this.btEdit_add.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(91)))), ((int)(((byte)(150)))));
            this.btEdit_add.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(57)))), ((int)(((byte)(108)))));
            this.btEdit_add.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(102)))), ((int)(((byte)(185)))));
            this.btEdit_add.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btEdit_add.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.btEdit_add.ForeColor = System.Drawing.Color.White;
            this.btEdit_add.Location = new System.Drawing.Point(62, 523);
            this.btEdit_add.Name = "btEdit_add";
            this.btEdit_add.Size = new System.Drawing.Size(135, 66);
            this.btEdit_add.TabIndex = 143;
            this.btEdit_add.Text = "ADD";
            this.btEdit_add.UseVisualStyleBackColor = false;
            this.btEdit_add.Click += new System.EventHandler(this.BtEdit_add_Click);
            // 
            // btEdit_cancelLocal
            // 
            this.btEdit_cancelLocal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(67)))), ((int)(((byte)(53)))));
            this.btEdit_cancelLocal.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btEdit_cancelLocal.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btEdit_cancelLocal.ForeColor = System.Drawing.Color.White;
            this.btEdit_cancelLocal.Location = new System.Drawing.Point(240, 523);
            this.btEdit_cancelLocal.Name = "btEdit_cancelLocal";
            this.btEdit_cancelLocal.Size = new System.Drawing.Size(135, 66);
            this.btEdit_cancelLocal.TabIndex = 142;
            this.btEdit_cancelLocal.Text = "CANCEL";
            this.btEdit_cancelLocal.UseVisualStyleBackColor = false;
            this.btEdit_cancelLocal.Click += new System.EventHandler(this.BtEdit_cancelLocal_Click);
            // 
            // btEdit_addLocal
            // 
            this.btEdit_addLocal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(91)))), ((int)(((byte)(150)))));
            this.btEdit_addLocal.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btEdit_addLocal.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btEdit_addLocal.ForeColor = System.Drawing.Color.White;
            this.btEdit_addLocal.Location = new System.Drawing.Point(62, 523);
            this.btEdit_addLocal.Name = "btEdit_addLocal";
            this.btEdit_addLocal.Size = new System.Drawing.Size(135, 66);
            this.btEdit_addLocal.TabIndex = 136;
            this.btEdit_addLocal.Text = "ADD";
            this.btEdit_addLocal.UseVisualStyleBackColor = false;
            this.btEdit_addLocal.Click += new System.EventHandler(this.BtEdit_addLocal_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(79)))), ((int)(((byte)(91)))), ((int)(((byte)(102)))));
            this.label6.Location = new System.Drawing.Point(57, 475);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(62, 25);
            this.label6.TabIndex = 141;
            this.label6.Text = "Slot : ";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(79)))), ((int)(((byte)(91)))), ((int)(((byte)(102)))));
            this.label7.Location = new System.Drawing.Point(57, 423);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(63, 25);
            this.label7.TabIndex = 140;
            this.label7.Text = "Side :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(79)))), ((int)(((byte)(91)))), ((int)(((byte)(102)))));
            this.label8.Location = new System.Drawing.Point(57, 310);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(73, 25);
            this.label8.TabIndex = 139;
            this.label8.Text = "Zone : ";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(79)))), ((int)(((byte)(91)))), ((int)(((byte)(102)))));
            this.label9.Location = new System.Drawing.Point(57, 254);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(68, 25);
            this.label9.TabIndex = 138;
            this.label9.Text = "Shelf :";
            // 
            // comEdit_slot
            // 
            this.comEdit_slot.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comEdit_slot.FormattingEnabled = true;
            this.comEdit_slot.Location = new System.Drawing.Point(172, 476);
            this.comEdit_slot.Name = "comEdit_slot";
            this.comEdit_slot.Size = new System.Drawing.Size(203, 28);
            this.comEdit_slot.TabIndex = 5;
            // 
            // comEdit_side
            // 
            this.comEdit_side.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comEdit_side.FormattingEnabled = true;
            this.comEdit_side.Location = new System.Drawing.Point(172, 420);
            this.comEdit_side.Name = "comEdit_side";
            this.comEdit_side.Size = new System.Drawing.Size(203, 28);
            this.comEdit_side.TabIndex = 4;
            this.comEdit_side.SelectedIndexChanged += new System.EventHandler(this.ComEdit_shelf_SelectedIndexChanged);
            // 
            // comEdit_zone
            // 
            this.comEdit_zone.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comEdit_zone.FormattingEnabled = true;
            this.comEdit_zone.Location = new System.Drawing.Point(172, 307);
            this.comEdit_zone.Name = "comEdit_zone";
            this.comEdit_zone.Size = new System.Drawing.Size(203, 28);
            this.comEdit_zone.TabIndex = 3;
            this.comEdit_zone.SelectedIndexChanged += new System.EventHandler(this.ComEdit_zone_SelectedIndexChanged);
            // 
            // comEdit_shelf
            // 
            this.comEdit_shelf.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comEdit_shelf.FormattingEnabled = true;
            this.comEdit_shelf.Location = new System.Drawing.Point(172, 251);
            this.comEdit_shelf.Name = "comEdit_shelf";
            this.comEdit_shelf.Size = new System.Drawing.Size(203, 28);
            this.comEdit_shelf.TabIndex = 2;
            this.comEdit_shelf.SelectedIndexChanged += new System.EventHandler(this.ComEdit_area_SelectedIndexChanged);
            // 
            // dataGridView5
            // 
            this.dataGridView5.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridView5.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView5.Location = new System.Drawing.Point(3, 34);
            this.dataGridView5.Name = "dataGridView5";
            this.dataGridView5.Size = new System.Drawing.Size(434, 149);
            this.dataGridView5.TabIndex = 0;
            // 
            // panel14
            // 
            this.panel14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(57)))), ((int)(((byte)(108)))));
            this.panel14.Controls.Add(this.btEdit_cancel);
            this.panel14.Controls.Add(this.btEdit_save);
            this.panel14.Controls.Add(this.groupBox6);
            this.panel14.Location = new System.Drawing.Point(991, 78);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(662, 807);
            this.panel14.TabIndex = 23;
            // 
            // btEdit_cancel
            // 
            this.btEdit_cancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(67)))), ((int)(((byte)(53)))));
            this.btEdit_cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btEdit_cancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btEdit_cancel.ForeColor = System.Drawing.Color.White;
            this.btEdit_cancel.Location = new System.Drawing.Point(366, 702);
            this.btEdit_cancel.Name = "btEdit_cancel";
            this.btEdit_cancel.Size = new System.Drawing.Size(214, 66);
            this.btEdit_cancel.TabIndex = 145;
            this.btEdit_cancel.Text = "CANCEL";
            this.btEdit_cancel.UseVisualStyleBackColor = false;
            this.btEdit_cancel.Click += new System.EventHandler(this.BtEdit_cancel_Click);
            // 
            // btEdit_save
            // 
            this.btEdit_save.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(91)))), ((int)(((byte)(150)))));
            this.btEdit_save.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btEdit_save.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btEdit_save.ForeColor = System.Drawing.Color.White;
            this.btEdit_save.Location = new System.Drawing.Point(88, 702);
            this.btEdit_save.Name = "btEdit_save";
            this.btEdit_save.Size = new System.Drawing.Size(214, 66);
            this.btEdit_save.TabIndex = 144;
            this.btEdit_save.Text = "SAVE";
            this.btEdit_save.UseVisualStyleBackColor = false;
            this.btEdit_save.Click += new System.EventHandler(this.BtEdit_save_Click);
            // 
            // groupBox6
            // 
            this.groupBox6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(227)))), ((int)(((byte)(228)))));
            this.groupBox6.Controls.Add(this.checkEdit_rank);
            this.groupBox6.Controls.Add(this.checkEdit_FIFO);
            this.groupBox6.Controls.Add(this.label10);
            this.groupBox6.Controls.Add(this.label11);
            this.groupBox6.Controls.Add(this.label12);
            this.groupBox6.Controls.Add(this.label13);
            this.groupBox6.Controls.Add(this.label14);
            this.groupBox6.Controls.Add(this.label15);
            this.groupBox6.Controls.Add(this.label16);
            this.groupBox6.Controls.Add(this.label17);
            this.groupBox6.Controls.Add(this.label18);
            this.groupBox6.Controls.Add(this.comEdit_parttype);
            this.groupBox6.Controls.Add(this.comEdit_partpackage);
            this.groupBox6.Controls.Add(this.numEdit_min);
            this.groupBox6.Controls.Add(this.tEdit_model);
            this.groupBox6.Controls.Add(this.numEdit_max);
            this.groupBox6.Controls.Add(this.numEdit_qty);
            this.groupBox6.Controls.Add(this.tEdit_supplier);
            this.groupBox6.Controls.Add(this.tEdit_partname);
            this.groupBox6.Controls.Add(this.tEdit_partno);
            this.groupBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox6.ForeColor = System.Drawing.Color.Black;
            this.groupBox6.Location = new System.Drawing.Point(138, 37);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(414, 604);
            this.groupBox6.TabIndex = 143;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Add Part";
            // 
            // checkEdit_rank
            // 
            this.checkEdit_rank.AutoSize = true;
            this.checkEdit_rank.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkEdit_rank.Location = new System.Drawing.Point(36, 546);
            this.checkEdit_rank.Name = "checkEdit_rank";
            this.checkEdit_rank.Size = new System.Drawing.Size(153, 29);
            this.checkEdit_rank.TabIndex = 151;
            this.checkEdit_rank.Text = "Rank Enabled";
            this.checkEdit_rank.UseVisualStyleBackColor = true;
            // 
            // checkEdit_FIFO
            // 
            this.checkEdit_FIFO.AutoSize = true;
            this.checkEdit_FIFO.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkEdit_FIFO.Location = new System.Drawing.Point(36, 501);
            this.checkEdit_FIFO.Name = "checkEdit_FIFO";
            this.checkEdit_FIFO.Size = new System.Drawing.Size(153, 29);
            this.checkEdit_FIFO.TabIndex = 152;
            this.checkEdit_FIFO.Text = "FIFO Enabled";
            this.checkEdit_FIFO.UseVisualStyleBackColor = true;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(79)))), ((int)(((byte)(91)))), ((int)(((byte)(102)))));
            this.label10.Location = new System.Drawing.Point(31, 451);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(119, 25);
            this.label10.TabIndex = 150;
            this.label10.Text = "ประเภท Part :";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(79)))), ((int)(((byte)(91)))), ((int)(((byte)(102)))));
            this.label11.Location = new System.Drawing.Point(31, 397);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(118, 25);
            this.label11.TabIndex = 149;
            this.label11.Text = "ลักษณะ Part :";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(79)))), ((int)(((byte)(91)))), ((int)(((byte)(102)))));
            this.label12.Location = new System.Drawing.Point(31, 342);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(115, 25);
            this.label12.TabIndex = 148;
            this.label12.Text = "Min Stock : ";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(79)))), ((int)(((byte)(91)))), ((int)(((byte)(102)))));
            this.label13.Location = new System.Drawing.Point(31, 289);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(121, 25);
            this.label13.TabIndex = 147;
            this.label13.Text = "Max Stock : ";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(79)))), ((int)(((byte)(91)))), ((int)(((byte)(102)))));
            this.label14.Location = new System.Drawing.Point(31, 235);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(82, 25);
            this.label14.TabIndex = 146;
            this.label14.Text = "Model : ";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(79)))), ((int)(((byte)(91)))), ((int)(((byte)(102)))));
            this.label15.Location = new System.Drawing.Point(31, 183);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(104, 25);
            this.label15.TabIndex = 145;
            this.label15.Text = "Q\'ty pcs  : ";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(79)))), ((int)(((byte)(91)))), ((int)(((byte)(102)))));
            this.label16.Location = new System.Drawing.Point(31, 130);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(98, 25);
            this.label16.TabIndex = 144;
            this.label16.Text = "Supllier  : ";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(79)))), ((int)(((byte)(91)))), ((int)(((byte)(102)))));
            this.label17.Location = new System.Drawing.Point(31, 82);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(120, 25);
            this.label17.TabIndex = 143;
            this.label17.Text = "Part Name : ";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label18.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(79)))), ((int)(((byte)(91)))), ((int)(((byte)(102)))));
            this.label18.Location = new System.Drawing.Point(31, 38);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(93, 25);
            this.label18.TabIndex = 142;
            this.label18.Text = "Part No : ";
            // 
            // comEdit_parttype
            // 
            this.comEdit_parttype.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comEdit_parttype.FormattingEnabled = true;
            this.comEdit_parttype.Location = new System.Drawing.Point(166, 452);
            this.comEdit_parttype.Name = "comEdit_parttype";
            this.comEdit_parttype.Size = new System.Drawing.Size(203, 28);
            this.comEdit_parttype.TabIndex = 9;
            // 
            // comEdit_partpackage
            // 
            this.comEdit_partpackage.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comEdit_partpackage.FormattingEnabled = true;
            this.comEdit_partpackage.Location = new System.Drawing.Point(166, 398);
            this.comEdit_partpackage.Name = "comEdit_partpackage";
            this.comEdit_partpackage.Size = new System.Drawing.Size(203, 28);
            this.comEdit_partpackage.TabIndex = 8;
            // 
            // numEdit_min
            // 
            this.numEdit_min.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numEdit_min.Location = new System.Drawing.Point(166, 344);
            this.numEdit_min.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.numEdit_min.Name = "numEdit_min";
            this.numEdit_min.Size = new System.Drawing.Size(203, 26);
            this.numEdit_min.TabIndex = 7;
            // 
            // tEdit_model
            // 
            this.tEdit_model.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tEdit_model.Location = new System.Drawing.Point(166, 236);
            this.tEdit_model.Name = "tEdit_model";
            this.tEdit_model.Size = new System.Drawing.Size(203, 26);
            this.tEdit_model.TabIndex = 5;
            // 
            // numEdit_max
            // 
            this.numEdit_max.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numEdit_max.Location = new System.Drawing.Point(166, 291);
            this.numEdit_max.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.numEdit_max.Name = "numEdit_max";
            this.numEdit_max.Size = new System.Drawing.Size(203, 26);
            this.numEdit_max.TabIndex = 4;
            // 
            // numEdit_qty
            // 
            this.numEdit_qty.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numEdit_qty.Location = new System.Drawing.Point(166, 182);
            this.numEdit_qty.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.numEdit_qty.Name = "numEdit_qty";
            this.numEdit_qty.Size = new System.Drawing.Size(203, 26);
            this.numEdit_qty.TabIndex = 3;
            // 
            // tEdit_supplier
            // 
            this.tEdit_supplier.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tEdit_supplier.Location = new System.Drawing.Point(166, 131);
            this.tEdit_supplier.Name = "tEdit_supplier";
            this.tEdit_supplier.Size = new System.Drawing.Size(203, 26);
            this.tEdit_supplier.TabIndex = 2;
            // 
            // tEdit_partname
            // 
            this.tEdit_partname.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tEdit_partname.Location = new System.Drawing.Point(166, 81);
            this.tEdit_partname.Name = "tEdit_partname";
            this.tEdit_partname.Size = new System.Drawing.Size(203, 26);
            this.tEdit_partname.TabIndex = 1;
            // 
            // tEdit_partno
            // 
            this.tEdit_partno.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tEdit_partno.Location = new System.Drawing.Point(166, 37);
            this.tEdit_partno.Name = "tEdit_partno";
            this.tEdit_partno.Size = new System.Drawing.Size(203, 26);
            this.tEdit_partno.TabIndex = 0;
            // 
            // panel15
            // 
            this.panel15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(57)))), ((int)(((byte)(108)))));
            this.panel15.Controls.Add(this.label19);
            this.panel15.Location = new System.Drawing.Point(0, 0);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(1653, 79);
            this.panel15.TabIndex = 0;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label19.ForeColor = System.Drawing.Color.White;
            this.label19.Location = new System.Drawing.Point(767, 20);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(118, 39);
            this.label19.TabIndex = 22;
            this.label19.Text = "MENU";
            // 
            // panel12
            // 
            this.panel12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(57)))), ((int)(((byte)(108)))));
            this.panel12.Controls.Add(this.label4);
            this.panel12.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel12.Location = new System.Drawing.Point(0, 0);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(1670, 45);
            this.panel12.TabIndex = 9;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(759, 8);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(104, 29);
            this.label4.TabIndex = 34;
            this.label4.Text = "Edit Part";
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.panel4.Controls.Add(this.btmain_back);
            this.panel4.Controls.Add(this.btmain_logout);
            this.panel4.Location = new System.Drawing.Point(0, 488);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(242, 589);
            this.panel4.TabIndex = 0;
            // 
            // btmain_back
            // 
            this.btmain_back.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(91)))), ((int)(((byte)(150)))));
            this.btmain_back.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(57)))), ((int)(((byte)(108)))));
            this.btmain_back.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(102)))), ((int)(((byte)(185)))));
            this.btmain_back.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btmain_back.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.btmain_back.ForeColor = System.Drawing.Color.White;
            this.btmain_back.Location = new System.Drawing.Point(0, 520);
            this.btmain_back.Name = "btmain_back";
            this.btmain_back.Size = new System.Drawing.Size(242, 69);
            this.btmain_back.TabIndex = 126;
            this.btmain_back.Text = "BACK";
            this.btmain_back.UseVisualStyleBackColor = false;
            this.btmain_back.Click += new System.EventHandler(this.Btmain_back_Click);
            // 
            // btmain_logout
            // 
            this.btmain_logout.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(67)))), ((int)(((byte)(53)))));
            this.btmain_logout.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(57)))), ((int)(((byte)(108)))));
            this.btmain_logout.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(102)))), ((int)(((byte)(185)))));
            this.btmain_logout.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btmain_logout.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.btmain_logout.ForeColor = System.Drawing.Color.White;
            this.btmain_logout.Location = new System.Drawing.Point(0, 452);
            this.btmain_logout.Name = "btmain_logout";
            this.btmain_logout.Size = new System.Drawing.Size(242, 69);
            this.btmain_logout.TabIndex = 125;
            this.btmain_logout.Text = "LOG OUT";
            this.btmain_logout.UseVisualStyleBackColor = false;
            this.btmain_logout.Click += new System.EventHandler(this.Btmain_logout_Click);
            // 
            // timer_addstock
            // 
            this.timer_addstock.Interval = 2000;
            this.timer_addstock.Tick += new System.EventHandler(this.Timer_addstock_Tick);
            // 
            // timer_pickupstock
            // 
            this.timer_pickupstock.Interval = 2000;
            this.timer_pickupstock.Tick += new System.EventHandler(this.Timer_pickupstock_Tick);
            // 
            // timer_logout
            // 
            this.timer_logout.Interval = 20000;
            this.timer_logout.Tick += new System.EventHandler(this.Timer_logout_Tick);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(1920, 1080);
            this.ControlBox = false;
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.bt_userSetup);
            this.Controls.Add(this.bt_manage);
            this.Controls.Add(this.bt_addStock);
            this.Controls.Add(this.bt_Pickup);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.buttonClose);
            this.Controls.Add(this.buttonMinimize);
            this.Controls.Add(this.labelLineUserNameTopPanel);
            this.Controls.Add(this.label66);
            this.Controls.Add(this.labelLevel);
            this.Controls.Add(this.label31);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.KeyPreview = true;
            this.Name = "Main";
            this.ShowIcon = false;
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Main_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Main_KeyDown);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabMain.ResumeLayout(false);
            this.tabPickup.ResumeLayout(false);
            this.pap_lotidwrong.ResumeLayout(false);
            this.pap_lotidwrong.PerformLayout();
            this.panel25.ResumeLayout(false);
            this.panel26.ResumeLayout(false);
            this.panel26.PerformLayout();
            this.panel24.ResumeLayout(false);
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.panel23.ResumeLayout(false);
            this.panel23.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView7)).EndInit();
            this.panel20.ResumeLayout(false);
            this.panel20.PerformLayout();
            this.tabAddStock.ResumeLayout(false);
            this.panel22.ResumeLayout(false);
            this.panel22.PerformLayout();
            this.pas_duplicate.ResumeLayout(false);
            this.pas_duplicate.PerformLayout();
            this.panel52.ResumeLayout(false);
            this.panel52.PerformLayout();
            this.panel21.ResumeLayout(false);
            this.panel21.PerformLayout();
            this.panel19.ResumeLayout(false);
            this.panel19.PerformLayout();
            this.panel18.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.panel37.ResumeLayout(false);
            this.panel37.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView6)).EndInit();
            this.panel16.ResumeLayout(false);
            this.panel16.PerformLayout();
            this.tabManage.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.tabPart.ResumeLayout(false);
            this.panel42.ResumeLayout(false);
            this.panel42.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.groupBox15.ResumeLayout(false);
            this.groupBox15.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView16)).EndInit();
            this.GroupAddPartControl.ResumeLayout(false);
            this.GroupAddPartControl.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.tabEditStock.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView9)).EndInit();
            this.panel30.ResumeLayout(false);
            this.panel30.PerformLayout();
            this.panel28.ResumeLayout(false);
            this.panel28.PerformLayout();
            this.panel29.ResumeLayout(false);
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            this.tabLogs.ResumeLayout(false);
            this.panel48.ResumeLayout(false);
            this.panel48.PerformLayout();
            this.panel47.ResumeLayout(false);
            this.groupBox19.ResumeLayout(false);
            this.groupBox19.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.panel27.ResumeLayout(false);
            this.panel27.PerformLayout();
            this.panel34.ResumeLayout(false);
            this.panel34.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView13)).EndInit();
            this.panel49.ResumeLayout(false);
            this.panel49.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView8)).EndInit();
            this.panel33.ResumeLayout(false);
            this.panel33.PerformLayout();
            this.tabMCLine.ResumeLayout(false);
            this.panel32.ResumeLayout(false);
            this.panel32.PerformLayout();
            this.panel51.ResumeLayout(false);
            this.panel51.PerformLayout();
            this.panel43.ResumeLayout(false);
            this.groupBox18.ResumeLayout(false);
            this.groupBox18.PerformLayout();
            this.panel45.ResumeLayout(false);
            this.panel46.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView11)).EndInit();
            this.panel50.ResumeLayout(false);
            this.panel50.PerformLayout();
            this.panel44.ResumeLayout(false);
            this.panel44.PerformLayout();
            this.tabAddPart.ResumeLayout(false);
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.panel10.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numAdd_minstock)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numAdd_maxstock)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numAdd_qty)).EndInit();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.panel17.ResumeLayout(false);
            this.panel17.PerformLayout();
            this.tabEditPart.ResumeLayout(false);
            this.panel31.ResumeLayout(false);
            this.panel31.PerformLayout();
            this.panel13.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView4)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView5)).EndInit();
            this.panel14.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numEdit_min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numEdit_max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numEdit_qty)).EndInit();
            this.panel15.ResumeLayout(false);
            this.panel15.PerformLayout();
            this.panel12.ResumeLayout(false);
            this.panel12.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label labelLineUserNameTopPanel;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Label labelLevel;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Button buttonClose;
        private System.Windows.Forms.Button buttonMinimize;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button bt_Pickup;
        private System.Windows.Forms.Button bt_addStock;
        private System.Windows.Forms.Button bt_manage;
        private System.Windows.Forms.Button bt_userSetup;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabMain;
        private System.Windows.Forms.Button bt_LPickupPart;
        private System.Windows.Forms.TabPage tabPickup;
        private System.Windows.Forms.TabPage tabAddStock;
        private System.Windows.Forms.TabPage tabManage;
        private System.Windows.Forms.TabPage tabPart;
        private System.Windows.Forms.TabPage tabEditStock;
        private System.Windows.Forms.TabPage tabLogs;
        private System.Windows.Forms.TabPage tabMCLine;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button btmain_back;
        private System.Windows.Forms.Button btmain_logout;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button bt_LAddStock;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel42;
        private System.Windows.Forms.Label label121;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.GroupBox GroupAddPartControl;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.TextBox tpd_search;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label120;
        private System.Windows.Forms.Button button29;
        private System.Windows.Forms.Label label131;
        private System.Windows.Forms.Label label178;
        private System.Windows.Forms.TextBox tpd_partname;
        private System.Windows.Forms.TextBox tpd_partno;
        private System.Windows.Forms.DataGridViewTextBoxColumn pd_partno;
        private System.Windows.Forms.DataGridViewTextBoxColumn pd_partname;
        private System.Windows.Forms.DataGridViewTextBoxColumn pd_model;
        private System.Windows.Forms.DataGridViewTextBoxColumn pd_fifo;
        private System.Windows.Forms.DataGridViewTextBoxColumn pd_rank;
        private System.Windows.Forms.DataGridViewTextBoxColumn pd_maxstock;
        private System.Windows.Forms.DataGridViewTextBoxColumn pd_minstock;
        private System.Windows.Forms.DataGridViewTextBoxColumn pd_qty;
        private System.Windows.Forms.DataGridViewTextBoxColumn pd_createuser;
        private System.Windows.Forms.DataGridViewTextBoxColumn pd_createdate;
        private System.Windows.Forms.DataGridViewTextBoxColumn pd_updateuser;
        private System.Windows.Forms.DataGridViewTextBoxColumn pd_updatedate;
        private System.Windows.Forms.DataGridViewTextBoxColumn pd_supplier;
        private System.Windows.Forms.DataGridViewTextBoxColumn pd_packagepart;
        private System.Windows.Forms.DataGridViewTextBoxColumn pd_parttype;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label label144;
        private System.Windows.Forms.Label label145;
        private System.Windows.Forms.Button bt_lmcline;
        private System.Windows.Forms.Button bt_Lusersetup;
        private System.Windows.Forms.Button bt_LLogs;
        private System.Windows.Forms.Button bt_leditstock;
        private System.Windows.Forms.Button bt_partdata;
        private System.Windows.Forms.TabPage tabAddPart;
        private System.Windows.Forms.TabPage tabEditPart;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Label label105;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btAdd_delLocal;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label174;
        private System.Windows.Forms.ComboBox comAdd_Line;
        private System.Windows.Forms.DataGridView dataGridView3;
        private System.Windows.Forms.Button btAdd_del;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label175;
        private System.Windows.Forms.Label label176;
        private System.Windows.Forms.Label label168;
        private System.Windows.Forms.Label label172;
        private System.Windows.Forms.ComboBox comAdd_Slot;
        private System.Windows.Forms.ComboBox comAdd_side;
        private System.Windows.Forms.ComboBox comAdd_Zone;
        private System.Windows.Forms.ComboBox comAdd_shelf;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox textAdd_model;
        private System.Windows.Forms.NumericUpDown numAdd_maxstock;
        private System.Windows.Forms.NumericUpDown numAdd_qty;
        private System.Windows.Forms.TextBox textAdd_supplier;
        private System.Windows.Forms.TextBox textAdd_partname;
        private System.Windows.Forms.TextBox textAdd_partno;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btAdd_cancel;
        private System.Windows.Forms.Button btAdd_save;
        private System.Windows.Forms.CheckBox checkAdd_Rank;
        private System.Windows.Forms.CheckBox checkAdd_FIFO;
        private System.Windows.Forms.Label label126;
        private System.Windows.Forms.Label label125;
        private System.Windows.Forms.Label label101;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.Label label94;
        private System.Windows.Forms.Label label95;
        private System.Windows.Forms.Label label98;
        private System.Windows.Forms.Label label99;
        private System.Windows.Forms.Label label100;
        private System.Windows.Forms.ComboBox comAdd_parttype;
        private System.Windows.Forms.ComboBox comAdd_partpackage;
        private System.Windows.Forms.NumericUpDown numAdd_minstock;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button btEdit_delLine;
        private System.Windows.Forms.Button btEdit_addLine;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox comEdit_line;
        private System.Windows.Forms.DataGridView dataGridView4;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button btEdit_cancelLocal;
        private System.Windows.Forms.Button btEdit_addLocal;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox comEdit_slot;
        private System.Windows.Forms.ComboBox comEdit_side;
        private System.Windows.Forms.ComboBox comEdit_zone;
        private System.Windows.Forms.ComboBox comEdit_shelf;
        private System.Windows.Forms.DataGridView dataGridView5;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Button btEdit_cancel;
        private System.Windows.Forms.Button btEdit_save;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.CheckBox checkEdit_rank;
        private System.Windows.Forms.CheckBox checkEdit_FIFO;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ComboBox comEdit_parttype;
        private System.Windows.Forms.ComboBox comEdit_partpackage;
        private System.Windows.Forms.NumericUpDown numEdit_min;
        private System.Windows.Forms.TextBox tEdit_model;
        private System.Windows.Forms.NumericUpDown numEdit_max;
        private System.Windows.Forms.NumericUpDown numEdit_qty;
        private System.Windows.Forms.TextBox tEdit_supplier;
        private System.Windows.Forms.TextBox tEdit_partname;
        private System.Windows.Forms.TextBox tEdit_partno;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btEdit_cancelLine;
        private System.Windows.Forms.Button btEdit_addLineNo;
        private System.Windows.Forms.Button btEdit_delLocate;
        private System.Windows.Forms.Button btEdit_add;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Button btas_cancel;
        private System.Windows.Forms.Button btas_ok;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox tas_partname;
        private System.Windows.Forms.TextBox tas_scan;
        private System.Windows.Forms.Panel panel52;
        private System.Windows.Forms.TextBox tas_alertadd;
        private System.Windows.Forms.Label label171;
        private System.Windows.Forms.Panel panel37;
        private System.Windows.Forms.TextBox tas_alertnotfound;
        private System.Windows.Forms.Label label136;
        private System.Windows.Forms.Label label179;
        private System.Windows.Forms.Label label180;
        private System.Windows.Forms.DataGridView dataGridView6;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.Button buttonLM_Refresh;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Panel panel19;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.TextBox tas_spare;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox tas_stock;
        private System.Windows.Forms.TextBox tas_timeIn;
        private System.Windows.Forms.TextBox tas_userIn;
        private System.Windows.Forms.TextBox tas_statusDB;
        private System.Windows.Forms.TextBox tas_statusM;
        private System.Windows.Forms.TextBox tas_slot;
        private System.Windows.Forms.TextBox tas_zone;
        private System.Windows.Forms.TextBox tas_shelf;
        private System.Windows.Forms.TextBox tas_area;
        private System.Windows.Forms.TextBox tas_dcode;
        private System.Windows.Forms.TextBox tas_mpn;
        private System.Windows.Forms.TextBox tas_maker;
        private System.Windows.Forms.TextBox tas_rank;
        private System.Windows.Forms.TextBox tas_qty;
        private System.Windows.Forms.TextBox tas_boxID;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Timer timer_addstock;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.TextBox tas_ccode;
        private System.Windows.Forms.Panel pas_duplicate;
        private System.Windows.Forms.TextBox tas_alertboxid;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Panel panel21;
        private System.Windows.Forms.TextBox tas_alert_addStockcom;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Panel panel22;
        private System.Windows.Forms.TextBox tas_alertoverstock;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.TextBox tas_maxstock;
        private System.Windows.Forms.Label labellll;
        private System.Windows.Forms.DataGridView dataGridView7;
        private System.Windows.Forms.Panel panel20;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Panel panel23;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Panel panel24;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.TextBox tp_maxstock;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.TextBox tp_ccode;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.TextBox tp_minstock;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.TextBox tp_stock;
        private System.Windows.Forms.TextBox tp_ordertime;
        private System.Windows.Forms.TextBox tp_userout;
        private System.Windows.Forms.TextBox tp_statusDB;
        private System.Windows.Forms.TextBox tp_statusM;
        private System.Windows.Forms.TextBox tp_slot;
        private System.Windows.Forms.TextBox tp_zone;
        private System.Windows.Forms.TextBox tp_shelf;
        private System.Windows.Forms.TextBox tp_area;
        private System.Windows.Forms.TextBox tp_dcode;
        private System.Windows.Forms.TextBox tp_mpn;
        private System.Windows.Forms.TextBox tp_maker;
        private System.Windows.Forms.TextBox tp_rank;
        private System.Windows.Forms.TextBox tp_qty;
        private System.Windows.Forms.TextBox tp_boxid;
        private System.Windows.Forms.Button bt_pickup_cancel;
        private System.Windows.Forms.Button btPickup_OK;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.TextBox tp_partname;
        private System.Windows.Forms.TextBox tp_scan;
        private System.Windows.Forms.Panel panel25;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.Timer timer_pickupstock;
        private System.Windows.Forms.Panel panel26;
        private System.Windows.Forms.TextBox tp_alertwrong;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.Panel pap_lotidwrong;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.Label label84;
        private System.Windows.Forms.Panel panel48;
        private System.Windows.Forms.Label label146;
        private System.Windows.Forms.Panel panel47;
        private System.Windows.Forms.GroupBox groupBox19;
        private System.Windows.Forms.Button btlog_search;
        private System.Windows.Forms.DateTimePicker datelog_to;
        private System.Windows.Forms.DateTimePicker datelog_form;
        private System.Windows.Forms.Label label152;
        private System.Windows.Forms.Label label151;
        private System.Windows.Forms.Label label150;
        private System.Windows.Forms.Label label149;
        private System.Windows.Forms.Label label148;
        private System.Windows.Forms.TextBox tlog_qty;
        private System.Windows.Forms.TextBox tlog_partno;
        private System.Windows.Forms.Label label147;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.TextBox tlog_eventid;
        private System.Windows.Forms.Panel panel27;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.Panel panel34;
        private System.Windows.Forms.Panel panel49;
        private System.Windows.Forms.Label label153;
        private System.Windows.Forms.DataGridView dataGridView8;
        private System.Windows.Forms.Panel panel33;
        private System.Windows.Forms.Button button23;
        private System.Windows.Forms.Label label90;
        private System.Windows.Forms.Button btlog_back;
        private System.Windows.Forms.Button btlog_next;
        private System.Windows.Forms.TextBox tlog_page;
        private System.Windows.Forms.ComboBox clog_event;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.TextBox tlog_boxid;
        private System.Windows.Forms.CheckBox checklog_date;
        private System.Windows.Forms.Panel panel28;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.Panel panel29;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.Label label86;
        private System.Windows.Forms.TextBox tes_maxstock;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.TextBox tes_minstock;
        private System.Windows.Forms.Label label89;
        private System.Windows.Forms.Label label91;
        private System.Windows.Forms.Label label92;
        private System.Windows.Forms.Label label93;
        private System.Windows.Forms.Label label97;
        private System.Windows.Forms.Label label102;
        private System.Windows.Forms.Label label103;
        private System.Windows.Forms.Label label104;
        private System.Windows.Forms.Label label107;
        private System.Windows.Forms.Label label108;
        private System.Windows.Forms.Label label109;
        private System.Windows.Forms.Label label110;
        private System.Windows.Forms.Label label111;
        private System.Windows.Forms.TextBox tes_stock;
        private System.Windows.Forms.TextBox tes_timein;
        private System.Windows.Forms.TextBox tes_userin;
        private System.Windows.Forms.TextBox tes_slot;
        private System.Windows.Forms.TextBox tes_zone;
        private System.Windows.Forms.TextBox tes_shelf;
        private System.Windows.Forms.TextBox tes_area;
        private System.Windows.Forms.TextBox tes_maker;
        private System.Windows.Forms.TextBox tes_rank;
        private System.Windows.Forms.TextBox tes_qty;
        private System.Windows.Forms.TextBox tes_boxid;
        private System.Windows.Forms.Button btEditstock_del;
        private System.Windows.Forms.Button btEditstock_OK;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.Label label112;
        private System.Windows.Forms.Label label113;
        private System.Windows.Forms.TextBox tes_partname;
        private System.Windows.Forms.TextBox tes_searchpart;
        private System.Windows.Forms.Panel panel30;
        private System.Windows.Forms.Label label114;
        private System.Windows.Forms.DataGridView dataGridView10;
        private System.Windows.Forms.DataGridView dataGridView9;
        private System.Windows.Forms.DataGridViewTextBoxColumn es_partno;
        private System.Windows.Forms.DataGridViewTextBoxColumn es_stockava;
        private System.Windows.Forms.ComboBox ces_stockstatus;
        private System.Windows.Forms.ComboBox ces_pickupline;
        private System.Windows.Forms.ComboBox ces_pickupuser;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.Label label96;
        private System.Windows.Forms.DateTimePicker des_pickuptime;
        private System.Windows.Forms.Panel panel51;
        private System.Windows.Forms.Label label115;
        private System.Windows.Forms.Panel panel43;
        private System.Windows.Forms.GroupBox groupBox18;
        private System.Windows.Forms.Label label140;
        private System.Windows.Forms.Panel panel45;
        private System.Windows.Forms.Button btmc_search;
        private System.Windows.Forms.Button btmc_del;
        private System.Windows.Forms.Button btmc_add;
        private System.Windows.Forms.Label label139;
        private System.Windows.Forms.ComboBox cmc_part;
        private System.Windows.Forms.Label label138;
        private System.Windows.Forms.ComboBox cmc_line;
        private System.Windows.Forms.Panel panel46;
        private System.Windows.Forms.DataGridView dataGridView11;
        private System.Windows.Forms.Panel panel50;
        private System.Windows.Forms.Label label141;
        private System.Windows.Forms.Panel panel44;
        private System.Windows.Forms.Label label106;
        private System.Windows.Forms.Button btmc_canceladd;
        private System.Windows.Forms.Button btmc_okadd;
        private System.Windows.Forms.ComboBox comAdd_floor;
        private System.Windows.Forms.Label label116;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn side;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private System.Windows.Forms.Label label117;
        private System.Windows.Forms.TextBox tas_side;
        private System.Windows.Forms.Label label118;
        private System.Windows.Forms.TextBox tp_side;
        private System.Windows.Forms.Label label119;
        private System.Windows.Forms.ComboBox comEdit_floor;
        private System.Windows.Forms.Label label122;
        private System.Windows.Forms.TextBox tes_side;
        private System.Windows.Forms.Timer timer_logout;
        private System.Windows.Forms.Panel panel31;
        private System.Windows.Forms.Label label127;
        private System.Windows.Forms.Label label128;
        private System.Windows.Forms.GroupBox groupBox15;
        private System.Windows.Forms.Button bt_pddelpart;
        private System.Windows.Forms.Label label124;
        private System.Windows.Forms.Label label123;
        private System.Windows.Forms.DataGridView dataGridView17;
        private System.Windows.Forms.DataGridView dataGridView16;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button bt_pdaddpart;
        private System.Windows.Forms.DataGridViewTextBoxColumn es_boxid;
        private System.Windows.Forms.DataGridViewTextBoxColumn es_rank;
        private System.Windows.Forms.DataGridViewTextBoxColumn es_stocklotid;
        private System.Windows.Forms.DataGridViewTextBoxColumn es_stocktime;
        private System.Windows.Forms.DataGridViewTextBoxColumn es_stock_transaction;
        private System.Windows.Forms.DataGridViewTextBoxColumn es_stock_user;
        private System.Windows.Forms.DataGridViewTextBoxColumn es_stock_status;
        private System.Windows.Forms.DataGridViewTextBoxColumn es_pickup_time;
        private System.Windows.Forms.DataGridViewTextBoxColumn es_pickup_transaction;
        private System.Windows.Forms.DataGridViewTextBoxColumn es_pickup_user;
        private System.Windows.Forms.DataGridViewTextBoxColumn es_pickup_line;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn18;
        private System.Windows.Forms.DataGridViewTextBoxColumn MCline;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn16;
        private System.Windows.Forms.DataGridViewTextBoxColumn fifo_enable;
        private System.Windows.Forms.DataGridViewTextBoxColumn rank_enable;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn17;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn20;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn22;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn21;
        private System.Windows.Forms.DataGridViewTextBoxColumn side_pickup;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn23;
        private System.Windows.Forms.DataGridViewTextBoxColumn minstock;
        private System.Windows.Forms.DataGridViewTextBoxColumn Qty;
        private System.Windows.Forms.DataGridViewTextBoxColumn qty_stock;
        private System.Windows.Forms.DataGridViewTextBoxColumn pu_boxID;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn26;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn25;
        private System.Windows.Forms.DataGridViewTextBoxColumn StatusM;
        private System.Windows.Forms.Button btEditstock_search;
        private System.Windows.Forms.Label label129;
        private System.Windows.Forms.TextBox tes_searchetc;
        private System.Windows.Forms.DataGridView dataGridView12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn24;
        private System.Windows.Forms.DataGridViewTextBoxColumn LineNo_MCLine;
        private System.Windows.Forms.Panel panel32;
        private System.Windows.Forms.Label label130;
        private System.Windows.Forms.Label label132;
        private System.Windows.Forms.DataGridView dataGridView13;
        private System.Windows.Forms.DataGridViewTextBoxColumn PartsNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn19;
        private System.Windows.Forms.DataGridViewTextBoxColumn User_Out;
        private System.Windows.Forms.DataGridViewTextBoxColumn Spare_;
        private System.Windows.Forms.DataGridViewTextBoxColumn User_In;
        private System.Windows.Forms.DataGridViewTextBoxColumn Time_In;
        private System.Windows.Forms.DataGridViewTextBoxColumn waiting_in;
        private System.Windows.Forms.DataGridViewTextBoxColumn transaction_id;
    }
}