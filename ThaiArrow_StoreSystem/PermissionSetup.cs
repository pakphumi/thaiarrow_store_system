﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using System.Data.SqlClient;

namespace ThaiArrow_StoreSystem
{
    public partial class PermissionSetup : Form
    {
        private SqlConnection con = new SqlConnection(ConfigurationSettings.AppSettings["SQLConnection"]);
        private SqlCommand cmd;
        private SqlDataReader reader;
        public PermissionSetup()
        {
            InitializeComponent();
            load_position();
        }
        private void load_position()
        {
            positionSelected.Items.Clear();
           // permissionSelected.Items.Clear();
            cmd = new SqlCommand();
            cmd.Connection = con;
            try
            {
                con.Open();
                cmd.CommandText = " SELECT DISTINCT [position] FROM [THAIARROW_PART_STORE].[dbo].[User_position] ORDER BY position ASC";

                reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        string position = reader.GetValue(0).ToString();
                        // string permission = reader.GetValue(1).ToString();
                        positionSelected.Items.Add(position);
                        // load_permission();

                    }
                }
            }
            finally
            {
                reader.Close();
                con.Close();
            }
        }
        private void load_permission()
        {
            cmd = new SqlCommand();
            cmd.Connection = con;
            string position = positionSelected.Text;
            checkBox1.Checked = false;
            checkBox2.Checked = false;
            checkBox3.Checked = false;
            checkBox4.Checked = false;
            checkBox5.Checked = false;
            checkBox6.Checked = false;
            checkBox7.Checked = false;
            checkBox8.Checked = false;

            try
            {
                con.Open();
                cmd.CommandText = " SELECT DISTINCT [position],[default_permission] FROM [THAIARROW_PART_STORE].[dbo].[User_position] where position ='" + position + "'";

                reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        string permission = reader.GetValue(1).ToString();
                        if (permission == "UserSetup") { checkBox1.Checked = true; }
                        if (permission == "PickupPart") { checkBox2.Checked = true; }
                        if (permission == "AddStock") { checkBox3.Checked = true; }
                        if (permission == "PermissionSetup") { checkBox4.Checked = true; }
                        if (permission == "PartsData") { checkBox5.Checked = true; }
                        if (permission == "EditStock") { checkBox6.Checked = true; }
                        if (permission == "Logs") { checkBox7.Checked = true; }
                        if (permission == "MCLine") { checkBox8.Checked = true; }
                    }
                }
            }
            finally
            {
                reader.Close();
                con.Close();
            }
        }

        private void ButtonClose_Click(object sender, EventArgs e)
        {
            this.Hide();
            Main fm = new Main();
            fm.Show();
        }

        private void Newbtn1_Click(object sender, EventArgs e)
        {
            string check = "";
            update_edit.Visible = false;
            add_button.Visible = true;
            bt_cancel.Visible = true;
            update_del.Visible = false;
            if (checkBox1.Checked == true) { check += "UserSetup,"; }
            if (checkBox2.Checked == true) { check += "PickupPart,"; }
            if (checkBox3.Checked == true) { check += "AddStock,"; }
            if (checkBox4.Checked == true) { check += "PermissionSetup,"; }
            if (checkBox5.Checked == true) { check += "PartsData,"; }
            if (checkBox6.Checked == true) { check += "EditStock,"; }
            if (checkBox7.Checked == true) { check += "Logs,"; }
            if (checkBox8.Checked == true) { check += "MCLine"; }
            textBox3.Text = check;
        }
        private void premission_checkbox()
        {

            string check = "";
            string check_ = "";

            if (checkBox1.Checked == true) { check += "UserSetup,"; }
            if (checkBox2.Checked == true) { check += "PickupPart,"; }
            if (checkBox3.Checked == true) { check += "AddStock,"; }
            if (checkBox4.Checked == true) { check += "PermissionSetup,"; }
            if (checkBox5.Checked == true) { check += "PartsData,"; }
            if (checkBox6.Checked == true) { check += "EditStock,"; }
            if (checkBox7.Checked == true) { check += "Logs,"; }
            if (checkBox8.Checked == true) { check += "MCLine"; }
            for (int i = 0; i < check.Length; i++)
            {
                check_ += check[i];
            }

            textBox3.Text = check_;

        }

        private void Editbtn1_Click(object sender, EventArgs e)
        {
            update_edit.Visible = true;
            add_button.Visible = false;
            update_del.Visible = false;
            bt_cancel.Visible = true;
            textBox2.Text = positionSelected.Text;
        }

        private void Delbtn1_Click(object sender, EventArgs e)
        {
            update_edit.Visible = false;
            add_button.Visible = false;
            update_del.Visible = true;
            bt_cancel.Visible = true;
            textBox2.Text = positionSelected.Text;
        }

        private void Update_edit_Click(object sender, EventArgs e)
        {
            List<string> check = new List<string>();
            cmd = new SqlCommand();
            cmd.Connection = con;
            DialogResult dialogResult = MessageBox.Show("Update", "Do you want to edit ?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dialogResult == DialogResult.Yes)
            {
                if (checkBox1.Checked == true) { check.Add("UserSetup"); }
                if (checkBox2.Checked == true) { check.Add("PickupPart"); }
                if (checkBox3.Checked == true) { check.Add("AddStock"); }
                if (checkBox4.Checked == true) { check.Add("PermissionSetup"); }
                if (checkBox5.Checked == true) { check.Add("PartsData"); }
                if (checkBox6.Checked == true) { check.Add("EditStock"); }
                if (checkBox7.Checked == true) { check.Add("Logs"); }
                if (checkBox8.Checked == true) { check.Add("MCLine"); }
                string position = textBox2.Text;
                string position_old = positionSelected.Text;
                try
                {
                    con.Open();
                    for (int i = 0; i < check.Count; i++)
                    {
                        cmd.CommandText = "DELETE FROM [THAIARROW_PART_STORE].[dbo].[User_position] where position ='" + position_old + "'";
                        int row = cmd.ExecuteNonQuery();

                    }

                }
                catch (Exception a)
                {
                    con.Close();
                    MessageBox.Show(a.Message);
                }
                finally
                {
                    //reader.Close();
                    con.Close();
                    update_position();
                    update_edit.Visible = false;
                    bt_cancel.Visible = false;
                }
            }
            else if (dialogResult == DialogResult.No)
            {
                update_edit.Visible = false;
                bt_cancel.Visible = false;
                this.Hide();
                PermissionSetup am = new PermissionSetup();
                am.Show();
            }
        }
        private void update_position()
        {
            List<string> check = new List<string>();
            cmd = new SqlCommand();
            cmd.Connection = con;

            if (checkBox1.Checked == true) { check.Add("UserSetup"); }
            if (checkBox2.Checked == true) { check.Add("PickupPart"); }
            if (checkBox3.Checked == true) { check.Add("AddStock"); }
            if (checkBox4.Checked == true) { check.Add("PermissionSetup"); }
            if (checkBox5.Checked == true) { check.Add("PartsData"); }
            if (checkBox6.Checked == true) { check.Add("EditStock"); }
            if (checkBox7.Checked == true) { check.Add("Logs"); }
            if (checkBox8.Checked == true) { check.Add("MCLine"); }
            string position = textBox2.Text;
            string position_old = positionSelected.Text;
            try
            {
                con.Open();
                for (int i = 0; i < check.Count; i++)
                {
                    cmd.CommandText = "INSERT INTO [THAIARROW_PART_STORE].[dbo].[User_position] (position,default_permission) VALUES ('" + position + "','" + check[i] + "')";
                    int row = cmd.ExecuteNonQuery();
                }

            }
            finally
            {
                con.Close();
                MessageBox.Show("Edit Position", "Edit Position Completed", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }

            this.Hide();
            PermissionSetup am = new PermissionSetup();
            am.Show();

        }
        private Boolean checkposition(string position)
        {
            bool result = true;
            cmd = new SqlCommand();
            cmd.Connection = con;
            try
            {
                con.Open();
                cmd.CommandText = "SELECT DISTINCT [position] FROM [THAIARROW_PART_STORE].[dbo].[User_position] WHERE position = '" + position + "' ";
                reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    result = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            con.Close();
            return result;
        }

        private void Add_button_Click(object sender, EventArgs e)
        {
            //string[] check = new String[9];
            List<string> check = new List<string>();
            int count = 0;
            string check_ = "";
            string position = textBox2.Text;

            if (checkposition(position))
            {
                cmd = new SqlCommand();
                cmd.Connection = con;
                con.Open();
                if (checkBox1.Checked == true) { check.Add("UserSetup"); }
                if (checkBox2.Checked == true) { check.Add("PickupPart"); }
                if (checkBox3.Checked == true) { check.Add("AddStock"); }
                if (checkBox4.Checked == true) { check.Add("PermissionSetup"); }
                if (checkBox5.Checked == true) { check.Add("PartsData"); }
                if (checkBox6.Checked == true) { check.Add("EditStock"); }
                if (checkBox7.Checked == true) { check.Add("Logs"); }
                if (checkBox8.Checked == true) { check.Add("MCLine"); }

                try
                {
                    for (int i = 0; i < check.Count; i++)
                    {
                        cmd = new SqlCommand("INSERT INTO [THAIARROW_PART_STORE].[dbo].[User_position] (position,default_permission) VALUES ('" + position + "','" + check[i] + "')", con);
                        //cmd.Parameters.AddWithValue("@position", textBox2.Text);
                        //cmd.Parameters.AddWithValue("@default_permission", check[i]);
                        cmd.ExecuteNonQuery();

                    }
                    MessageBox.Show("Add Position", "Add Position Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception a)
                {
                    MessageBox.Show(a.Message);
                }
            }
            else
            {
                MessageBox.Show("มีตำแหน่งนี้อยู่แล้ว", "ไม่สามารถเพิ่มซ้ำได้", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            con.Close();
            load_position();
            add_button.Visible = false;
            bt_cancel.Visible = false;
            textBox2.Clear();
            textBox3.Clear();
            clear_cleckbox();
        }

        //when you click update (ipdate del button) for delete
        private void Update_del_Click(object sender, EventArgs e)
        {
            List<string> check = new List<string>();
            cmd = new SqlCommand();
            cmd.Connection = con;
            DialogResult dialogResult = MessageBox.Show("Update", "Do you want to delete ?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dialogResult == DialogResult.Yes)
            {
                if (checkBox1.Checked == true) { check.Add("UserSetup"); }
                if (checkBox2.Checked == true) { check.Add("PickupPart"); }
                if (checkBox3.Checked == true) { check.Add("AddStock"); }
                if (checkBox4.Checked == true) { check.Add("PermissionSetup"); }
                if (checkBox5.Checked == true) { check.Add("PartsData"); }
                if (checkBox6.Checked == true) { check.Add("EditStock"); }
                if (checkBox7.Checked == true) { check.Add("Logs"); }
                if (checkBox8.Checked == true) { check.Add("MCLine"); }
                string position = textBox2.Text;
                string position_old = positionSelected.Text;
                try
                {
                    con.Open();
                    for (int i = 0; i < check.Count; i++)
                    {
                        cmd.CommandText = "DELETE FROM [THAIARROW_PART_STORE].[dbo].[User_position] where position ='" + position_old + "'";
                        int row = cmd.ExecuteNonQuery();

                    }

                }
                catch (Exception a)
                {
                    con.Close();
                    MessageBox.Show(a.Message);
                }
                finally
                {
                    con.Close();
                    textBox2.Text = "";
                    clear_cleckbox();
                    load_position();
                    update_del.Visible = false;
                    bt_cancel.Visible = false;
                }
            }
            else if (dialogResult == DialogResult.No)
            {
                update_del.Visible = false;
                bt_cancel.Visible = false;
                this.Hide();
                PermissionSetup am = new PermissionSetup();
                am.Show();
            }
        }
        private void clear_cleckbox()
        {
            checkBox1.Checked = false;
            checkBox2.Checked = false;
            checkBox3.Checked = false;
            checkBox4.Checked = false;
            checkBox5.Checked = false;
            checkBox7.Checked = false;
            checkBox8.Checked = false;
        }

        private void CheckBox1_CheckedChanged(object sender, EventArgs e)
        {
            premission_checkbox();
        }

        private void CheckBox2_CheckedChanged(object sender, EventArgs e)
        {
            premission_checkbox();
        }

        private void CheckBox3_CheckedChanged(object sender, EventArgs e)
        {
            premission_checkbox();
        }

        private void CheckBox4_CheckedChanged(object sender, EventArgs e)
        {
            premission_checkbox();
        }

        private void CheckBox5_CheckedChanged(object sender, EventArgs e)
        {
            premission_checkbox();
        }

        private void CheckBox6_CheckedChanged(object sender, EventArgs e)
        {
            premission_checkbox();
        }

        private void CheckBox7_CheckedChanged(object sender, EventArgs e)
        {
            premission_checkbox();
        }

        private void CheckBox8_CheckedChanged(object sender, EventArgs e)
        {
            premission_checkbox();
        }

        private void PositionSelected_SelectedIndexChanged(object sender, EventArgs e)
        {
            load_permission();
            textBox2.Text = positionSelected.Text;
        }

        private void Bt_cancel_Click(object sender, EventArgs e)
        {
            update_del.Visible = false;
            update_edit.Visible = false;
            add_button.Visible = false;
            textBox2.Clear();
            textBox3.Clear();
            clear_cleckbox();
            load_position();
            bt_cancel.Visible = false;
        }

        private void B_usersetup_Click(object sender, EventArgs e)
        {
            this.Hide();
            userSetup fm1 = new userSetup();
            fm1.Show();
        }
    }
}
